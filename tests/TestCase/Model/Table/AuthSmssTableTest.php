<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuthSmssTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuthSmssTable Test Case
 */
class AuthSmssTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuthSmssTable
     */
    public $AuthSmss;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auth_smss',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AuthSmss') ? [] : ['className' => AuthSmssTable::class];
        $this->AuthSmss = TableRegistry::getTableLocator()->get('AuthSmss', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AuthSmss);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
