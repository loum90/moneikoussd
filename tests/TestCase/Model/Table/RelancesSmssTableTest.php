<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelancesSmssTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelancesSmssTable Test Case
 */
class RelancesSmssTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RelancesSmssTable
     */
    public $RelancesSmss;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relances_smss'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RelancesSmss') ? [] : ['className' => RelancesSmssTable::class];
        $this->RelancesSmss = TableRegistry::getTableLocator()->get('RelancesSmss', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelancesSmss);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
