<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UssdHistoricsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UssdHistoricsTable Test Case
 */
class UssdHistoricsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UssdHistoricsTable
     */
    public $UssdHistorics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ussd_historics'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UssdHistorics') ? [] : ['className' => UssdHistoricsTable::class];
        $this->UssdHistorics = TableRegistry::getTableLocator()->get('UssdHistorics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UssdHistorics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
