<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OzekimessageinsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OzekimessageinsTable Test Case
 */
class OzekimessageinsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OzekimessageinsTable
     */
    public $Ozekimessageins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ozekimessageins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Ozekimessageins') ? [] : ['className' => OzekimessageinsTable::class];
        $this->Ozekimessageins = TableRegistry::getTableLocator()->get('Ozekimessageins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ozekimessageins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
