<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelevesContentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelevesContentsTable Test Case
 */
class RelevesContentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RelevesContentsTable
     */
    public $RelevesContents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.releves_contents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RelevesContents') ? [] : ['className' => RelevesContentsTable::class];
        $this->RelevesContents = TableRegistry::getTableLocator()->get('RelevesContents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelevesContents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
