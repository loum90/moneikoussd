<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RemerciementSmssTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RemerciementSmssTable Test Case
 */
class RemerciementSmssTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RemerciementSmssTable
     */
    public $RemerciementSmss;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.remerciement_smss'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RemerciementSmss') ? [] : ['className' => RemerciementSmssTable::class];
        $this->RemerciementSmss = TableRegistry::getTableLocator()->get('RemerciementSmss', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RemerciementSmss);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
