<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmssTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmssTable Test Case
 */
class SmssTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SmssTable
     */
    public $Smss;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.smss'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Smss') ? [] : ['className' => SmssTable::class];
        $this->Smss = TableRegistry::getTableLocator()->get('Smss', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Smss);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
