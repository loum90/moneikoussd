<?php 

/**
* 
*/

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Chronos\Chronos;


class DateHelper extends Helper
{
	
	function show($datetime, $heure = false)
	{
		$tmstamp = strtotime(($datetime));
		$jour = array('Lundi', 'Mardi','Mercredi', 'Jeudi','Vendredi', 'Samedi', 'Dimanche' );
		$mois = array('Janvier', 'Fevrier','Mars', 'Avril','Mai', 'Juin', 'Juillet', 'Août','Septembre', 'Octobre', 'Novembre', 'Décemre' );
		$date = $jour[date("N", $tmstamp)-1]." ".date("d", $tmstamp)." ".$mois[date("n", $tmstamp)-1]." ".date("Y", $tmstamp);
		if($heure==true){ $date.= " à ".date("H:i:s", $tmstamp);}
		return ($date);
	}

    function ago($time){
           $diff_time = time() - strtotime($time);
           if($diff_time < 1){
             return '0 secondes';
         }

         $sec = array(   31556926    => 'an',
           2629743.83  => 'mois',
           86400       => 'jour',
           3600        => 'heure',
           60          => 'minute',
           1           => 'seconde'
       );

         foreach($sec as $sec => $value){
             $div = $diff_time / $sec;
             if($div >= 1){
               $time_ago = round($div);
               $time_type = $value;
               if($time_ago >= 2 && $time_type != "mois"){
                   $time_type .= "s";
               }
               return '' . $time_ago . ' ' . $time_type;
           }
       }
    }

	function ago2($time){
           $diff_time = time() - strtotime($time);
           if($diff_time < 1){
             return '0 secondes';
         }

         $sec = array(   31556926    => 'an',
           2629743.83  => 'mois',
           86400       => 'jour',
           3600        => 'heure',
           60          => 'min',
           1           => 'sec'
       );

         foreach($sec as $sec => $value){
             $div = $diff_time / $sec;
             if($div >= 1){
               $time_ago = round($div);
               $time_type = $value;
               if($time_ago >= 2 && $time_type != "mois"){
                   $time_type .= "s";
               }
               return '' . $time_ago . ' ' . $time_type;
           }
       }
    }

    public function ago3($datetime)
    {
        $ago = false;

        if ((Chronos::parse($datetime)->wasWithinLast('1 hour'))) {
            $ago = $this->ago2($datetime);
        } else {
            $ago = Chronos::parse($datetime)->format('d/m, H\\hi');
        }
        
        return $ago;
    }
	 
    function ilya($date)
    {
        $date_a_comparer = Chronos::parse($date);
        $date_actuelle =  Chronos::now();

        $intervalle = $date_a_comparer->diff($date_actuelle);

        if ($date_a_comparer > $date_actuelle)
        {
            $prefixe = 'dans ';
        }
        else
        {
            $prefixe = 'il y a ';
        }

        $ans = $intervalle->format('%y');
        $mois = $intervalle->format('%m');
        $jours = $intervalle->format('%d');
        $heures = $intervalle->format('%h');
        $minutes = $intervalle->format('%i');
        $secondes = $intervalle->format('%s');

        if ($ans != 0)
        {
            $relative_date = $prefixe . $ans . ' an' . (($ans > 1) ? 's' : '');
            if ($mois >= 6) $relative_date .= ' et demi';
        }
        elseif ($mois != 0)
        {
            $relative_date = $prefixe . $mois . ' mois';
            if ($jours >= 15) $relative_date .= ' et demi';
        }
        elseif ($jours != 0)
        {
            $relative_date = $prefixe . $jours . ' jour' . (($jours > 1) ? 's' : '');
        }
        elseif ($heures != 0)
        {
            $relative_date = $prefixe . $heures . ' heure' . (($heures > 1) ? 's' : '');
        }
        elseif ($minutes != 0)
        {
            $relative_date = $prefixe . $minutes . ' minute' . (($minutes > 1) ? 's' : '');
        }
        else
        {
            $relative_date = $prefixe . ' quelques secondes';
        }

        return $relative_date;
    }

	function ilya2($date)
	{
	    $date_a_comparer = Chronos::parse($date);
	    $date_actuelle =  Chronos::now();

	    $intervalle = $date_a_comparer->diff($date_actuelle);

	    if ($date_a_comparer > $date_actuelle)
	    {
	        $prefixe = 'dans ';
	    }
	    else
	    {
	        $prefixe = 'il y a ';
	    }

	    $ans = $intervalle->format('%y');
	    $mois = $intervalle->format('%m');
	    $jours = $intervalle->format('%d');
	    $heures = $intervalle->format('%h');
	    $minutes = $intervalle->format('%i');
	    $secondes = $intervalle->format('%s');

	    if ($ans != 0)
	    {
	        $relative_date = $prefixe . $ans . ' an' . (($ans > 1) ? 's' : '');
	        if ($mois >= 6) $relative_date .= ' et demi';
	    }
	    elseif ($mois != 0)
	    {
	        $relative_date = $prefixe . $mois . ' mois';
	        if ($jours >= 15) $relative_date .= ' et demi';
	    }
	    elseif ($jours != 0)
	    {
	        $relative_date = $prefixe . $jours . ' jour' . (($jours > 1) ? '' : '');
	    }
	    elseif ($heures != 0)
	    {
	        $relative_date = $prefixe . $heures . ' h' . (($heures > 1) ? '' : '');
	    }
	    elseif ($minutes != 0)
	    {
	        $relative_date = $prefixe . $minutes . ' m' . (($minutes > 1) ? '' : '');
	    }
	    else
	    {
	        $relative_date = $prefixe . ' quelques secondes';
	    }

	    return $relative_date;
	}

	// pour moment et il y a il faut faire comme cela $this->date->moment(strtotime($post->created))

	function moment($timestamp){
	    $dif = time()-$timestamp;
	    if($dif<60){
	        return "A l'instant";
	    }elseif($dif<3600){
	        $min = round($dif/60,0);
	        return "Il y a ".$min." minutes";
	    }elseif($dif<3600*24){
	        $h = round($dif/3600,0);
	        return "Il y a ".$h." heures";
	    }elseif($dif<3600*24*7){
	       $j = round($dif/3600/24,0);
	        return "Il y a ".$j." jours";
	    }elseif($dif<3600*24*7*4){
	        $sem = round($dif/3600/24/7,0);
	        return "Il y a ".$sem." semaines";
	    }else{
	        $z = date('d/m/y',$timestamp);
	        return "Le ".$z."";
	    }
	}
}

?>