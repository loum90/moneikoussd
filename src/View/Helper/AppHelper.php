<?php 

/**
* 
*/

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Chronos\Chronos;


class AppHelper extends Helper
{
	public function formatMontant($montant)
    {
        return @number_format($montant, 0, ',', ' ');
    }

    public function formatNumberFull($numero)
    {
        if (substr($numero, 0, 1) == 3) {
            return $this->formatNumberFull2($numero);
        }
        return substr($numero, 0, 3). ' ' . substr($numero, 3, 2) .' '.substr($numero, 5, 3).' '. substr($numero, 8, 2);
    }
}

?>