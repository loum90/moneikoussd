Bonjour, <br>

Il semble que le montant transféré sur notre compte au titre de votre paiement ne correspond pas à celui indiqué sur la page de votre commande. Nous vous invitons à nous contacter via notre page Facebook afin que nous puissions résoudre cette situation. Nous vous remercions par avance.

<hr> <br>
<i>Ceci est un email automatique, prière de ne pas y répondre</i> <br>
<i>Pour toute information supplémentaire, vous pouvez ouvrir un ticket d'assistance sur <a href="https://m.me/102686648325815">https://m.me/102686648325815</a></i> <br>

<br>

<img src="https://moneiko.net/logo.png" width="170px" alt="Moneiko"> <br>