

<!-- Modal -->
<div class="modal fade" style="margin-top: 8rem;" id="step-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Dépôt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->control('id', ['type' => 'number', 'label' => 'ID Utilisateur']); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Refuser</button>
                <button type="submit" class="btn btn-dark step-one-ok">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" style="margin-top: 8rem;" id="step-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Dépôt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->control('id', ['type' => 'number', 'label' => 'ID Utilisateur']); ?>
                <?= $this->Form->control('Nom', ['value' => 'XXXXX', 'label' => 'Nom']); ?>
                <?= $this->Form->control('montant', ['label' => 'Montant']); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Refuser</button>
                <button type="button" class="btn btn-dark step-two-ok">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" style="margin-top: 8rem;" id="step-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Succès</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center text-white">
                Success !<br>
                <span class="amount-onex"></span>
                deposited into account
                <span class="account-onex"></span>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-dark step-3-ok">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<a type="button" class="w-100" data-toggle="modal" data-target="#step-1">
    <div class="card">
        <div class="card-body text-white">
            Recharger le compte
        </div>
    </div>
</a>
