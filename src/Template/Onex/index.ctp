<?php 
    use Cake\I18n\FrozenTime;
    use \Cake\Chronos\Chronos;
    // debug($this->getVars());
    // debug($this->App->formatMontant(4500));
?>

<div class="param-url" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>"></div>
<div class="type" data-type="<?= $type ?>"></div>

<?php $this->start('listeAnnulations') ?>
    <?php foreach ($annulations as $key => $annulation): ?>
        <div class='text-left'>
            <p><?= $annulation->senttime; ?> <br> <?= $annulation->sender ?></p>
            <p><?= $annulation->msg ?></p>
        </div>
    <?php endforeach ?>
<?php $this->end() ?>

<div class="modal fade" id="detail-msg" tabindex="-1" role="dialog" aria-labelledby="detail-msg" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detail-msg">Détail commande</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body detail-modal-onex">
                <!--  -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<div class="clearfix">
    <!-- <a class="btn btn-sm" href="<?= $this->Url->build(['action' => 'autresSms']) ?>"><span class="fa fa-envelope-open-o text-white"></span></a> -->
    <span class="container-reload">
        <span class="block-reload">
            <!-- <a class="btn btn-sm">sms : <?= $nbTotalSms ?></a> -->
            <?php if (in_array($type, ['mvola'])): ?>
                <?php if ($nbNonRelgesMvola): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesMvola > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesMvola > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesMvola ?></a>
                <?php endif ?>
            <?php endif ?>

            <?php if (in_array($type, ['airtel'])): ?>
                <?php if ($nbNonRelgesAirtel): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesAirtel > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesAirtel > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesAirtel ?></a>
                <?php endif ?>
            <?php endif ?>

            <?php if (in_array($type, ['orange'])): ?>
                <?php if ($nbNonRelgesOrange): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesOrange > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesOrange > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesOrange ?></a>
                <?php endif ?>
            <?php endif ?>


            <?php if ($nbDemandeAnnulation): ?>
                <?php if (in_array($type, ['mvola', 'airtel'])): ?>
                    <a class="btn btn-sm text-<?= $nbDemandeAnnulation > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $this->fetch('listeAnnulations') ?>"><span class="fa fa-exclamation-triangle text-warning"></span> &nbsp;<?= $nbDemandeAnnulation ?></a>
                <?php endif ?>
            <?php endif ?>
        </span>
    </span>
    <a data-toggle="tooltip" data-placement="bottom" title="Auto Maj solde et mise à jour Numéro" class="btn btn-sm text-secondary d-none d-sm-inline-block automajsolde" href="<?= $this->Url->build(['action' => 'autoMajNumEtSolde']) ?>" ><span class="fa fa-circle-o-notch"></span><span class=""></span></a>
    <a class="btn btn-sm text-white" href="javascript:void(0);" data-type="<?= $type ?>" data-toggle="modal" data-target="#search" href="javascript:void(0);"><span class="fa fa-search text-white"></span></a>
    <a class="btn btn-sm text-white" href="javascript:void(0);" data-toggle="modal" data-target="#searchsms" href="javascript:void(0);"><span class="fa fa-search text-info"></span></a>
    <?php if ($type == 'mvola'): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'mvola2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Mvola Serveur" class="btn btn-sm text-white disable-mvola" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotMvola']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '034']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php elseif ($type == "orange"): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'orange2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Orange Serveur" class="btn btn-sm text-white disable-orange" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotOrange']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '032']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php elseif ($type == "airtel"): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'airtel2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Airtel Serveur" class="btn btn-sm text-white disable-orange" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotAirtel']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '033']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php endif ?>
    
    <!-- Modal Orange -->
    <div class="modal fade" id="sendsms" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <?= $this->Form->create(false, ['url' => ['action' => 'sendSms'], 'class' => 'form-sendsms', 'type' => 'file']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="label-title">SMS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">
                                   <?= $this->Form->control('numero', ['value' => '0347085930']); ?>
                                </div>
                                <div class="col-md-3">
                                   <?= $this->Form->control('raison', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                                </div>
                                <div class="col-md-3">
                                   <?= $this->Form->control('montant_recu', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                                </div>
                                <div class="col-md-3">
                                   <?= $this->Form->control('solde_actuel', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary text-white button-send" >Envoyer</button>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <?= $this->Form->create(false, ['url' => ['action' => 'rechercher', 'mobile' => $type], 'class' => 'form-search', 'type' => 'GET']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title  text-white" id="label-title">Rechercher commande non payée <span class="search-type"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 col-lg">
                               <?= $this->Form->control('montant', ['type' => 'number']); ?>
                            </div>
                            <div class="col-5 col-lg">
                               <?= $this->Form->control('type', ['options' => ['btc' => 'Crypto', 'payeer' => 'Payeer', 'pm' => 'PM', 'advcash' => 'ADV'], 'empty' => 'Sélectionner']); ?>
                            </div>
                            <div class="col-5 col-lg">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Date</label>
                                   <?= $this->Form->text('date', ['type' => 'date', 'value' => @$options['date'] ? @$options['date'] : date('Y-m-d')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Heure</label>
                                   <?= $this->Form->text('heure', ['type' => 'time', 'value' => @$options['heure'] ? @$options['heure'] : date('H:i')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="mt-2 btn btn-primary text-white button-send" >Filtrer</button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix msg-reclam text-white">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="searchsms" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <?= $this->Form->create(false, ['url' => ['type' => @$options['type']], 'class' => '', 'type' => 'GET']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title  text-white" id="label-title">Rechercher SMS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 col-lg-3">
                               <?= $this->Form->hidden('type', ['value' => @$options['type']]); ?>
                               <?= $this->Form->control('keyword', ['label' => 'Mot clé', 'autocomplete' => 'off']); ?>
                            </div>
                            <div class="col-5 col-lg-3">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Date</label>
                                   <?= $this->Form->text('date', ['type' => 'date', 'value' => @$options['date'] ? @$options['date'] : date('Y-m-d')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg-3">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="mt-2 btn btn-primary text-white button-filter" >Filtrer</button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix msg-reclam text-white">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="searchpanier" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label-title">Détail Commande <span class="infos-type"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <!-- code -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-class text-white" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>


    <!-- <a href="#" class="float-right ml-2" data-toggle="tooltip" data-placement="bottom" title="Si paiement par hook activé"><span class="fa fa-database <?= $params->is_webhook_sms_paiement ? 'text-success' : 'text-danger' ?>"></span></a> -->
    <a href="http://127.0.0.1:9501/" class="d-none d-sm-block float-right text-secondary ml-2" target="blank"> Hook <?= $params['hook_serveur']; ?></a>
    <a href="http://127.0.0.1:9501/" class="float-right ml-2" target="blank"></a>
    <a href="javascript:void(0);" class="float-right reloadsms d-none d-sm-block"><span class="fa fa-spinner text-secondary"></span></a>
</div>


<div class="row mt-4 ">
    <div class="col-md-4">
        <div class="container-onex-form">
            Chargement Terminal ... <span class="fa fa-circle-o-notch text-white fa-spin loadme"></span>
            <!-- JS -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="">
            <div class="row">
                <div class="col-md-6">
                    <a href="<?= $this->Url->build(['statut_1x' => 'pending']) ?>" class="btn btn-block w-100 btn-primary">En cours <span class="text-danger"><?= $nbPaiementEnCours ?></span></a>
                </div>
                <div class="col-md-6">
                    <a href="<?= $this->Url->build(['statut_1x' => 'paid']) ?>" class="btn btn-block w-100 btn-primary">Réglé <span class="text-success"><?= $nbPaiementPaid ?></span></a>
                </div>
            </div>
            <div class="clearfix mt-4">
                <div class="card">
                    <div class="card-body">
                        <?= $this->Form->create(false, ['url' => ['action' => 'sendSmsOrangeOnex'], 'class' => 'form-horizontal', 'type' => 'POST', 'novalidate' , 'context' => ['validator' => 'default']]); ?>
                            <?php $amount = rand(1, 100) ?>
                            <?= $this->Form->control('sms_text', ['label' => 'SMS OG', 'default' => 'Vous avez recu un transfert de '.$amount.'00Ar venant du 0325541393 Nouveau Solde: 6800Ar. Trans Id: PP220712.1414.TEST. Orange Money vous remercie.']); ?>
                            <?= $this->Form->control('id_1xbet', ['label' => 'ID OneX', 'default' => rand(10000000, 99999999)]); ?>
                            <button class="btn btn-primary">Envoyer</button>
                        <?= $this->form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix my-4 d-none d-sm-block"><?= $this->element('pagination/pagination') ?></div>

<div class="container-sms">
    <div class="sms-block">
        <?php foreach ($smss as $key => $sms): ?>

            <div class="row-sms clearfix" data-sms-id="<?= $smsId = $sms->id ?>">
                <div class="clearfix">
                    <span class="float-left mb-2">
                        <?php if (in_array($sms->sender, $mvolaSenders)): ?>
                            <span class="text-success">MV</span>
                        <?php elseif (in_array($sms->sender, $orangeSenders)) :?>
                            <span class="text-warning">OG</span>
                        <?php elseif (in_array($sms->sender, $airtelSenders)) :?>
                            <span class="text-danger">AT</span>
                        <?php endif ?>
                        SMS ID : <?= $sms->id ?>, 
                        <?php if ($sms->statut_1x == 'paid'): ?>
                            <span data-toggle="tooltip" data-placement="bottom" title="Règlement effectué" class="fa text-success fa-check"></span>
                        <?php else: ?>
                            <span data-toggle="tooltip" data-placement="bottom" title="Règlement en attente" class="fa fa-clock-o text-warning"></span>
                        <?php endif ?>,
                        <?php if ($sms->id_1xbet): ?>
                            <span data-toggle="tooltip" data-placement="bottom" title="Compte BET ID Client">CID</span> : <?= $sms->id_1xbet ?>,
                        <?php else: ?>
                            <span class="text-warning" data-toggle="tooltip" data-placement="bottom" title="Compte BET ID Client">No CID</span>,
                        <?php endif ?>
                        <span data-toggle="tooltip" data-placement="bottom" title="Compte BET ID Client">Le</span> : <?= $sms->senttime ?>
                    </span>
                    <span class="float-right mb-2">
                        <?php if (Chronos::parse($sms->senttime)->wasWithinLast('1 hour')): ?>
                            <?= $this->Time->timeAgoInWords($sms->senttime); ?>
                        <?php else: ?>
                            <?= Chronos::parse($sms->senttime)->format('d/m/y, H\\hi'); ?>
                        <?php endif ?>
                    </span>
                </div>
                <div class="text-white" data-url="<?= $this->Url->build(['action' => 'editSms', $sms->id])?>" data-id="<?= $sms->id ?>"><?= $sms->msg ?></div>
                
                <div class="clearfix actions">

                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:void" data-link-id="<?= $smsId ?>" class="float-left btn btn-sm mt-2" data-toggle="modal" data-target="#detail-msg"> 
                                <span class="fa fa-eye text-info"></span>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <div class="clearfix mt-2">

                                <a href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'deleteSms', @$sms->id]) ?>" class="ml-2 float-right btn btn-sm deletesms"> 
                                    <span class="fa fa-remove text-danger"></span>
                                </a>

                                <a data-toggle="tooltip" data-placement="bottom" title="<?= $sms->get('tooltipStatut') ?>" data-status="<?= $sms->paiement_status ?>"href="<?= $this->Url->build(['action' => 'majestate', $sms->id]) ?>" class="mr-2 float-right btn btn-sm majestate"> 
                                    <span class="fa <?= $sms->statut_1x != 'paid' ? 'fa-play' : 'fa-exclamation-circle text-success' ?> <?= $sms->get('ClassStatut') ?>"></span>
                                </a>

                                <span class="result-reglement float-right mr-2">
                                    <!-- JS -->
                                </span> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <hr class="light">
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $this->element('pagination/pagination') ?>
    </div>
    <div class="col-md-6">
        <span class="float-right"><?= $this->Paginator->counter('{{count}}') ?></span>
    </div>
</div>

<div class="d-none">
    <?= $this->Form->text('sms', ['class' => 'form-control', 'id' => 'sms-src']); ?>
</div>