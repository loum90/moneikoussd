<div class="card">
    <div class="card-body">
        <?php 
            // debug($paiementEnCours); 
        ?>
        <div class="text-white clearfix"> 
            <div class="row">
                <div class="col-md-6 ">
                    <span class="mt-2 pt-1 d-inline-block">
                        Traitement en cours : <span class=""><?= $nbPaiementEnCours ?></span>
                    </span>
                </div>
                <div class="col-md-6">
                    <button class="float-right btn btn-md btn-primary text-white reload">Recharger</button>
                </div>
            </div>

        </div>       

       <?php /*if (!$autrePaiementRecent):*/ ?>
       <?php if ($paiementEnCours): ?>
            <?= $this->Form->create($paiementEnCours, ['url' => ['action' => 'majPaiement'], 'class' => 'mt-4', 'class' => 'onex-form']); ?>
                <div class="row clearfix">
                    <div class="col-md-8">
                        <?= $this->Form->control('id_1xbet', [/*'value' => '456546546',*/ 'id' => 'id1x', 'value' => @$paiementEnCours->id_1xbet, 'label' => '1x ID', 'type' => 'number']); ?>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary w-100 mt-label copy" data-clipboard-target="#id1x">Copier</button>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-8">
                        <?= $this->Form->control('montant', [/*'value' => '5000',*/ 'id' => 'amount', 'value' => $paiementEnCoursData['montant_recu'], 'label' => 'Montant', 'type' => 'number']); ?>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary w-100 mt-label copy" data-clipboard-target="#amount">Copier</button>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-8">
                        <?= $this->Form->control('id', ['value' => $paiementEnCours->id, 'id' => 'sms_id', 'label' => 'SMS ID', 'type' => 'number']); ?>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary w-100 mt-label copy" data-clipboard-target="#sms_id">Copier</button>
                    </div>
                </div>
                <button class="btn btn-primary text-white btn-md validate-onex">Valider</button>
            <?= $this->form->end() ?>
       <?php else: ?>
            Aucun paiement récent 
       <?php endif ?>

    </div>
</div>