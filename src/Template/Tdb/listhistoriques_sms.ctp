<?php if ($nbSms = $smss->count()): ?>
    <?php foreach ($smss as $key => $sms): ?>
        <div class="row-sms clearfix">
            <div class="mb-1"> 
                <span class="numero"><?= $sms['numero'] ?> <span class="verification-solde"></span></span>
                <span class="float-right">
                    <?= $sms['date'] ?>
                </span>
            </div>
            <div class="text-white mb-1 ">
                <?= $sms['content'] ?>
                <?php if ($mobile == "orange"): ?>
                    <?php $montants = $sms->get('ExtractMontant'); ?>
                    <?php if (!empty($montants)): ?>
                        <div class="container-montant clearfix text-secondary">
                            Transfert : <span class="transfertAvecFrais"><?= $montants['transfertAvecFrais'] ?></span> Ar,
                            Solde : <span class="nouveauSolde"><?= $montants['nouveauSolde'] ?></span> Ar
                        </div>
                    <?php endif ?>
                <?php endif ?>
            </div>
        </div>

        <?php if ($key < $nbSms-1): ?>
            <hr>
        <?php endif ?>
    <?php endforeach ?>
<?php else: ?>
    Aucun sms
<?php endif ?>