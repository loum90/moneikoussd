<?php foreach ($ussdHistorics as $key => $ussdHistoric): ?>
    <div class="clearfix mb-2">
        <u><?= $ussdHistoric->numero ?? 'Numero non enregistré' ?> : </u>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <span class="text-white"><?= $ussdHistoric->command ?></span>
        </div>
        <div class="col-sm-6">
            <span class="float-right"><?= $ussdHistoric->created->format('d/m/Y H:i:s') ?></span>
        </div>
    </div>
    <p class="">
        <?= $ussdHistoric->response ?>
    </p>
    <hr>
<?php endforeach ?>