<?php 
    use \Cake\Chronos\Chronos;
?>


<!-- Modal -->
<div class="modal fade" id="stat-prod" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="label-title">Statistique & Suivi Prod</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        Nb Doublon Exchange Mobile Ipn
                    </div>
                    <div class="col-md-6">
                        <div class="nb-stat-doublon text-white">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-class" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div class="param-url" data-operateurs='<?= json_encode($operateurs_fronts, JSON_PRETTY_PRINT) ?>' data-param="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'getParams']) ?>"></div>

<div class="clearfix mb-4">
    <div class="row ">
        <div class="col-md-6">
            <div class="mb-2">
                <a class="" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'mvola']) ?>">P. Mvola</a> | 
                <a class="" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'orange']) ?>">P. Orange</a> | 
                <a class="" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'airtel']) ?>">P. Airtel</a> | 
                
                <!-- Button trigger modal -->
                <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#ussd-historics">H. AT</a> |
                <a target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'autresSms']) ?>">Sms</a> |
                <a target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'smsMerci']) ?>">Merci</a> |
                <a target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'rates']) ?>">Rates</a> |
                <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#at-cmd">AT</a>
            </div>

            <div class="clearfix mb-3">
               <div class="mb-2"> <button class="btn btn-primary btn-sm" data-action="<?= $this->Url->build(['action' => 'execUssd']) ?>" data-type="032" data-linkread-sms="<?= $this->Url->build(['action' => 'debugSmsModem', '032']) ?>" data-mobile="ORANGE" type="button" data-toggle="modal" data-target="#command-ussd">
                       AT O
                   </button>
                   <button class="btn btn-primary btn-sm" data-action="<?= $this->Url->build(['action' => 'execUssd']) ?>" data-type="034" data-linkread-sms="<?= $this->Url->build(['action' => 'debugSmsModem', '034']) ?>" data-mobile="TELMA" type="button" data-toggle="modal" data-target="#command-ussd">
                       AT M
                   </button> 
                   <button class="btn btn-primary btn-sm" data-action="<?= $this->Url->build(['action' => 'execUssd']) ?>" data-type="033" data-linkread-sms="<?= $this->Url->build(['action' => 'debugSmsModem', '033']) ?>" data-mobile="AIRTEL" type="button" data-toggle="modal" data-target="#command-ussd">
                       AT A
                   </button> 
                   <!-- <button class="btn btn-primary btn-sm" data-action="<?= $this->Url->build(['action' => 'execUssd']) ?>" data-type="033" data-mobile="AIRTEL" type="button" data-toggle="modal" data-target="#command-ussd">
                       AT Rtl
                   </button>  -->

                    <?php $this->start('resetUsb') ?>
                        <a data-toggle="tooltip" data-placement="bottom" title="Réinitialise GSM Orange" href="<?= $this->Url->build(['action' => 'resetUsb', $driverOrange]) ?>" class="btn btn-primary reset-usb btn-sm">
                            <span class="fa fa-refresh text-warning"></span>
                        </a> 
                        <a data-toggle="tooltip" data-placement="bottom" title="Réinitialise GSM Telma" href="<?= $this->Url->build(['action' => 'resetUsb', $driverTelma]) ?>" class="btn btn-primary reset-usb btn-sm">
                            <span class="fa fa-refresh text-success"></span>
                        </a> 
                        <a data-toggle="tooltip" data-placement="bottom" title="Réinitialise GSM Airtel" href="<?= $this->Url->build(['action' => 'resetUsb', $driverAirtel]) ?>" class="btn btn-primary reset-usb btn-sm">
                            <span class="fa fa-refresh text-danger"></span>
                        </a> 
                    <?php $this->end() ?>

                    <a data-placement="bottom" title="Log ussd Orange" href="<?= $this->Url->build(['action' => 'histoCallback2', 'orange', 'ussd_response.log']) ?>" data-titre="Historique des appels USSD" class="btn btn-primary gethistocallback btn-sm" data-toggle="modal" data-target="#histo_callback">
                        <span class="fa fa-file-text text-warning"></span>
                    </a> 
                    <a title="Log ussd Mvola" href="<?= $this->Url->build(['action' => 'histoCallback2', 'mvola', 'ussd_response.log']) ?>" data-titre="Historique des marquages ref mobile" class="btn btn-primary gethistocallback btn-sm" data-toggle="modal" data-target="#histo_callback">
                        <span class="fa fa-file-text text-success"></span>
                    </a> 
                    <a title="Log ussd Airtel" href="<?= $this->Url->build(['action' => 'histoCallback2', 'airtel', 'ussd_response.log']) ?>" data-titre="Historique des marquages ref mobile" class="btn btn-primary gethistocallback btn-sm" data-toggle="modal" data-target="#histo_callback">
                        <span class="fa fa-file-text text-danger"></span>
                    </a> 
                    <!-- <a title="Historique Vente IPN" href="<?= $this->Url->build(['action' => 'histoCallback', 'vente_ipn.log']) ?>" data-titre="Historique des appels USSD" class="btn btn-primary gethistocallback btn-sm" data-toggle="modal" data-target="#histo_callback">
                        <span class="fa fa-file-text text-white"></span>
                    </a> 
                    <a data-placement="bottom" title="Historique Callback Marquage ORANGE TXNID" href="<?= $this->Url->build(['action' => 'histoCallback', 'callback.log']) ?>" data-titre="Historique des marquages ref mobile" class="btn btn-primary gethistocallback btn-sm" data-toggle="modal" data-target="#histo_callback">
                        <span class="fa fa-file"></span>
                    </a>  -->
                    <a title="Maj url Ngrok" href="<?= $this->Url->build(['action' => 'majUrlNgrok']) ?>" data-titre="Historique des marquages ref mobile" class="d-none d-sm-inline-block btn btn-primary majgrockurl btn-sm">
                        <span class="fa fa-cloud-upload text-danger"></span>
                    </a> 
                    <a href="<?= $this->Url->build(['action' => 'checkStateUrl']) ?>" data-toggle="tooltip" data-placement="bottom" title="Vérification des URL Local & Prod" class="d-none d-sm-inline-block btn btn-primary checkngrockurl btn-sm">
                        <span class="fa fa-globe text-secondary"></span>
                    </a> 
                    <a href="<?= $this->Url->build(['action' => 'synchronizeLocalServerTime']) ?>" data-toggle="tooltip" data-placement="bottom" title="Synchronise heure serveur vers heure local sinon le timeur des paiements en ligne bug" class="btn btn-primary setsynchronisedtime btn-sm">
                        <span class="fa fa-clock-o text-secondary"></span>
                    </a> 
                    <a href="javascript:void()" data-toggle="tooltip" data-placement="bottom" title="Mise à jour des données du site (section du droite)" class="btn btn-primary check_state_local_connexion btn-sm">
                        <span class="fa fa-file-text text-secondary"></span>
                    </a> 
               </div>
                <a data-titre="Historiques sms Orange" class="btn btn-primary btn-sm" data-href="<?= $this->Url->build(['action' => 'listhistoriquesSms', 'orange']) ?>" type="button" data-toggle="modal" data-target="#histo_sms_modal">
                    Sms Orange
                </a> 
                <a data-titre="Historiques sms Mvola" class="btn btn-primary btn-sm" data-href="<?= $this->Url->build(['action' => 'listhistoriquesSms', 'mvola']) ?>" type="button" data-toggle="modal" data-target="#histo_sms_modal">
                    Sms Mvola
                </a> 
                <a data-titre="Historiques sms Airtel" class="btn btn-primary btn-sm" data-href="<?= $this->Url->build(['action' => 'listhistoriquesSms', 'airtel']) ?>" type="button" data-toggle="modal" data-target="#histo_sms_modal">
                    Sms Airtel
                </a> 
                <a data-titre="Relance SMS" class="btn btn-primary btn-sm" data-href="" type="button" data-toggle="modal" data-target="#relance-sms">
                    Relance
                </a> 

                <div class="container-notifs clearfix mt-4">
                    <a data-toggle="tooltip" data-placement="bottom" title="Nb nouveau inscrit" href="https://moneiko.net/exchanger/manager/users?step_completed=2&entry=me" target="blank" class="float-right ml-3 ml-lg-0 mr-lg-3 float-lg-none link-notif btn btn-sm rounded-circle notif">
                        <span class="fa fa-user-circle text-secondary user-notif"></span>
                        <!-- <span class="badge"></span> -->
                    </a>
                    <a data-toggle="tooltip" data-placement="bottom" title="Nb total de vente echouée" href="https://moneiko.net/exchanger/manager/liste-ventes-payeer?entry=me" target="blank" class="float-right ml-3 ml-lg-0 mr-lg-3 float-lg-none link-failed btn btn-sm rounded-circle notif">
                        <span class="fa fa-times-circle text-secondary failed-notif"></span>
                        <!-- <span class="badge"></span> -->
                    </a>
                    
                    <span class="container-mobilemoney-notifs">
                        <a data-toggle="tooltip" data-placement="bottom" title="Commandes Airtel non réglé(s)" href=<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'airtel', 'failed' => true]) ?> target="blank" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle notif">
                            <span class="fa fa-coffee <?= $nbNonRelgesAirtel > 0 ? "text-danger" : "text-danger-dark" ?> "></span>
                            <?php if ($nbNonRelgesAirtel > 0): ?>
                                <span class="badge"><?= $nbNonRelgesAirtel > 0 ? $nbNonRelgesAirtel : 0 ?></span>
                            <?php endif ?>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Commandes Orange non réglé(s)" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'orange', 'failed' => true]) ?>" target="blank" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle notif">
                            <span class="fa fa-coffee <?= $nbNonRelgesOrange > 0 ? "text-warning" : "text-warning-dark" ?> "></span>
                            <?php if ($nbNonRelgesOrange > 0): ?>
                                <span class="badge"><?= $nbNonRelgesOrange > 0 ? $nbNonRelgesOrange : 0 ?></span>
                            <?php endif ?>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Commandes Mvola non réglé(s)" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'mvola', 'failed' => true]) ?>" target="blank" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle notif">
                            <span class="fa fa-coffee <?= $nbNonRelgesMvola > 0 ? "text-success" : "text-success-dark" ?> "></span>
                            <?php if ($nbNonRelgesMvola > 0): ?>
                                <span class="badge"><?= $nbNonRelgesMvola > 0 ? $nbNonRelgesMvola : 0 ?></span>
                            <?php endif ?>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Etat serveur crypto retrait" href="javascript:void(0);" target="blank" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle etat-server-crypto">
                            <span class="fa fa-database text-secondary "></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Vérification état du serveur NG Gateway et cliquer pour redémarrer le Ozeki" href="<?= $this->Url->build(['action' => 'restartGateway']) ?>" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle etat-server-gateway">
                            <span class="fa fa-archive text-secondary "></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Vérification état de l'auto redémarrage du NG" href="#" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle etat-auto-start-ozeki">
                            <span class="fa fa-download text-secondary"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Redémarrer PC" href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'restartPc']) ?>" class="float-right ml-2 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm rounded-circle redemarrage-ordi">
                            <span class="fa fa-recycle text-secondary"></span>
                        </a>
                    </span>
                </div>

                <div class="container-check-rx clearfix mt-4">
                    <!-- <button class="float-right ml-3 ml-lg-0 mr-lg-3 float-lg-none btn btn-sm"><span class="ago-rx" data-toggle="tooltip" data-placement="bottom" title="Dernier rappel moi Orange check RX/Gateway"><?php /*$this->Date->ago3($lastRappelMoiSmsOrange->senttime)*/ ?></span></button> -->
                    <button data-toggle="tooltip" data-placement="bottom" title="NB mail envoyés Mailjet 1 : moneiko.manager, 2 : andriam.nars" class="ml-lg-0 mr-lg-1 btn btn-sm"><span class="ago-rx"><span class="fa fa-envelope-o"></span> n° 1 : <span class="nb_mail_jet_one">--</span></span>, n° 2 : <span class="nb_mail_jet_second">--</span></span></button>
                    <button data-toggle="tooltip" data-placement="bottom" title="Voir estimation NB achat par jour" class="ml-lg-0 mr-lg-1 btn btn-sm get-est-j">
                        <span class="fa fa-question-circle"></span>
                        <span class="nb-est-j text-white">--</span>
                        <span class="nb-variation">--</span>
                    </button>
                </div>

                <div class="mt-4">
                    TOTAL = 
                        <span class="fa fa-spin fa-circle-o-notch load-total-solde"></span>
                        <span class="total-local-reserve"> <!-- JS --></span>
                        <span class="total-online-reserve"> <!-- JS --></span>
                        <span class="total-reserve"></span>
                </div>

                <div class="mt-4">
                    <div class="container-reclamation-final d-inline-block">
                        <div class="clearfix">
                            <a  data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation retard Airtel" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payFailedAirtel']) ?>" class="<?= $param->is_auto_recla_general == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-general mr-3"><span class="fa fa-share-alt text-danger"></span> </a>
                            <a  data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation retard Orange" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payFailedOrange']) ?>" class="<?= $param->is_auto_recla_general == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-general mr-3"><span class="fa fa-share-alt text-warning"></span> </a>
                            <a  data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation retard Mvola" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payFailedMvola']) ?>" class="<?= $param->is_auto_recla_general == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-general mr-3"><span class="fa fa-share-alt text-success"></span> </a>
                            <a  data-toggle="tooltip" data-placement="bottom" title="Dernière requête auto recla générale : <?= $params->last_auto_recla_executed_at->format('d/m/Y H:i') ?>" href="javascript:void(0);" class="<?= $param->is_auto_recla_general == false ? 'disabled' : '' ?> btn btn-sm rounded-circle info-confirm-reclamation mr-3">
                                <span class="fa fa-exclamation-circle text-secondary"></span>
                            </a>
                        </div>
                        
                        <div class="clearfix recla-results mt-4">
                            <!-- JS -->
                        </div>
                    </div>

                    <div class="container-reclamation-final d-inline-block">
                        <div class="clearfix">
                            <a data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation erreur de réseau inaccessible Airtel" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payError', 'airtel']) ?>" class="<?= $param->is_auto_recla_erreur_reseau == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-erreur-reseau mr-3">
                                <span class="fa fa-send-o text-danger"></span>
                                <?php if ($nbErrorAirtel > 0): ?>
                                    <span class="badge"><?= $nbErrorAirtel > 0 ? $nbErrorAirtel : 0 ?></span>
                                <?php endif ?>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation erreur de réseau inaccessible Orange" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payError', 'orange']) ?>" class="<?= $param->is_auto_recla_erreur_reseau == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-erreur-reseau mr-3">
                                <span class="fa fa-send-o text-warning"></span>
                                <?php if ($nbErrorOrange > 0): ?>
                                    <span class="badge"><?= $nbErrorOrange > 0 ? $nbErrorOrange : 0 ?></span>
                                <?php endif ?>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Traiter réclamation erreur de réseau inaccessible Mvola" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'payError', 'mvola']) ?>" class="<?= $param->is_auto_recla_erreur_reseau == false ? 'disabled' : '' ?> btn btn-sm rounded-circle notif confirm-reclamation confirm-reclamation-erreur-reseau mr-3">
                                <span class="fa fa-send-o text-success"></span>
                                <?php if ($nbErrorMvola > 0): ?>
                                    <span class="badge"><?= $nbErrorMvola > 0 ? $nbErrorMvola : 0 ?></span>
                                <?php endif ?>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Dernière requête erreur réseau : <?= $params->last_auto_recla_erreur_reseau_executed_at->format('d/m/Y H:i') ?>" href="javascript:void(0);" class="<?= $param->is_auto_recla_erreur_reseau == false ? 'disabled' : '' ?> btn btn-sm rounded-circle info-confirm-reclamation-erreur-reseau mr-3">
                                <span class="fa fa-exclamation-circle text-secondary"></span>
                            </a>

                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-sm rounded-circle mr-2" data-toggle="modal" data-target="#stat-prod">
                                <span class="fa fa-building-o text-white"></span>
                            </button>

                            <a data-toggle="tooltip" data-placement="bottom" title="Reset Cache Achat manuellement" href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'resetCacheManu']) ?>" class="btn btn-sm rounded-circle mr-3 reset-cache-achat-manu">
                                <span class="fa fa-trash text-danger"></span>
                            </a>
                            
                            <a data-toggle="tooltip" data-placement="bottom" title="Envoi test push" href="<?= $this->Url->build(['controller' => 'Tdb', 'action' => 'testSendPushApi']) ?>" class="btn btn-sm rounded-circle mr-3 testSendPushApi">
                                <span class="fa fa-bullhorn text-info"></span>
                            </a>
                            
                        </div>
                        
                        <div class="clearfix recla-results mt-4">
                            <!-- JS -->
                        </div>
                    </div>
                </div>

                <div class="mt-2 mb-4">
                    <div class="clearfix">
                        <?php foreach ($listTunnels as $key => $tun): ?>
                            <a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'majNgrokToken', $tun->id]) ?>" data-progress="<?= $tun->get('Pourcentage') ?>" data-id="<?= $tun->id ?>" class="<?= $tun->is_active == 1 ? 'border-grey '.($tun->get('Pourcentage') < 90 ? 'text-white active_ngrok_state' : '') : '' ?> float-lg-none mb-2 btn btn-sm switch-tunnel <?= $tun->get('Pourcentage') > 90 ? 'text-danger' : '' ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $tun->account ?>"><?= $tun->id ?> - <span class="ng-percent"><?= $tun->get('Pourcentage') ?></span> %</a>
                        <?php endforeach ?>
                        <a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'resetNgrokProgression']) ?>" class="btn btn-sm reset-ngrok mb-2"><span class="fa fa-refresh"></span></a>
                    </div>
                </div>

                <div class="mt-2 row">
                    <div class="col-md-12">
                        <a href="<?= $this->Url->build(['controller' => 'apis', 'action' => '']) ?>" class="btn btn-sm start-auto-rec mb-2">Démarrer Auto Rec <span class="fa fa-bolt"></span></a>
                        <a href="<?= $this->Url->build(['controller' => 'apis', 'action' => '']) ?>" class="btn btn-sm close-auto-rec mb-2"><span class="fa fa-remove"></span></a>
                    </div>
                    <div class="col-md-12 container-card-results-recla d-none">
                        <div class="card mt-2">
                            <div class="card-body container-result-recla">
                                <!-- JS -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <span class="heurelocal">Local : <span class="text-white"><?= $timeServerNow->format('H:i:s') ?></span></span> 
            <span class="heureprod"> <!-- JS --> </span>
            <span class="ratio-seuil-auto-maj pointer" data-toggle="tooltip" data-placement="bottom" title="Seuil du ratio auto maj. Quand le ratio courant est en dessus de ce ratio, le switch du solde se déclenche automatiquement">Seuil : <span class="text-white"><?= $params->ratio_maj ?> %</span></span>
            <span class="ratio-seuil-auto-maj pointer" data-toggle="tooltip" data-placement="bottom" title="Nb http appelé sur 20 000 requêtes">Nb Conn : <span class="nb_conn">--</span></span>
            <div class="recap-num-container">
                <div class="num-recap">
                    <div class="clearfix  mb-4">

                        <?php $this->start('unpaid') ?>
                            <?php if ($nbUnpaid = $nbNonRelgesMvola + $nbNonRelgesOrange + $nbNonRelgesAirtel): ?>
                                <div class="clearfix">
                                    <span class="text-secondary titre-unpaid">Non réglé<?= $nbUnpaid > 0 ? 's' : '' ?> :</span>
                                </div>
                            <?php endif ?>
                        
                            <div class="row unpaid">
                                <?php if ($nbNonRelgesMvola): ?>
                                    <div class="col-4">
                                        <a class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'mvola', 'failed' => true]) ?>" class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Mvola non réglé">
                                            <span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesMvola > 0 ? 'danger' : 'success' ?>"></span> 
                                            <?= $nbNonRelgesMvola ?> Mvola
                                        </a>
                                    </div>
                                <?php endif ?>

                                <?php if ($nbNonRelgesOrange): ?>
                                    <div class="col-4">
                                        <a class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'orange', 'failed' => true]) ?>" class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Orange non réglé">
                                            <span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesOrange > 0 ? 'danger' : 'success' ?>"></span> 
                                            <?= $nbNonRelgesOrange ?> Orange
                                        </a>
                                    </div>
                                <?php endif ?>

                                <?php if ($nbNonRelgesAirtel): ?>
                                    <div class="col-4">
                                        <a class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'airtel', 'failed' => true]) ?>" class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Airtel non réglé">
                                            <span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesAirtel > 0 ? 'danger' : 'success' ?>"></span> 
                                            <?= $nbNonRelgesAirtel ?> Airtel
                                        </a>
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php $this->end() ?>
                        
                        <?php if ($nbAnnulations = $nbAnnulationsMvola + $nbAnnulationsOrange + $nbAnnulationsAirtel): ?>
                            <div class="clearfix titre-annulation mt-2">
                                <span class="text-secondary">Annulé<?= $nbAnnulations > 0 ? 's' : '' ?> :</span>
                            </div>
                        <?php endif ?>
                        
                        <div class="row">
                            <?php if ($nbAnnulationsMvola): ?>
                                <div class="col-4">
                                    <span class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Mvola annulé"><span class="fa fa-exclamation-triangle text-<?= $nbAnnulationsMvola > 0 ? 'warning' : 'success' ?>"></span> <span class="text-danger"><?= $nbAnnulationsMvola ?></span> <span class="text-white">Mvola</span></span>
                                </div>
                            <?php endif ?>
                            <?php if ($nbAnnulationsOrange): ?>
                                <div class="col-4">
                                    <span class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Orange annulé"><span class="fa fa-exclamation-triangle text-<?= $nbAnnulationsOrange > 0 ? 'warning' : 'success' ?>"></span> <span class="text-danger"><?= $nbAnnulationsOrange ?></span> <span class="text-white">Orange</span></span>
                                </div>
                            <?php endif ?>
                            <?php if ($nbAnnulationsAirtel): ?>
                                <div class="col-4">
                                    <span class="" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction Airtel annulé"><span class="fa fa-exclamation-triangle text-<?= $nbAnnulationsAirtel > 0 ? 'warning' : 'success' ?>"></span> <span class="text-danger"><?= $nbAnnulationsAirtel ?></span> <span class="text-white">Airtel</span></span>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">   
                            <a data-toggle="tooltip" data-placement="bottom" title="Solde Mvola" class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'mvola']) ?>"><!-- <span class="fa fa-coffee text-success"></span>  -->
                                <div class="clearfix mb-2">
                                    <span class="fa fa-coffee text-success"></span> <span class="text-white">Mvola</span> 
                                    <span class="d-none d-sm-block text-secondary float-right pointer" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_reception_telma)) ?></span>
                                </div>
                                <div class="container-bat bat-mvola" data-progress="<?= $soldeMvolaPercent ?>" data-solde="<?= $smsMvolaData['solde_actuel'] ?>">
                                    <span class="bat">
                                        <span class="progress-bat"></span>
                                    </span>
                                </div>
                                <div class="clearfix d-inline-block">
                                    <span class="text-white solde-recu"><?= $this->App->formatMontant($smsMvolaData['solde_actuel']) ?></span> <span class="libelle-min">Ar</span>
                                    <div class="text-secondary libelle-min">
                                        <?= $this->Date->ago3($lastSmsMvola->senttime) ?>
                                    </div>
                                </div>

                            </a> 
                        </div>
                        <div class="col">
                            <a data-toggle="tooltip" data-placement="bottom" title="Solde Orangemoney" class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'orange']) ?>"><!-- <span class="fa fa-coffee text-warning"></span>  -->
                                <div class="clearfix mb-2 ">
                                    <span class="fa fa-coffee text-warning"></span> <span class="text-white">Orange</span>
                                    <span class="d-none d-sm-block text-secondary float-right pointer mr-2" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_reception_orange)) ?></span>
                                </div>
                                <div class="container-bat bat-orange" data-progress="<?= $soldeOrangePercent ?>" data-solde="<?= $smsOrangeData['solde_actuel'] ?>">
                                    <span class="bat">
                                        <span class="progress-bat"></span>
                                    </span>
                                </div>
                                <div class="clearfix d-inline-block">
                                    <span class="text-white solde-recu"><?= $this->App->formatMontant($smsOrangeData['solde_actuel']) ?></span> <span class="libelle-min">Ar</span>
                                    <div class="text-secondary libelle-min">
                                        <?php if (Chronos::parse($lastSmsOrange->senttime)->wasWithinLast('1 hour')): ?>
                                            <?= $this->Date->ago2($lastSmsOrange->senttime); ?>
                                        <?php else: ?>
                                            <?= Chronos::parse($lastSmsOrange->senttime)->format('d/m, H\\hi'); ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </a> 
                        </div>
                        <div class="col pr-0">
                            <a data-toggle="tooltip" data-placement="bottom" title="Solde Airtelmoney" class="text-decoration-none" target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'type' => 'airtel']) ?>"><!-- <span class="fa fa-coffee text-danger"></span>  -->
                                <div class="clearfix mb-2 titre-bat-airtel">
                                    <span class="fa fa-coffee text-danger"></span> <span class="text-white">Airtel</span>
                                    <span class="d-none d-sm-block text-secondary float-right pointer pr-4" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_reception_airtel)) ?></span>
                                </div>
                                <div class="container-bat bat-airtel l-20" data-progress="<?= $soldeAirtelPercent ?>" data-solde="<?= $smsAirtelData['solde_actuel'] ?>">
                                    <span class="bat">
                                        <span class="progress-bat"></span>
                                    </span>
                                </div>
                                <div class="clearfix d-inline-block">
                                    <span class="text-white solde-recu"><?= $this->App->formatMontant($smsAirtelData['solde_actuel']) ?></span> <span class="libelle-min">Ar</span> 
                                    <div class="text-secondary libelle-min">
                                        <?php if (Chronos::parse($lastSmsAirtel->senttime)->wasWithinLast('1 hour')): ?>
                                            <?= $this->Date->ago2($lastSmsAirtel->senttime); ?>
                                        <?php else: ?>
                                            <?= Chronos::parse($lastSmsAirtel->senttime)->format('d/m, H\\hi'); ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix mt-3 container-auto-switch-solde">
                <?php foreach ($mobiles as $key => $mobile): ?>
                    <?php $mobileName = $mobile['name'] ?>
                    <?php $mobileClass = $mobile['class'] ?>

                    <div class="col <?= $mobileName == 'airtel' ? 'pl-0' : '' ?>">

                        <div class="container-gauge <?= $mobileName == 'airtel' ? 'ml-6px' : '' ?>">
                            <div class="clearfix mb-3">
                                <span class="fa fa-refresh <?= $mobileClass ?>"></span>
                                <span class="text-white">Auto <span class="libelle-min text-secondary">Switch</span></span>
                            </div>
                            <div class="clearfix">
                                <div class="d-inline-block">
                                    <div class="gauge gauge-<?= $mobileName ?>" data-percent="0%">
                                        <span class="percent"></span>
                                    </div>
                                </div>
                                <div class="d-inline-block align-top ml-2">
                                    <div class="clearfix">
                                        <?php if ($params->{'is_activated_auto_switch_solde_'.$mobileName}): ?>
                                            <span class="align-top badge badge-secondary">
                                                <span class="text-light px-1 pointer" data-toggle="tooltip" data-placement="bottom" title="L'auto switch de solde dans les paramètres est activé">Activé</span>
                                            </span>
                                        <?php else: ?>
                                            <span class="align-top badge badge-secondary pointer" data-toggle="tooltip" data-placement="bottom" title="L'auto switch de solde dans les paramètres est désactivé">
                                                <span class="text-dark">Desact.</span>
                                            </span>
                                        <?php endif ?>
                                    </div>
                                    <div class="clearfix libelle-min statut-si-pret-<?= $mobileName ?>">
                                        ...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

            <div class="clearfix mt-4 ">
                <div class="row">
                    <div class="col pr-0">
                        <button data-toggle="tooltip" data-placement="bottom" title="1. Pause, 2. puis vérifie le nb de commandes jusqu'à 0 et désactive temporairement le bouton radio de dépôt du MM. 3. ajuste le solde du local" data-type="mvola" class="pause btn btn-sm btn-dark pause-telma" data-panier-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'getPendingTx', 'mvola2']) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotMvola', 1]) ?>" data-ajustement-solde-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'ajusterSolde', "034"]) ?>"><span class="fa fa-pause text-secondary disable-mvola alternate"></span> &nbsp;<span class="fa fa-refresh text-secondary"></span> <span class="checkstatemvola"></span> <span class="nbpendingtx"></span> <span class="checkstate"></span></button>
                        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait io. Màj numero retrait, et lance play pause" class="btn btn-sm play-depot play-mvola play-telma" data-type="mvola" data-switch-num-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'switchNumero', 34]) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotMvola', 2]) ?>"><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="last-num-mvola onlineretraitnumTelma"></span></a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Process switch general" class="btn btn-sm switchgeneral switch-mvola" data-type="telma"><span class="fa fa-refresh text-info"></span></a>

                        <div class="clearfix mt-4 container-reserve">
                            <span class="fa fa-beer text-success maj-solde pointer" data-url-majsolde="<?= $this->Url->build(['action' => 'majSolde', 'mvola']) ?>" data-op="mvola"></span><span class="ml-2 solde-online-mvola" data-solde-mvola="null">
                                <!-- loadme -->
                            </span>
                            <span class="d-none d-sm-block text-secondary float-right pointer mr-2" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_telma)) ?></span>
                        </div>
                        <div class="libelle-min heure-online-mvola heur-left mt-1 text-secondary"> --</div>
                    </div>
                    <div class="col pr-0">
                        <button data-toggle="tooltip" data-placement="bottom" title="1. Pause, 2. puis vérifie le nb de commandes jusqu'à 0 et désactive temporairement le bouton radio de dépôt du MM. 3. ajuste le solde du local" class="pause btn btn-sm btn-dark pause-orange" data-type="orange" data-panier-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'getPendingTx', 'orange2']) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotOrange', 1]) ?>" data-ajustement-solde-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'ajusterSolde', "032"]) ?>"><span class="fa fa-pause text-secondary disable-orange alternate"></span> &nbsp;<span class="fa fa-refresh text-secondary"></span> <span class="checkstatemvola"></span> <span class="nbpendingtx"></span> <span class="checkstate"></span></button>
                        <a data-toggle="tooltip" data-placement="bottom" title="Màj numero retrait, et lance play pause" class="btn btn-sm play-depot play-orange" data-type="orange" data-switch-num-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'switchNumero', 32]) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotOrange', 2]) ?>"><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="last-num-orange onlineretraitnumOrange"></span></a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Process switch general" class="btn btn-sm switchgeneral switch-orange" data-type="orange"><span class="fa fa-refresh text-info"></span></a>

                        <div class="clearfix mt-4 container-reserve">
                            <span class="fa fa-beer text-warning maj-solde pointer" data-url-majsolde="<?= $this->Url->build(['action' => 'majSolde', 'orange']) ?>" data-op="orange"></span><span class="ml-2 solde-online-orange" data-solde-orange="null">
                                <!-- loadme -->
                            </span>
                            <span class="d-none d-sm-block pointer text-secondary float-right mr-2" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_orange)) ?></span>
                        </div>
                        <div class="libelle-min heure-online-orange heur-left mt-1 text-secondary"> --</div>
                    </div>
                    <div class="col pr-0">
                        <div class="mleftnegatif">
                            <button data-toggle="tooltip" data-placement="bottom" title="1. Pause, 2. puis vérifie le nb de commandes jusqu'à 0 et désactive temporairement le bouton radio de dépôt du MM. 3. ajuste le solde du local" class="pause btn btn-sm btn-dark pause-airtel" data-type="airtel" data-panier-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'getPendingTx', 'airtel2']) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotAirtel', 1]) ?>" data-ajustement-solde-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'ajusterSolde', "033"]) ?>"><span class="fa fa-pause text-secondary disable-airtel alternate"></span> &nbsp;<span class="fa fa-refresh text-secondary"></span> <span class="checkstatemvola"></span> <span class="nbpendingtx"></span> <span class="checkstate"></span></button>
                            <a data-toggle="tooltip" data-placement="bottom" title="Màj numero retrait, et lance play pause" class="btn btn-sm play-depot play-airtel" data-type="airtel" data-switch-num-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'switchNumero', 33]) ?>" data-switchpause-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'playPauseDepotAirtel', 2]) ?>"><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="last-num-airtel onlineretraitnumAirtel"></span></a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Process switch general" class="btn btn-sm switchgeneral switch-airtel" data-type="airtel"><span class="fa fa-refresh text-info"></span></a>


                            <div class="clearfix mt-4 container-reserve">
                                <span class="fa fa-beer text-danger maj-solde pointer" data-url-majsolde="<?= $this->Url->build(['action' => 'majSolde', 'airtel']) ?>" data-op="airtel"></span><span class="ml-2 solde-online-airtel" data-solde-airtel="null">
                                    <!-- loadme -->
                                </span>
                                <span class="d-none d-sm-block pointer text-secondary float-right pr-4" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', strtoupper($params->port_airtel)) ?></span>
                            </div>
                            <div class="libelle-min heure-online-airtel heur-left mt-1 text-secondary"> --</div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="clearfix mt-4 container-reserve">
                <div class="row">
                    <div class="col pr-0">
                        <span data-toggle="tooltip" data-placement="bottom" title="Solde USDT">
                            <span class="fa fa-glass text-success"></span> <span class="solde-usdt text-white"></span>
                        </span>
                    </div>
                    <div class="col pr-0">
                        <span class="pointer majsoldeonline" data-url-majsolde="<?= $this->Url->build(['action' => 'majSoldeUsd', 'payeer']) ?>" data-ptf="payeer">
                            <span class="fa fa-glass text-info"></span> 
                            <span data-toggle="tooltip" data-placement="bottom" title="Solde Payeer" class="solde-payeer text-white" data-solde-payeer="null" data-reserve-payeer="null">
                                <!-- JS -->
                            </span>
                        </span>
                    </div>
                    <div class="col pr-0">
                        <div class="mleftnegatif">
                            <span class="pointer majsoldeonline" data-url-majsolde="<?= $this->Url->build(['action' => 'majSoldeUsd', 'pm']) ?>" data-ptf="pm">
                                <span class="fa fa-glass text-danger"></span> 
                                <span data-toggle="tooltip" data-placement="bottom" title="Solde Perfectmoney" class="solde-pm text-white" data-solde-pm="null" data-reserve-pm="null">
                                    <!-- JS -->
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <?= $this->element('modal_sendsms_fromport') ?>

        <div class="col-md-6">
            <div class="container-stats-membres">
                <div class="clearfix mb-2 text-white">Stats membres <span class="fa fa-bar-chart-o"></span>, <span class="pointer" data-toggle="tooltip" data-placement="bottom" title="Si rouge ou error, vérifier si port com vérificateur vide"><span class="text-secondary">Com vérificateur</span> <span class="fa <?= $params->statut_port_verificateur == 'success' ? 'fa-check-circle text-success' : 'fa-times-circle text-warning' ?> fa-check"></span></span></div>
                <div class="row">
                    <?php foreach ($operateurs_fronts as $key => $op): ?>
                        <div class="col <?= $op['name'] ?>">
                            <div class="mb-2"><?= $op['label'] ?></div>
                            <div class="users_total">Total : <span class="text-white detail">--</span></div>
                            <div class="users_checked">Vérifiés : <span class="text-white detail">--</span></div>
                            <div class="users_unchecked">Non vérifiés : <span class="text-white detail">--</span></div>
                            <div class="users_pending">En attente : <span class="text-white detail">--</span></div>
                            <div class="users_success">Validés : <span class="text-white detail">--</span></div>
                            <div class="users_failed">Echoués : <span class="text-white detail">--</span></div>
                        </div>
                    <?php endforeach ?>
                </div>

                <div class="row">
                    <?php foreach ($operateurs_fronts as $key => $op): ?>
                        <div class="col <?= $op['name'] ?>">
                            <div class="progress mt-4">
                              <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark text-white" role="progressbar" aria-valuenow="0" style="width: 0%" aria-valuemin="0" aria-valuemax="100">0%</div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="container-2fa-stats mt-4">
                <div class="2fa-stats">
                    <div class="clearfix mb-2 text-white"><a target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'authSms']) ?>">2FA Stats <span class="fa fa-bar-chart-o"></span></a></div>
                    <div class="row">
                        <?php foreach ($operateurs_fronts as $key => $op): ?>
                            <?php $port = @$params->{'port_sms_out_'.$op['name']} ?>
                            <?php $opName = $op['name'] ?>
                            <div class="col pr-0 container-sms-<?= $opName ?>" >
                                <div class="mb-2">
                                    <b><?= $op['label'] ?></b> 
                                    <span class="d-none d-sm-block pointer text-secondary float-right <?= $opName == 'airtel' ? 'pr-4' : 'mr-2' ?>" data-toggle="tooltip" data-placement="bottom" title="Serial Com Port"><?= str_replace('COM', 'C.', $port) ?></span>
                                    <?php if ($params->{'is_recharge_auto_forfait_'.$opName}): ?>
                                        <span data-toggle="tooltip" data-placement="bottom" title="Recharge Forfait SMS <?= ucfirst($opName) ?> Auto activé" class="pointer fa fa-refresh text-success"></span>
                                    <?php else: ?>
                                        <span data-toggle="tooltip" data-placement="bottom" title="Recharge Forfait SMS <?= ucfirst($opName) ?> Auto désactivé" class="pointer fa fa-refresh text-secondary"></span>
                                    <?php endif ?>
                                </div>
                                <span class="<?= $op['class'] ?> fa fa-send-o"></span> <span class="text-white">EN : <?= ${'authSmss'.$op['label']}->count() ?></span> <br>
                                <span class="text-secondary"><span class="fa fa-envelope-o"></span> UO :</span>  <span class="text-white"><?= $params->{'nb_sms_uo_'.$op['name']} ?></span><br>
                                <span class="text-secondary"><span class="fa fa-envelope"></span> TO :</span>  <span class="text-white"><?= $params->{'nb_sms_to_'.$op['name']} ?></span><br>

                                <div class="btn-2fa-updated-at mt-3">
                                    <?php if (!empty($port)): ?>
                                        <span data-toggle="tooltip" data-placement="bottom" title="Date de dernier achat forfait SMS" class="pointer"><?= $params->{'last_recharge_forfait_'.$op['name']} ?> <span class="fa fa-clock-o text-info"></span></span>
                                        <br>
                                        <span data-toggle="tooltip" data-placement="bottom" title="Date de dernière consultation consommation du Forfait SMS" class="text-secondary pointer"><?= $params->{'last_checked_forfait_at_'.$op['name']} ?> <span class="fa fa-clock-o"></span></span>
                                        
                                        <div class="btn-2fa mt-2 pt-2">
                                            <a data-toggle="tooltip" data-placement="bottom" title="Recharge <?= $op['label'] ?> Forfait SMS" href="<?= $this->Url->build(['controller' => 'Ussd', 'action' => 'rechargeForfait', $op['name']]) ?>" class="btn btn-sm btn-dark recharge-sms" data-op="<?= $op['name'] ?>" data-label="<?= $op['label'] ?>"><span class="fa fa-cloud-download"></span></a>
                                            <a data-toggle="tooltip" data-placement="bottom" title="Consulter Consommation <?= $op['label'] ?> Forfait SMS" href="<?= $this->Url->build(['controller' => 'Ussd', 'action' => 'majAndConsultationForfait', $op['name']]) ?>" class="btn btn-sm btn-dark info-conso-forfait"><span class="fa fa-file-text-o"></span></a>
                                            <a class="btn btn-sm" href="javascript:void(0);" data-port="<?= $port ?>" data-label="<?= $op['label'] ?>" data-href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'sendCustomSmsfromOrange', $port]) ?>" data-toggle="modal" data-target="#sendsmsfromport"><span class="fa fa-envelope-open-o"></span></a>
                                        </div>
                                    <?php endif ?>
                                </div>

                            </div>
                        <?php endforeach ?>
                    </div>

                </div>
            </div>

            <div class="progression-switch mt-4">
                <!-- JS -->
            </div>
        </div>
    </div>

    <div class="clearfix msg-forfait-result mt-2">
        <!-- JS -->
    </div>

    <div class="row mt-3 d-none">
        <div class="col-md-6">

            <div class="clearfix">
                <?= date('d/m/Y') ?> :
                <span class="fa fa-envelope-o text-success"></span> <span class="text-white mr-2"><?= $nbAuthTelma =  $authSmssTelma->count() ?></span> 
                <span class="fa fa-envelope-o  text-warning"></span> <span class="text-white mr-2"><?= $nbAuthOrange =  $authSmssOrange->count() ?></span> 
                <span class="fa fa-envelope-o  text-danger"></span> <span class="text-white mr-2"><?= $nbAuthAirtel =  $authSmssAirtel->count() ?>,</span>
                Total : <span class="fa fa-envelope-o  text-white"></span> <span class="text-white"><?= $nbAuthTelma+$nbAuthOrange+$nbAuthAirtel ?></span> <br>
            </div>

        </div>
    </div>
</div>

<?= $this->element('at') ?>
<?= $this->element('at_cmd') ?>
<?= $this->element('ussd') ?>
<?= $this->element('debug_tout_sms') ?>
<?= $this->element('histo_sms_modal') ?>
<?= $this->element('histo_callback') ?>
<?= $this->element('modal_maj_solde') ?>
<?= $this->element('modal_maj_solde_usd') ?>
<?= $this->element('relance_sms') ?>


<div class="clearfix mt-4">
    <div class="d-none d-sm-block">
        <p>
            <a class="btn btn-sm" data-toggle="collapse" href="#search" role="button" aria-expanded="false" aria-controls="search">
                <span class="fa fa-search"></span>
            </a>
        </p>
        <div class="collapse" id="search">
            <?= $this->Form->create(false, ['class' => 'd-none d-sm-block', 'type' => 'get']); ?>
                <div class="row mb-2">
                    <div class="col-lg-2 col-6"><?= $this->Form->control('numero', ['default' => @$options['numero'], 'label' => false, 'placeholder' => 'Numero', 'class' => 'form-control']); ?></div>
                    <div class="col-lg-2 col-6"><?= $this->Form->control('montant', ['default' => @$options['montant'], 'label' => false, 'placeholder' => 'Montant', 'class' => 'form-control']); ?></div>
                    <div class="col-lg-2 col-6"><?= $this->Form->control('ref', ['default' => @$options['ref'], 'label' => false, 'placeholder' => 'Ref', 'class' => 'form-control']); ?></div>
                    <div class="col-lg-2 col-6"><?= $this->Form->text('date', ['type' => 'date', 'default' => @$options['date'], 'label' => false, 'placeholder' => 'Ref', 'class' => 'form-control']); ?></div>
                    <div class="col-lg-2 col-6">
                        <?= $this->Form->submit('filtrer', ['label' => 'sss', 'class' => 'btn btn-primary', 'type' => 'submit']); ?>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>

        <div class="clearfix ">
            <div class="float-right">
                <div class="last-sms-maj mb-4">
                    <!-- JS -->
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix">
        <a class="btn btn-sm float-right" data-toggle="collapse" href="#options" role="button" aria-expanded="false" aria-controls="options">
            Actions
        </a>
    </div>
    <div class="mt-4 collapse" id="options">
        <div class="clearfix my-auto ">
            <a href="<?= $this->Url->build(['action' => 'index', 'orange']) ?>" class="<?= $paiement == 'orange' ? 'active' : '' ?> btn btn-primary btn-sm"><span class="fa fa-cloud-upload"></span> TX Orange</a>
            <a href="<?= $this->Url->build(['action' => 'majRefMobileVersSite', 'orange']) ?>" data-mobile="orange" class="btn btn-primary maj-trans-bdd-vers-site btn-sm"><span class="fa fa-cloud-upload"></span> TX &rarr; SITE <span class="loading"><span class="fa fa-circle-o-notch"></span></span></a>
            <a href="<?= $this->Url->build(['action' => 'sendAllSmsTransactionInBdd', "032"]) ?>" class="btn btn-primary btn-sm"><span class="fa fa-cloud-download"></span> SMS &rarr; BDD</a>
            <a data-titre="Sms Orange" href="<?= $this->Url->build(['action' => 'saveAllSms', '032']) ?>" class="btn btn-primary btn-sm backup-sms"> <span class="fa fa-cloud-download"></span> Backup</a>
            <a data-titre="Sms Orange" data-href="<?= $this->Url->build(['action' => 'debugSmsModem', '032']) ?>" data-toggle="modal" data-target="#debugall-sms" class="btn btn-primary btn-sm"> <span class="fa fa-exclamation-circle text-warning"></span> LIRE SMS</a>
            <a href="<?= $this->Url->build(['action' => 'vider-transactions', 'orange']) ?>" onclick='return confirm("Êtes-vous de vouloir procéder ?")' class=" btn-sm btn btn-danger float-right mb-4"> <span class="fa fa-trash-o"></span> BDD</a>
            <a href="<?= $this->Url->build(['action' => 'vidageSmsModem', '032']) ?>" class="mr-2 btn btn-primary btn-sm float-right vidageSmsModem"> <span class="fa fa-trash-o"></span> SMS</a>
        </div>
        <div class="clearfix my-auto">
            <a href="<?= $this->Url->build(['action' => 'index', 'mvola']) ?>" class="<?= $paiement == 'mvola' ? 'active' : '' ?> btn btn-primary btn-sm"><span class="fa fa-cloud-upload"></span> TX Mvola &nbsp;</a>
            <a href="<?= $this->Url->build(['action' => 'majRefMobileVersSite', 'mvola']) ?>" data-mobile="mvola" class="btn btn-primary maj-trans-bdd-vers-site btn-sm"><span class="fa fa-cloud-upload"></span> TX &rarr; SITE <span class="loading"><span class="fa fa-circle-o-notch"></span></span></a>
            <a href="<?= $this->Url->build(['action' => 'sendAllSmsTransactionInBdd', "034"]) ?>" class="btn btn-primary btn-sm"><span class="fa fa-cloud-download"></span> SMS &rarr; BDD</a>
            <!-- <a data-titre="Sms Telma" href="<?= $this->Url->build(['action' => 'saveAllSms', '034']) ?>" class="btn btn-primary btn-sm backup-sms-mvola"> <span class="fa fa-cloud-download"></span> Backup</a> -->
            <a data-titre="Sms Telma" href="<?= $this->Url->build(['action' => 'saveAllSms', '034']) ?>" class="btn btn-primary btn-sm backup-sms-mvola"> <span class="fa fa-cloud-download"></span> Backup</a>
            <a data-titre="Sms Telma" data-href="<?= $this->Url->build(['action' => 'debugSmsModem', '034']) ?>"  data-toggle="modal" data-target="#debugall-sms" class="btn btn-primary btn-sm"> <span class="fa fa-exclamation-circle text-warning"></span> LIRE SMS</a>
            <a href="<?= $this->Url->build(['action' => 'vider-transactions', 'mvola']) ?>" onclick='return confirm("Êtes-vous de vouloir procéder ?")' class=" btn-sm btn btn-danger float-right mb-4"><span class="fa fa-trash-o"></span> BDD</a>
            <a href="<?= $this->Url->build(['action' => 'vidageSmsModem', '034']) ?>" class="mr-2 btn btn-primary btn-sm float-right vidageSmsModem"> <span class="fa fa-trash-o"></span> SMS</a>
        </div>
        <div class="clearfix my-auto">
            <a data-titre="Backup Sms Airtel" href="<?= $this->Url->build(['action' => 'saveAllSms', '033']) ?>" class="btn btn-primary btn-sm backup-sms"> <span class="fa fa-cloud-download"></span> Backup</a>
            <a data-titre="Sms Airtel" data-href="<?= $this->Url->build(['action' => 'debugSmsModem', '033']) ?>"  data-toggle="modal" data-target="#debugall-sms" class="btn btn-primary btn-sm"> <span class="fa fa-exclamation-circle text-warning"></span> LIRE SMS</a>
            <a href="<?= $this->Url->build(['action' => 'vider-transactions', 'airtel']) ?>" onclick='return confirm("Êtes-vous de vouloir procéder ?")' class=" btn-sm btn btn-danger float-right mb-4"> <span class="fa fa-trash-o"></span> BDD</a>
            <a href="<?= $this->Url->build(['action' => 'vidageSmsModem', '033']) ?>" class="mr-2 btn btn-primary btn-sm float-right vidageSmsModem"> <span class="fa fa-trash-o"></span> SMS</a>
        </div>


        <div class="row-fluid to-mg-top">
            <?php if (!$transactionsHistorics->isEmpty()): ?>
                <div class="table-responsive">
                    <table class="table  table-bordered table-hover mt-4 ">
                        <thead>
                            <tr>
                                <th width="16.66%">Numéro</th>
                                <th width="16.66%">Ref</th>
                                <th width="16.66%">Montant</th>
                                <th width="16.66%">Solde</th>
                                <th width="16.66%">Wallet</th>
                                <th width="16.66%">Date SMS</th>
                            </tr>
                        </thead>
                        <tbody >
                            <?php foreach ($transactionsHistorics as $key => $transactionsHistoric): ?>
                                <tr>
                                    <td>
                                        <?= $transactionsHistoric->numero ?> 
                                        <!-- <span data-toggle="tooltip" data-placement="bottom" title="<?= ucfirst($transactionsHistoric->paiement) ?>" class="fa fa-circle <?= $transactionsHistoric->get('MobileColor') ?>"></span> -->
                                    </td>
                                    <td class="<?= $transactionsHistoric->is_traitee ? 'text-white' : 'text-secondary' ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $transactionsHistoric->is_traitee ? 'Ref marquée et envoyée avec succès sur le site' : "La ref n'a pas encore été marquée ni envoyée sur le site" ?>"><?= $transactionsHistoric->ref ?></td>
                                    <td><?= $this->Number->format($transactionsHistoric->montant); ?></td>
                                    <td><?= $this->Number->format($transactionsHistoric->solde_actuel); ?></td>
                                    <td><?= $transactionsHistoric->type ?></td>
                                    <td data-toggle="<?= $transactionsHistoric->modified ? 'tooltip' : '' ?>" data-placement="bottom" title="<?= $transactionsHistoric->modified ? 'Màj le : '.$transactionsHistoric->modified : '' ?>"><?= $transactionsHistoric->date->format('d/m/Y H:i:s') ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                Aucun résultat
            <?php endif ?>
        </div>
        
        <?= $this->element('pagination/pagination') ?>
    </div>

</div>