<?php 
    $options = [
        'Envoi' => [
            'telma' => 'port_telma',
            'orange' => 'port_orange',
            'airtel' => 'port_airtel',
        ]
    ]
?>

<?php 
    // debug($param); 
    // debug($statutServMon);
?>

<?= $this->Form->control('array_ports', ['type' => 'hidden', 'class' => 'form-control', 'value' => json_encode($arrayPorts ?? [], JSON_PRETTY_PRINT)]); ?>

<?php $this->start('menuportlist') ?>
    <div class="clearfix mt-4 mb-4">
        <div class="row">
            <div class="col-md-4">
                <div class="containerstatusserver">
                    <div class="divstatusserver">
                        <a class="btn btn-outline-secondary btn-sm" href="http://127.0.0.1:9501/" target="_blank" onclick="this.blur();">
                            <span class="">ServiceNg</span>
                            <span data-toggle="tooltip" data-placement="bottom" title="StatutOzeki" class=" sm-size d-inline-block align-middle statusserver ozeking fa fa-circle <?= $statutOzeki == 'True' ? 'serversuccess' : 'text-danger' ?>"></span>
                            <span data-toggle="tooltip" data-placement="bottom" title="StatutServMon" class="sm-size d-inline-block align-middle statusserver servmon fa fa-circle <?= $statutServMon == 'True' ? 'serversuccess' : 'text-danger' ?>"></span>
                        </a>
                        <a href="<?= $this->Url->build(['action' => 'startOrStopServerNg']) ?>" class="btn btn-outline-secondary btn-sm startOrStopServerNg" data-is_server_ng_started="<?= $param->is_server_ng_started ?>">
                            <?php if ($param->is_server_ng_started == true): ?>
                                <span class="fa fa-stop"></span>
                            <?php else: ?>
                                <span class="fa fa-play"></span>
                            <?php endif ?>
                        </a>
                        <a href="<?= $this->Url->build(['action' => 'startServiceNg']) ?>" class="btn btn-outline-secondary btn-sm forceserver forcestartserver">
                            <span class="fa fa-play text-warning"></span>
                        </a>
                        <a href="<?= $this->Url->build(['action' => 'stopServiceNg']) ?>" class="btn btn-outline-secondary btn-sm forceserver forcestopserver">
                            <span class="fa fa-stop text-warning"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Générer le ComPortManager Config à partir du ComPortInfo.exe pour fixer tous les ports (Générer manuellement le fichier report.txt par défaut du ComPortInfo avant de générer le ComPortMan.ini)" onclick="this.blur();" href="<?= $this->Url->build(['action' => 'generateComPortManConfig']) ?>" class="btn btn-outline-secondary btn-sm forceserver">
                            <span class="fa fa-code"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Reset tous les GSM" href="<?= $this->Url->build(['action' => 'forceResetAllComPort']) ?>" class="btn btn-outline-secondary btn-sm stopOzekiAndStartForAction">
                            <span class="text-danger fa fa-refresh"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <a href="javascript:void(0);" class="refreshport btn btn-outline-primary btn-sm float-right">Réactualise <span class="fa fa-refresh"></span></a>
                <a href="<?= $this->Url->build(['action' => 'remapPortToNum']) ?>" class="text-white remapmsidn btn btn-outline-primary btn-sm float-right mr-2" data-toggle="tooltip" data-placement="bottom" title="Remplie les numéros pour chaque port selon les AT op">Remap Msidn <span class="progression-port text-white"></span> <span class="fa fa-refresh"></span></a>
                <a href="<?= $this->Url->build(['action' => 'remapRoutage']) ?>" class="text-white btn btn-outline-primary btn-sm float-right remapRoutage mr-2" data-toggle="tooltip" data-placement="bottom" title="Affecte les ports dans paramétrages selon les types (envoi, sms, reception) et les numéros, met à jour les fichiers de config ServiceNgOzeki et affecte les drivers cf Envoi OP. Vérifier les cases des numéros.">Remap Routage <span class="fa fa-refresh"></span></a>
                <a href="<?= $this->Url->build(['action' => 'remapLettre']) ?>" class="text-white btn btn-outline-primary btn-sm float-right remapLettre mr-2" data-toggle="tooltip" data-placement="bottom" title="Affecte les lettres USB selon les ports, processus long.">Remap Lettre <span class="fa fa-refresh"></span></a>
                <a href="<?= $this->Url->build(['action' => 'remapDriverEnvoi']) ?>" class="text-white btn btn-outline-primary btn-sm float-right remapDriverEnvoi mr-2" data-toggle="tooltip" data-placement="bottom" title="Affecte les lettres USB selon les ports">Remap Driver Envoi <span class="fa fa-refresh"></span></a>
                <a href="<?= $this->Url->build(['action' => 'definirNumeroTransactionVersProd']) ?>" class="text-white btn btn-outline-primary btn-sm float-right definirNumeroTransactionVersProd mr-2" data-toggle="tooltip" data-placement="bottom" title="Définir les numéros de dépôt et retrait dans paramètre sur le prod">Définir Num Prod <span class="fa fa-refresh"></span></a>
            </div>
        </div>
    </div>
<?php $this->end() ?>

<?= $this->fetch('menuportlist') ?>

<div class="msgres mb-4 clearfix">
    <!-- JS -->
</div>


<div class="alert alert-mappage" role="alert">
    <!-- JS -->
</div>

<h6>Routage MSIDN</h6>

<div class="container-routage table-responsive text-nowrap">
    <table class="table table-bordered table-condensed mb-0 table-routage">
        <thead>
            <tr>
                <th width="20%" class="text-secondary">Port <?= $nbPorts ?></th>
                <th width="14.2%" class="text-secondary">Statut</th>
                <th width="21%" class="text-secondary">Mobile</th>
                <th width="12%" class="text-secondary">Driver</th>
                <th width="11%" class="text-secondary">Numéro</th>
                <th width="16%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ports as $key => $port): ?>
                <?php $entityPort = @$comPorts[$port] ?>
                <tr class="align-middle port actif port-<?= $port ?>" data-classport="port-<?= $port ?>" data-port="<?= $port ?>" data-url="<?= $this->Url->build(['action' => 'remapPortToNum', $port]) ?>">
                    <td class="align-middle">
                        <span class="<?= $comPorts[$port] ?? 'text-warning' ?>">
                            <?= $port ?>
                            <span class="statuscheck">
                                <!-- JS -->
                            </span>
                        </span>
                    </td>
                    <td>
                        <span class="statutport">
                            <!-- js -->
                        </span>
                    </td>
                    <td>
                        <span class="fa fa-circle <?= $entityPort ? $entityPort->get('ClassHtml') : 'text-secondary' ?>"></span>
                        <span class="text-secondary"><?= @ucfirst($entityPort->mobile) ?></span>
                    </td>
                    <td>
                        <span class="text-secondary">
                            <?= @$entityPort->letter ?> 
                            <?php if (!empty($param->{'driver_'.@$entityPort->mobile}) && $param->{'driver_'.@$entityPort->mobile} == $entityPort->letter): ?>
                                <span class="fa fa-hdd-o text-white right-float mt-11" data-toggle="tooltip" data-placement="bottom" title="Driver pour les resets d'envois"></span>
                            <?php endif ?>
                            <?php if (@$entityPort->numero == $param->num_port_titulaire): ?>
                                <span data-toggle="tooltip" data-placement="bottom" title="Vérificateur Titulaire" class="fa fa-user-circle right-float mt-11  mr-2"></span>
                            <?php endif ?>
                        </span>
                    </td>
                    <td class="op-type">
                        <?php if ($entityPort): ?>
                            <div class="<?= $entityPort->numero && !in_array($entityPort->type, ['envoi', 'reception', 'sms']) ? 'err-types' : '' ?>">
                                <span class="port-numero"><?= (@$entityPort->numero) ?></span>
                                
                                <?php if (@$entityPort->type == 'envoi'): ?>
                                    <?php $typeMobile = $entityPort->mobile; ?>
                                    <?php if ($typeMobile == 'telma'): ?>
                                        <?php $typeMobile = 'mvola'; ?>
                                    <?php endif ?>
                                    <span class="typerempli fa fa-arrow-circle-up text-danger mobile-key-num" data-op-type="numero_<?= $typeMobile ?>_retrait"></span>
                                <?php elseif(@$entityPort->type == 'reception'): ?>
                                    <span class="typerempli fa fa-arrow-circle-down text-success mobile-key-num" data-op-type="numero_depot_<?= $entityPort->mobile ?>"></span>
                                <?php elseif(@$entityPort->type == 'sms'): ?>
                                    <span class="typerempli fa fa-envelope-o text-info mobile-sms"></span>
                                <?php endif ?>

                                <span class="state-num-local-prod float-right">
                                    <!--  -->
                                </span>
                            </div>
                        <?php endif ?>
                    </td>
                    <td class="actions">
                        <!-- <a href="<?= $this->Url->build(['action' => 'remapPortToNum', $port]) ?>" class="btn btn-outline-secondary text-white btn-sm refreshsingle" data-toggle="tooltip" data-placement="bottom" title="Remap Routage">R.R <span class="fa fa-refresh"></span></a> -->
                        <a href="<?= $this->Url->build(['action' => 'remapPortToNum', $port]) ?>" class="btn btn-outline-secondary text-white btn-sm refreshsingle" data-toggle="tooltip" data-placement="bottom" title="Remap Msidn">R.M <span class="fa fa-refresh"></span></a>
                        <a href="<?= $this->Url->build(['action' => 'resetUsb2', @$entityPort->letter]) ?>" class="btn btn-outline-secondary text-white btn-sm resetUsb" data-toggle="tooltip" data-placement="bottom" title="Reset <?= @$entityPort->letter ?>"><span class="resultcheck text-white"></span>R.U <span class="fa fa-refresh"></span></a>
                        <a href="<?= $this->Url->build(['action' => 'checkPort', $port]) ?>" class="btn btn-outline-secondary btn-sm checkPort" data-toggle="tooltip" data-placement="bottom" title="Check statut port si occupé, dernière modification <?= @$entityPort->modified ?>"><span class="text-white"></span> <span class="fa fa-question-circle"></span></a>
                    </td>
                </tr>
            <?php endforeach ?>

            <?php foreach ($missingsPorts as $key => $entityPort): ?>
                <?php $port = $entityPort->com ?>
                <tr class="align-middle port port-<?= $port ?>" data-classport="port-<?= $port ?>" data-port="<?= $port ?>" data-url="<?= $this->Url->build(['action' => 'remapPortToNum', $port]) ?>">
                    <td class="align-middle">
                        <span class="text-secondary">
                            <?= $port ?>
                            <span class="statuscheck">
                                <!-- JS -->
                            </span>
                            Déconnecté
                        </span>
                    </td> 
                    <td>
                        <div class="text-secondary">--</div>
                    </td>
                    <td>
                        <span class="fa fa-circle <?= $entityPort ? $entityPort->get('ClassHtml') : 'text-secondary' ?>"></span>
                        <span class="text-secondary"><?= @ucfirst($entityPort->mobile) ?></span>
                    </td>
                    <td>
                        <span class="text-secondary"><?= $entityPort->letter ?></span>
                    </td>
                    <td>
                        <div class="">
                            <?= $this->App->formatNumberFull(@$entityPort->numero) ?>
                            
                            <?php if (@$entityPort->type == 'envoi'): ?>
                                <span class="fa fa-arrow-circle-up text-danger"></span>
                            <?php elseif(@$entityPort->type == 'reception'): ?>
                                <span class="fa fa-arrow-circle-down text-success"></span>
                            <?php elseif(@$entityPort->type == 'sms'): ?>
                                <span class="fa fa-envelope-o text-secondary"></span>
                            <?php endif ?>

                        </div>
                    </td>
                    <td class="actions">
                        <a href="<?= $this->Url->build(['action' => 'remapPortToNum', $port]) ?>" class="btn disabled btn-outline-secondary text-white btn-sm refreshsingle">Init <span class="fa fa-refresh"></span></a>
                        <a href="javascript:void(0);" class="btn btn-outline-secondary btn-sm disabled" data-toggle="tooltip" data-placement="bottom" title="Check statut port si occupé"><span class="resultcheck text-white"></span> <span class="fa fa-question-circle"></span></a>
                        <span class="text-secondary"><?= @$entityPort->modified ?></span>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<div class="clearix mt-4">
    <h6 class="text-danger"><b>ENVOI</b></h6>
    <div class="row">
        <?php foreach ($operateurs_fronts as $key => $op): ?>
            <?php $opName = $op['name'] ?>
            <?php $opLabel = $op['label'] ?>
            <div class="col-md-4">
                <?= $opLabel ?>
                <div class="row">
                    <div class="col-md-4 containerport containerportenvoi <?= $opName ?>">
                        <?= $this->Form->text('port_'.$opName, ['class' => 'port form-control', 'label' => false, 'default' => $param->{'port_'.$opName}]); ?>
                    </div>
                    <div class="col-md-8 containernumselect containernumenvoi <?= $opName ?>">
                        <?= $this->Form->select('num_port_envoi_'.$opName, ${'portList'.$opLabel}, ['empty' => true, 'label' => false, 'default' => $param->{'num_port_envoi_'.$opName}]); ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="clearfix mt-4">

    <div class="row clearfix">
        <div class="col-md-12">
            <h6 class="text-success"><b>RECEPTION</b></h6>
                <a 
                    data-toggle="collapse" 
                    data-parent="#accordion" 
                    aria-expanded="false" 
                    href="#switch" 
                    aria-controls="switch"
                    class="float-right "
                >
                    Switch
                </a>
        </div>
    </div>

    <div class="row">
        <?php foreach ($operateurs_fronts as $key => $op): ?>
            <?php $opName = $op['name'] ?>
            <?php $opLabel = $op['label'] ?>
            <div class="col-md-4">
                <div id="switch" class="collapse">
                    <div class="row my-2">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="clearfix">
                                <a href="<?= $this->Url->build(['action' => 'switchport', $opName]) ?>" data-toggle="tooltip" data-placement="bottom" data-html="true" title="Après avoir switcher : <br> 1. remapper les routages pour re génerer les configs du NG, <br> 2. définir Num Prod" data-op="<?= $opName ?>" class="switchport  btn btn-outline-secondary float-right <?= $param->{'is_exchange_activated_'.$opName} == 1 ? 'activated' : '' ?>"><span class="fa fa-arrows-v text-danger"></span><span class="fa fa-arrows-v text-success"></span> Envoi/Réception</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $opLabel ?>
                <div class="row">
                    <div class="col-md-4 containerport containerportreception <?= $opName ?>">
                        <?= $this->Form->text('port_reception_'.$opName, ['class' => 'port form-control', 'label' => false, 'default' => $param->{'port_reception_'.$opName}]); ?>
                    </div>
                    <div class="col-md-8 containernumselect containernumreception <?= $opName ?>">
                        <?= $this->Form->select('num_port_reception_'.$opName, ${'portList'.$opLabel}, ['empty' => true, 'label' => false, 'default' => $param->{'num_port_reception_'.$opName}]); ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="clearix mt-4 form-routagecom">
    <div class="row">
        <div class="col-md-6">
            <h6 class="text-info"><b>SMS</b></h6>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>
    <div class="row">
        <?php foreach ($operateurs_fronts as $key => $op): ?>
            <?php $opName = $op['name'] ?>
            <?php $opLabel = $op['label'] ?>
            <div class="col-md-4">
                <?= $opLabel ?>
                <div class="row">
                    <div class="col-md-4 containerport">
                        <?= $this->Form->text('port_sms_out_'.$opName, ['class' => 'port form-control', 'label' => false, 'default' => $param->{'port_sms_out_'.$opName}]); ?>
                    </div>
                    <div class="col-md-8 containernumselect">
                        <?= $this->Form->select('num_port_sms_'.$opName, ${'portList'.$opLabel}, ['empty' => true, 'label' => false, 'default' => $param->{'num_port_sms_'.$opName}]); ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="clearix mt-4 form-routagecom">
    <div class="row">
        <div class="col-md-6">
            <h6 class="text-white">Driver Reset cf Envoi</h6>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>
    <div class="row">
        <?php foreach ($operateurs_fronts as $key => $op): ?>
            <?php $opName = $op['name'] ?>
            <?php $opLabel = $op['label'] ?>
            <div class="col-md-4">
                <?= $opLabel ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->select('driver_'.$opName, ${'driverList'.$opLabel}, ['empty' => true, 'label' => false, 'default' => $param->{'driver_'.$opName}]); ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-4">
        <h6 class="text-white">Vérificateur Titulaire <span class="float-right">Free Port</span></h6>
        <div class="row">
            <div class="col-md-4 containerport">
                <?= $this->Form->text('port_titulaire', ['class' => 'port form-control', 'label' => false, 'default' => $param->{'port_titulaire'}]); ?>
            </div>
            <div class="col-md-8 containernumselect" >
                <?= $this->Form->control('num_port_titulaire', ['empty' => true, 'options' => $freeNumeros, 'label' => false, 'default' => $param->{'num_port_titulaire'}]); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->fetch('menuportlist') ?>