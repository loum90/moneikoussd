<table class="table table-condensed">
    <thead>
        <tr>
            <th width="10%">De</th>
            <th width="90%">Contenu</th>
        </tr>
    </thead>
    <tbody>
        <?php if ($histoSms): ?>
            <?php foreach ($histoSms as $sms): ?>
                <tr>
                    <td><?= $sms['numero'] ?></td>
                    <td><?= $sms['date'] ?><br><?= $sms['content'] ?></td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>