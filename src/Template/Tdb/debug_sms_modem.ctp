<?php if (!empty($allSms)): ?>
    <?php foreach ($allSms as $key => $sms): ?>
        <p>
            <div class="mb-2"><?= $sms['from'] ?><span class="float-right"><?= $sms['date'] ?></span></div>
            <div class="text-white mb-1"><?= $sms['content'] ?></div>
        </p>

        <?php if ($key < count($allSms)-1): ?>
            <hr>
        <?php endif ?>
    <?php endforeach ?>
<?php else: ?>
    Aucun sms
<?php endif ?>