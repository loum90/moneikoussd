<?php 
  // $result = $this->Paginator->getTemplates();
  $this->Paginator->setTemplates([
    'number'       => '<li class="mr-3"><a class="no-lang" href="{{url}}">{{text}}</a></li>',
    'current'      => '<li class="mr-3"><a class="active text-success" href="">{{text}}</a></li>',
    'nextActive'   => '<li class="mr-3 next"><a class="no-lang" rel="next" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="mr-3 next disabled"><a class="no-lang" href="" onclick="return false;">{{text}}</a></li>',
    'prevActive'   => '<li class="prev"><a class="no-lang" rel="prev" href="{{url}}">{{text}}</a></li>',
    'prevDisabled' => '<li class="prev disabled"><a class="no-lang" href="" onclick="return false;">{{text}}</a></li>',
  ]);
 ?>


<div class="row">
    <div class="col-md-6">
        <?php if ($this->Paginator->counter('{{count}}')> 0): ?>
            <ul class="pagination pagination-md m-0">
                <li>
                    <?php echo $this->Paginator->prev('<span class="fa fa-angle-left mr-3"></span>', array('escape' => false,'aria-label' => 'Previous')); ?>
                </li>
                <li>
                    <?php echo $this->Paginator->numbers(['separator' => '',]);?>
                </li>
                <li>
                    <?php echo $this->Paginator->next('<span class="fa fa-angle-right"></span>', array('escape' => false,'aria-label' => 'Next')); ?>
                </li>
            </ul>
        <?php endif ?>
    </div>
    <div class="col-md-6">
    </div>
</div>