<?php 
  $this->Paginator->templates([
      'prevDisabled' => '<button class="btn btn-sm btn-default type="button">{{text}}</button>',
      'prevActive' => '<a class="btn btn-sm btn-default" href="{{url}}">{{text}}</a>',
      'number' => '<a class="btn btn-sm btn-default" href="{{url}}">{{text}}</a>',
      'current' => '<a class="btn btn-sm btn-primary" href="{{url}}">{{text}}</a>',
      'active' => '<a class="btn btn-sm btn-primary" href="{{url}}">{{text}}</a>',
      'prev' => '<a class="btn btn-sm btn-default" href="{{url}}">{{text}}</a>',
      'next' => '<a class="btn btn-sm btn-default" href="{{url}}">{{text}}</a>',
      'nextActive' => '<a class="btn btn-sm btn-default" href="{{url}}">{{text}}</a>',
      'nextDisabled' => '<button class="btn btn-sm btn-default type="button">{{text}}</button>',
  ]);
 ?>

 <div class="btn-group <?= $class ?>">
     <?php echo $this->Paginator->prev('<span class="glyphicon glyphicon-menu-left"></span>', ['escape' => false]); ?>
     <?php echo $this->Paginator->numbers();?>
     <?php echo $this->Paginator->next('<span class="glyphicon glyphicon-menu-right"></span>', ['escape' => false]); ?>
 </div>