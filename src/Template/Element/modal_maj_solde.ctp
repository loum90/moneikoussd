<div class="modal fade" id="maj_solde" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <?= $this->Form->create(null, ['class' => 'form-majsolde', 'type' => 'post']); ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="titreussd">Màj solde <span class="mobile"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div> 

                <div class="modal-body">
                    <?= $this->Form->control('montant'); ?>
                    <button type="button" class="btn btn-primary btn-sm majinputsoldeDown float-right" data-down="1000000">- 1M</button>
                    <button type="button" class="btn btn-primary btn-sm majinputsoldeUp float-right mr-2" data-up="1000000">+ 1M</button>
                    <button type="button" class="btn btn-primary btn-sm resetSolde float-right mr-2">Réinit</button>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-outline-secondary text-white btn-submit-maj-solde">Soumettre <span class="fa fa-check text-success statemajed d-none"></span></button>
                </div>
            </div>
        <?= $this->form->end() ?>
    </div>
</div>