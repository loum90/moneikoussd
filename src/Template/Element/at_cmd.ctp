<div class="modal fade" id="at-cmd" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="titreussd">AT CMD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>  
            <div class="modal-body">
                <?= $this->Form->create(false, ['url' => ['controller' => 'Tdb', 'action' => 'execUssdByPort'], 'class' => 'form-ussd', 'type' => 'POST']); ?>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('command', ['required', 'escape' => false, 'label' => 'Saisir la commande <span class="loading"></span>', 'placeholder' => '', 'class' => 'form-control']); ?></div>
                        <div class="col-md-3"><?= $this->Form->control('port', ['empty' => true, 'type' => 'select', 'options' => $listTypeNumeros, 'required', 'escape' => false, 'label' => 'Port libre par type<span class=""></span>', 'placeholder' => '', 'class' => 'form-control', 'id' => 'selectports']); ?></div>
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-primary exec" style="position:relative;margin-top:31px">Exécuter</button>
                            <button type="reset" class="btn btn-primary" style="position:relative;margin-top:31px"><span class="fa fa-refresh"></span></button>
                            <button class="btn btn-primary unlock-code" style="position:relative;margin-top:31px"><span class="fa fa-unlock"></span></button>
                        </div>
                    </div>

                    <div class="clearfix mt-4">
                        <div class="ussd-response text-white">
                            <!-- JS XHR -->
                        </div>
                    </div>

                    <div class="clearfix mt-4">
                        <?php foreach ($listTypeNumeros as $type => $lists): ?>
                            <div class="clearfix mb-4">
                                <div class="text-white"><?= strtoupper($type) ?> :</div>
                                <?php foreach ($lists as $key => $num): ?>
                                    <div class="ml-4"><?= $num ?></div>
                                <?php endforeach ?>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <span class="text-white">Memoire code</span> : <br> 

                    Envoi Mvola : #111*1*2*numero*montant*raison*2*raison# <br>
                    Envoi Orange : #144*1*1*numero*numero*montant*2# <br>
                    Envoi Airtel Money :  *436*2# 1 1 numero numero montant ref <br>
                <?= $this->form->end() ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>