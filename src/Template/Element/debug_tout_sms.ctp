<!-- Modal -->
<div class="modal fade" id="debugall-sms" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-title">
                    <!-- JS -->
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <!-- code -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-class" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>