<div class="modal fade" id="sendsmsfromport" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?= $this->Form->create(false, ['url' => ['action' => 'sendsmsfromOrange'], 'class' => 'form-sendsms', 'type' => 'file']); ?>
                    <div class="modal-header">
                        <h5 class="modal-title" id="label-title">SMS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md">
                               <?= $this->Form->control('numero', ['id' => 'num_orange_msg', 'data-num' => '0321490606'/*'0327764453'*/ /*0321490606*/, 'label' => 'Numéro destinataire (GSM)']); ?>
                            </div>
                            <div class="col-md">
                               <?= $this->Form->control('numero_paiement', ['value' => '0325541393' /*0321490606*/, 'label' => 'Numéro paiement (Site)']); ?>
                            </div>
                            <div class="col-md">
                               <?= $this->Form->control('montant_recu', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                            </div>
                        </div>

                        <div class="res-sms">
                            <!-- JS -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary text-white button-send btn-sm" >Envoyer</button>
                    </div>
            <?= $this->form->end() ?>
            <?= $this->Form->create(false, ['url' => ['action' => 'sendCustomSmsfromOrange'], 'class' => 'form-sendsms formsms-full mt-4', 'type' => 'file']); ?>
                <div class="modal-body">
                    <div class="clearfix">
                        <span class="float-right com">
                            <!-- COM -->
                        </span>
                    </div>
                    <div class="alert alert-sendsms">
                        <!-- JS -->
                    </div>
                    <div class="row">
                        <div class="col-md">
                           <?= $this->Form->control('numero', ['label' => 'Numéro destinataire (GSM)', 'type' => 'number']); ?>
                           <?= $this->Form->control('is_flashmode', ['label' => 'Si mode flash', 'type' => 'checkbox']); ?>
                        </div>
                    </div>
                   <?= $this->Form->control('sms', ['class' => 'form-control msg-tosend', 'maxlength' => 160, 'type' => 'textarea', 'required']); ?>
                   <span id="counterWord">0 / 160 mots</span>
                   <div class="mt-4 res-sms-send text-white">
                       <!-- JS -->
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary text-white button-send btn-sm" >Envoyer</button>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>
</div>