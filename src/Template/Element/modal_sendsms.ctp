<div class="modal fade" id="sendsms" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <?= $this->Form->create(false, ['url' => ['controller' => 'SmsRecus', 'action' => 'sendSms'], 'class' => 'form-sendsms', 'type' => 'file']); ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label-title">SMS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                               <?= $this->Form->control('numero', ['value' => '0347085930']); ?>
                            </div>
                            <div class="col-md-3">
                               <?= $this->Form->control('raison', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                            </div>
                            <div class="col-md-3">
                               <?= $this->Form->control('montant_recu', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                            </div>
                            <div class="col-md-3">
                               <?= $this->Form->control('solde_actuel', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary text-white button-send" >Envoyer</button>
                </div>
            </div>
        <?= $this->form->end() ?>
    </div>
</div>