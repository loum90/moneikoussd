<div class="modal fade" id="histo_callback" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <?= $this->Form->create(false, ['class' => 'form-ussd', 'type' => 'POST']); ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="titreussd">Historique Callback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>  
                <div class="modal-body text-white">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        <?= $this->form->end() ?>
    </div>
</div>