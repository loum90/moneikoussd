<div class="modal fade" id="command-ussd" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="titreussd"><span class="mobile"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>  
            <div class="modal-body">
                <?= $this->Form->create(false, ['class' => 'form-ussd', 'type' => 'POST']); ?>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="code-orange d-none">
                                <!-- <div class="mb-2">#144# 1 1 numero numero montant 2 2958</div> -->
                                <div class="clearfix mb-2">
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144*4*2*1*1*3000000# 2958"]) ?>">3M BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144*4*2*1*1*2000000# 2958"]) ?>">2M BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144*4*2*1*1*1000000# 2958"]) ?>">1 BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144*4*2*1*1*500000# 2958"]) ?>">500K BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 4 2 1 4 2958"]) ?>">Relevé BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 4 2 1 3 2958"]) ?>">Solde BOA</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144#"]) ?>">#144#</a>
                                    <!-- <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 5 2 2958"]) ?>">Dern TX</a> -->
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 5*2*2958 1"]) ?>">Dern2 TX</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 5*2*2958 1 1"]) ?>">Dern3 TX</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#888#"]) ?>">#888#</a>
                                    <?php $numOrangeRecept = $numReceptions['orange']->numero ?>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#123# 3 1 $numOrangeRecept"]) ?>">Rappel <?= $numOrangeRecept ?></a>
                                    <!-- <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '032', 'code' => "#144# 5 6 1 100000"]) ?>">T-1</a> -->
                                </div>
                            </div>

                            <div class="code-mvola d-none">
                                <div class="clearfix mb-2">
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*1# #*6*2 2958"]) ?>">Dern TX</a>
                                    <!-- <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111# 1 2 1 2958"]) ?>">Solde Mvola</a> -->
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*1# #*6*1 2958"]) ?>">Solde Mvola</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*1*6*13#"]) ?>">Annulation</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*1*6*13# 1 2958 2958"]) ?>">Annulation (conf)</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*1#"]) ?>">#111*1#</a>
                                    <?php $numTelmaRecept = $numReceptions['telma']->numero ?>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "*120#"]) ?>">*120#</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111*2*1*$numTelmaRecept#"]) ?>">Rappel <?= $numTelmaRecept ?></a>

                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '034', 'code' => "#111# 1*2*0321490606*5000000*ecolage 2958"]) ?>"><span class="text-warning">M&rarr;O</span></a>
                                </div>
                            </div>

                            <div class="code-airtel d-none">
                                <div class="clearfix mb-2">
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '033', 'code' => "*436*6*1# 2958"]) ?>">Solde par SMS</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '033', 'code' => "*436# 6 5 1 2958"]) ?>">5 Dern TX</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '033', 'code' => "*436*6#"]) ?>">*436#</a>
                                    <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'presetUssd', '033', 'code' => "*123#"]) ?>">*123#</a>

                                </div>
                            </div>

                            <div class="code-js-response text-white">
                                <!-- js -->
                            </div>
                        </div>

                        <div class="col-md-6">
                            <a data-titre="Sms" data-href="<?= $this->Url->build(['action' => 'debugSmsModem', '033']) ?>"  data-toggle="modal" data-target="#debugall-sms" class="btn btn-primary btn-sm lire-sms"> <span class="fa fa-exclamation-circle text-warning"></span> LIRE SMS</a> 
                            <a href="<?= $this->Url->build(['action' => 'checkStateDiffNum', '032']) ?>"  class="code-orange d-none btn btn-primary btn-sm check_state_diff_num">État DIFF Num Orange <span class="fa fa-check-circle"></span></a> 
                            <a href="<?= $this->Url->build(['action' => 'checkStateDiffNum', '034']) ?>"  class="code-mvola d-none btn btn-primary btn-sm check_state_diff_num">État DIFF Num Telma <span class="fa fa-check-circle"></span></a> 

                            <div class="mt-2 code-orange d-none">
                                Simple : <span class="text-white">0325541393, 0321490606, 0327232084, 0329965466, 0329965433</span> <br>
                                C.P : <span class="text-white">MCM : 0321521759, B2W : 0321521758</span>
                            </div>

                            <div class="mt-2 code-airtel d-none">
                                Simple : <span class="text-white">0337590891, 0338524425, 0334093330</span> <br>
                            </div>

                        </div>

                        <div class="col-md-6 ">
                            <div class="code-mvola d-none">
                                <a class="btn btn-primary btn-sm code" href="<?= $this->Url->build(['action' => 'majLastSoldeMvola']) ?>">
                                    Màj dern solde Mvola
                                </a>
                                <div class="container-solde-mvola">
                                    <div class="solde-mvola">
                                        <span class="float-right mt-1 text-white"><?= $this->Number->format($params->last_solde_mvola) ?> Ar</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"><?= $this->Form->control('numero', ['escape' => false, 'label' => 'Numero', 'placeholder' => '', 'class' => 'form-control mb-2']); ?></div>
                        <div class="col-md-6"><?= $this->Form->control('montant', ['escape' => false, 'label' => 'Montant', 'placeholder' => '', 'class' => 'form-control mb-2']); ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"><?= $this->Form->control('command', ['required', 'escape' => false, 'label' => 'Saisir la commande <span class="loading"></span>', 'placeholder' => '', 'class' => 'form-control', 'id' => 'at-com']); ?></div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary exec" style="position:relative;margin-top:31px">Exécuter</button>
                            <button type="reset" class="btn btn-primary" style="position:relative;margin-top:31px"><span class="fa fa-refresh"></span></button>
                            <button type="button" class="btn btn-primary paste" id="paste-button" style="position:relative;margin-top:31px"><span class="fa fa-paste"></span></button>
                            <button class="btn btn-primary unlock-code" style="position:relative;margin-top:31px"><span class="fa fa-unlock"></span></button>
                        </div>
                    </div>

                    <div class="clearfix mt-4">
                        <div class="ussd-response text-white">
                            <!-- JS XHR -->
                        </div>
                    </div>
                <?= $this->form->end() ?>

                <?= $this->Form->create(false, ['url' => ['action' => 'transferMvolaToOrange'], 'class' => 'transfert', 'type' => 'POST']); ?>

                    <div class="text-white mb-4">Transfert M-OM</div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $this->Form->control('montant_transfert', ['escape' => false, 'default' => '5000000', 'label' => 'Montant', 'placeholder' => '', 'class' => 'form-control mb-2']); ?></div>
                                <div class="col-md-6"><?= $this->Form->control('numero', ['empty' => true, 'required', 'escape' => false, 'label' => 'Numero', 'options' => $numOranges, 'placeholder' => '', 'class' => 'form-control mb-2']); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary exec-transfert" style="position:relative;margin-top:31px">Exécuter</button>
                        </div>
                    </div>

                    <div class="clearfix result-transfert text-white">
                        <!-- JS -->
                    </div>
                     
                <?= $this->form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>