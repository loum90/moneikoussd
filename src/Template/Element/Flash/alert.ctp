<div class="clearfix mt-2">
    <div class="<?= @$params['class'] ?> alert-dismissible no-radius clear-mg ">
        <?php if (@$params['class'] == 'alert alert-success'): ?>
            <span class="glyphicon mark-alert glyphicon-ok text-success" aria-hidden="true"></span>
        <?php elseif (@$params['class'] == 'alert alert-danger'): ?>
            <span class="glyphicon mark-alert glyphicon-exclamation-sign text-danger" aria-hidden="true"></span>
        <?php endif; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="to-xs-size "><?= @$message; ?></span>
    </div>
</div>


<style>
    .mark-alert {
        position: relative;
        border-radius: 50%;
        background-color: #FFF;
        font-size: 11px;
        width: 20px;
        height: 20px;
        top: 0px;
        text-align: center;
        line-height: 20px;
    }
</style>