<div class="modal fade" id="maj_solde_usd" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <?= $this->Form->create(null, ['class' => 'form-majsolde-usd', 'type' => 'post']); ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="titreussd">Màj solde <span class="mobile"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div> 

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->control('montant', ['type' => 'number', 'step' => '0.01', 'label' => 'Montant affiché sur site']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('reserve', ['type' => 'number', 'step' => '0.01', 'class' => 'form-control reserve-usd', 'label' => 'Réserve <span class="reserve-initial text-white"></span> <span class="reserve-statut"></span>', 'escape' => false]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <?= $this->Form->control('reserve', ['type' => 'checkbox', 'class' => 'simulated-solde', 'default' => true, 'label' => 'Si simulé', 'escape' => false]); ?>
                        </div>
                        <div class="col-8">
                            <button type="button" class="btn btn-primary btn-sm majinputsoldeDown float-right" data-down="10">- 10</button>
                            <button type="button" class="btn btn-primary btn-sm majinputsoldeUp float-right mr-2" data-up="10">+ 10</button>
                            <button type="button" class="btn btn-primary btn-sm resetSolde float-right mr-2">Réinit</button>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-outline-secondary text-white btn-submit-maj-solde">Soumettre <span class="fa fa-check text-success statemajed d-none"></span></button>
                </div>
            </div>
        <?= $this->form->end() ?>
    </div>
</div>