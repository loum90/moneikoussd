<div class="modal fade" id="config" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <?= $this->Form->create($parameter, ['class' => 'majparam', 'url' => ['controller' => 'Tdb', 'action' => 'editParameter'], 'templates' => 'app_form']); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="label-title">Paramètres</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">

                    <div class="clearfix">
                        <a href="javascript:void(0);" class="majprocess-mappage float-right">Màj général</a>
                    </div>
                    <div class="process-progressbar clearfix mb-4 d-none">
                        <div class="mb-4">
                            Chargement du process <span class="progress-label text-white">0 % <span class="text-secondary step-process"></span></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-animated bg-secondary text-white" role="progressbar" aria-valuenow="0" style="width: 0%" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>


                    <div class="port-list text-white">
                        <!-- JS -->
                        <!-- port_list.ctp -->
                    </div>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('is_recharge_auto_forfait_telma'); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_recharge_auto_forfait_orange'); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_recharge_auto_forfait_airtel'); ?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('hook_serveur', ['type' => 'select', 'options' => ['https://moneiko.net' => 'https://moneiko.net', 'http://127.0.0.1/moneyko' => 'http://127.0.0.1/moneyko'], 'label' => 'Hook sms d\'envoi de paiement : http://127.0.0.1/moneyko']); ?></div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-4">
                            <?= $this->Form->control('python_path', ['escape' => false, 'label' => '<span data-toggle="tooltip" data-placement="bottom" title="C:\Users\Loum\AppData\Local\Programs\Python\Python38">Python Path</span>']); ?>                  
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->control('cin_path'); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->control('timeout_auto_recla'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('is_webhook_sms_paiement'); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_notifications_push_active'); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_notifications_push_for_failed_paiement_active'); ?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('msg_merci', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_2', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_3', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_4', ['type' => 'text']); ?></div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('is_auto_sms_merci_depot_send_activated', ['type' => 'checkbox', 'label' => 'Auto sms remerciement depôt']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_auto_sms_merci_retrait_send_activated', ['type' => 'checkbox', 'label' => 'Auto sms remerciement retrait']); ?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_5', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_6', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_7', ['type' => 'text']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('msg_merci_8', ['type' => 'text']); ?></div>
                    </div>


                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('is_activated_auto_switch_solde_mvola', ['type' => 'checkbox', 'label' => 'Si auto switch Mvola activé']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_activated_auto_switch_solde_orange', ['type' => 'checkbox', 'label' => 'Si auto switch Orange activé']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('is_activated_auto_switch_solde_airtel', ['type' => 'checkbox', 'label' => 'Si auto switch Airtel activé']); ?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $this->Form->control('is_auto_recla_general', ['type' => 'checkbox']); ?>
                            <?= $this->Form->control('time_interval_auto_recla_general', ['label' => 'Interval min auto recla']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->control('is_auto_recla_erreur_reseau', ['type' => 'checkbox']); ?>
                            <?= $this->Form->control('time_interval_auto_recla_erreur_reseau', ['label' => 'Interval min auto réseau']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->control('is_reserve', ['type' => 'checkbox']); ?>
                            <?= $this->Form->control('montant_max_auto_recla'); ?>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-4">
                            <?= $this->Form->control('is_opcache_reseted', ['type' => 'checkbox']); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"><?= $this->Form->control('ratio_maj', ['type' => 'number', 'label' => 'Seuil en % auto maj']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('seuil_mga_maj', ['type' => 'number', 'label' => 'Seuil min MGA auto maj']); ?></div>
                        <div class="col-md-4"><?= $this->Form->control('pushkey', ['label' => 'Push Api Key']); ?></div>
                    </div>

                    <a href="https://api.myip.com" target="_blank">https://api.myip.com</a>
                    <div class="clearfix errs-edit-param text-danger">
                        <!--  -->
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-dark text-white param-save">Enregistrer</button>
                    <button type="button" class="btn" data-dismiss="modal">Fermer</button>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>
</div>