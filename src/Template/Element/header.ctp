<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#" data-toggle="modal" data-target="#config">
            <img src="<?= $this->Url->build('/img/logo.png') ?>" width="30px" class="align-middle" alt=""> <span class="d-inline-block align-middle">Moneiko <?= $title = @$queryParams['type'] ? ' - '.($queryParams['type']) : '' ?></span> 
            <span data-toggle="tooltip" data-placement="bottom" title="Statut OZ" class="sm-size d-inline-block fa fa-circle <?= $statutOzeki == 'True' ? 'serversuccess' : 'text-danger' ?>"></span>
            <span data-toggle="tooltip" data-placement="bottom" title="Statut ServMon" class="sm-size d-inline-block fa fa-circle <?= $statutServMon == 'True' ? 'serversuccess' : 'text-danger' ?>"></span>
            <span data-toggle="tooltip" data-placement="bottom" title="Statut Ngrok" class="sm-size d-inline-block statut_ngrok fa fa-circle text-secondary"></span>
            <span data-toggle="tooltip" data-placement="bottom" title="Statut check ngrok depuis Prod" class="sm-size d-inline-block statut_ngrok_from_prod fa fa-circle text-secondary"></span>
            <span class="lg-size d-inline-block timer-bloc"></span>
        </a>
        <!-- <a class="navbar-brand" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'users']) ?>" >
            <span class="d-inline-block align-middle">Membres </span> 
        </a> -->
        <a class="navbar-brand" target="_blank" href="<?= $this->Url->build(['controller' => 'Onex', 'action' => 'index']) ?>" >
            <span class="d-inline-block align-middle">1x </span> 
        </a>

    </div>
</nav>

<?= $this->element('config') ?>