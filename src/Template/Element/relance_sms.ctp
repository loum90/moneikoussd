<div class="modal fade" id="relance-sms" tabindex="-1" role="dialog" aria-labelledby="titreussd" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="titreussd">Relance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>  
            <div class="modal-body">
                <a data-titre="Relance SMS" class="btn btn-primary btn-sm simpleload" href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'getRelancesFromProd']) ?>" type="button">
                    Importer depuis Prod
                </a> 
                <?= $this->Form->create(false, ['class' => 'form-relance-sms mt-4', 'type' => 'POST']); ?>
                    <?= $this->Form->control('msg_relance', ['default' => $params->msg_relance, 'type' => 'textarea', 'maxlength' => '160', 'class' => 'form-control msg-tosend msg-relance']); ?>
                    <div class="clearfix">
                        <span id="counterWord"><?= strlen($params->msg_relance) ?> / 160 mots</span>
                    </div>
                    <div class="clearfix mt-4">
                        <span class="text-white">cake console sendRelanceSms stat</span> (Connaître les stats) <br>
                        <span class="text-white">cake console sendRelanceSms telma</span> (Envoi uniquement telma ou orange ou airtel) <br>
                        <span class="text-white">cake console sendRelanceSms</span> (Envoi tout sans filtre) <br><br>
                        <a href="file:///D:/wamp/www/moneiko-ussd/bin/relance_sms.bat">Relance.bat</a>
                    </div>
                <?= $this->form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>