<?php if (strlen($user->cin_recto) > 0 && $isCinFileExists): ?>
    <div class="clearfix relative">
        <a href="javascript:void(0);" class="rotate"><span class="fa fa-rotate-right"></span></a>
        <img src="<?= $this->Url->build('/'.$user->get('CinRectoPath')) ?>" class="img-viewuser" width="100%" alt="">
    </div>
<?php elseif (strlen($user->cin_recto) > 0 && !$isCinFileExists): ?>
    <span class="text-warning">Fichier sur serveur en ligne mais inexistant sur le local</span>
<?php else: ?>
    <span class="text-danger">Fichier non existant sur le serveur</span>
<?php endif ?>