<?php $type = @$options['type'] ?>

<a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'test']) ?>" class="text-secondary testrec">1</a>
<a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'test']) ?>" class="text-secondary testrec">2</a>
<a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'test']) ?>" class="text-secondary testrec">3</a>
<a href="<?= $this->Url->build(['controller' => 'apis', 'action' => 'test']) ?>" class="text-secondary testrec">4</a>


<div class="row-fluid to-mg-top clearfix">

    <?= $this->Form->create(false, ['class' => 'mt-4', 'type' => 'get']); ?>

            <div class="row">
                <div class="col-md-2 mb-2"><?= $this->Form->control('user_id', ['default' => @$options['user_id'], 'label' => false, 'class' => 'form-control', 'placeholder' => 'ID Membre', 'type' => 'text']); ?></div>
                <div class="col-md-2 mb-2"><?= $this->Form->control('nom_complet', ['default' => @$options['nom_complet'], 'label' => false, 'class' => 'form-control', 'placeholder' => 'Nom complet']); ?></div>
                <div class="col-md-6 mb-2">
                    <button class="btn btn-lg btn-primary">Filtrer</button>
                    <a class="btn btn-lg btn-primary" href="<?= $this->Url->build(['action' => 'users', false]) ?>">Réinitialiser</a>
                </div>
            </div>
    <?= $this->form->end() ?>

    <div class="clearfix mb-4">
        Total : <span class="text-white"><?= $count = $this->Paginator->params()['count']; ?></span> <br>
        Nb Cin en prod et en local : <span class="text-white"><?= $nbUsersCinEnLigneMsEtPresentLocal ?> (<?= (round(($nbUsersCinEnLigneMsEtPresentLocal/$count)*100, 2)) ?>%)</span> <br>
        Nb Cin en prod et non en local : <span class="text-white"><?= $nbUsersCinEnLigneMsInexistantLocal ?> (<?= (round(($nbUsersCinEnLigneMsInexistantLocal/$count)*100, 2)) ?>%)</span> <br>
        Nb Cin absent prod et absent local : <span class="text-white"><?= $nbUsersCinInexistantEnLigneEtInexistantLocal ?> (<?= (round(($nbUsersCinInexistantEnLigneEtInexistantLocal/$count)*100, 2)) ?>%)</span> <br>
    </div>

    <div class="row align-middle">
        <div class="col-md-6"><?= $this->element('pagination/pagination') ?></div>
        <div class="col-md-6">
            <a data-href="<?= $this->Url->build(['action' => 'majNomUsers']) ?>" href="javascript:;" class="btn btn-primary float-right majnoms">Mettre à jour</a>
            <a data-href="<?= $this->Url->build(['action' => 'importUsers']) ?>" href="javascript:;" class="btn btn-primary float-right majnoms mr-2">Téleverser</a>
        </div>
    </div>

    <div class="clearfix">
        <div class="alert alert-maj " role="alert">
          
        </div>
    </div>

    <?php if (!$users->isEmpty()): ?>
        <div class="clearfix table-responsive mt-4 tableau-users">
            <table class="table table-bordered table-hover table-users ">
                <thead>
                    <tr>
                        <th width="22%">Nom complet</th>
                        <th width="15%">Fichier</th>
                        <th width="20%">Etat</th>
                        <th width="10%">ID</th>
                        <th width="12%">Inscrit le</th>
                        <!-- <th width="15%"></th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $key => $user): ?>
                        <tr>
                            <td><a class="<?= $user->is_modified_local ? 'text-success' : '' ?> viewuser" data-userid="<?= $user->id ?>" data-nom_complet="<?= $user->nom_complet ?>" data-href="<?= $this->Url->build(['action' => 'viewUser', $user->id]) ?>"  href="javascript:;"><?= $user->nom_complet ?></a> </td>
                            <td>
                                <?php if ($user->statut == 1): ?>
                                    <span class="text-white fa fa-file-o"></span> 
                                    <a class="viewuser" data-userid="<?= $user->id ?>" data-nom_complet="<?= $user->nom_complet ?>" data-href="<?= $this->Url->build(['action' => 'viewUser', $user->id]) ?>" href="javascript:;">À modifier</a>
                                <?php elseif($user->statut == 2): ?>
                                    <span class="text-warning fa fa-file-o"></span>
                                    <span class="text-secondary">En ligne, inexistant local</span>
                                <?php elseif($user->statut == 3): ?>
                                    <span class="text-danger fa fa-file-o"></span>
                                <?php endif ?>
                            </td>
                            <td>
                                <span data-toggle="tooltip" data-placement="bottom" title="<?= @$raisons[$user->raison_kyc_echec] ?>" class="text-<?= !empty($user->raison_kyc_echec) ? 'warning' : '' ?>"><?= $user->get('StatutProgression') ?></span>
                                <span class="float-right">
                                    <?php if ($user->is_quotas_reached): ?>
                                        <span class="fa fa-warning text-warning" data-toggle="tooltip" data-placement="bottom" title="Quota d'achat atteint"></span>
                                    <?php endif ?>
                                </span>
                            </td>
                            <td><?= $user->id ?></td>
                            <td><?= $user->created->format('d/m/Y H:i:s') ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        Aucun résultat
    <?php endif ?>
</div>

<?= $this->element('pagination/pagination') ?>


