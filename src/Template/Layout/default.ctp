<!DOCTYPE html>
<html lang="fr">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?? 'Accueil' ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?php echo $this->Html->css('bootstrap.min?t=8') ?>
    <!-- <?php $this->Html->css('bootstrap.min?t='.time()) ?> -->

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body data-url="<?= $this->Url->build('/') ?>" data-islocal="<?= $isLocal ?>" data-ip="<?= $this->request->getEnv('REMOTE_ADDR') ?>">
    <?= $this->element('header') ?>

    <input type="hidden" class="params" data-action="<?= $action ?>" data-controller="<?= $controller ?>">

    <div class="container py-4">
        <?= $this->Flash->render('default') ?>
        <?= $this->fetch('content') ?>
    </div>
    <?= $this->Html->script('jquery.min.js?time=6') ?>
    <?= $this->Html->script('moneiko.js?time='.time()) ?>
    <?= $this->Html->script('onex.js?time='.time()); ?>
</body>
</html>