<?php 
    use Cake\I18n\FrozenTime;
    use \Cake\Chronos\Chronos;
    // debug($this->App->formatMontant(4500));
?>

<a class="btn btn-sm mb-3" href="<?= $this->Url->build(['controller' => 'tdb']) ?>"><span class="fa fa-home"></span></a><span class="result-solde"></span>
<a href="javascript:void(0);" class="float-right reloadsms d-none d-sm-block"><span class="fa fa-spinner text-secondary"></span></a>

<div class="clearfix mt-4 d-none d-sm-block"><?= $this->element('pagination/pagination') ?></div>

<div class="container-sms">
    <div class="sms-block">
        <?php foreach ($smss as $key => $sms): ?>

            <div class="row-sms clearfix">
                <div class="clearfix">
                    <span class="float-left mb-2">
                        De <?= $sms->sender ?>, vers <?= $sms->receiver ?>
                    </span>
                    <span class="float-right mb-2">
                        <?php if (Chronos::parse($sms->senttime)->wasWithinLast('1 hour')): ?>
                            <?= $this->Time->timeAgoInWords($sms->senttime); ?>
                        <?php else: ?>
                            <?= Chronos::parse($sms->senttime)->format('d/m, H\\hi'); ?>
                        <?php endif ?>
                    </span>
                </div>
                <div class="text-white" data-url="<?= $this->Url->build(['action' => 'editSms', $sms->id])?>" data-id="<?= $sms->id ?>"><?= $sms->msg ?></div>
                <div class="clearfix actions">

                </div>
                <div class="clearfix">
                    <hr class="light">
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="clearfix mt-4"><?= $this->element('pagination/pagination') ?></div>
