<?php 
    use Cake\I18n\FrozenTime 
?>

<?php if (@$result): ?>
    <table class="table table-bordered text-white">
        <thead>
            <tr>
                <th>Libellé</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Type</td>
                <td><?= @$result['type']?></td>
            </tr>
            <tr>
                <td>Montant</td>
                <td>
                    <?= @$result['montant']?> Ariary
                </td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    <?php if (@$result['status'] == 4): ?>
                        <span class="badge badge-success">Succès</span>
                    <?php else: ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td>Txnid</td>
                <td><?= @$result['txnid']?></td>
            </tr>
            <tr>
                <td>Date</td>
                <td><?= FrozenTime::parse(@$result['created'])->nice(); ?></td>
            </tr>
        </tbody>
    </table>

    Lien de paiement : <a target="_blank" href="<?= $this->Url->build($url) ?>"><?= $url ?></a>
<?php else: ?>
    Aucun résultat trouvé
<?php endif ?>