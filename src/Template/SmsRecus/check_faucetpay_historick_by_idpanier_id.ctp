<table class="table table-hover">
    <thead>
        <tr>
            <th>Montant</th>
            <th>Compte</th>
            <th>Asset</th>
            <th>Reference</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $key => $result): ?>
            <tr>
                <td><?= $result['usd'] ?></td>
                <td><?= $result['compte'] ?></td>
                <td><?= $result['asset'] ?></td>
                <td><?= $result['ref'] ?></td>
                <td><?= $result['date'] ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>