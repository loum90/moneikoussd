<?php 
    use Cake\I18n\FrozenTime;
    use \Cake\Chronos\Chronos;
    // debug($this->App->formatMontant(4500));
?>

<div class="param-url" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>"></div>
<div class="type" data-type="<?= $type ?>"></div>

<?php $this->start('listeAnnulations') ?>
    <?php foreach ($annulations as $key => $annulation): ?>
        <div class='text-left'>
            <p><?= $annulation->senttime; ?> <br> <?= $annulation->sender ?></p>
            <p><?= $annulation->msg ?></p>
        </div>
    <?php endforeach ?>
<?php $this->end() ?>

<div class="clearfix">
    <a class="btn btn-sm" href="<?= $this->Url->build(['controller' => 'tdb']) ?>"><span class="fa fa-home"></span></a><span class="result-solde"></span>
    <?php if ($type == 'mvola'): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="MAJ dernier Solde Mvola" class="checksolde btn btn-sm" href="<?= $this->Url->build(['action' => 'ajusterSolde', "034"]) ?>"><span class="fa fa-refresh"></span></a><span class="result-solde"></span>
    <?php elseif(in_array($type, ['orange'])): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="MAJ dernier Solde Orange" class="checksolde btn btn-sm" href="<?= $this->Url->build(['action' => 'ajusterSolde', "032"]) ?>"><span class="fa fa-refresh"></span></a><span class="result-solde"></span>
    <?php elseif(in_array($type, ['airtel'])): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="MAJ dernier Solde Airtel" class="checksolde btn btn-sm" href="<?= $this->Url->build(['action' => 'ajusterSolde', "033"]) ?>"><span class="fa fa-refresh"></span></a><span class="result-solde"></span>
    <?php endif ?>
    
    <a data-toggle="tooltip" data-placement="bottom" title="Réinitialise GSM SMS Telma" href="<?= $this->Url->build(['controller' => 'tdb', 'action' => 'resetUsb', $driverTelmaSms]) ?>" class="btn reset-usb btn-sm">
        <span class="fa fa-refresh text-success"></span>
    </a> 
    <a data-toggle="tooltip" data-placement="bottom" title="Nettoyage paiement effectué" href="<?= $this->Url->build(['controller' => 'tdb', 'action' => 'cleanPayment']) ?>" class="btn clean-payment btn-sm">
        <span class="fa fa-recycle"></span>
    </a>
    <!-- <a class="btn btn-sm" href="javascript:void(0);" data-toggle="modal" data-target="#sendsms"><span class="fa fa-envelope-open-o"></span></a> -->
    <?php if (in_array($type, ['orange'])): ?>
        <a class="btn btn-sm d-none d-sm-inline-block" href="javascript:void(0);" data-toggle="modal" data-target="#sendsmsfromport"><span class="fa fa-envelope-open-o"></span></a>
    <?php elseif($type == 'airtel'): ?>
        <a class="btn btn-sm d-none d-sm-inline-block" href="javascript:void(0);" data-toggle="modal" data-target="#sendsmsfromportAirtel"><span class="fa fa-envelope-open-o"></span></a>
    <?php endif ?>
    <!-- <a class="btn btn-sm" href="<?= $this->Url->build(['action' => 'autresSms']) ?>"><span class="fa fa-envelope-open-o text-white"></span></a> -->
    <span class="container-reload">
        <span class="block-reload">
            <!-- <a class="btn btn-sm">sms : <?= $nbTotalSms ?></a> -->
            <?php if (in_array($type, ['mvola'])): ?>
                <?php if (@$nbNonRelgesMvola): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesMvola > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesMvola > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesMvola ?></a>
                <?php endif ?>
            <?php endif ?>

            <?php if (in_array($type, ['airtel'])): ?>
                <?php if (@$nbNonRelgesAirtel): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesAirtel > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesAirtel > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesAirtel ?></a>
                <?php endif ?>
            <?php endif ?>

            <?php if (in_array($type, ['orange'])): ?>
                <?php if (@$nbNonRelgesOrange): ?>
                    <a href="<?= $this->Url->build(['type' => $type, 'failed' => true]) ?>" class="btn btn-sm text-<?= $nbNonRelgesOrange > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="Nombre de transaction non réglé"><span class="fa fa-exclamation-triangle text-<?= $nbNonRelgesOrange > 0 ? 'danger' : 'success' ?>"></span> &nbsp; <?= $nbNonRelgesOrange ?></a>
                <?php endif ?>
            <?php endif ?>


            <?php if (@$nbDemandeAnnulation): ?>
                <?php if (in_array($type, ['mvola', 'airtel'])): ?>
                    <a class="btn btn-sm text-<?= $nbDemandeAnnulation > 0 ? 'danger' : 'success' ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $this->fetch('listeAnnulations') ?>"><span class="fa fa-exclamation-triangle text-warning"></span> &nbsp;<?= $nbDemandeAnnulation ?></a>
                <?php endif ?>
            <?php endif ?>
        </span>
    </span>
    <a data-toggle="tooltip" data-placement="bottom" title="Auto Maj solde et mise à jour Numéro" class="btn btn-sm text-secondary d-none d-sm-inline-block automajsolde" href="<?= $this->Url->build(['action' => 'autoMajNumEtSolde']) ?>" ><span class="fa fa-circle-o-notch"></span><span class=""></span></a>
    <a class="btn btn-sm text-white" href="javascript:void(0);" data-toggle="modal" data-target="#reclamation" href="javascript:void(0);"><span class="fa fa-exclamation-circle text-white"></span></a>
    <a class="btn btn-sm text-white" href="javascript:void(0);" data-type="<?= $type ?>" data-toggle="modal" data-target="#search" href="javascript:void(0);"><span class="fa fa-search text-white"></span></a>
    <a class="btn btn-sm text-white" href="javascript:void(0);" data-toggle="modal" data-target="#searchsms" href="javascript:void(0);"><span class="fa fa-search text-info"></span></a>
    <?php if ($type == 'mvola'): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'mvola2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Mvola Serveur" class="btn btn-sm text-white disable-mvola" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotMvola']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '034']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php elseif ($type == "orange"): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'orange2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Orange Serveur" class="btn btn-sm text-white disable-orange" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotOrange']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '032']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php elseif ($type == "airtel"): ?>
        <a data-toggle="tooltip" data-placement="bottom" title="Check Nombre de réservation" class="btn btn-sm text-white check-paniers" href="<?= $this->Url->build(['action' => 'getPendingTx', 'airtel2']) ?>" ><span class="fa fa-shopping-cart"></span><span class="nbpendingtx"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Désactiver paiement Airtel Serveur" class="btn btn-sm text-white disable-orange" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'playPauseDepotAirtel']) ?>" ><span class="fa fa-pause alternate"></span><span class="checkstatemvola"></span></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Numéro retrait en ligne, switch numero" class="btn btn-sm switchNumero" data-type="<?= $type ?>" data-param="<?= $this->Url->build(['action' => 'getParams']) ?>" href="<?= $this->Url->build(['action' => 'switchNumero', '033']) ?>" ><span class="fa fa-rotate-right"></span><span class="checkstatemvola"></span> <span class="lastMvolaNumero onlineretraitnum"></span></a>
    <?php endif ?>
    <a class="btn btn-sm text-white btn-checklocalconnexion" data-toggle="tooltip" data-placement="bottom" title="Etat check local connexion" href="<?= $this->Url->build(['action' => 'updateCheckLocalConnexion']) ?>" data-toggle="modal" data-target="#searchsms" href="javascript:void(0);"><span class="fa fa-link check-autolocalconnexion"></span></a>
    
    <!-- Modal Orange -->
    <?= $this->element('modal_sendsms') ?>
    
    <!-- Modal -->
    <?= $this->element('modal_sendsms_fromport') ?>


    <!-- Modal -->
    <div class="modal fade" id="sendsmsfromportAirtel" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <?= $this->Form->create(false, ['url' => ['action' => 'sendsmsfromAirtel'], 'class' => 'form-sendsms', 'type' => 'file']); ?>
                        <div class="modal-header">
                            <h5 class="modal-title" id="label-title">SMS Airtel</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md">
                                   <?= $this->Form->control('numero', ['id' => 'num_airtel_msg', 'data-num' => '+261337590891'/*'0327764453'*/ /*0321490606*/, 'label' => 'N° destinataire (code site)']); ?>
                                </div>
                                <div class="col-md">
                                   <?= $this->Form->control('numero_paiement', ['value' => '334093330' /*0321490606*/, 'label' => 'N° Client paiement (Site)']); ?>
                                </div>
                                <div class="col-md">
                                   <?= $this->Form->control('montant_recu', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                                </div>
                            </div>

                            <div class="res-sms">
                                <!-- JS -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary text-white button-send btn-sm" >Envoyer</button>
                        </div>
                <?= $this->form->end() ?>
                <?= $this->Form->create(false, ['url' => ['action' => 'sendCustomSmsfromOrange'], 'class' => 'form-sendsms mt-4', 'type' => 'file']); ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md">
                               <?= $this->Form->control('numero', ['label' => 'Numéro destinataire (GSM)', 'type' => 'number']); ?>
                            </div>
                        </div>
                       <?= $this->Form->control('sms', ['class' => 'form-control ', 'type' => 'textarea', 'required']); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary text-white button-send btn-sm" >Envoyer</button>
                    </div>
                <?= $this->form->end() ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="reclamation" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <?= $this->Form->create(false, ['url' => ['action' => 'verifierTransaction'], 'class' => 'form-reclamation', 'type' => 'file']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title  text-white" id="label-title">Réclamation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 col-lg-3">
                               <?= $this->Form->control('raison', ['class' => 'reset', 'type' => 'number', 'required']); ?>
                            </div>
                            <div class="col-5 col-lg-6">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="mt-2 btn btn-primary text-white button-send" >Vérifier</button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix msg-reclam">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <?= $this->Form->create(false, ['url' => ['action' => 'rechercher', 'mobile' => $type], 'class' => 'form-search', 'type' => 'GET']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title  text-white" id="label-title">Rechercher commande non payée <span class="search-type"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 col-lg">
                               <?= $this->Form->control('montant', ['type' => 'number']); ?>
                            </div>
                            <div class="col-5 col-lg">
                               <?= $this->Form->control('type', ['options' => ['btc' => 'Crypto', 'payeer' => 'Payeer', 'pm' => 'PM', 'advcash' => 'ADV'], 'empty' => 'Sélectionner']); ?>
                            </div>
                            <div class="col-5 col-lg">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Date</label>
                                   <?= $this->Form->text('date', ['type' => 'date', 'value' => @$options['date'] ? @$options['date'] : date('Y-m-d')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Heure</label>
                                   <?= $this->Form->text('heure', ['type' => 'time', 'value' => @$options['heure'] ? @$options['heure'] : date('H:i')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="mt-2 btn btn-primary text-white button-send" >Filtrer</button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix msg-reclam text-white">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="searchsms" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <?= $this->Form->create(false, ['url' => ['type' => @$options['type']], 'class' => '', 'type' => 'GET']); ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title  text-white" id="label-title">Rechercher SMS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 col-lg-3">
                               <?= $this->Form->hidden('type', ['value' => @$options['type']]); ?>
                               <?= $this->Form->control('keyword', ['label' => 'Mot clé', 'autocomplete' => 'off']); ?>
                            </div>
                            <div class="col-5 col-lg-3">
                               <div class="form-group clearfix">
                                   <label for="" class="label-control">Date</label>
                                   <?= $this->Form->text('date', ['type' => 'date', 'value' => @$options['date'] ? @$options['date'] : date('Y-m-d')]); ?>
                               </div>
                            </div>
                            <div class="col-5 col-lg-3">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="mt-2 btn btn-primary text-white button-filter" >Filtrer</button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix msg-reclam text-white">
                            <!-- AJAX -->
                        </div>
                    </div>
                </div>
            <?= $this->form->end() ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="searchpanier" tabindex="-1" role="dialog" aria-labelledby="label-title" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label-title">Détail Commande <span class="infos-type"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <!-- code -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-class text-white" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>


    <!-- <a href="#" class="float-right ml-2" data-toggle="tooltip" data-placement="bottom" title="Si paiement par hook activé"><span class="fa fa-database <?= $params->is_webhook_sms_paiement ? 'text-success' : 'text-danger' ?>"></span></a> -->
    <a href="http://127.0.0.1:9501/" class="d-none d-sm-block float-right text-secondary ml-2" target="blank"> Hook <?= $params['hook_serveur']; ?></a>
    <a href="http://127.0.0.1:9501/" class="float-right ml-2" target="blank"></a>
    <a href="javascript:void(0);" class="float-right reloadsms d-none d-sm-block"><span class="fa fa-spinner text-secondary"></span></a>
</div>


<div class="clearfix mt-4 d-none d-sm-block"><?= $this->element('pagination/pagination') ?></div>
<div class="block-sms container-sms mt-4">
    <div class="sms-block">
        <div class="clearfix">
            <?php foreach ($smss as $key => $sms): ?>
                <?php 
                    $diffMinutes = $sms->get('Diff');
                    // debug($sms); 
                ?>

                <?php $this->start('infos') ?>
                    <div class="text-left">
                        Statut : <?= $sms->paiement_status ?> <br>
                        De <?= $sms->sender ?> <br>
                        Vers <?= $sms->receiver ?> <br>
                        Mark_err_synch_as_paid <?= $sms->mark_err_synch_as_paid ? 'True' : 'False' ?> <br>
                        Marked_simple_as_paid <?= $sms->marked_simple_as_paid ? 'True' : 'False' ?> <br>
                        Mark_err_synch_as_paid <?= $sms->mark_err_synch_as_paid ? 'True' : 'False' ?> <br>
                        <?php if ($sms->paiement_status_result): ?>
                            Description : <?= str_replace('\'', " ", $sms->paiement_status_result) ?> <br>
                        <?php endif ?>
                        <?php if ($sms->date_transfert): ?>
                            Reçu le : <?= date('d/m/Y H:i:s', strtotime($sms->senttime))  ?><br>
                            Envoyé le : <?= $sms->date_transfert->format('d/m/Y H:i:s') ?>
                        <?php endif ?>
                    </div>
                <?php $this->end() ?>

                <div class="row-sms clearfix" data-diff="<?= $diffMinutes ?>" data-paiement-status="<?= $sms->paiement_status ?>">

                    <div class="clearfix">
                        <span class="float-left mb-2">
                            <span class="pointer" data-toggle="tooltip" data-html="true" data-placement="bottom" title='<?= $this->fetch('infos') ?>'>
                                ID : 
                                <span class="<?= strrpos($sms->paiement_status_result, 'non conformes') !== false ? 'text-warning' : '' ?>">
                                    <?php if (strrpos($sms->paiement_status_result, 'non conformes') !== false): ?>
                                        <span class="fa fa-warning text-danger"></span>
                                    <?php endif ?>
                                    <?= $sms->id ?>
                                </span>
                                <?= ucfirst($sms->ptf) ?>

                                <?php if ($sms->paiement_status == "error"): ?>
                                    <span class="text-danger"><?= $sms->paiement_status_result ?></span>
                                <?php endif ?>


                                <?php /*debug($sms);*/ ?>
                            </span>
                            <?php if ($sms->is_check_recla_paid_done == 1): ?>
                                <span data-toggle="tooltip" data-placement="bottom" title="Recla Failed" class="fa fa-check-circle text-info pointer"></span>
                            <?php endif ?>

                            <?php if ($sms->is_check_sent_done == 1): ?>
                                <span data-toggle="tooltip" data-placement="bottom" title="Recla Error" class="fa fa-check-circle text-warning pointer"></span>
                            <?php endif ?>
                            <span class="verification-solde"><!-- JS --></span>
                        </span>
                        <span class="float-right mb-2">
                            <?php if (Chronos::parse($sms->senttime)->wasWithinLast('1 hour')): ?>
                                <?= $this->Time->timeAgoInWords($sms->senttime); ?>
                            <?php else: ?>
                                <?= Chronos::parse($sms->senttime)->format('d/m, H\\hi'); ?>
                            <?php endif ?>
                        </span>
                    </div>
                    <div class="text-white msg" data-url="<?= $this->Url->build(['action' => 'editSms', $sms->id])?>" data-id="<?= $sms->id ?>"><?= $sms->msg ?></div>
                    <div class="clearfix actions">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="container-montant clearfix text-secondary">
                                    Reçu : <span class="montant_recu"><?= $sms->montant_recu ?></span> Ar,
                                    Solde : <span class="solde_actuel"><?= $sms->solde_actuel ?></span> Ar,

                                    <?php /*if (!in_array($type, ['orange', 'airtel'])): ?>
                                        Raison : <span class="raison"><?= $sms->raison ?></span>
                                        <?php if (!is_int($sms->raison_text)): ?>
                                            <?php $sms->raison_text ?>
                                        <?php endif ?>
                                    <?php else: ?>
                                    <?php endif*/ ?>
                                    <?= date('d/m/Y H:i:s', strtotime($sms->senttime)) ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="clearfix mt-2">
                                        
                                    <a href="<?= $this->Url->build(['action' => 'deleteSms', @$sms->id]) ?>" class="ml-2 float-right btn btn-sm deletesms"> 
                                        <span class="fa fa-remove text-danger"></span>
                                    </a>

                                    <a href="<?= $this->Url->build(['action' => 'markSms', $sms->id]) ?>" class=" float-right btn btn-sm <?= $sms->marked == 1 ? 'btn-info' : 'btn-dark' ?> marksms"> 
                                        <span class="fa fa-star <?= $sms->marked == 1 ? 'text-white' : 'text-secondary' ?>"></span>
                                    </a>

                                    <a data-toggle="tooltip" data-placement="bottom" title="Ajuster Solde" href="<?= $this->Url->build(['action' => 'ajusteMontant', $type, $sms->id]) ?>" class="mr-2 float-right btn btn-sm ajustermontant"> 
                                        <span class="fa fa-check-circle majstatemontant"></span>
                                    </a>

                                    <a data-toggle="tooltip" data-placement="bottom" title="<?= $sms->get('tooltipStatut') ?>, cliquer pour marquer à vert (ok)" data-status="<?= $sms->paiement_status ?>"href="<?= $this->Url->build(['action' => 'majestate', $sms->id]) ?>" class="mr-2 float-right btn btn-sm majestate"> 
                                        <span class="fa <?= $sms->paiement_status != 'locked_paiement' ? 'fa-exclamation-circle' : 'fa-lock text-success' ?> <?= $sms->get('ClassStatut') ?>"></span>
                                    </a>

                                    <a data-toggle="tooltip" data-placement="bottom" title="Mettre le solde à 0 Ar" href="<?= $this->Url->build(['action' => 'majSoldeAZero', $sms->id]) ?>" class="switchSolde0 mr-2 float-right btn btn-sm <?= $sms->is_backup_sms_majsolde_zero == 1 ? 'text-warning' : '' ?>"><span class="fa fa-refresh"></span></a>

                                    <?php if ($type == "mvola"): ?>
                                        <a href="<?= $this->Url->build(['action' => 'searchIdPanierMvola', $type, $sms->id]) ?>" class="mr-2 float-right btn btn-sm" data-toggle="modal" data-type="<?= $type ?>" data-target="#searchpanier"><span class="fa fa-search"></span></a>
                                    <?php elseif ($type == "orange"): ?>
                                        <a href="<?= $this->Url->build(['action' => 'searchIdPanierOrange', $type, $sms->id]) ?>" class="mr-2 float-right btn btn-sm" data-toggle="modal" data-type="<?= $type ?>" data-target="#searchpanier"><span class="fa fa-search"></span></a>
                                    <?php elseif($type == "airtel"): ?>
                                        <a href="<?= $this->Url->build(['action' => 'searchIdPanierAirtel', $type, $sms->id]) ?>" class="mr-2 float-right btn btn-sm" data-toggle="modal" data-type="<?= $type ?>" data-target="#searchpanier"><span class="fa fa-search"></span></a>
                                    <?php endif ?>
                                    
                                    <span class="result-reglement float-right mr-2">
                                        <!-- JS -->
                                    </span> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <hr class="light">
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $this->element('pagination/pagination') ?>
    </div>
    <div class="col-md-6">
        <span class="float-right"><?= $this->Paginator->counter('{{count}}') ?></span>
    </div>
</div>

<div class="d-none">
    <?= $this->Form->text('sms', ['class' => 'form-control', 'id' => 'sms-src']); ?>
</div>