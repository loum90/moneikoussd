<table class="table table-condensed">
    <thead>
        <tr>
            <th></th>
            <th>Dépôt</th>
            <th>Retrait</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-success">Telma</td>
            <td><?= $prodParams->numero_depot_telma ?></td>
            <td><?= $prodParams->numero_mvola_retrait ?></td>
        </tr>
        <tr>
            <td class="text-warning">Orange</td>
            <td><?= $prodParams->numero_depot_orange ?></td>
            <td><?= $prodParams->numero_orange_retrait ?></td>
        </tr>
        <tr>
            <td class="text-danger">Airtel</td>
            <td><?= $prodParams->numero_depot_airtel ?></td>
            <td><?= $prodParams->numero_airtel_retrait ?></td>
        </tr>
    </tbody>
</table>