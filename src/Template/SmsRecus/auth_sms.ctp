<?php 
    use Cake\I18n\FrozenTime;
    use \Cake\Chronos\Chronos;
    // debug($this->App->formatMontant(4500));
?>

<a class="btn btn-sm mb-3" href="<?= $this->Url->build(['controller' => 'tdb']) ?>"><span class="fa fa-home"></span></a><span class="result-solde"></span>
<a href="javascript:void(0);" class="float-right reloadsms d-none d-sm-block"><span class="fa fa-spinner text-secondary"></span></a>

<?= $this->Form->create(false, ['class' => 'mb-4', 'type' => 'get']); ?>
    <div class="row mb-2">
        <div class="col-lg-2 col"><?= $this->Form->control('mobile', ['type' => 'select', 'default' => @$options['mobile'], 'options' => ['034' => 'Telma', '032' => 'Orange', '033' => 'Airtel'], 'label' => false, 'placeholder' => 'Ref', 'class' => 'form-control']); ?></div>
        <div class="col-lg-2 col"><?= $this->Form->text('date', ['type' => 'date', 'value' => @$options['date'] ? @$options['date'] : date('Y-m-d'), 'label' => false, 'placeholder' => 'Ref', 'class' => 'form-control']); ?></div>
        <div class="col-lg-2 col">
            <button type="submit" class="btn btn-primary d-inline-block">Filtrer</button>
            <a href="<?= $this->Url->build(['action' => 'authSms', false]) ?>" class="btn btn-primary d-inline-block">Réinit</a>
        </div>
    </div>
<?= $this->form->end() ?>

<div class="clearfix mt-4 mb-4 d-none d-sm-block"><?= $this->element('pagination/pagination') ?></div>
<div class="clearfix mb-4">
    Aujourd'hui 
    <?php foreach ($operateurs_fronts as $key => $op): ?>
        <span class="fa fa-envelope-o <?= $op['class'] ?>"></span> : <span class="text-white"><?= ${'nbSms'.$op['label'].'Today'} ?></span>
    <?php endforeach ?>
    Total
    <?php foreach ($operateurs_fronts as $key => $op): ?>
        <span class="fa fa-envelope-o <?= $op['class'] ?>"></span> : <span class="text-white"><?= ${'nbSms'.$op['label']} ?></span>
    <?php endforeach ?>
</div>
<div class="container-sms">


    <div class="sms-block">
        <?php foreach ($smss as $key => $sms): ?>

            <div class="row-sms clearfix">
                <div class="clearfix">
                    <span class="float-left mb-2">
                        <a target="_blank" href="https://moneiko.net/exchanger/manager/forcelogin/<?= $sms->user_id ?>?entry=me"><span class="fa fa-user-circle-o"></span></a> <?= $sms->numero ?>
                    </span>
                    <span class="float-right mb-2">
                        <?php if ($sms->created->wasWithinLast('1 hour')): ?>
                            <?= $this->Time->timeAgoInWords($sms->created); ?>
                        <?php else: ?>
                            <?= $sms->created->format('d/m, H\\hi'); ?>
                        <?php endif ?>
                    </span>
                </div>
                <div class="text-white" data-url="<?= $this->Url->build(['action' => 'editSms', $sms->id])?>" data-id="<?= $sms->id ?>"><?= $sms->msg ?></div>
                <div class="clearfix actions">

                </div>
                <div class="clearfix">
                    <hr class="light">
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="clearfix mt-4"><?= $this->element('pagination/pagination') ?></div>
