<div class="mb-4"><?= $results->count(); ?> résultats</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th>USD</th>
            <th>Compte</th>
            <th>Reference</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $key => $result): ?>
            <tr>
                <td><?= $result['usd'] ?></td>
                <td><?= $result['compte'] ?></td>
                <td><?= $result['ref'] ?></td>
                <td><?= $result['date'] ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>