<?= $this->Form->create(false, ['class' => '']); ?>
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('base', ['type' => 'select', 'options' => $assets, 'default' => @$data['base']]); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('qty', ['default' => @$data['qty'] ?? 1]); ?>
                </div>
            </div>
        </div>
    </div>
    
     <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group  clearfix">
                        <label for="" class="label-control">Date</label>
                        <?= $this->Form->text('date', ['type' => 'date', 'default' => @$data['base'] ?? date('Y-m-d')]); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="form-group  clearfix">
                        <label for="" class="label-control">Heure</label>
                        <?= $this->Form->text('hour', ['type' => 'time', 'default' => @$data['hour'] ?? date('H:i')]); ?>
                    </div>
                </div>
            </div>
        </div>
     </div>

     <div class="row">
        <div class="col-md-4">
            <div class="form-group  clearfix">
                <button type="submit" class="btn btn-primary">Soumettre</button>
            </div>
        </div>
     </div>

     <?php if (isset($result)): ?>
        <div class="row">
             <div class="col-md-4">
                <div class="form-group clearfix">
                    <label for="" class="label-control">Taux / u = <?= round($result['rate'], 2) ?></label>
                    <?= $this->Form->text('rate', ['value' => round($result['rate'], 2)*$qty]); ?>
                </div>
             </div>
         </div> 
        <div class="row">
             <div class="col-md-4">
                <div class="form-group clearfix">
                    <label for="" class="label-control">Taux Ariary / u = <?= round($result['rate'], 2) ?></label>
                    <?= $this->Form->text('rate', ['value' => round($result['rate'], 2)*4325*$qty]); ?>
                </div>
             </div>
         </div> 
     <?php endif ?>
    
<?= $this->form->end() ?>