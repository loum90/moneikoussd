<?php $heure = @$options['heure'].':00'; ?>

<?php if (!empty($idpaniers)): ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Raison</th>
                    <th>Compte</th>
                    <th>Montant</th>
                    <th>Type</th>
                    <th>Statut</th>
                    <th>Intval</th>
                    <th>URL</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($idpaniers as $idpanier): ?>
                    <?php $start_at = date('H:i:s', strtotime($idpanier['created'])) ?>
                    <?php $end_at = date('H:i:s', strtotime($idpanier['expired_at'])) ?>

                    <?php if (!in_array($idpanier['status'], ['FAILED', 'SUCCESS', 4,])): ?>
                    <?php endif ?>
                        <?php $url = $host.'/paiement-mvola/'.$idpanier['id'].'?open=1&dev_mode=1'; ?>
                        <tr>
                            <td><?= $idpanier['raison'] ?></td>
                            <td>
                                <?= $idpanier['datas']['compte'] ?? strtoupper($idpanier['datas']['asset']).' : '.$idpanier['datas']['to_btc_addr'] ?>
                                <?php if (in_array($options['mobile'], ['orange'])): ?>
                                    , <?= $idpanier['numero_orange'] ?>
                                <?php endif ?>
                            </td>
                            <td><?= $idpanier['montant'] ?></td>
                            <td><?= $idpanier['type'] ?></td>
                            <td><?= $idpanier['status'] ?></td>
                            <td>
                                <?php $isInInterval = $heure > $start_at && $heure < $end_at ? true : false ?>
                                <span class="text-danger">[</span><span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $start_at ?></span><span class="text-danger">,</span> <span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $end_at ?></span><span class="text-danger">]</span>
                            </td>
                            <td>
                                <a target="blank" href="<?= $url ?>">URL</a>
                                <?php if (in_array($options['mobile'], ['mvola', 'airtel'])): ?>
                                    <a class="text-danger" target="blank" href="<?= $host.'/paiement-airtel-money/'.$idpanier['id'].'?open=1&dev_mode=1'; ?>">URL</a>
                                <?php endif ?>
                            </td>
                            <td>
                                <?php if ($options['mobile'] == 'mvola'): ?>
                                    <a target="_blank" data-search-url="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'majAndResetRaisonSms', 'raison' => $idpanier['raison'], 'created_at' => $idpanier['created_at'], 'expired_at' => $idpanier['expired_at'], 'type' => @$options['mobile'], 'keyword' => $this->App->formatMontant($idpanier['montant'])]) ?>" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'created_at' => $idpanier['created_at'], 'expired_at' => $idpanier['expired_at'], 'type' => @$options['mobile'], 'keyword' => $this->App->formatMontant($idpanier['montant'])]) ?>" class="btn btn-sm majandreset"><span class="fa fa-search text-warning"></span></a>
                                <?php endif ?>
                                <a target="_blank" href="<?= $this->Url->build(['controller' => 'SmsRecus', 'action' => 'index', 'created_at' => $idpanier['created_at'], 'expired_at' => $idpanier['expired_at'], 'type' => @$options['mobile'], 'keyword' => $this->App->formatMontant($idpanier['montant']), 'search-mobile-sms' => $options['mobile'] == 'orange' ? $options['mobile'] : '' ]) ?>" class="btn btn-sm"><span class="fa fa-search"></span></a>
                            </td>
                        </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <div class="mb-4">Aucun résultat</div>
<?php endif ?>