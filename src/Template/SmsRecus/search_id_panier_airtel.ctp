<a href="" class="loadlink">
    <!-- JS -->
</a>

<div class="clearfix mb-4">
    <?= $sms->msg ?>
</div>

<?php if (isset($panierOnServer->id)): ?>
    <p class="text-white">
        ID PANIER : <?= $panierOnServer->id ?>, PTF : <?= $ptf = strtoupper($panierOnServer->type) ?> <?= strtoupper($panierOnServer->asset) ?>, du <?= date('d/m/Y', strtotime($sms->senttime)) ?>
    </p>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="25%">Libellé</th>
                    <th width="25%">Idpanier Descr</th>
                    <th width="25%">Sms Descr</th>
                    <th width="25%">Statut</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Montant</td>
                    <td><?= $montantPanier = $panierOnServer->montant ?> Ariary 
                        <?php if ($panierOnServer->type == 'faucetpay'): ?>
                            <span class="text-warning" data-toggle="tooltip" data-placement="bottom" title="Liaison ExchangeMobileIpn"><?= $panierOnServer->exchange_mobile_ipn['montant_usd'] ?> <?= $panierOnServer->asset ?></span>
                        <?php else: ?>
                            <span class="text-warning"><?= @$panierOnServer->converted_usd['montantNet'] ?></span>
                        <?php endif ?>
                    </td>
                    <td><?= $montantSms = $airtelMoneyData['montant_recu'] ?> Ariary</td>
                    <td>
                        <?php if ($montantPanier == $montantSms): ?>
                            <span class="badge badge-success">
                                SUCCÈS
                            </span>
                        <?php else: ?>
                            <span class="badge badge-warning">
                                NON PAYÉ
                            </span>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td>Numéro</td>
                    <td><?= $numeroPanier = $panierOnServer->numero_airtel ?></td>
                    <td><?= $numeroSms = $airtelMoneyData['numero'] ?></td>
                    <td>
                        <?php if ($numeroPanier == $numeroSms): ?>
                            <span class="badge badge-success">
                                SUCCÈS
                            </span>
                        <?php else: ?>
                            <span class="badge badge-warning">
                                NON PAYÉ
                            </span>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td>Crée le </td>
                    <td><?= date('H:i:s', strtotime($panierOnServer->created_date)) ?></td>
                    <td>
                        <span class="<?= $sms->receivedtime < $panierOnServer->created_date ? 'text-danger' : '' ?>">
                            <?= date('H:i:s', strtotime($sms->receivedtime)) ?> 
                            <?php if ($sms->receivedtime < $panierOnServer->created_date): ?>
                                <span class="text-danger pointer" data-toggle="tooltip" data-placement="bottom" title="<div class='text-left'>Sms Received time : <br><?= $this->Time->nice($sms->receivedtime) ?>, <br>Sms Created date : <br> <?= $this->Time->nice($panierOnServer->created_date) ?></div>">(Err Synchronise)</span>
                            <?php else: ?>
                                <span class="fa fa-check-circle text-success pointer" data-toggle="tooltip" data-placement="bottom" title="Horaire Local/Prod synchronisé avec succès, Receivedtime"></span>
                            <?php endif ?>
                        </span>
                        <?php if ($sms->senttime_backup): ?>
                            <span class="text-wargning">
                                <span class="text-warning" data-toggle="tooltip" data-placement="bottom" title="Heure d'arrivée originale">
                                    (<?= date('H:i:s', strtotime($sms->senttime_backup)) ?>)
                                </span>
                            </span>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php if ($panierOnServer->created_date <= $sms->senttime && $panierOnServer->expired_date >= $sms->senttime): ?>
                            <span class="badge badge-success">
                                SUCCÈS
                            </span>
                        <?php else: ?>
                            <span class="badge badge-warning">
                                ERREUR
                            </span>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td>Expiration</td>
                    <td><?= date('H:i:s', strtotime($panierOnServer->expired_date)) ?></td>
                    <td>
                        <?php if ($diffTime <= 60): ?>
                            <?= $diffTime ?> secondes
                        <?php else: ?>
                            <?php 
                                $minutes = floor(($diffTime / 60) % 60);
                                $seconds = $diffTime % 60;
                            ?>
                            <?= $minutes ?> minutes<?= $seconds > 0 ? ', '.$seconds. ' secondes' : '' ?>
                        <?php endif ?>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>Statut</td>
                    <td>
                        <?php if (in_array($panierOnServer->status, ['SUCCESS', 4])): ?>
                            <span class="badge badge-success">
                                SUCCÈS
                            </span>
                        <?php else: ?>
                            <span class="badge badge-warning">
                                NON PAYÉ
                            </span>
                        <?php endif ?>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TXNID</td>
                    <td>
                        <?php if (!is_null($panierOnServer->txnid)): ?>
                            <?= $txnidPanier = $panierOnServer->txnid ?>
                        <?php else: ?>
                            <?php $txnidPanier = '' ?>
                            <span class="badge badge-warning">
                                VIDE
                            </span>
                        <?php endif ?>
                    </td>
                    <td><?= $txnidSms = $airtelMoneyData['ref'] ?></td>
                    <td>
                        <?php if ($txnidPanier == $txnidSms): ?>
                            <span class="badge badge-success">
                                SUCCÈS
                            </span>
                        <?php else: ?>
                            <span class="badge badge-warning">
                                NON PAYÉ
                            </span>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td>Compte</td>
                    <td colspan="2">
                        <?= $this->Form->control('compte', ['value' => isset($panierOnServer->datas['compte']) ? $panierOnServer->datas['compte'] : $panierOnServer->datas['asset'], 'type' => 'text', 'label' => false, 'class' => false]); ?>
                        <?php if ($panierOnServer->type == 'deriv'): ?>
                            <a target="_blank" href="https://app.deriv.com/cashier/payment-agent-transfer?lang=FR">Transfert &rarr;</a>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php if (isset($panierOnServer->datas['asset'])): ?>
                            <?= $datas['to_btc_addr'] ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td>Nom</td>
                    <td colspan="3"><?= $panierOnServer->nom_complet ?></td>
                </tr>
                <tr>
                    <td>User ID</td>
                    <td>
                        <a href="https://moneiko.net/exchanger/manager/forcelogin/<?= $panierOnServer->user_id ?>" target="_blank"><?= $panierOnServer->user_id ?></a>
                        <a href="https://moneiko.net/exchanger/manager/users?user_id=<?= $panierOnServer->user_id ?>&entry=me&show=1" target="_blank">Voir sur manager</a>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- <a target="_blank" href="<?= $this->Url->build('http://127.0.0.1/moneyko/paiement-airtel-money/'.$panierOnServer->id.'?open=1&entry=me') ?>" class="float-right btn btn-sm text-white">Lien commande</a> -->
    <a target="_blank" href="<?= $this->Url->build($params['hook_serveur'].'/paiement-airtel-money/'.$panierOnServer->id.'?open=1&entry=me') ?>" class="btn btn-sm text-white">Lien</a>
    <!-- <a target="_blank" href="javascript:void(0);" data-smstranid="<?= $airtelMoneyData['ref'] ?>" data-panierref="<?= $panierOnServer->txnid ?>" class="mr-2 float-right btn btn-sm text-white">Rechercher SMS</a> -->
<?php else: ?>

    <span class="text-warning">Aucune commande trouvée dans l'interval.</span> <br>
   
    <?php if ($findedPaniers): ?>

        <div class="clearfix mb-4 text-white">
            <br> Commandes trouvées le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, au montant <?= $airtelMoneyData['montant_recu'] ?> Ar, avec le numéro <?= $airtelMoneyData['numero'] ?> du SMS
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Numéro</th>
                        <th>Ariary</th>
                        <th>Compte</th>
                        <th>Interval</th>
                        <th class="text-warning">Sms Arrivé le <span class="fa fa-exclamation-circle" data-html="true" data-toggle="tooltip" data-placement="bottom" title="Par rapport à expired_at"></span></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($findedPaniers as $key => $idpanier): ?>

                        <?php $created_at = date('Y-m-d', strtotime($idpanier['created'])) ?>
                        <?php $start_at = date('H:i:s', strtotime($idpanier['created'])) ?>
                        <?php $end_at = date('H:i:s', strtotime($idpanier['expired_at'])) ?>
                        <?php $arrived_at = date('H:i:s', strtotime($sms->senttime)) ?>
                        <?php $isInInterval = $arrived_at > $start_at && $arrived_at < $end_at ? true : false ?>

                        <tr>
                            <td><?= $idpanier['numero_airtel'] ?></td>
                            <td><?= $idpanier['montant'] ?></td>
                            <td><?= $type = strtoupper($idpanier['type']).' : '.$idpanier['datas']['compte'] ?></td>
                            <td>
                                <span class="text-danger">[</span><span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $start_at ?></span><span class="text-danger">,</span> <span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $end_at ?></span><span class="text-danger">]</span>
                            </td>
                            <td><?= date('H:i:s', strtotime($sms->senttime)) ?> 
                                (<?= $idpanier['diff'] ?>)
                            </td>
                            <td>
                                <a data-toggle="tooltip" data-placement="bottom" title="Ajuster date dans l'intervalle" target="_blank" href="<?= $this->Url->build(['action' => 'ajustTime', 'created_at' => date('Y-m-d H:i:s', strtotime($idpanier['created_at'])), 'expired_at' => date('Y-m-d H:i:s', strtotime($idpanier['expired_at'])), 'sms_id' => $sms['id']]) ?>" class="btn btn-sm text-white"><span class="fa fa-refresh text-warning reajustpaniersms"></span></a>
                                <a target="_blank" href="<?= $this->Url->build($params['hook_serveur'].'/paiement-airtel-money/'.$idpanier['id'].'?open=1&entry=me') ?>" class="btn btn-sm text-white">Lien</a>
                                <a target="_blank" href="<?= $this->Url->build($params['hook_serveur'].'/exchanger/manager/commandes-mobiles?numero='.$idpanier['numero_airtel'].'&mobile=airtel2&wallet='.strtolower($type).'&date='.$created_at) ?>" class="btn btn-sm text-white">C M</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        
    <?php endif ?>

    <?php if ($findedPaniersByMontants): ?>
        <div class="clearfix text-danger">
            <br> Aucune commande trouvée le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, avec le numéro <?= $airtelMoneyData['numero'] ?> du SMS
        </div>

        <div class="clearfix mb-4 text-white">
            <br> Autres commandes trouvées le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, au montant <?= $airtelMoneyData['montant_recu'] ?> Ar :
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Numéro</th>
                        <th>Ariary</th>
                        <th>Compte</th>
                        <th>Interval</th>
                        <th class="text-warning">Sms Arrivé le</th>
                        <th>Il y a</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($findedPaniersByMontants as $key => $idpanier): ?>

                        <?php $start_at = date('H:i:s', strtotime($idpanier['created'])) ?>
                        <?php $end_at = date('H:i:s', strtotime($idpanier['expired_at'])) ?>
                        <?php $arrived_at = date('H:i:s', strtotime($sms->senttime)) ?>
                        <?php $isInInterval = $arrived_at > $start_at && $arrived_at < $end_at ? true : false ?>

                        <tr>
                            <td><?= $idpanier['numero_airtel'] ?></td>
                            <td><?= $idpanier['montant'] ?></td>
                            <td>
                                <?php if (isset($idpanier['datas']['compte'])): ?>
                                    <?= strtoupper($idpanier['type']).' : '.$idpanier['datas']['compte'] ?>
                                <?php else: ?>
                                    <?= $idpanier['datas']['asset']. ' : '. $idpanier['datas']['to_btc_addr'] ?>
                                <?php endif ?>
                            </td>
                            <td>
                                <span class="text-danger">[</span><span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $start_at ?></span><span class="text-danger">,</span> <span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $end_at ?></span><span class="text-danger">]</span>
                            </td>
                            <td><?= date('H:i:s', strtotime($sms->senttime)) ?></td>
                            <td><?= $idpanier['diff'] ?></td>
                            <td>
                                <!-- <a data-toggle="tooltip" data-placement="bottom" title="Ajuster date dans l'intervalle" target="_blank" href="<?= $this->Url->build(['action' => 'ajustTime', 'created_at' => date('Y-m-d H:i:s', strtotime($idpanier['created_at'])), 'expired_at' => date('Y-m-d H:i:s', strtotime($idpanier['expired_at'])), 'sms_id' => $sms['id']]) ?>" class="btn btn-sm text-white"><span class="fa fa-refresh text-warning reajustpaniersms"></span></a> -->
                                <a target="_blank" href="<?= $this->Url->build(['action' => 'corrigerNumero', 'airtel', 'numero_idpanier' => $idpanier['numero_airtel'], 'sms_id' => $sms->id]) ?>" class="btn btn-sm text-white corriger-numero">Corriger n°</a>
                                <a target="_blank" href="<?= $this->Url->build($params['hook_serveur'].'/paiement-airtel-money/'.$idpanier['id'].'?open=1&entry=me') ?>" class="btn btn-sm text-white">Lien</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    <?php endif ?>


    <?php if ($findedPaniersByNumeros): ?>
        <div class="clearfix text-danger">
            <br> Aucune commande trouvée le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, avec le numéro <?= $airtelMoneyData['numero'] ?> du SMS
        </div>

        <div class="clearfix text-danger">
            <br> Aucune commande trouvée le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, au montant <?= $airtelMoneyData['montant_recu'] ?> Ar
        </div>

        <div class="clearfix mb-4 text-white">
            <br> Commandes trouvées le <?= date('d/m/Y', strtotime($sms->senttime)) ?>, au numéro <?= $airtelMoneyData['numero'] ?> Ar (<span class="text-warning">Montant modifié</span>):
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Numéro</th>
                        <th>Ariary</th>
                        <th>Compte</th>
                        <th>Interval</th>
                        <th class="text-warning">Sms Arrivé le</th>
                        <th>Il y a</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($findedPaniersByNumeros as $key => $idpanier): ?>

                        <?php $start_at = date('H:i:s', strtotime($idpanier['created'])) ?>
                        <?php $end_at = date('H:i:s', strtotime($idpanier['expired_at'])) ?>
                        <?php $arrived_at = date('H:i:s', strtotime($sms->senttime)) ?>
                        <?php $isInInterval = $arrived_at > $start_at && $arrived_at < $end_at ? true : false ?>

                        <tr>
                            <td><?= $idpanier['numero_airtel'] ?></td>
                            <td><?= $idpanier['montant'] ?></td>
                            <td>
                                <?php if (isset($idpanier['datas']['compte'])): ?>
                                    <?= strtoupper($idpanier['type']).' : '.$idpanier['datas']['compte'] ?>
                                <?php else: ?>
                                    <?= $idpanier['datas']['asset']. ' : '. $idpanier['datas']['to_btc_addr'] ?>
                                <?php endif ?>        
                            </td>
                            <td>
                                <span class="text-danger">[</span><span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $start_at ?></span><span class="text-danger">,</span> <span class="<?= $isInInterval ? 'text-success' : '' ?>"><?= $end_at ?></span><span class="text-danger">]</span>
                            </td>
                            <td>
                                <?= date('H:i:s', strtotime($sms->senttime)) ?>
                                <?php if ($idpanier['expired_at'] > $sms->senttime && $idpanier['created_at'] < $sms->senttime): ?>
                                    <span class="fa fa-check text-success"></span>
                                <?php else: ?>
                                    <span class="fa fa-times-circle text-danger"></span>
                                <?php endif ?>
                            </td>
                            <td><?= $idpanier['diff'] ?></td>
                            <td>
                                <a target="_blank" href="<?= $this->Url->build(['action' => 'corrigerMontant', 'airtel', 'montant_idpanier' => $idpanier['montant'], 'sms_id' => $sms->id]) ?>" class="btn btn-sm text-white corriger-montant">Corriger montant</a>
                                <a target="_blank" href="<?= $this->Url->build(['action' => 'majStatutIdpanierBloqued', $idpanier['id'], 'user_email' => $idpanier['user_email']]) ?>" class="btn btn-sm text-white corriger-montant"><span class="<?= $idpanier['status'] == 'warning' ? 'text-warning' : '' ?>">Statut Bloqué</span></a>
                                <a target="_blank" href="<?= $this->Url->build(['action' => 'majStatutIdpanierBloqued', $idpanier['id'], 'warning_numero', 'user_email' => $idpanier['user_email']]) ?>" class="btn btn-sm text-white corriger-montant"><span class="<?= $idpanier['status'] == 'warning_numero' ? 'text-warning' : '' ?>">Statut Suspendu</span></a>
                                <!-- <a data-toggle="tooltip" data-placement="bottom" title="Ajuster date dans l'intervalle" target="_blank" href="<?= $this->Url->build(['action' => 'ajustTime', 'created_at' => date('Y-m-d H:i:s', strtotime($idpanier['created_at'])), 'expired_at' => date('Y-m-d H:i:s', strtotime($idpanier['expired_at'])), 'sms_id' => $sms['id']]) ?>" class="btn btn-sm text-white"><span class="fa fa-refresh text-warning reajustpaniersms"></span></a> -->
                                <a target="_blank" href="<?= $this->Url->build($params['hook_serveur'].'/paiement-airtel-money/'.$idpanier['id'].'?open=1&entry=me') ?>" class="btn btn-sm text-white">Lien</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    <?php endif ?>
<?php endif ?>

<div class="clearfix">
    <div class="clearfix my-4">NB Achat à cette date : <span class="text-white"><?= $smss->count() ?></span></div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="25%">Ar</th>
                <th width="25%">PTF</th>
                <th width="25%">Date</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smss as $key => $sms): ?>
                <tr>
                    <td><?= $sms->montant_recu ?></td>
                    <td><?= strtoupper($sms->ptf) ?></td>
                    <td><?= $sms->receivedtime ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<a href="javascript:;" class="btn btn-sm text-white reload-idpanier">Recharger <span class="fa fa-circle-o-notch"></span></a>

<?= $this->Form->create(false, ['class' => 'datesmsform mt-2', 'type' => 'POST']); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $this->Form->control('senttime', [ 'datasms-id' => $sms->id, 'value' => $sms->senttime, 'class' => 'lol']); ?>
        </div>
        <div class="col-md-4">
            <?= $this->Form->control('receivedtime', [ 'datasms-id' => $sms->id, 'value' => $sms->receivedtime, 'class' => 'lol']); ?>
        </div>
    </div>
<?= $this->form->end() ?>


<?php if (isset($panierOnServer->id)): ?>
    <?php $ptf = ucfirst(strtolower($ptf)) ?>
    <a class="btn mb-2 btn-sm mr-2 text-white checkhistoric" href="<?= $this->Url->build(['action' => 'check'.$ptf.'HistorickByIdpanierId', $panierOnServer->id]) ?>">Check Tx</span>
    <a class="btn mb-2 btn-sm text-white restaurersms" href="<?= $this->Url->build(['action' => 'restaurerSms', $sms->id]) ?>">Restaurer <span class="fa fa-cloud-upload"></span></a> 
    <a class="btn mb-2 btn-sm text-white ajustermontant" href="<?= $this->Url->build(['action' => 'ajusteMontant', $type, $sms->id]) ?>">Màj Solde <span class="fa fa-check-circle"></span></a>
    <a class="btn mb-2 btn-sm text-white voirlog" href="<?= $this->Url->build(['action' => 'voirlog', $sms->id]) ?>">Voir log <span class="fa fa-file-text-o"></span></a>
    <a class="btn mb-2 btn-sm text-white reglercommande" href="<?= $this->Url->build(['action' => 'reglerCommandeairtel', 'panier_id' => $panierOnServer->id, 'sms_id' => $sms->id]) ?>">Régler <span class="fa fa-check-circle"></span></a> <span class="result-reglercommande"> <!-- JS --> </span>
    <a class="btn mb-2 btn-sm text-white markaspaid" href="<?= $urlMarkAsPaid ?>">Payé <span class="fa fa-check-circle"></span></a>
    <a class="btn mb-2 btn-sm text-white checkAndPay" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'paySingleError', $sms->id]) ?>"><span class="text-danger">Check Tx & Régler</span> <span class="fa fa-check-circle"></span></a> <span class="result-check-pay"> <!-- JS --> </span>
    <a class="btn mb-2 btn-sm text-white voirLogReclas" href="<?= $this->Url->build(['controller' => 'Sms', 'action' => 'voirLogReclas', $sms->id]) ?>"><span class="text-danger">Voir log reclas</span> <span class="fa fa-check-circle"></span></a> <span class="result-check-pay"> <!-- JS --> </span>
<?php endif ?>


<div class="clearfix checkhistoric-results pt-4 text-white">
    <!-- JS -->
</div>