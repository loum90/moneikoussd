<?php
namespace App\Log\Engine;

use Cake\Log\Engine\BaseLog;
use Cake\Datasource\ConnectionManager;
use Cake\Http\ServerRequestFactory;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

class UrlSqlLogger extends BaseLog
{
    // public function log($level, $message, array $context = [])
    // {

    //     $request = ServerRequestFactory::fromGlobals();
    //     $baseUrl = $url = $request->getRequestTarget();

    //     $heure = (int) date('H');

    //     // if ($heure > 01 && $heure < 04) {
    //         // Filtrer uniquement les logs de requêtes SQL
    //         $date = date('dmy_Hi');
    //         if (isset($message->query)) {
    //             $logFolder = LOGS . 'queries' . DS. $date;
    //             // debug($logFolder);
    //             $folder = (new Folder())->create($logFolder, '0777');

    //             $url = str_replace('/', '_', $url);
    //             $url = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $url));

    //             $logFile = LOGS . 'queries' . DS. $date . DS . 'sql' . ($url) . '.log';  // Nom du fichier basé sur l'URL

    //             $log = '[' . date('Y-m-d H:i:s') . '] URL: ' . $baseUrl . ' Requête: ' . $message->query . "\n";

    //             // debug($logFile);
    //             if (!file_exists($logFile)) {
    //                 $file = new File($logFile);
    //                 $file->write($log);
    //             } else {
    //                 $file = new File($logFile);
    //                 $file->append($log);
    //             }

    //             $file->close();


    //             // debug($logFile);
    //             // debug($f);

    //             // debug($log);
    //             // file_put_contents($logFile, $log, FILE_APPEND);
    //         }
    //     // }

    // }


    public function log($level, $message, array $context = [])
    {

        $request = ServerRequestFactory::fromGlobals();
        $baseUrl = $url = $request->getRequestTarget();

        $heure = (int) date('H');

        // if ($heure > 01 && $heure < 04) {
            // Filtrer uniquement les logs de requêtes SQL
            $date = date('dmy_Hi');
            if (isset($message->query)) {

                // Récupérer l'URL
                $url = $request->getRequestTarget();
                // Méthode HTTP
                $method = $request->getMethod();
                // Données POST
                $postData = json_encode($request->getData());


                $logFolder = LOGS . 'request';
                // debug($logFolder);
                $folder = (new Folder())->create($logFolder, '0777');

                // $url = str_replace('/', '_', $url);
                // $url = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $url));

                $logFile = LOGS . 'request' . DS. 'request.log';  // Nom du fichier basé sur l'URL

                $log = '['.date('Y-m-d H:i:s').', URL: ' . $url . ', Requête : ' . $method . ' Data : '. $postData . "\n";

                // debug($logFile);
                if (!file_exists($logFile)) {
                    $file = new File($logFile);
                    $file->write($log);
                } else {
                    $file = new File($logFile);
                    $file->append($log);
                }

                $file->close();


                // debug($logFile);
                // debug($f);

                // debug($log);
                // file_put_contents($logFile, $log, FILE_APPEND);
            }
        // }

    }

    // Fonction pour obtenir l'URL actuelle
    private function getCurrentUrl()
    {
        // Accéder à la requête actuelle
        $request = ConnectionManager::get('default')->getConfig('request');
        return $request ? $request->getRequestTarget() : 'unknown';
    }
}
