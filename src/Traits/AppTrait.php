<?php

namespace App\Traits;

use Cake\Routing\Router;
use Cake\ORM\Query;
use Cake\Datasource\ModelAwareTrait;
use Cake\Http\ServerRequest;
use Cake\Network\Session;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\Cache\Cache;

trait AppTrait
{
    use ModelAwareTrait;

    public $parametre = null;

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
    }

    public function formatMontant($montant)
    {
        return @number_format($montant, 0, ',', ' ');
    }

    /**
     * Replace language-specific characters by ASCII-equivalents.
     * @param string $s
     * @return string
     */
    public static function normalizeChars($s) {

        $replace = array(
            'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
            'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
            'Þ'=>'B',
            'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
            'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
            'Ğ'=>'G',
            'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
            'Ł'=>'L',
            'Ñ'=>'N', 'Ń'=>'N',
            'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
            'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
            'Ț'=>'T',
            'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
            'Ý'=>'Y',
            'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
            'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
            'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
            'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
            'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
            'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
            'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
            'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
            'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
            'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
            'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
            'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
            'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
            'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
            'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
            'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
            'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
            'ק'=>'q',
            'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
            'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
            'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
            'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
            'в'=>'v', 'ו'=>'v', 'В'=>'v',
            'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
            'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
            'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
        );
        return strtr($s, $replace);
    }


    public function checkExistingByExplode($var1, $var2)
    {
        $var1Exploded = explode(' ', $var1);

        // pr($var1. ' à comparer à :'.$var2);
        foreach ($var1Exploded as $key => $value) {
            $wordMatch = $this->wordMatch($var2, $value);
            // pr($wordMatch.' '.$value.' : '.$var2);
            if (strrpos($var2, $value) !== false) {
                return true;
            } elseif ($wordMatch > 90) {
                return true;
            }

            // $var2Explodeds = explode(' ', $var2);
            // $closestWord = $this->closestWord($value, $var2Explodeds);
            // debug($this->wordMatch($value, $closestWord));
        }

        return false;
    }

    public function checkCoherence($var1, $var2)
    {
        $var1 = preg_replace('/[^a-zA-Z0-9 ]/m', ' ', $var1);
        $var1Exploded = explode(' ', $var1);


        $maxScore1 = $this->calculateScores($var1Exploded, $var2);
        $maxScore2 = $this->calculateScores($this->uniqueCombination($var1), $var2);

        $results = [
            'maxScore1' => $maxScore1['maxScore'],
            'maxScore2' => $maxScore2['maxScore'],
        ];
        return (object) $results;
    }

    public function calculateScores($datas, $var2)
    {
        $matchs = [];

        foreach ($datas as $key => $combinationInVar1) {
            $wordMatch = $this->wordMatch($var2, $combinationInVar1);
            $var2Explodeds = explode(' ', $var2);
            $closestWord = $this->closestWord($combinationInVar1, $var2Explodeds);
            $matchs[] = $this->wordMatch($combinationInVar1, $closestWord);
        }

        $matchsFiltrered = count($matchs) ? $matchs : [0];
        $maxScore = max($matchsFiltrered);
        $average = (float) $this->calculateMatchsAverage($matchs, 50);

        return compact('maxScore');
    }

    public function calculateMatchsAverage($datas, $sensibility = 60)
    {
        $results = [];
        foreach ($datas as $key => $data) {
            if ($data > $sensibility) {
                $results[] = $data;
            }
        }

        if (count($datas)) {
            return ((count($results)/count($datas))*100);
        } else {
            return 0;
        }
    }

    function closestWord($input, $words, &$percent = null) {
       $shortest = -1;
       foreach ($words as $word) {
         $lev = levenshtein($input, $word);

         if ($lev == 0) {
           $closest = $word;
           $shortest = 0;
           break;
         }

         if ($lev <= $shortest || $shortest < 0) {
           $closest  = $word;
           $shortest = $lev;
         }
       }

       $percent = @(1 - levenshtein($input, $closest) / max(strlen($input), strlen($closest)));

       return $closest;
     }

     function permutateArray($InArray, $InProcessedArray = array())
     {
         $ReturnArray = array();
         foreach($InArray as $Key=>$value)
         {
             $CopyArray = $InProcessedArray;
             $CopyArray[$Key] = $value;
             $TempArray = array_diff_key($InArray, $CopyArray);
             if (count($TempArray) == 0)
             {
                 $ReturnArray[] = $CopyArray;
             }
             else
             {
                 $ReturnArray = array_merge($ReturnArray, permutateArray($TempArray, $CopyArray));
             }
         }
         return $ReturnArray;
     }

    function wordMatch($str1, $str2) 
    {
        $strlen1=strlen($str1);
        $strlen2=strlen($str2);
        $max=max($strlen1, $strlen2);

        $splitSize=250;
        if($max>$splitSize)
        {
            $lev=0;
            for($cont=0;$cont<$max;$cont+=$splitSize)
            {
                if($strlen1<=$cont || $strlen2<=$cont)
                {
                    $lev=$lev/($max/min($strlen1,$strlen2));
                    break;
                }
                $lev+=levenshtein(substr($str1,$cont,$splitSize), substr($str2,$cont,$splitSize));
            }
        }

        else {
            $lev=levenshtein($str1, $str2);
        }

        $porcentage = @(-100*$lev/$max+100);
        if($porcentage>75)//Ajustar con similar_text
        similar_text($str1,$str2,$porcentage);

        return round($porcentage, 2);
    }


    /**
     * https://stackoverflow.com/questions/3742506/php-array-combinations
     * @return [type]             [description]
     */
    public function uniqueCombination($text, $minLength = 1, $max = 2000) {

        $texts = explode(' ', $text);
        $returns = [];
        if (count($texts) == 1) {
            return [$text];
        }
        foreach ($texts as $key => $text) {
            if (isset($texts[$key+1])) {
                $nextWord = $texts[$key+1];
                $returns[] = $text.$nextWord;
            }
        }

        return $returns;

        // $count = count($text);
        // $members = pow(2, $count);
        // $returns = array();
        // for($i = 0; $i < $members; $i ++) {
        //     $b = sprintf("%0" . $count . "b", $i);
        //     $out = array();
        //     for($j = 0; $j < $count; $j ++) {
        //         $b{$j} == '1' and $out[] = $text[$j];
        //     }

        //     count($out) >= $minLength && count($out) <= $max and $returns[] = $out;
        // }

        // $noms = [];
        // foreach ($returns as $key => $return) {
        //     $noms[] = implode('', $return);
        // }
        // return $noms;
    }

    public function getParam($field = null)
    {
        $this->loadModel('Exchanger.Parameters');
        if ($this->parametre == null) {
            $this->parametre = $this->Parameters->findById(1)->first();
        }

        if ($field != null) {
            return $this->parametre->{$field};
        }

        return $this->parametre;
    }

    public function countArrayValue($array) {
        $array = array_filter($array);
        // pr($this->countDuplicatedValues($array));
        // pr(array_count_values($array));
        // die();
        // return count($array) !== count(array_unique($array));
        return count($this->countDuplicatedValues($array)) > 0;
    }

    public function countDuplicatedValues($array){
       return array_unique(array_diff_assoc($array,array_unique($array)));
    }


    public function checkMobile($string)
    {
        $mobile = '';
        if (substr($string, 0, 3) === '032' || substr($string, 0, 3) === '037') {
            $mobile = 'orange';
        } elseif (substr($string, 0, 3) === '034' || substr($string, 0, 3) === '038') {
            $mobile = 'mvola';
        } elseif (substr($string, 0, 3) === '033') {
            $mobile = 'airtel';
        } 

        return $mobile;
    }

    public function checkMobile2($string)
    {
        $mobile = '';
        if (substr($string, 0, 3) == '032' || substr($string, 0, 3) == '037') {
            $mobile = 'orange';
        } elseif (substr($string, 0, 3) == '034' || substr($string, 0, 3) == '038') {
            $mobile = 'telma';
        } elseif (substr($string, 0, 3) == '033') {
            $mobile = 'airtel';
        } 

        return $mobile;
    }

    public function majConfigOzekiNg($comPort, $numero)
    {
        $ozekiConfigFiles = (new Folder(OZEKICONFIG))->find('.*smsc.*[.]txt');
        $op = $this->checkMobile2($numero);
        $comPort = strtoupper($comPort);

        $result = null;
        $configContent = '';
        $pathConfig = '';
        foreach ($ozekiConfigFiles as $key => $file) {
            if (file_exists(OZEKICONFIG.$file)) {
                $configContent = file_get_contents(OZEKICONFIG.$file);
                $configFormated = strtolower($configContent);
                if (strrpos($configFormated, strtolower($op)) !== false) {
                    $pathConfig = OZEKICONFIG.$file;
                    break;
                };
            }
        }

        if (!empty($configContent)) {

            $SMSServiceCenterAddress = '';
            if ($op == 'telma') {
                $SMSServiceCenterAddress = "SMSServiceCenterAddress +261340010000";
            } elseif ($op == 'orange') {
                $SMSServiceCenterAddress = "SMSServiceCenterAddress +261323232707";
            } elseif ($op == 'airtel') {
                $SMSServiceCenterAddress = "SMSServiceCenterAddress +261331110006";
            }

            $arrayConfigs = explode("\n", $configContent);
            
            foreach ($arrayConfigs as $line => $config) {
                if ($line == 34) {
                    $arrayConfigs[$line] = "ModemOrPort $comPort";
                }

                if ($line == 41) {
                    $arrayConfigs[$line] = "PhoneNumber $numero";
                }

                if ($line == 51) {
                    $arrayConfigs[$line] = $SMSServiceCenterAddress;
                }
            }

            $configContentFinal = implode("\n", $arrayConfigs);
            $result = file_put_contents($pathConfig, $configContentFinal);
        }

        return $result;
    }

    public function cleanChar($chars)
    {
        $adress = htmlentities(strip_tags($chars));
        // Strip HTML Tags
        $clear = strip_tags($adress);
        // Clean up things like &amp;
        $clear = html_entity_decode($clear);
        // Strip out any url-encoded stuff
        $clear = urldecode($clear);
        // Replace non-AlNum characters with space
        $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
        // Replace Multiple spaces with single space
        $clear = preg_replace('/ +/', ' ', $clear);
        // Trim the string of leading/trailing space
        $clear = str_replace(' ', '', trim($clear));

        return $clear;
    }    
    
    public function checkFinalNumero2($nom_titulaire, $nom_complet)
    {
        $normalizedNomComplet = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet)));

        $explodedNomTitulaire = explode(' ', $nom_titulaire);
        $explodedNomComplet = explode(' ', $nom_complet);
        $intersectedResultFromFicheUser = array_filter(array_intersect($explodedNomTitulaire, $explodedNomComplet));
        $scores = $this->checkCoherence($nom_complet, $nom_titulaire);
        $scoresNext = $this->checkCoherence($normalizedNomComplet, $nom_titulaire); // Ajout de ceci
        // debug($normalizedNomComplet);
        // debug($nom_titulaire);
        // die();

        $result = [];
        
        if (
            (
                (strrpos($nom_complet, $nom_titulaire) !== false) || 
                (strrpos($normalizedNomComplet, $nom_titulaire) !== false) ||
                $intersectedResultFromFicheUser ||
                $this->checkExistingByExplode($nom_complet, $nom_titulaire)
            ) 
            && 
            ($scores->maxScore1 > 70 || $scores->maxScore2 > 80 || collection((array) $scoresNext)->avg() >= 50)
        ) {
            $result = ['result' => true, 'scores' => $scores, 'score_next' => $scoresNext];
        } 
        elseif($scores->maxScore1 > 70 || $scores->maxScore2 > 80) {
            $result = ['result' => true, 'scores' => $scores, 'score_next' => $scoresNext];
        }
        else {
            $result = ['result' => false, 'scores' => $scores, 'score_next' => $scoresNext];
        }

        return $result;
    }

    public function getLinkMessenger()
    {
        return 'https://m.me/102686648325815';
    }

    public function readUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result=curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function sendSMSMerci($numero, $operation_type = "retrait")
    {
        $this->Ussd = new \App\Controller\Component\UssdComponent(new \Cake\Controller\ComponentRegistry());
        $typeMobile = $this->checkMobile($numero);

        $messageMerci = $this->getMessageMerci();
        $messageTexte = trim($messageMerci['messageTexte']);
        $messageModele = $messageMerci['messageModele'];

        $nbSmsMerciDepotToday = $this->RemerciementSmss->find()->where(['operation_type' => 'depot', 'DATE_FORMAT(created, "%Y-%m-%d") =' => date('y-m-d')])->count();
        if ($operation_type == 'depot' && $nbSmsMerciDepotToday > 70) {
            return false; // on n'evoie plus
        }

        $smsDataToSave = ['numero' => $numero, 'msg_modele' => $messageModele, 'operation_type' => $operation_type];
        // $this->logginto($messageMerci, 'sms_to_send');
        // $this->logginto($smsDataToSave, 'sms_merci');

        if ($typeMobile == 'mvola') {
            $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
        } elseif ($typeMobile == 'orange') {
            $this->Ussd->setPort($this->getParam('port_sms_out_orange'));
        }

        // $m = "Tsy maninona ve raha mba asina 5 * sy comment fankasitrahana ity raha mety ? https://fr.trustpilot.com/review/moneiko.net merci :)";
        // $m = "Misaotra tompoko. Tsy maninona ve raha mba asina kely 5 * sy comment fankasitrahana ity raha mety ? https://fr.trustpilot.com/review/moneiko.net ? Merci bcp :)";
        // tsy mety 
        // $responseSend = $this->Ussd->send($numero, "Mankasitraka tompoko. Raha mba mety dia mba aza adino ny mampafantatra ny fisian'ny site moneiko.net amin'ireo namana sy akaiky, mazotoa tompoko ;)", $flashmode = false);
        // debug($responseSend);
        // die();  

        // Mety
        // $responseSend = $this->Ussd->send($numero, $m, $flashmode = false);
        // debug($this->Ussd->getPort());
        // debug($responseSend);
        // die();

        if (!$this->RemerciementSmss->find()->where($smsDataToSave)->first()) {

            if (!empty($messageTexte)) {
                // die('aoooo');
                $responseSend = $this->Ussd->send($numero, $messageTexte, $flashmode = false);


                // debug($messageTexte);
                // debug($resSend);
                // die();

                $smsDataToSave['msg'] = $messageTexte;
                $remerciement = $this->RemerciementSmss->newEntity($smsDataToSave, ['validate' => false]);
                if (!empty($responseSend)) {
                    $this->logginto($remerciement, 'sms_merci_response');
                }
                // $this->logginto($remerciement, 'sms_merci_entity_new');
                if(!$remerciement->getErrors()) {
                    $remerciement = $this->RemerciementSmss->save($remerciement);
                    // $this->logginto($remerciement, 'sms_merci_entity_saved');
                }
            }
        }
    }

    public function getMessageMerci()
    {
        $rand = rand(0, 7);
        $msg = null;
        $modele = null;

        if ($rand == 0) {
            $msg = $this->getParam('msg_merci');
            $modele = 1;
        } elseif($rand == 1) {
            $msg = $this->getParam('msg_merci_2');
            $modele = 2;
        } elseif($rand == 2) {
            $msg = $this->getParam('msg_merci_3');
            $modele = 3;
        } elseif($rand == 3) {
            $msg = $this->getParam('msg_merci_4');
            $modele = 4;
        } elseif($rand == 4) {
            $msg = $this->getParam('msg_merci_5');
            $modele = 5;
        } elseif($rand == 5) {
            $msg = $this->getParam('msg_merci_6');
            $modele = 6;
        } elseif($rand == 6) {
            $msg = $this->getParam('msg_merci_7');
            $modele = 7;
        } else {
            $msg = $this->getParam('msg_merci_8');
            $modele = 8;
        }

        $message = [
            "messageTexte" => $msg,
            "messageModele" => $modele,
        ];

        return $message;
    }  

    public function generateFreePortComVerificateur()
    {
        $arrayComs = ['port_sms_out_telma', 'port_sms_out_orange', 'port_orange', 'port_telma'];
        $port = $arrayComs[rand(0, count($arrayComs)-1)];
        $com = $this->getParam($port);
        $this->loadModel('Ports');
        $numPort = $this->Ports->findByCom($com)->first()->numero;
        $this->Parameters->updateAll(['port_titulaire' => $com, 'num_port_titulaire' => $numPort], ['id' => 1]);
    }


    public function getDriverByMobile($mobile = "telma")
    {
        $this->loadModel('Ports');
        $driver =  $this->Ports->findByMobile($mobile)->where(['type' => 'envoi'])->first()->letter;
        $letter = str_replace('\\', '', $driver);

        return $letter;
    }
}
