<?php 

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;

class HttpClient
{
    public function get($url, $timeout = 30.0)
    {
        $timeout = (float) $timeout;
        $http = new GuzzleClient(['verify' => false]);
        // $response = $http->request('GET', $url, ['connect_timeout' => $timeout]);
        // $response = $http->get($url);
        $response = $http->get($url, ['connect_timeout' => $timeout]);


        $result = $response->getBody()->getContents();
        if ($this->isJson($result)) {
            return json_decode($result, true);
        }
        return $result;
    }

    public function post($url, $data)
    {
        $http = new GuzzleClient(['verify' => false]);
        $response = $http->post($url, [
            'form_params' => $data
        ]);

        $result = $response->getBody()->getContents();
        if ($this->isJson($result)) {
            return json_decode($result, true);
        }
        return $result;
    }

    // envoie seulement la réponse et n'attent pas le resultat de la requête
    // si on veut attendre on rajoute $promise->wait(); à la fin
    public function getAsync($url)
    {
        $client = new GuzzleClient(['verify' => false]);
        $request = new Request('GET', $url);
        $promise = $client->getAsync($request);
        return $promise;
    }

    public function postAsync($url, $data)
    {
        $client = new GuzzleClient(['verify' => false]);

        // // debug($data);

        // $header = ['Content-Type' => 'application/x-www-form-urlencoded'];
        // $body = json_encode($data);
        // $request = new Request('POST', $url, $header, $body);

        // $response = $client->sendAsync($request);

        // return $response;



        // debug($data);
        // die('eol');
        // Envoyer la requête POST asynchrone sans attendre la réponse
        $promise = $client->postAsync($url, [
            'form_params' => $data,
            'timeout' => 5
        ]);
        // $promise->then();
        
        $promise->then(
            function (ResponseInterface $res) {
                echo $res->getStatusCode() . "\n";
            },
            function (RequestException $e) {
                echo $e->getMessage() . "\n";
                echo $e->getRequest()->getMethod();
            }
        );

        return $promise->wait();
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function sendGetRequestAsync($url) 
    {
        $ch = curl_init();

        // Configuration de l'URL cible
        curl_setopt($ch, CURLOPT_URL, $url);

        // Options pour exécuter la requête sans attendre la réponse
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);       // Délai d'attente très court
        curl_setopt($ch, CURLOPT_HEADER, false);    // Ne pas retourner les en-têtes de réponse
        curl_setopt($ch, CURLOPT_NOBODY, true);     // N'attend pas de corps de réponse
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true); // Créer une nouvelle connexion

        // Exécution de la requête
        curl_exec($ch);

        // Fermeture de la session cURL
        curl_close($ch);
    }

    public function sendPostRequestAsync($url, $data) 
    {
        // Initialiser cURL
        $ch = curl_init();

        // Configurer les options cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        // Options pour ignorer la réponse
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Ne pas attendre de retour
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1); // Temps d'attente minimum en millisecondes

        // Exécuter la requête cURL
        curl_exec($ch);

        // Fermer cURL
        curl_close($ch);
    }
}