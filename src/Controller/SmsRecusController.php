<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;
use App\Traits\AppTrait;
use Cake\Cache\Cache;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Chronos\Chronos;
use GuzzleHttp\Client as GuzzleClient;

class SmsRecusController extends AppController
{
    use AppTrait;

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadComponent('Ussd');
        $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("032");
        $this->port = $this->parameter->port_telma_sms;
        $type = $this->request->getQuery('type');
        $title = 'SMS - '.ucfirst($type);
        $this->set(compact('title'));
        $this->loadModel('Ozekimessageins');
        $this->loadModel('AuthSmss');
    }

    public function index()
    {
        $options = $this->request->getQuery();
        $type = $this->request->getQuery('type');
        $senders = $this->senders;
        $smss = $this->Ozekimessageins->find('Recus')->order(['senttime' => 'DESC']);
        // $smss = $this->Ozekimessageins->find()->order(['senttime' => 'DESC']);
        // debug($smss ->count());
        // die
        // ;

        // debug($smss->where(['id' => 145033, 'sender' => 'MVola'])->limit(12)->toArray());
        // debug($smss->where([/*'id' => 145033,*/ 'sender' => 'MVola'])->limit(12)->toArray());
        // debug($smss->limit(12)->toArray());
        // die();

        if ($keyword = @$options['keyword']) {
            if (@$options['search-mobile-sms']) {
                $keyword = str_replace(' ', '', $keyword);  
                $smss->where(['msg LIKE' => "%$keyword%"]);
            } else {
                $smss->where(['msg LIKE' => "%$keyword%"]);
            }
        }


        // debug($options);

        $date = @$options['date'];
        $created_at = @$options['created_at'];
        $expired_at = @$options['expired_at'];

        if ($expired_at && $created_at) {
            $smss->where([
                'receivedtime >=' => $created_at, 
                'receivedtime <=' => $expired_at
            ]);
        } 
        else if ($date) {
            $smss->where(['receivedtime LIKE' => "%$date%"]);
        }

        if (@$options['failed'] && $type == 'mvola') {
            $smss = $smss->find('MvolaNonRegles');
        } elseif(@$options['failed'] && $type == 'orange') {
            $smss = $smss->find('OrangeNonRegles');
        } elseif(@$options['failed'] && $type == 'airtel') {
            $smss = $smss->find('AirtelNonRegles');
        }



        $nbTotalSms = $this->Ozekimessageins->find()->count();
        $annulations = $this->Ozekimessageins->find()->where(['operator !=' => 'ONEX', 
            'OR' => [
                ['msg LIKE' => "%annulation%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
                ['msg LIKE' => "%annuler%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
            ],
            'operator !=' => 'ONEX'
        ])->order(['id' => 'DESC']);

        // debug($smss->toArray());
        $nbDemandeAnnulation = $annulations->count();


        if ($type == 'mvola') {
            $smss->where(['sender in' => $this->senders]);
        } elseif ($type == 'airtel') {
            $smss->where(['sender in' => $this->airtelSenders]);
        } elseif ($type == 'orange') {
            $smss->where(['sender in' => $this->orangeSenders]);
        }


        $smss = $smss->formatResults(function ($query) use($type)
        {
            return $query->each(function ($item) use($type)
            {
                if ($type == 'mvola') {
                    $smsData = $this->Ussd->extractMvolaRecuData($item->msg);
                } elseif ($type == 'airtel') {
                    $smsData = $this->Ussd->extractAirtelRecuData($item->msg);
                } elseif ($type == 'orange') {
                    $smsData = $this->Ussd->extractOrangeRecuData($item->msg);
                }

                $item->montant_recu = @$smsData['montant_recu'];
                $item->solde_actuel = @$smsData['solde_actuel'];
                $item->idpanier = (int) @$smsData['idpanier'];
                $item->raison = (float) @$smsData['raison'];
                $item->raison_text =  @$smsData['raison'];
                return $item;
            });
        });


        $smss = $this->paginate($smss, ['limit' => 10]);
        // debug($smss ->toArray());
        // die();
        $driverTelmaSms = Configure::read('driverTelmaSms');

        $this->set(compact('nbNonRelgesAirtel', 'nbNonRelgesOrange', 'type', 'options', 'smss', 'annulations', 'driverTelmaSms', 'nbNonRelgesMvola', 'nbTotalSms', 'nbDemandeAnnulation'));
    }

    public function getNbFailedOrder()
    {

        // sleep(3);

        $mvolaNonRegles = $this->Ozekimessageins->find('MvolaNonRegles')->find('OneMinuteAgo');
        $orangeNonRegles = $this->Ozekimessageins->find('OrangeNonRegles')->find('OneMinuteAgo');
        $airtelNonRegles = $this->Ozekimessageins->find('AirtelNonRegles')->find('OneMinuteAgo');

        $nb_non_relges_mvola = $nbNonRelgesMvola = $mvolaNonRegles->count();
        $nb_non_relges_orange = $nbNonRelgesOrange = $orangeNonRegles->count();
        $nb_non_relges_airtel = $nbNonRelgesAirtel = $airtelNonRegles->count();

        $body = [
            'nb_total_mvola' => $nb_non_relges_mvola,
            'nb_total_orange' => $nb_non_relges_orange,
            'nb_total_airtel' => $nb_non_relges_airtel
        ];

        $idsMvola = collection($mvolaNonRegles)->extract('id')->toArray();
        $idsOrange = collection($orangeNonRegles)->extract('id')->toArray();
        $idsAirtel = collection($airtelNonRegles)->extract('id')->toArray();

        $ids = Hash::merge($idsMvola, $idsOrange, $idsAirtel);

        $body['details'] = [
            'sms_ids_mvola' => $idsMvola,
            'sms_ids_orange' => $idsOrange,
            'sms_ids_airtel' => $idsAirtel,
            'sms_ids' => $ids
        ];

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function majAndResetRaisonSms()
    {
        $options = $this->request->getQuery();
        $senders = $this->senders;
        $smss = $this->Ozekimessageins->find('Recus')->where(['paiement_status' => 'error'])->order(['senttime' => 'DESC']);
        if ($keyword = @$options['keyword']) {
            $smss->where(['msg LIKE' => "%$keyword%"]);
        }

        $date = @$options['date'];
        $created_at = @$options['created_at'];
        $expired_at = @$options['expired_at'];
        $correctRaison = @$options['raison'];


        $smss->where([
            'receivedtime >=' => $created_at, 
            'receivedtime <=' => $expired_at
        ]);
        // pr($created_at);
        // pr($expired_at);
        // pr($sms ->toArray());
        // die();

        // die();

        $body = ['status' => false];
        if ($smss->count() == 1) {
            $sms = $smss->first();
            $mvolaMoneyData = $this->Ussd->extractMvolaRecuData($sms->msg);
            $incorrectRaison = $mvolaMoneyData['raison'];
            $newCorrectMsg = str_replace($incorrectRaison, $correctRaison, $sms->msg);
            $this->Ozekimessageins->updateAll([
                'msg' => $newCorrectMsg,
                'msg_backup' => $sms->msg,
            ], ['id' => $sms->id]);
            $body = ['status' => true];
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function markSms($id)
    {
        $sms = $this->Ozekimessageins->findById($id)->first();
        $marked = $sms->marked == 1 ? 0 : 1;
        $smss = $this->Ozekimessageins->updateAll(['marked' => $marked], ['id' => $sms->id]);
        $body = ['marked' => $marked];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function majestate($id)
    {
        $sms = $this->Ozekimessageins->findById($id)->first();

        Cache::delete('panier_sms_id_'.$sms->id);
        $paiement_status = $sms->paiement_status == 'success' ? 'error' : 'success';
        $smss = $this->Ozekimessageins->updateAll(['paiement_status' => $paiement_status], ['id' => $sms->id]);
        $body = ['paiement_status' => $paiement_status];
        $this->deleteCacheStat($sms->get('Mobile'));
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function deleteSms($id)
    {
        $smss = $this->Ozekimessageins->deleteAll(['id' => $id]);
        $body = ['result' => $smss];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function editSms($id)
    {
        $sms = $this->Ozekimessageins->findById($id)->select(['id', 'msg'])->first();
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            
            $sms = $this->Ozekimessageins->patchEntity($sms, $data, ['validate' => false]);
            $this->Ozekimessageins->save($sms);
            $body = ['status' => 'success'];
            
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function ajusterSolde($type = "034")
    {
        $now = Chronos::now();

        $this->loadModel('Smss');
        $body = ['status' => 'error', 'solde' => 0];

        if ($type == "034") {
            
            $this->Ussd->port = $this->parameter->port_telma; // port devant pour evider le bug ressource utilisée par un autre processeur

            $lastSms = $this->Ozekimessageins->find('Recus')->where(['sender IN' => $this->senders])->order(['senttime' => 'DESC'])->first();
            $smsData = $this->Ussd->extractMvolaRecuData($lastSms->msg);
            $lastSmsSolde = @number_format($smsData['solde_actuel'], 0, ',', ' ');

            $resUssd = $this->Ussd->run("#111#", ['1', '#*6*1', '2958']);
            // debug($resUssd);
            // die();
            // $resUssd = "Votre solde MVola est de 4 400 Ar. Ref:1764375758. Tapez 1 pour recevoir le solde par SMS:";
            $soldePuceActuel = (int) str_replace(' ', '', $this->Ussd->extractBetween($resUssd, 'Votre solde MVola est de ', ' Ar. Ref'));


            if ($soldePuceActuel > 0) {
                $newSmsMsg = str_replace($lastSmsSolde, $soldePuceActuel, $lastSms->msg);
                $lastSms = $this->Ozekimessageins->patchEntity($lastSms, ['msg' => $newSmsMsg, 'solde_backup' => $smsData['solde_actuel'], 'marked' => 1], ['validate' => false]);
                $this->Ozekimessageins->save($lastSms);
                $body = ['status' => 'success', 'solde' => str_replace(' ', '', $soldePuceActuel).' Ariary'];
            } else {
                $this->logginto($resUssd, 'ajustement_solde', $erase = true);
            }

        }
        elseif ($type == "032") {

            $http = new Client();
            $url = Router::url(['controller' => 'Tdb', 'action' => 'saveAllSms', "032"], true);
            $saveAllSms = $http->get($url)->json;

            if ($saveAllSms['status'] == true) {
                $subDays = $now->subDays(1);
                $this->loadModel('Smss');
                $sms = $this->Smss->findByMobile('orange')->where(['content LIKE' => "%Votre transfert de %", 'date >=' => $subDays])->last();
                // $dataOrange = $this->Ussd->getSoldeFromTransfert($sms->content);

                $lastSmsOrangemoney = $this->Ozekimessageins->find('Recus')->where(['sender IN' => $this->orangeSenders])->order(['senttime' => 'DESC'])->first();
                $smsData = $this->Ussd->extractOrangeRecuData($lastSmsOrangemoney->msg);
                $soldeActuel = (int) $smsData['solde_actuel'];
                // $soldeActuel = (int) 0;

                if ($soldeActuel) {
                    // $newSmsMsg = str_replace($soldeActuel, $dataOrange['solde'], $lastSmsOrangemoney->msg);
                    $newSmsMsg = str_replace($soldeActuel, 0, $lastSmsOrangemoney->msg);
                    $lastSmsOrangemoney = $this->Ozekimessageins->patchEntity($lastSmsOrangemoney, ['msg' => $newSmsMsg, 'solde_backup' => $soldeActuel, 'marked' => 1], ['validate' => false]);
                    $this->Ozekimessageins->save($lastSmsOrangemoney);
                    $body = ['status' => 'success', 'solde' => str_replace(' ', '', $soldeActuel).' Ariary'];
                }

            }
        }
        elseif ($type == "033") {



            $http = new Client();
            $now = Chronos::now();
            $subDays = $now->subDays(1);
            $sms = $this->Smss->findByMobile('airtel')->where(['content LIKE' => "%Votre solde disponible%"/*, 'date >=' => $subDays*/])->last();

            $now = \Cake\Chronos\Chronos::now();
            $diff = $now->diffInMinutes($sms->date);
            // debug($sms->date->wasWithinLast('5 minutes'));
            // // debug($sms);
            // debug($diff);
            // die();
            
            if (!$sms->date->wasWithinLast('5 minutes')) {
                // $this->Ussd->port = $this->parameter->port_airtel; // port devant pour evider le bug ressource utilisée par un autre processeur
                // $resUssd = $this->Ussd->run("*436*6*1#", ['2958']);
                // $url = Router::url(['controller' => 'Tdb', 'action' => 'saveAllSms', "033"], true);
                // $saveAllSms = $http->get($url)->json;

            }

            // if ($saveAllSms['status'] == true) {
            if (true) {
                
                $sms = $this->Smss->findByMobile('airtel')->where(['content LIKE' => "%Votre solde disponible%"/*, 'date >=' => $subDays*/])->last();
    
                if ($sms) {
                    // $dataAirtel = $this->Ussd->getSoldeFromTransfert($sms->content, "airtel");

                    $lastSmsAirtelmoney = $this->Ozekimessageins->find()->where(['sender IN' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar %"])->order(['senttime' => 'DESC'])->first();
                    $smsData = $this->Ussd->extractAirtelRecuData($lastSmsAirtelmoney->msg);
                    // debug($smsData);
                    // die();
                    $soldeActuel = (int) $smsData['solde_actuel'];

                    if ($soldeActuel) {
                        // $newSmsMsg = str_replace($soldeActuel, $dataAirtel['solde'], $lastSmsAirtelmoney->msg);
                        $newSmsMsg = str_replace($soldeActuel, 0, $lastSmsAirtelmoney->msg);
                        $lastSmsAirtelmoney = $this->Ozekimessageins->patchEntity($lastSmsAirtelmoney, ['msg' => $newSmsMsg, 'solde_backup' => $soldeActuel, 'marked' => 1], ['validate' => false]);
                        $this->Ozekimessageins->save($lastSmsAirtelmoney);
                        $body = ['status' => 'success', 'solde' => str_replace(' ', '', $soldeActuel).' Ariary'];
                    }
                }

            }
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function sendSms()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $numero = $data['numero'];
            $raison = $data['raison'];
            $montant_recu = $data['montant_recu'];
            $solde_actuel = $data['solde_actuel'];
            // $msg = "1/2 Vous avez recu $montant_recu Ar de la part de Loum (0341035571) le 25/09/21 a 20:27. Raison: $raison. Votre solde est de $solde_actuel Ar. Ref: 1174698613";
            $msg = "Bella ? ".uniqid();
            $this->Ussd->port = $this->parameter->port_telma;
            $res = $this->Ussd->send($data['numero'], $msg);
            echo trim($res);
        }

        die();
    }

    /**
     * Pour les simulations de paiement orange
     * @return [type] [description]
     */
    public function sendsmsfromOrange()
    {
        $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Vous avez recu un transfert de%", 'sender LIKE' => "%26132%"])->order(['senttime' => 'DESC'])->first();
        if (is_null($sms)) {
            die('sms non trouvé');
        }


        // die();
        $lastSmsData = $this->Ussd->extractOrangeRecuData($sms->msg);
        $transId = substr($lastSmsData['ref'], -4);
        // debug($sms);
        // die();

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $numero = $data['numero'];
            $numero_paiement = $data['numero_paiement'];
            $montant_recu = $data['montant_recu'];
            $solde_actuel = $lastSmsData['solde_actuel']+$montant_recu;
            // die();
            // $trans_id = "PP220712.1414.A".($transId+1);
            $trans_id = "PP220712.1414.A".strtoupper(uniqid());
            // $msg = "1/2 Vous avez recu $montant_recu Ar de la part de Loum (0341035571) le 25/09/21 a 20:27. Raison: $raison. Votre solde est de $solde_actuel Ar. Ref: 1174698613";
            $msg = "Vous avez recu un transfert de ".$montant_recu."Ar venant du $numero_paiement Nouveau Solde: ".$solde_actuel."Ar.  Trans Id: $trans_id. Orange Money vous remercie.";
            // debug($lastSmsData);
            // debug($msg);
            // die();
            // $msg = "Bella ? ".uniqid();
            $this->Ussd->port = $this->parameter->port_orange;
            $res = $this->Ussd->send($data['numero'], $msg);
            // sleep(10);
            // $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Vous avez recu un transfert de%", 'sender LIKE' => "%+26132%"])->last();
            // echo trim($sms->msg);
            echo trim($res);
        }

        die();
    }

    /**
     * Pour les simulations de paiement airtel
     * @return [type] [description]
     */
    public function sendsmsfromAirtel()
    {
        $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Vous avez recu Ar%"/*, 'sender LIKE' => "%26133%"*/])->order(['senttime' => 'DESC'])->first();
        $lastSmsData = $this->Ussd->extractAirtelRecuData($sms->msg);
        $transId = substr($lastSmsData['ref'], -4);
        // debug($sms->msg);
        // debug($lastSmsData);
        // debug($transId);
        // die();

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $numero = $data['numero'];
            $numero_paiement = $data['numero_paiement'];
            $montant_recu = $data['montant_recu'];
            $solde_actuel = $lastSmsData['solde_actuel']+$montant_recu;
            $day = date('ymd.Hi');
            $trans_id = "PP$day.A".($transId+1);
            // debug($trans_id);
            // die();
            // $msg = "1/2 Vous avez recu $montant_recu Ar de la part de Loum (0341035571) le 25/09/21 a 20:27. Raison: $raison. Votre solde est de $solde_actuel Ar. Ref: 1174698613";
            // $msg = "Vous avez recu un transfert de ".$montant_recu."Ar venant du $numero_paiement Nouveau Solde: ".$solde_actuel."Ar.  Trans Id: $trans_id. Orange Money vous remercie.";
            $msg = "Vous avez recu Ar ".$montant_recu." du $numero_paiement pour NA. Le solde de votre compte est Ar ".$solde_actuel.". Trans ID: $trans_id";
            // debug($lastSmsData);
            // debug($msg);
            // die();
            // $msg = "Bella ? ".uniqid();
            $this->Ussd->port = $this->parameter->port_airtel;
            $res = $this->Ussd->send($data['numero'], $msg);
            // sleep(10);
            // $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Vous avez recu un transfert de%", 'sender LIKE' => "%+26132%"])->last();
            // echo trim($sms->msg);
            echo trim($msg).'<hr>'.date('d/m/Y H:i:s', strtotime($sms->senttime));
        }

        die();
    }

    public function sendCustomSmsfromOrange($port = null)
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();

            $numero = $data['numero'];

            if ($port == null) {
                $this->Ussd->port = $this->parameter->port_orange;
            } else {
                $this->Ussd->setPort($port);
            }
            $res = $this->Ussd->send($numero, $data['sms'], $data['is_flashmode']);

            $mobile = $this->Ussd->checkMobile2($numero);

            if (strrpos($res, 'CmsError: CMS 302') !== false) {
                $driver =  $this->Ports->findByMobile($mobile)->where(['type' => 'sms'])->first()->letter;
                $letter = str_replace('\\', '', $driver);
                $this->Ussd->resetUsb($this->getDriverByMobile($mobile));
            }
            // sleep(10);
            // $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Vous avez recu un transfert de%", 'sender LIKE' => "%+26132%"])->last();
            // echo trim($sms->msg);
            echo trim($res);
            $this->logginto($res, 'sms/res_orange');
        }

        die();
    }

    public function verifierTransaction()
    {
        $result = [];
        // $host = 'http://127.0.0.1/moneyko';
        $host = $this->serveurDisant;
        $url = '';

        if ($this->request->is(['post'])) {
            $smsTypes = Configure::read('smsType');
            $data = $this->request->getData();
            $raison = $data['raison'];
            $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%$raison%"])->select(['id', 'msg'])->first();
            $verificationRaison = $this->Ussd->extractBetween($sms, '. Raison: ', '. Votre solde');

            $panierId = substr($verificationRaison, 1);

            if (@($smsTypes[$verificationRaison[0]])) {
                $panierType = $smsTypes[$verificationRaison[0]];
                $http = new Client();
                $url = $host.'/exchanger/support/verificationTransaction/'.$panierId;
                $res = $http->get($url, [], $this->noSslParams)->json;
                $result = @$res['result'];

                $url = $host.'/paiement-mvola/'.$result['id'].'?open=1&dev_mode=1';
            }
        }

        $this->set(compact('result', 'url'));
    }

    public function rechercher()
    {
        $host = $this->serveurDisant;
        $options = $this->request->getQuery();
        if (@$options['date']) {
            $options['date'] = date('Y-m-d', strtotime($options['date']));
        }
        $http = new Client();
        $query = http_build_query($options);
        $url = $host.'/exchanger/support/getPaniers?'.$query;

        // debug($http->get($url, [], $this->noSslParams)->body);
        // die();

        $idpaniers = $http->get($url, [], $this->noSslParams)->json;

        // debug($idpaniers);
        // die();
        // echo $idpaniers;
        // die();
        $host = $this->serveurDisant;

        $this->set(compact('idpaniers', 'host', 'options'));
    }

    public function ajusteMontant($type = 'mvola', $smsId)
    {
        $body = ['status' => false];

        $http = new Client();

        $serveurDisant = $this->serveurDisant;
        // $serveurDisant = 'http://127.0.0.1/moneyko/';

        $currentSms = $this->Ozekimessageins->findById($smsId)->first();
        $previousSms = $this->Ozekimessageins
            ->find()
            ->order(['id' => 'DESC'])
        ;

        if ($type == 'mvola') {
            $previousSms = $previousSms->where(['sender in' => $this->senders, 'msg LIKE' => "%Ar recu de%", 'id <' => $smsId])->first();
            $smsData = $this->Ussd->extractMvolaRecuData($currentSms->msg);
        } elseif ($type == 'airtel') {
            $previousSms = $previousSms->where(['sender in' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'id <' => $smsId])->first();
            $smsData = $this->Ussd->extractAirtelRecuData($currentSms->msg);
        } elseif ($type == 'orange') {
            $previousSms = $previousSms->where(['sender in' => $this->orangeSenders, 'msg LIKE' => "%Vous avez recu un transfert%", 'senttime <' => $currentSms->senttime])->first();
            $smsData = $this->Ussd->extractOrangeRecuData($currentSms->msg);
        }


        $currentSms->montant_recu = @$smsData['montant_recu'];
        $currentSms->solde_actuel = @$smsData['solde_actuel'];
        $currentSms->idpanier = (int) @$smsData['idpanier'];
        $currentSms->raison = (int) @$smsData['raison'];
        $currentSms->numero = (int) @$smsData['numero'];

        if ($type == 'mvola') {
            $url = $serveurDisant.'/exchanger/support/verificationTransactionMvola/';
            $res = $http->post($url, [
                'montant' => $smsData['montant_recu'],
                'numero' => $smsData['numero'],
                'received_at' => $currentSms->senttime,
            ], $this->noSslParams)->json;
            $result = @$res['result'];
            $smsData = $this->Ussd->extractMvolaRecuData($previousSms->msg);
        } elseif ($type == 'airtel') {
            $url = $serveurDisant.'/exchanger/support/verificationTransactionAirtel/'.$currentSms->idpanier;
            $res = $http->post($url, [
                'montant' => $smsData['montant_recu'],
                'numero' => $smsData['numero'],
                'received_at' => $currentSms->senttime,
            ], $this->noSslParams)->json;

            $result = @$res['result'];
            $smsData = $this->Ussd->extractAirtelRecuData($previousSms->msg);
        } elseif ($type == 'orange') {
            $url = $serveurDisant.'/exchanger/support/verificationTransactionOrange/'.$currentSms->idpanier;
            $res = $http->post($url, [
                'montant' => $smsData['montant_recu'],
                'numero' => $smsData['numero'],
                'received_at' => $currentSms->senttime,
            ], $this->noSslParams)->json;
            $result = @$res['result'];
            $smsData = $this->Ussd->extractOrangeRecuData($previousSms->msg);
        }

        $previousSms->montant_recu = @$smsData['montant_recu'];
        $previousSms->solde_actuel = @$smsData['solde_actuel'];
        $previousSms->idpanier = (int) @$smsData['idpanier'];
        $previousSms->raison = (int) @$smsData['raison'];

        $soldeActuelAjusted = $previousSms->solde_actuel + $result['montant'];

        $formatSoldeActuel = in_array($type, ['mvola', 'airtel']) ? number_format($currentSms->solde_actuel, 0, ',', ' ') : $currentSms->solde_actuel.'Ar';
        $formatSoldeActuelAjusted = in_array($type, ['mvola', 'airtel']) ? number_format($soldeActuelAjusted, 0, ',', ' ') : $soldeActuelAjusted.'Ar';
        $formatCurrentMontant = in_array($type, ['mvola', 'airtel']) ? number_format($result['montant'], 0, ',', ' ') : $result['montant'];
        $montantRecuCurrentSmss = in_array($type, ['mvola', 'airtel']) ? number_format($currentSms->montant_recu, 0, ',', ' ') : $currentSms->montant_recu;

        $smsAjustedMontantRecu = str_replace($montantRecuCurrentSmss, $formatCurrentMontant, $currentSms->msg);

        // debug('type: '.$type);
        // debug('soldeActuelAjusted: '.$soldeActuelAjusted);
        // debug('formatSoldeActuel: '.$formatSoldeActuel);
        // debug('formatSoldeActuelAjusted: '.$formatSoldeActuelAjusted);
        // debug('formatCurrentMontant: '.$formatCurrentMontant);
        // debug('montantRecuCurrentSmss: '.$montantRecuCurrentSmss);
        // debug('smsAjustedMontantRecu: '.$smsAjustedMontantRecu);


        if ($type == 'mvola') {
            $smsAjustedSolde = str_replace('Solde : '.$formatSoldeActuel, 'Solde : '.$formatSoldeActuelAjusted, $smsAjustedMontantRecu);
            // debug($smsAjustedSolde);
            // die();
        }

        if ($type == 'airtel') {
            $montantRecuCurrentSmss = $currentSms->montant_recu;
            // debug('montantRecuCurrentSmss : ' . $montantRecuCurrentSmss);
            $formatCurrentMontant = $result['montant'];
            // debug('formatCurrentMontant : ' . $formatCurrentMontant);
            $formatSoldeActuel = $currentSms->solde_actuel;
            // debug('formatSoldeActuel : ' . $formatSoldeActuel);
            $formatSoldeActuelAjusted = $soldeActuelAjusted;
            // debug('formatSoldeActuelAjusted : ' . $formatSoldeActuelAjusted);
            $smsAjustedMontantRecu = str_replace($montantRecuCurrentSmss, $formatCurrentMontant, $currentSms->msg);
            // debug('smsAjustedMontantRecu : ' . $smsAjustedMontantRecu);
            $smsAjustedSolde = str_replace('Ar '.$formatSoldeActuel, 'Ar '.$formatSoldeActuelAjusted, $smsAjustedMontantRecu);
            // debug('smsAjustedSolde : ' . $smsAjustedSolde);
            // die();
        }

        if ($type == 'orange') {
            $part2SmsAjustedMontantRecu = $this->Ussd->extractBetween($smsAjustedMontantRecu, 'Nouveau Solde: ', '');
            $part1SmsAjustedMontantRecu = explode('Nouveau Solde', $smsAjustedMontantRecu)[0];
            // debug($formatSoldeActuel);
            // debug($formatSoldeActuelAjusted);
            // debug($part2SmsAjustedMontantRecu);
            $smsAjustedSoldePart2 = str_replace($formatSoldeActuel, $formatSoldeActuelAjusted, $part2SmsAjustedMontantRecu);
            $smsAjustedSolde = $part1SmsAjustedMontantRecu.'Nouveau Solde: '.$smsAjustedSoldePart2;
            // debug($smsAjustedSolde);
            // die();
        }
        // debug($smsAjustedSolde);
        // debug($smsAjustedMontantRecu);
        // die();

        $data = ['msg' => $smsAjustedSolde];

        if ($currentSms->is_backup_sms == 0) {
            $data['msg_backup'] = $currentSms->msg;
            $data['is_backup_sms'] = 1;
        }

        $currentSms = $this->Ozekimessageins->patchEntity($currentSms, $data, ['validate' => false]);
        if ($this->Ozekimessageins->save($currentSms)) {
            $body = ['status' => true];
        };

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function restaurerSms($smsId)
    {
        $body = ['status' => false];

        $currentSms = $this->Ozekimessageins->findById($smsId)->first();
        $data = [
            'msg' => $currentSms->msg_backup
        ];

        $currentSms = $this->Ozekimessageins->patchEntity($currentSms, $data, ['validate' => false]);
        if ($this->Ozekimessageins->save($currentSms)) {
            $body = ['status' => 'success'];
        };
        return $this->response->withType('application/json')->withStringBody(json_encode($body));

    }

    public function getPendingTx($type = 'mvola2')
    {
        $http = new Client();
        $nb = $http->get($this->serveurDisant."/exchanger/support/getNbPeningPaniers/$type")->json['nb'];
        $body = ['nb' => $nb];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));

    }

    /**
     * [playPauseDepotMvola description]
     * @param  boolean $mode false = play pause, 1 pause, 2 play
     * @return [type]        [description]
     */
    public function playPauseDepotMvola($mode = false)
    {
        $http = new Client();
        $result = $http->get($host = $this->serveurDisant."/exchanger/apis/playPauseDepotMvola/$mode")->json;
        $this->generateFreePortComVerificateur();
        // $result = $http->get("http://127.0.0.1/moneyko/exchanger/support/desactiverMvolaPaiement")->json;
        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * [playPauseDepotOrange description]
     * @param  boolean $mode false = play pause, 1 pause, 2 play
     * @return [type]        [description]
     */
    public function playPauseDepotOrange($mode = false)
    {
        $http = new Client();
        $result = $http->get($this->serveurDisant."/exchanger/apis/playPauseDepotOrange/$mode")->json;
        $this->generateFreePortComVerificateur();
        // $result = $http->get("http://127.0.0.1/moneyko/exchanger/support/desactiverMvolaPaiement")->json;
        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * [playPauseDepotAirtel description]
     * @param  boolean $mode false = play pause, 1 pause, 2 play
     * @return [type]        [description]
     */
    public function playPauseDepotAirtel($mode = false)
    {
        $http = new Client();
        $result = $http->get($this->serveurDisant."/exchanger/apis/playPauseDepotAirtel/$mode")->json;
        $this->generateFreePortComVerificateur();
        // $result = $http->get("http://127.0.0.1/moneyko/exchanger/support/desactiverMvolaPaiement")->json;
        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function updateCheckLocalConnexion()
    {
        $http = new Client();
        $result = $http->get("https://moneiko.net/exchanger/apis/updateAutoCheckLocalConnexion")->json;
        return $this->response->withType('application/json')->withStringBody(json_encode($result));
    }

    public function getParams()
    {
        $r = $this->checkUrl();
        $result = null;
        if ($r) {
            $http = new Client();
            $url = "$this->serveurDisant/exchanger/apis/getParams";
            $result = $http->post($url, [], $this->noSslParams)->json;
            // $result = $http->get($url, [], $this->noSslParams)->body();
            // echo $url;
            // echo $result;
            // die();
        }
        // $result = $http->get("http://127.0.0.1/moneyko/exchanger/apis/getParams")->json;
        // $result = $http->get("https://moneiko.net/exchanger/apis/getParams", [], $this->noSslParams)->body;
        // $result = $http->get("http://127.0.0.1/moneyko/exchanger/apis/getParams")->json;

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function numProdsList()
    {
        $prodParams = json_decode($this->getParams());
        $this->set(compact('prodParams'));
    }


    public function autoMajNumEtSolde()
    {
        $param = $this->getParam();
        $http = new Client();

        $serverParam = $http->get("https://moneiko.net/exchanger/apis/getParams")->json;
        // $serverParam = $http->get("http://127.0.0.1/moneyko/exchanger/apis/getParams")->json;

        $result = ['modified' => false];

        // if ($serverParam['numero_depot_telma'] != $param['last_telma_server_numero']) {
        //     $data = [
        //         'last_telma_server_numero' => $serverParam['numero_depot_telma']
        //     ];
        //     $param = $this->Parameters->patchEntity($param, $data, ['validate' => false]);
        //     $param = $this->Parameters->save($param);
        //     $resultNewSolde = Router::url(['controller' => 'Tdb', 'action' => 'majLastSoldeMvola'], true);

        //     $resultNewSolde = $http->get($resultNewSolde)->json;
        //     $result = ['modified' => true, 'result' => $resultNewSolde];

        //     $soldeMvolaAjuste = round($resultNewSolde['solde_mvola']/1.01, 0);
        //     $soldeMvolaAjuste = round($soldeMvolaAjuste/100)*100;

        //     if ($soldeMvolaAjuste > 3000000) {
        //         $soldeMvolaAjuste = 3000000;
        //     }
        //     $http->post("https://moneiko.net/exchanger/apis/updateParams", ['solde_mvola' => $soldeMvolaAjuste])->json;

        // }
        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function switchNumero($type ="034", $try = 0)
    {
        $param = $this->getParam();
        $http = new Client();
        $response = [];
        $data = [];
        $host = $this->serveurDisant;
        // $host = "http://127.0.0.1/moneyko";


        if ($type == "034") {
            $data['newNumber'] = $param['last_telma_server_numero'] == '0347085930' ? '0341035571' : '0347085930';

            $url = Router::url(['controller' => 'Tdb', 'action' => 'presetUssd', "034", 'code' => "*120#"], true);
            // debug($url);
            // die();
            $res = $http->get($url)->json;
            $urlMajMvola = Router::url(['controller' => 'Tdb', 'action' => 'majLastSoldeMvola'], true);
            $http->get($urlMajMvola)->json;
            $numeroTelma = $this->Ussd->extractBetween($res['msg'], 'Votre numero est ', '<br />');

            if (strlen($numeroTelma) == 10) {
                $data = $http->post("https://moneiko.net/exchanger/apis/updateNumTelma", ['numero_depot_telma' => $numeroTelma])->json;
                // $data = $http->post("https://moneiko.net/exchanger/apis/updateNumTelma", ['numero_depot_telma' => $numeroTelma])->body;
                // echo ($data);
                // die();
            }
        } elseif($type == "032") {

            $url = Router::url(['controller' => 'Tdb', 'action' => 'presetUssd', "032", 'code' => "#888#"], true);
            $res = $http->get($url)->json;
            $numeroOrange = $this->Ussd->extractBetween($res['msg'], '261', 'Session fermée');
            $numeroOrange = '0'.str_replace('<br />', '', $numeroOrange);

            $lastSmsOrangemoneyMarked = $this->Ozekimessageins->find()->where(['marked' => 1, 'sender IN' => $this->orangeSenders, 'msg LIKE' => "%Vous avez recu%"])->order(['senttime' => 'DESC'])->first();
            $previousLastSmsOrangemoneyMarked = $this->Ozekimessageins->find()->where(['sender IN' => $this->orangeSenders, 'msg LIKE' => "%Vous avez recu%", 'id <' => $lastSmsOrangemoneyMarked->id])->last();
            $smsData = $this->Ussd->extractOrangeRecuData($previousLastSmsOrangemoneyMarked->msg);
            $lastSoldeActuel = (int) $smsData['solde_actuel'];


            if (strlen($numeroOrange) == 10) {

                $siteParam = $http->get("https://moneiko.net/exchanger/apis/getParams", [], $this->noSslParams)->json;
                $data = $http->post("https://moneiko.net/exchanger/apis/updateNumOrange", ['numero_depot_orange' => $numeroOrange], $this->noSslParams)->json;
                if ($siteParam['numero_depot_orange'] == $numeroOrange) {

                    if (is_integer($lastSoldeActuel) && $lastSoldeActuel > 0) {
                        $dataSolde = ['last_solde_orange' => $lastSoldeActuel, 'last_maj_solde_orange_at' => Chronos::now()];
                        $parameter = $this->Parameters->patchEntity($this->parameter, $dataSolde, ['validate' => false]);

                        if(!$parameter->getErrors()) {
                            $res = (bool) $this->Parameters->save($parameter);

                            $soldeOrangeAjuste = round($lastSoldeActuel/1.01, 0);
                            $soldeOrangeAjuste = round($soldeOrangeAjuste/100)*100;

                            if ($soldeOrangeAjuste > 3000000) {
                                $soldeOrangeAjuste = 3000000;
                            }
                            $http->post("https://moneiko.net/exchanger/support/updateParams", ['solde_mobile' => $soldeOrangeAjuste])->json;
                        }
                    }
                }

                // $data = $http->post("https://moneiko.net/exchanger/apis/updateNumOrange", ['numero_depot_orange' => $numeroOrange])->body;
                // echo ($data);
                // die();
                // $data = $http->post("http://127.0.0.1/moneyko/exchanger/apis/updateNumOrange", ['numero_depot_orange' => $numeroOrange])->json;
            }
        } elseif($type == "033") {
            $url = Router::url(['controller' => 'Tdb', 'action' => 'presetUssd', "033", 'code' => "*123#"], true);
            $res = $http->get($url)->json;
            // debug($res);
            // die();

            $tries = 2;
            if (strrpos($res['msg'], 'Application Down') !== false) {
                if ($try < $tries) {
                    $try++;
                    return $this->switchNumero('033');
                }
            }

            $numeroAirtel = "0".$this->Ussd->extractBetween($res['msg'], '261', '. ');

            $lastSmsAirtelmoneyMarked = $this->Ozekimessageins->find()->where(['marked' => 1, 'sender IN' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%"])->order(['senttime' => 'DESC'])->first();
            $previousLastSmsAirtelmoneyMarked = $this->Ozekimessageins->find()->where(['sender IN' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'id <' => $lastSmsAirtelmoneyMarked->id])->last();
            $smsData = $this->Ussd->extractAirtelRecuData($previousLastSmsAirtelmoneyMarked->msg);
            $lastSoldeActuel = (int) $smsData['solde_actuel'];


            if (strlen($numeroAirtel) == 10) {

                $siteParam = $http->get("$host/exchanger/apis/getParams", [], $this->noSslParams)->json;
                $data = $http->post("$host/exchanger/apis/updateNumAirtel", ['numero_depot_airtel' => $numeroAirtel], $this->noSslParams)->json;
                if ($siteParam['numero_depot_airtel'] == $numeroAirtel) {

                    if (is_integer($lastSoldeActuel) && $lastSoldeActuel > 0) {
                        $dataSolde = ['last_solde_airtel' => $lastSoldeActuel, 'last_maj_solde_airtel_at' => Chronos::now()];
                        $parameter = $this->Parameters->patchEntity($this->parameter, $dataSolde, ['validate' => false]);

                        if(!$parameter->getErrors()) {
                            $res = (bool) $this->Parameters->save($parameter);

                            $soldeAirtelAjuste = round($lastSoldeActuel/1.01, 0);
                            $soldeAirtelAjuste = round($soldeAirtelAjuste/100)*100;

                            if ($soldeAirtelAjuste > 3000000) {
                                $soldeAirtelAjuste = 3000000;
                            }
                            $http->post("https://moneiko.net/exchanger/support/updateParams", ['solde_airtel' => $soldeAirtelAjuste])->json;
                        }
                    }
                }

                // $data = $http->post("https://moneiko.net/exchanger/apis/updateNumAirtel", ['numero_depot_airtel' => $numeroAirtel])->body;
                // echo ($data);
                // die();
                // $data = $http->post("http://127.0.0.1/moneyko/exchanger/apis/updateNumAirtel", ['numero_depot_airtel' => $numeroAirtel])->json;
            }
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($data));
    }

    public function searchIdPanierOrange($type, $smsId, $typeResult = null)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        // $http = new Client();
        $http = new \HttpClient();
        $host = $this->serveurDisant;
        // $host = "http://127.0.0.1/moneyko";
        $urlVerification = $host.'/exchanger/support/verificationTransactionOrange';
        $orangeMoneyData = $this->Ussd->extractOrangeRecuData($sms['msg']);

        $dataToCheck = [
            'montant' => $orangeMoneyData['montant_recu'],
            'numero' => $orangeMoneyData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $orangeMoneyData['ref'],
        ];

        $senttime = Chronos::parse($sms->senttime);

        $smsData = (object) $dataToCheck;
        $smss = $this->Ozekimessageins->find()->where(['msg LIKE' => "%$smsData->numero%", 'senttime LIKE ' => '%'.$senttime->format('Y-m-d').'%'])->order(['id' => 'DESC']);
        $smss = $this->getSmsWithDatas($smss, $type);
        $nbAssociatedSmss = $smss->count();

        // $panierOnServer = (new Client())->post($urlVerification, $dataToCheck, $this->noSslParams)->body();
        // echo ($panierOnServer);
        // die();

        $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)['result'];
        // debug($panierOnServer);
        // $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)->json['result'];
        // $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)->json;
        // $panierOnServer = $http->post($urlVerification, $dataToCheck, $this->noSslParams)->body;

        $diff = '';
        $findedPaniers = false;
        $findedPaniersByMontants = false;
        $findedPaniersByNumeros = false;
        $urlMarkAsPaid = false;

        if (isset($panierOnServer->id)) {
            $smsSentTime = Chronos::parse($sms->senttime);
            $panierSentTime = Chronos::parse($panierOnServer->created_date);
            $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
            $urlMarkAsPaid = $host.'/exchanger/pm/mark-panier-as-paid/'.$panierOnServer->id;
        } else {
            $urlVerification = $host.'/exchanger/apis/findOrangePaniers?numero_orange='.$orangeMoneyData['numero'].'&montant='.$orangeMoneyData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
            // $findedPaniers = $http->get($urlVerification, [], $this->noSslParams)->json;
            $findedPaniers = $http->get($urlVerification, [], $this->noSslParams);
            if ($findedPaniers) {
                foreach ($findedPaniers as $key => $panier) {
                    $smsSentTime = Chronos::parse($sms->senttime);
                    $panierSentTime = Chronos::parse($panier['expired_at']);
                    $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                    $diff = $smsSentTime->diff($panierSentTime);
                    $minutes = floor(($diffTime / 60) % 60);
                    $seconds = $diffTime % 60;
                    $panier['diff'] = ($diff->invert == 0 ? '<span class="text-danger">-</span>' : '') . $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                    // debug($minutes);
                    $findedPaniers[$key] = $panier;
                }
            }
            

            if (!$findedPaniers) {
                $urlVerification = $host.'/exchanger/apis/findOrangePaniers?montant='.$orangeMoneyData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                // pr($urlVerification);
                // $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams)->json;
                $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams);
                if ($findedPaniersByMontants) {
                    foreach ($findedPaniersByMontants as $key => $panier) {
                        $smsSentTime = Chronos::parse($sms->senttime);
                        $panierSentTime = Chronos::parse($panier['created_at']);
                        $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                        $minutes = floor(($diffTime / 60) % 60);
                        $seconds = $diffTime % 60;
                        $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                        // debug($minutes);
                        $findedPaniersByMontants[$key] = $panier;
                    }
                }

                // debug($findedPaniersByMontants);
                // die();
            }

            if (!$findedPaniers && !$findedPaniersByMontants) {
                $urlVerification = $host.'/exchanger/apis/findOrangePaniers2?numero_orange='.$orangeMoneyData['numero'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->json;
                $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams);
                // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->body;
                // echo ($findedPaniersByNumeros);
                // die();
                if ($findedPaniersByNumeros) {
                    foreach ($findedPaniersByNumeros as $key => $panier) {
                        $smsSentTime = Chronos::parse($sms->senttime);
                        $panierSentTime = Chronos::parse($panier['created_at']);
                        $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                        $minutes = floor(($diffTime / 60) % 60);
                        $seconds = $diffTime % 60;
                        $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                        // debug($minutes);
                        $findedPaniersByNumeros[$key] = $panier;
                    }
                }

            }
        }


        if ($typeResult == 'array') {
            return compact('type', 'nbAssociatedSmss', 'findedPaniers', 'panierOnServer', 'sms', 'orangeMoneyData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros');
        }
        $this->set(compact('urlMarkAsPaid', 'nbAssociatedSmss', 'smss', 'type', 'findedPaniers', 'panierOnServer', 'sms', 'orangeMoneyData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros'));
    }

    public function searchIdPanierMvola($type, $smsId, $typeResult = null)
    {

        $sms = $this->Ozekimessageins->findById($smsId)->first();
        // $http = new Client();
        $http = new \HttpClient();
        $host = $this->serveurDisant;
        // $host = "http://127.0.0.1/moneyko";
        $urlVerification = $host.'/exchanger/support/verificationTransactionMvola';
        $mvolaData = $this->Ussd->extractMvolaRecuData($sms['msg']);
        $urlMarkAsPaid = '';

        // debug($mvolaData);
        // die();

        $dataToCheck = [
            'montant' => $mvolaData['montant_recu'],
            'numero' => $mvolaData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $mvolaData['ref'],
        ];

        $smsData = (object) $dataToCheck;
        $senttime = Chronos::parse($sms->senttime);
        $smss = $this->Ozekimessageins->find()->where(['msg LIKE' => "%$smsData->numero%", 'receivedtime LIKE ' => '%'.$senttime->format('Y-m-d').'%'])->order(['id' => 'DESC']);
        $smss = $this->getSmsWithDatas($smss, $type);
        $nbAssociatedSmss = $smss->count();
        // debug($smss->toArray());
        // die;

        $diff = '';
        $findedPaniers = false;
        $findedPaniersByMontants = false;
        $findedPaniersByNumeros = false;

        // if (($panierOnServer = Cache::read('panier_sms_id_'.$sms->id)) === false) {
            // $panierOnServer = (new Client())->post($urlVerification, $dataToCheck, $this->noSslParams)->body();
            // echo ($panierOnServer);
            // die();
            $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)['result'];

            // $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)->json;
            // $panierOnServer = $http->post($urlVerification, $dataToCheck, $this->noSslParams)->body;
            // echo ($panierOnServer);
            // die();

            if (isset($panierOnServer->id)) {
                $smsSentTime = Chronos::parse($sms->senttime);
                $panierSentTime = Chronos::parse($panierOnServer->created_date);
                $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                $urlMarkAsPaid = $host.'/exchanger/pm/mark-panier-as-paid/'.$panierOnServer->id;
            } else {
                $urlVerification = $host.'/exchanger/apis/findMvolaPaniers?numero_telma='.$mvolaData['numero'].'&montant='.$mvolaData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                // $findedPaniers = $http->get($urlVerification, [], $this->noSslParams)->json;
                $findedPaniers = $http->get($urlVerification, [], $this->noSslParams);
                if ($findedPaniers) {
                    foreach ($findedPaniers as $key => $panier) {
                        $smsSentTime = Chronos::parse($sms->senttime);
                        $panierSentTime = Chronos::parse($panier['expired_at']);
                        $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                        $diff = $smsSentTime->diff($panierSentTime);
                        $minutes = floor(($diffTime / 60) % 60);
                        $seconds = $diffTime % 60;
                        $panier['diff'] = ($diff->invert == 0 ? '<span class="text-danger">-</span>' : '') . $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                        // debug($minutes);
                        $findedPaniers[$key] = $panier;


                    }
                }
                
                if (!$findedPaniers) {
                    $urlVerification = $host.'/exchanger/apis/findMvolaPaniers?montant='.$mvolaData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                    // pr($urlVerification);
                    // $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams)->json;
                    $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams);
                    if ($findedPaniersByMontants) {
                        foreach ($findedPaniersByMontants as $key => $panier) {
                            $smsSentTime = Chronos::parse($sms->senttime);
                            $panierSentTime = Chronos::parse($panier['created_at']);
                            $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                            $minutes = floor(($diffTime / 60) % 60);
                            $seconds = $diffTime % 60;
                            $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                            // debug($minutes);
                            $findedPaniersByMontants[$key] = $panier;
                        }
                    }

                    // debug($findedPaniersByMontants);
                    // die();
                }

                if (!$findedPaniers && !$findedPaniersByMontants) {
                    $urlVerification = $host.'/exchanger/apis/findMvolaPaniers?numero_telma='.$mvolaData['numero'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                    // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->json;
                    $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams);
                    // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->body;
                    // echo ($findedPaniersByNumeros);
                    // die();
                    if ($findedPaniersByNumeros) {
                        foreach ($findedPaniersByNumeros as $key => $panier) {
                            $smsSentTime = Chronos::parse($sms->senttime);
                            $panierSentTime = Chronos::parse($panier['created_at']);
                            $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                            $minutes = floor(($diffTime / 60) % 60);
                            $seconds = $diffTime % 60;
                            $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                            // debug($minutes);
                            $findedPaniersByNumeros[$key] = $panier;
                        }
                    }

                }
            }
            // Cache::write('panier_sms_id_'.$sms->id, $panierOnServer);
        // }


        if ($typeResult == 'array') {
            return compact('type', 'findedPaniers', 'panierOnServer', 'sms', 'nbAssociatedSmss', 'mvolaData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros');
        }
        $this->set(compact('urlMarkAsPaid', 'smss', 'dataToCheck', 'type', 'findedPaniers', 'panierOnServer', 'sms', 'mvolaData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros'));
    }

    public function getSmsWithDatas($smss, $type)
    {
        $smss = $smss->formatResults(function ($query) use($type)
        {
            return $query->each(function ($item) use($type)
            {
                $funcType = 'extract'.ucfirst($type).'RecuData';
                $smsData = $this->Ussd->$funcType($item->msg);

                $item->montant_recu = @$smsData['montant_recu'];
                $item->solde_actuel = @$smsData['solde_actuel'];
                $item->idpanier = (int) @$smsData['idpanier'];
                $item->raison = (int) @$smsData['raison'];

                return $item;
            });
        });

        return $smss;
    }

    public function majStatutIdpanierBloqued($idpanierId, $statut = 'warning')
    {
        $http = new Client();
        $host = $this->serveurDisant;
        $url = $host.'/exchanger/apis/updatePanier/'.$idpanierId;
        $user_email = $this->request->getQuery('user_email');
        $apisController = new \App\Controller\ApisController();



        if ($user_email) {
            if ($statut != 'warning') {
                $template = APP.DS.'Template'.DS.'Email'.DS.'html'.DS.'transaction_suspendu.ctp';
                $res = $apisController->sendEmail([
                    'subject' => 'Transaction #'.$idpanierId.' suspendue',
                    'message' => file_get_contents($template),
                    'email' => $user_email,
                ]);
            } else {
                $template = APP.DS.'Template'.DS.'Email'.DS.'html'.DS.'transaction_suspendu_warning.ctp';
                $res = $apisController->sendEmail([
                    'subject' => 'Transaction #'.$idpanierId.' suspendue',
                    'message' => file_get_contents($template),
                    'email' => $user_email,
                ]);
            }
        }

        $res = $http->post($url, ['status' => $statut])->json;

        $body = $res;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }   

    public function searchIdPanierAirtel($type, $smsId, $typeResult = null)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        // $http = new Client();
        $http = new \HttpClient();
        $host = $this->serveurDisant;
        // $host = "http://127.0.0.1/moneyko";
        $urlVerification = $host.'/exchanger/support/verificationTransactionAirtel';
        $airtelMoneyData = $this->Ussd->extractAirtelRecuData($sms['msg']);

        $dataToCheck = [
            'montant' => $airtelMoneyData['montant_recu'],
            'numero' => $airtelMoneyData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $airtelMoneyData['ref'],
        ];

        $smsData = (object) $dataToCheck;
        $senttime = Chronos::parse($sms->senttime);
        $smss = $this->Ozekimessageins->find()->where(['msg LIKE' => "%$smsData->numero%", 'date_transfert LIKE ' => '%'.$senttime->format('Y-m-d').'%'])->order(['id' => 'DESC']);
        $smss = $this->getSmsWithDatas($smss, $type);
        $nbAssociatedSmss = $smss->count();

        // $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)->json['result'];
        $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)['result'];
        // $panierOnServer = (object) $http->post($urlVerification, $dataToCheck, $this->noSslParams)->json;
        // $panierOnServer = $http->post($urlVerification, $dataToCheck, $this->noSslParams)->body;
        // echo ($panierOnServer);
        // die();

        $diff = '';
        $findedPaniers = false;
        $findedPaniersByMontants = false;
        $findedPaniersByNumeros = false;
        $urlMarkAsPaid = false;

        if (isset($panierOnServer->id)) {
            $smsSentTime = Chronos::parse($sms->senttime);
            $panierSentTime = Chronos::parse($panierOnServer->created_date);
            $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
            $urlMarkAsPaid = $host.'/exchanger/pm/mark-panier-as-paid/'.$panierOnServer->id;
        } else {
            $urlVerification = $host.'/exchanger/apis/findAirtelPaniers?numero_airtel='.$airtelMoneyData['numero'].'&montant='.$airtelMoneyData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
            // $findedPaniers = $http->get($urlVerification, [], $this->noSslParams)->json;
            $findedPaniers = $http->get($urlVerification, [], $this->noSslParams);
            if ($findedPaniers) {
                foreach ($findedPaniers as $key => $panier) {
                    $smsSentTime = Chronos::parse($sms->senttime);
                    $panierSentTime = Chronos::parse($panier['expired_at']);
                    $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                    $diff = $smsSentTime->diff($panierSentTime);
                    $minutes = floor(($diffTime / 60) % 60);
                    $seconds = $diffTime % 60;
                    $panier['diff'] = ($diff->invert == 0 ? '<span class="text-danger">-</span>' : '') . $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                    // debug($minutes);
                    $findedPaniers[$key] = $panier;
                }
            }
            

            if (!$findedPaniers) {
                $urlVerification = $host.'/exchanger/apis/findAirtelPaniers?montant='.$airtelMoneyData['montant_recu'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                // $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams)->json;
                $findedPaniersByMontants = $http->get($urlVerification, [], $this->noSslParams);
                foreach ($findedPaniersByMontants as $key => $panier) {
                    $smsSentTime = Chronos::parse($sms->senttime);
                    $panierSentTime = Chronos::parse($panier['created_at']);
                    $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                    $minutes = floor(($diffTime / 60) % 60);
                    $seconds = $diffTime % 60;
                    $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                    // debug($minutes);
                    $findedPaniersByMontants[$key] = $panier;
                }

                // debug($findedPaniersByMontants);
                // die();
            }

            if (!$findedPaniers && !$findedPaniersByMontants) {
                $urlVerification = $host.'/exchanger/apis/findAirtelPaniers?numero_airtel='.$airtelMoneyData['numero'].'&date='.date('Y-m-d', strtotime($sms->senttime));
                // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->json;
                $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams);
                // $findedPaniersByNumeros = $http->get($urlVerification, [], $this->noSslParams)->body;
                // echo ($findedPaniersByNumeros);
                // die();
                if (!is_null($findedPaniersByNumeros)) {
                    foreach ($findedPaniersByNumeros as $key => $panier) {
                        $smsSentTime = Chronos::parse($sms->senttime);
                        $panierSentTime = Chronos::parse($panier['created_at']);
                        $diffTime = $smsSentTime->diffInSeconds($panierSentTime);
                        $minutes = floor(($diffTime / 60) % 60);
                        $seconds = $diffTime % 60;
                        $panier['diff'] = $minutes .' min'. ($seconds > 0 ? ', '.$seconds. ' s' : '');
                        // debug($minutes);
                        $findedPaniersByNumeros[$key] = $panier;
                    }
                }

            }
        }

        if ($typeResult == 'array') {
            return compact('type', 'nbAssociatedSmss', 'findedPaniers', 'panierOnServer', 'sms', 'orangeMoneyData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros');
        }
        
        $this->set(compact('urlMarkAsPaid', 'nbAssociatedSmss', 'smss', 'type', 'findedPaniers', 'panierOnServer', 'sms', 'airtelMoneyData', 'diffTime', 'findedPaniersByMontants', 'findedPaniersByNumeros'));
    }

    public function corrigerNumero($type)
    {
        $numero_idpanier = $this->request->getQuery('numero_idpanier');
        $smsId = $this->request->getQuery('sms_id');
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        $msg = $sms->msg;
        if ($type == "airtel") {
            if ($numero_idpanier[0] == "0") {
                $numero_idpanier = substr($numero_idpanier, 1);
            }

            $airtelMoneyData = $this->Ussd->extractAirtelRecuData($msg);
            $newMsg = str_replace($airtelMoneyData['numero'], $numero_idpanier, $msg);
        }
        elseif ($type == "orange") {
            $orangeMoneyData = $this->Ussd->extractOrangeRecuData($msg);
            $newMsg = str_replace($orangeMoneyData['numero'], $numero_idpanier, $msg);
        }
        elseif ($type == "telma") {
            $mvolaData = $this->Ussd->extractMvolaRecuData($msg);
            $newMsg = str_replace($mvolaData['numero'], $numero_idpanier, $msg);
        }

        $success = false;
        // if (empty($sms->msg_backup)) {
            $success = $this->Ozekimessageins->updateAll([
                'msg' => $newMsg,
                'msg_backup' => $sms->msg,
            ], ['id' => $sms->id]);
        // }

        $body = ['status' => (bool) $success];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function corrigerMontant($type)
    {
        $montant_idpanier = $this->request->getQuery('montant_idpanier');
        $smsId = $this->request->getQuery('sms_id');
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        $msg = $sms->msg;

        $previousSms = $this->Ozekimessageins
            ->find()
            ->order(['id' => 'DESC'])
        ;

        if ($type == "airtel") {
            if ($montant_idpanier[0] == "0") {
                $montant_idpanier = substr($montant_idpanier, 1);
            }

            $airtelMoneyData = $this->Ussd->extractAirtelRecuData($msg);
            
            $previousSms = $previousSms->where(['sender in' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'id <' => $sms->senttime])->first();
            $smsPreviousData = $this->Ussd->extractAirtelRecuData($previousSms->msg);
            $soldeActuelCorrige = $smsPreviousData['solde_actuel']+$montant_idpanier;
            $newMsg = str_replace($airtelMoneyData['montant_recu'], $montant_idpanier, $msg);
            $newMsg = str_replace($airtelMoneyData['solde_actuel'], $soldeActuelCorrige, $newMsg);

        }
        elseif ($type == "orange") {
            $orangeMoneyData = $this->Ussd->extractOrangeRecuData($msg);

            $previousSms = $previousSms->where(['sender in' => $this->orangeSenders, 'msg LIKE' => "%Vous avez recu un transfert%", 'senttime <' => $sms->senttime])->first();
            $smsPreviousData = $this->Ussd->extractOrangeRecuData($previousSms->msg);
            $soldeActuelCorrige = $smsPreviousData['solde_actuel']+$montant_idpanier;
            $newMsg = str_replace($orangeMoneyData['montant_recu'], $montant_idpanier, $msg);
            $newMsg = str_replace($orangeMoneyData['solde_actuel'], $soldeActuelCorrige, $newMsg);
        }

        $success = false;
        // if (empty($sms->msg_backup)) {
            $success = $this->Ozekimessageins->updateAll([
                'msg' => $newMsg,
                'msg_backup' => $sms->msg,
            ], ['id' => $sms->id]);
        // }

        $body = ['status' => (bool) $success];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function autresSms()
    {
        $ops = ['Mvola','Orangemoney','Airtelmoney'];
        $smss = $this->Ozekimessageins->find('autres', ['ops' => $ops]);
        $title = 'Autres SMS';
        $smss = $this->paginate($smss, ['limit' => 50]);
        $this->set(compact('title', 'smss'));
    }


    public function smsMerci()
    {
        $title = 'SMS - Remerciement';
        $smss = $this->RemerciementSmss->find();
        $smss = $this->paginate($smss, ['limit' => 50, 'order' => ['id' => 'DESC']]);
        $this->set(compact('title', 'smss'));
    }

    public function authSms()
    {
        $smss = $this->AuthSmss->find();

        $options = $this->request->getQuery();
        $date = @$options['date'];

        $smsTelma = $this->AuthSmss->find()->where(['numero LIKE' => '034%']);
        $smsOrange = $this->AuthSmss->find()->where(['numero LIKE' => '032%']);
        $smsAirtel = $this->AuthSmss->find()->where(['numero LIKE' => '033%']);

        if ($date) {
            $smss->where(['created LIKE' => "%$date%"]);

            $smsTelma->where(['created LIKE' => "%$date%"]);
            $smsOrange->where(['created LIKE' => "%$date%"]);
            $smsAirtel->where(['created LIKE' => "%$date%"]);

        }
        if (@$options['mobile']) {
            $smss->where(['numero LIKE' => $options['mobile'].'%']);
        }
        $title = '2FA Sécurité SMS';

        $nbSmsTelma = $smsTelma->count();
        $nbSmsOrange = $smsOrange->count();
        $nbSmsAirtel = $smsAirtel->count();

        $today = date('Y-m-d');
        $nbSmsTelmaToday = $this->AuthSmss->find()->where(['numero LIKE' => '034%', 'created LIKE' => "%$today%"])->count();
        $nbSmsOrangeToday = $this->AuthSmss->find()->where(['numero LIKE' => '032%', 'created LIKE' => "%$today%"])->count();
        $nbSmsAirtelToday = $this->AuthSmss->find()->where(['numero LIKE' => '033%', 'created LIKE' => "%$today%"])->count();

        $smss = $this->paginate($smss, ['limit' => 50, 'order' => ['id' => 'DESC']]);

        $this->set(compact('nbSmsTelmaToday', 'nbSmsOrangeToday', 'nbSmsAirtelToday', 'nbSmsTelma', 'nbSmsOrange', 'nbSmsAirtel', 'title', 'smss', 'options'));
    }

    public function ajustTime()
    {
        $created_at = $this->request->getQuery('created_at');
        $expired_at = $this->request->getQuery('expired_at');
        $smsId = $this->request->getQuery('sms_id');
        $sms = $this->Ozekimessageins->findById($smsId)->first();

        $addOneMinuteToCreatedAt = Chronos::parse($created_at)->addMinutes(1);

        $data = [
            'senttime_backup' => $sms->senttime,
            'senttime' => $addOneMinuteToCreatedAt->format('Y-m-d H:i:s'),
        ];

        // Tokony hisy vérification zay mbola tsy aiko tsara
        // $urlVerification = $host.'/exchanger/apis/findOrangePaniers?montant='.$orangeMoneyData['montant_recu'].'date='.date('Y-m-d', strtotime($sms->senttime));
        // $findedPaniers = $http->get($urlVerification, [], $this->noSslParams)->json;


        // $this->Ozekimessageins->find()->where([
        //     ''
        // ]);

        $body = ['status' => false];

        if ($sms->senttime_backup == null || !empty($sms->senttime_backup)) {
            $smsEntity = $this->Ozekimessageins->patchEntity($sms, $data, ['validate' => false]);
            $$smsEntity = $this->Ozekimessageins->save($smsEntity);
            $body = ['status' => (bool) $smsEntity];
        }
        
        return $this->response->withType('application/json')->withStringBody(json_encode($body));

    }

    public function reglerCommandeOrange()
    {
        $serveurDisant = $this->serveurDisant;
        $panierID = $this->request->getQuery('panier_id');
        $smsID = $this->request->getQuery('sms_id');

        $sms = $this->Ozekimessageins->findById($smsID)->first();

        $orangeMoneyData = $this->Ussd->extractOrangeRecuData($sms['msg']);

        $data = [
            'montant' => $orangeMoneyData['montant_recu'],
            'numero' => $orangeMoneyData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $orangeMoneyData['ref'],
        ];

        // debug($data);
        $http = new Client();
        $url = $serveurDisant.'/paiement-orange-money/'.$panierID.'?isFromLocal=1&open=1';
        // echo ($http->post($url, $data, $this->noSslParams)->body);
        // die();
        $res = $http->post($url, $data, $this->noSslParams)->json;
        $body = $res;

        if ($res['status'] == 'success' || $res['msg'] == 'already_paid' || $res['msg'] == 'locked_paiement') {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'success'], ['id' => $sms->id]);
            $this->deleteCacheStat('orange');
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function reglerCommandeMvola()
    {
        $serveurDisant = $this->serveurDisant;
        $panierID = $this->request->getQuery('panier_id');
        $smsID = $this->request->getQuery('sms_id');

        $sms = $this->Ozekimessageins->findById($smsID)->first();

        $mvolaData = $this->Ussd->extractMvolaRecuData($sms['msg']);

        $data = [
            'montant' => $mvolaData['montant_recu'],
            'numero' => $mvolaData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $mvolaData['ref'],
        ];

        // debug($data);
        // die();

        // debug($data);
        $http = new Client();
        $url = $serveurDisant.'/paiement-mvola/'.$panierID.'?isFromLocal=1&isFromReglement=1&open=1';
        // echo ($http->post($url, $data, $this->noSslParams)->body);
        // die();
        $request = $http->post($url, $data, $this->noSslParams);

        $this->logginto($request->body(), 'reclamation_response/'.$sms->id);
        $res = $request->json;

        // $res = $http->post($url, [], $this->noSslParams)->body;
        // debug($url);
        // echo ($res);
        // die();
        $body = $res;

        if ($res['status'] == 'success' || $res['msg'] == 'already_paid' || $res['msg'] == 'locked_paiement') {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'success'], ['id' => $sms->id]);
            $this->deleteCacheStat('mvola');
        } elseif ($res['status'] == "warning") {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'warning', 'paiement_status_result' => $res['msg'], 'date_transfert' => $res['date_transfert']], ['id' => $sms->id]);
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function reglerCommandeAirtel()
    {
        $serveurDisant = $this->serveurDisant;
        // $serveurDisant = "http://127.0.0.1/moneyko";

        $panierID = $this->request->getQuery('panier_id');
        $smsID = $this->request->getQuery('sms_id');

        $sms = $this->Ozekimessageins->findById($smsID)->first();

        $airtelMoneyData = $this->Ussd->extractAirtelRecuData($sms['msg']);

        $data = [
            'montant' => $airtelMoneyData['montant_recu'],
            'numero' => $airtelMoneyData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $airtelMoneyData['ref'],
        ];

        // debug($data);
        $http = new Client();
        $url = $serveurDisant.'/paiement-airtel-money/'.$panierID.'?isFromLocal=1&open=1';
        // echo ($http->post($url, $data, $this->noSslParams)->body);
        // die();
        $res = $http->post($url, $data, $this->noSslParams)->json;
        $body = $res;

        if ($res['status'] == 'success' || $res['msg'] == 'already_paid' || $res['msg'] == 'locked_paiement') {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'success'], ['id' => $sms->id]);
            $this->deleteCacheStat('airtel');
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }



    public function reglerCommandeMvolaDebug()
    {
        $serveurDisant = $this->serveurDisant;
        $panierID = $this->request->getQuery('panier_id');
        $smsID = $this->request->getQuery('sms_id');

        $sms = $this->Ozekimessageins->findById($smsID)->first();

        $mvolaData = $this->Ussd->extractMvolaRecuData($sms['msg']);

        $data = [
            'montant' => $mvolaData['montant_recu'],
            'numero' => $mvolaData['numero'],
            'received_at' => $sms->senttime,
            'trans_id' => $mvolaData['ref'],
        ];

        // debug($data);
        // die();

        // debug($data);
        $http = new Client();
        $url = $serveurDisant.'/exchanger/pm/debugPay/'.$panierID.'?isFromLocal=1&isFromReglement=1&open=1';
        // echo ($http->post($url, $data, $this->noSslParams)->body);
        // die();
        $request = $http->post($url, $data, $this->noSslParams);

        echo $request->body();
        die();

        $this->logginto($request->body(), 'reglement_mvola');
        $res = $request->json;

        // $res = $http->post($url, [], $this->noSslParams)->body;
        // debug($url);
        // echo ($res);
        // die();
        $body = $res;

        if ($res['status'] == 'success' || $res['msg'] == 'already_paid' || $res['msg'] == 'locked_paiement') {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'success'], ['id' => $sms->id]);
            $this->deleteCacheStat('mvola');
        } elseif ($res['status'] == "warning") {
            $this->Ozekimessageins->updateAll(['ptf' => @$res['ptf'], 'paiement_status' => 'warning', 'paiement_status_result' => $res['msg'], 'date_transfert' => $res['date_transfert']], ['id' => $sms->id]);
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function rates()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $http = new Client();
            $base = $data['base'];
            $qty = $data['qty'];
            $target = "USDT";
            $date = $data['date'];
            $hour = $data['hour'];
            $time = trim($date.'T'.$hour);
            $url = "https://rest.coinapi.io";
            $result = $http->get("$url/v1/exchangerate/$base/$target?time=$time", [], [
                'headers' => [
                    'X-CoinAPI-Key' => 'E1567B32-7528-45E5-B2FC-2F6CADB1F293'
                ]
            ])->json;

        }
        $assets = Configure::read('assets');
        $this->set(compact('assets', 'result', 'data', 'qty'));
    }

    public function majSoldeAZero($smsId)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        $result = ['status' => null];
        $msg = $sms->msg;
        // debug($msg);
        // die();
        $mobile = $this->Ussd->checkMobile2($sms->receiver);
        if ($mobile == 'airtel') {
            if ($sms->is_backup_sms_majsolde_zero == 0) {
                $currentSolde = (float) $this->Ussd->extractBetween($msg, 'compte est Ar ', '. Trans');
                $smsSoldeAjusted = str_replace('Ar '.$currentSolde, 'Ar 0', $msg);
                $this->Ozekimessageins->updateAll(['msg' => $smsSoldeAjusted, 'is_backup_sms_majsolde_zero' => 1, 'backup_sms_majsolde_zero' => $msg], ['id' => $sms->id]);
                $result = ['status' => true];
            } else{
                $backup_sms_majsolde_zero = $sms->backup_sms_majsolde_zero;
                $this->Ozekimessageins->updateAll(['is_backup_sms_majsolde_zero' => 0, 'msg' => $backup_sms_majsolde_zero], ['id' => $sms->id]);
                $result = ['status' => false];
            }
        }
        elseif ($mobile == 'orange') {
            if ($sms->is_backup_sms_majsolde_zero == 0) {
                $currentSolde = (float) $this->Ussd->extractBetween($msg, 'Nouveau Solde: ', 'Ar. Trans');
                $smsSoldeAjusted = str_replace($currentSolde.'Ar. Trans', '0Ar. Trans', $msg);
                $this->Ozekimessageins->updateAll(['msg' => $smsSoldeAjusted, 'is_backup_sms_majsolde_zero' => 1, 'backup_sms_majsolde_zero' => $msg], ['id' => $sms->id]);
                $result = ['status' => true];
            } else{
                $backup_sms_majsolde_zero = $sms->backup_sms_majsolde_zero;
                $this->Ozekimessageins->updateAll(['is_backup_sms_majsolde_zero' => 0, 'msg' => $backup_sms_majsolde_zero], ['id' => $sms->id]);
                $result = ['status' => false];
            }
        }
        elseif ($mobile == 'telma') {
            if ($sms->is_backup_sms_majsolde_zero == 0) {
                $currentSolde = $this->Ussd->extractBetween($msg, 'Solde : ', ' Ar. Ref');
                $smsSoldeAjusted = str_replace($currentSolde.' Ar.', '0 Ar.', $msg);
                $this->Ozekimessageins->updateAll(['msg' => $smsSoldeAjusted, 'is_backup_sms_majsolde_zero' => 1, 'backup_sms_majsolde_zero' => $msg], ['id' => $sms->id]);
                $result = ['status' => true];
            } else{
                $backup_sms_majsolde_zero = $sms->backup_sms_majsolde_zero;
                $this->Ozekimessageins->updateAll(['is_backup_sms_majsolde_zero' => 0, 'msg' => $backup_sms_majsolde_zero], ['id' => $sms->id]);
                $result = ['status' => false];
            }
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkPmHistorickByIdpanierId($idPanierId, $asArray = false)
    {
        $url = 'https://moneiko.net/exchanger/apis/checkPmHistorickByIdpanierId/'.$idPanierId;
        // $http = new Client();
        $http = new \HttpClient();
        $historics = $http->get($url, $timeout = 10);
        
        // $historics = $http->get($url, $this->noSslParams)->json;
        $results = [];

        if (!empty($historics)) {
            foreach ($historics as $key => $historic) {
                if (!isset($historic[4])) {
                    $results = [] ;
                    break;
                }

                $montant = abs($historic[4]);
                $monantSansFrais = round($montant/1.005, 2);
                $results[] = [
                    'usd' => $monantSansFrais,
                    'compte' => $historic[7],
                    'ref' => $historic[2],
                    'date' => Chronos::parse($historic[0])->addMinutes(63),
                    'date_short' => Chronos::parse($historic[0])->format('Y-m-d'),
                    'usd_rounded' => round($monantSansFrais), // utile pour la vérification cron auto paiement
                ];
            }

            $results = collection($results)->sortBy('date', SORT_DESC);
        }

        if ($asArray) {
            $body = $results;
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        // debug($results->toArray());
        // debug($historics);
        // die();
        $this->set(compact('historics', 'results'));
    }  

    public function checkAdvcashHistorickByIdpanierId($idPanierId, $asArray = false)
    {
        $http = new Client();
        // $url = $this->serveurDisant.'/exchanger/apis/checkAdvcashHistorickByIdpanierId/'.$idPanierId;
        $url = 'https://moneiko.net/exchanger/apis/checkAdvcashHistorickByIdpanierId/'.$idPanierId;
        // debug($url);
        // die();

        $historics = $http->get($url, $this->noSslParams)->json;
        // $historics = $http->get($url, $this->noSslParams)->body();

        // echo $historics;

        // debug($historics);
        // die();

        $results = [];

        if (!empty($historics)) {
            foreach ($historics as $key => $historic) {
                // debug($historic);

                $montant = abs(trim($this->Ussd->extractBetween($historic['comment'], 'Paiement ', ' USD')));
                $results[] = [
                    'usd' => $montant,
                    'compte' => $historic['receiverEmail'],
                    'ref' => $historic['id'],
                    'date' => Chronos::parse($historic['startTime']),
                    'date_short' => Chronos::parse($historic['startTime'])->format('Y-m-d'),
                    'usd_rounded' => round($montant), // utile pour la vérification cron auto paiement
                ];
            }

            $results = collection($results)->sortBy('date', SORT_DESC);
        }


        // die();

        if ($asArray) {
            $body = $results;
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        // debug($results->toArray());
        // debug($historics);
        // die();
        $this->set(compact('historics', 'results'));
    }  


    public function checkFaucetpayHistorickByIdpanierId($idPanierId, $asArray = false)
    {
        $http = new Client();
        $url = $this->serveurDisant.'/exchanger/apis/checkFaucetpayHistorickByIdpanierId/'.$idPanierId;
        $historics = $http->get($url, $this->noSslParams)->json;
        // $historics = $http->get($url, $this->noSslParams)->body;
        // debug($historics);
        // die();
        $results = [];

        // debug($results);
        // die();

        if (!empty($historics)) {
            foreach ($historics as $key => $historic) {
                $montant = $historic['amount']/100000000;

                // debug($historic);

                $results[] = [
                    'usd' => $montant,
                    'compte' => $historic['to'],
                    'ref' => 'No ID',
                    'asset' => $historic['asset'],
                    'date' => Chronos::parse($historic['datetime']),
                    'date_short' => Chronos::parse($historic['datetime'])->format('Y-m-d'),
                    'usd_rounded' => round($montant), // utile pour la vérification cron auto paiement
                ];
            }
            $results = collection($results)->sortBy('date', SORT_DESC);
        }

        if ($asArray) {
            $body = $results;
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        // debug($results->toArray());
        // debug($historics);
        // die();
        $this->set(compact('historics', 'results'));
    }   

    public function checkPayeerHistorickByIdpanierId($idPanierId, $asArray = false)
    {
        $http = new Client();
        $url = 'https://moneiko.net/exchanger/apis/checkPayeerHistorickByIdpanierId/'.$idPanierId;
        $historics = $http->get($url, $this->noSslParams)->json;
        // $historics = $http->get($url, $this->noSslParams)->body();

        // echo ($historics);
        // die();

        $results = collection([]);
        if (!empty($historics)) {
            foreach ($historics as $key => $historic) {

                $results[] = [
                    'usd' => $historic['creditedAmount'],
                    'compte' => $historic['to'],
                    'ref' => $historic['id'],
                    'date' => Chronos::parse($historic['date']),
                    'date_short' => Chronos::parse($historic['date'])->format('Y-m-d'),
                    'usd_rounded' => round($historic['creditedAmount']), // utile pour la vérification cron auto paiement
                ];
            }

            $results = collection($results)->sortBy('date', SORT_DESC);
        }

        if (($results->count() == 0)) {
            $results = collection([]);
        }

        if ($asArray) {
            $body = $results;
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        // debug($results->count());
        // debug($historics);
        // die();
        $this->set(compact('historics', 'results'));
        $this->render('checkPmHistorickByIdpanierId');
    }   

    // public function checkPmHistorickByIdpanierId($idPanierId, $asArray = false)
    // {
    //     $url = 'https://moneiko.net/exchanger/apis/checkPmHistorickByIdpanierId/'.$idPanierId;
    //     // $http = new Client();
    //     $http = new \HttpClient();
    //     $historics = $http->get($url, $timeout = 5);
    //     $timeout = (float) $timeout;
    //     $http = new GuzzleClient(['verify' => false]);
    //     $response = $http->get($url, ['connect_timeout' => $timeout]);

    //     $statusCode = $response->getStatusCode();

    //     if ($statusCode == 200) {

    //         $results = [];

    //         if (!empty($historics)) {
    //             foreach ($historics as $key => $historic) {
    //                 if (!isset($historic[4])) {
    //                     $results = [] ;
    //                     break;
    //                 }

    //                 $montant = abs($historic[4]);
    //                 $monantSansFrais = round($montant/1.005, 2);
    //                 $results[] = [
    //                     'usd' => $monantSansFrais,
    //                     'compte' => $historic[7],
    //                     'ref' => $historic[2],
    //                     'date' => Chronos::parse($historic[0])->addMinutes(63),
    //                     'date_short' => Chronos::parse($historic[0])->format('Y-m-d'),
    //                     'usd_rounded' => round($monantSansFrais), // utile pour la vérification cron auto paiement
    //                 ];
    //             }

    //             $results = collection($results)->sortBy('date', SORT_DESC);
    //         }

    //         if ($asArray) {
    //             $body = $results;
    //         } else {
    //             $body = $results;
    //         }
            
    //     } elseif (in_array($statusCode, [500,501,502,503,504,505,506])) {

    //         $body = ['status' => 'error', 'msg' => 'server_'.$statusCode];
            
    //     } else {

    //         $body = ['status' => 'error', 'msg' => 'server_'.$statusCode];

    //     }


    //     return $this->response->withType('application/json')->withStringBody(json_encode($body));
    // }  

    public function voirlog($smsId)
    {

        // $sms = $this->Ozekimessageins->findById($smsId)->first();
        // $body = $sms->log;
        // return $this->response->withType('application/text')->withStringBody(($body));

        $path = LOGS.'achat_response'.DS.$smsId.'.log';
        $sms = file_exists($path) ? file_get_contents($path) : 'Aucun fichier de log trouvé';
        $body = $sms;
        return $this->response->withType('application/text')->withStringBody(($body));
    }
}
?>