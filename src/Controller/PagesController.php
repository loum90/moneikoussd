<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Utility\Xml;
use Cake\Http\Client;
use Cake\Filesystem\File;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadModel('Users');
    }

    public function users()
    {
        $options = $this->request->getQuery();
        $users = $usersOrigine = $this->Users
            ->find()
            ->find('Simple')
            ->where([
                'or' => [
                    // ['wordcount(nom_complet)' => 1],
                    // ['is_modified_local' => true]
                ]
                // 'coalesce(Users.cin_recto, "") !=' => '',
            ])
            ->order(['id' => 'DESC'])
            ->limit(1)
        ;


        if (@$options['email']) {
            $users->where(['email LIKE' => '%'.@$options['email'].'%']);
        }

        if (@$options['user_id']) {
            $users->where(['id' => @$options['user_id']]);
        }
        // debug($users ->toArray());
        // die();

        if (@$options['nom_complet']) {
            $users->where(['nom_complet LIKE' => '%'.@$options['nom_complet'].'%']);
        }

        
        $users = $this->paginate($users, ['limit' => 200]);
        $users = $users->map(function ($user)
        {
            $isCinFileExists = (new File(WWW_ROOT.$user->get('CinRectoPath')))->exists();
            $statut = 0;
            if (strlen($user->cin_recto) > 0 && $isCinFileExists) {
                $statut = 1; // fichier cin existe en prod et en local
            } elseif (strlen($user->cin_recto) > 0 && !$isCinFileExists) {
                $statut = 2; // fichier cin existe en prod et mais n'existe pas en local
            } else {
                $statut = 3; // fichier cin n'existe pas en prod et n'existe pas en local
            }
            $user->statut = $statut;
            return $user;
        });


        $usersOrigine = $this->getTotalUserStats();

        $nbUsersCinEnLigneMsEtPresentLocal = $usersOrigine->match(['statut' => 1])->count();
        $nbUsersCinEnLigneMsInexistantLocal = $usersOrigine->match(['statut' => 2])->count();
        $nbUsersCinInexistantEnLigneEtInexistantLocal = $usersOrigine->match(['statut' => 3])->count();

        $raisons = Configure::read('raisons');
        $this->set(compact('nbUsersCinEnLigneMsEtPresentLocal', 'nbUsersCinEnLigneMsInexistantLocal', 'nbUsersCinInexistantEnLigneEtInexistantLocal', 'statsVerification', 'users', 'raisons', 'options', 'nbUsers', 'nbUsersUnverified', 'nbUsersUnverifiedSansRappel'));
    }

    public function getTotalUserStats()
    {
        return $this->Users
            ->find('Simple')
            ->where([
                'or' => [
                    ['wordcount(nom_complet)' => 1],
                    ['is_modified_local' => true]
                ]
                // 'coalesce(Users.cin_recto, "") !=' => '',
            ])
            ->order(['id' => 'DESC'])
            ->limit(1)
            // ->map(function ($user)
            // {
            //     $isCinFileExists = (new File(WWW_ROOT.$user->get('CinRectoPath')))->exists();
            //     $statut = 0;
            //     if (strlen($user->cin_recto) > 0 && $isCinFileExists) {
            //         $statut = 1; // fichier cin existe en prod et en local
            //     } elseif (strlen($user->cin_recto) > 0 && !$isCinFileExists) {
            //         $statut = 2; // fichier cin existe en prod et mais n'existe pas en local
            //     } else {
            //         $statut = 3; // fichier cin n'existe pas en prod et n'existe pas en local
            //     }
            //     $user->statut = $statut;
            //     return $user;
            // })
        ;
    }   

    public function viewUser($userId)
    {
        $user = $this->Users->findById($userId)->first();
        $http = new Client();
        $cinUrl = Router::url($user->get('CinRectoPath'), true);
        $isCinFileExists = file_exists(WWW_ROOT.$user->get('CinRectoPath'));
        $this->set(compact('user', 'isCinFileExists'));
    }

    public function modifUser($userId)
    {
        $res = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $user = $this->Users->findById($userId)->first();
            $data['is_modified_local'] = true;
            $user = $this->Users->patchEntity($user, $data, ['validate' => false]);
            $user = $this->Users->save($user);
            $res = (bool) $user;
        }

        $body = ['status' => $res];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function majNomUsers()
    {
        $users = $this->Users->find()->where(['is_modified_local' => 1]);
        $datas = [];
        foreach ($users as $key => $user) {
            $datas[] = [
                'id' => $user->id,
                'is_modified_local' => true,
                'step_modification_nom' => true,
                'nom_complet' => $user->nom_complet
            ];
        }

        $http = new Client();
        // $url = 'http://127.0.0.1/moneyko/exchanger/apis/majnoms';
        $url = 'https://moneiko.net/exchanger/apis/majnoms';
        $res = $http->post($url, $datas);
        $body = $res->json;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function importUsers()
    {
        $cm = new \Cake\Datasource\ConnectionManager;
        $http = new Client();
        // $sql = $http->get('https://moneiko.net/db-backup/sql/getUserDb')->body();
        $file = new File(ROOT.DS.'sql'.DS.'users.sql');
        $sql = $file->read();
        // $sql = gzdecode($sql);
        // $file->write($sql);
        // $cm::get('default')->execute("DROP TABLE IF EXISTS users");
        echo $sql;die;
        $cm::get('default')->execute($sql);
        $body = ['status' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function sms()
    {
        $http = new \Cake\Http\Client();
        $smss  = Xml::toArray(Xml::build(WWW_ROOT.'sms-20210711180053.xml'));
        $logSms = collection($smss['smses']['sms']);
        $logSms = $logSms->match(['@address' => 'MVola'])->sortBy('@date_sent', SORT_ASC)->toArray();
        debug($logSms);
        die();
    }
}
