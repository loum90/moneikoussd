<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Chronos\Chronos;
use Cake\Mailer\Email;
use Cake\Http\Client;
use Cake\Mailer\Transport\DebugTransport;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
use App\Traits\AppTrait;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Symfony\Component\Yaml\Yaml;

class ApisController extends AppController
{
    use AppTrait;

    public $parametre = null;

    public function initialize(array $config = []) : void
    {
        $this->loadModel('Parameters');
        $this->loadModel('EmailsSents');
        parent::initialize($config);
    }

    public function params($field = null)
    {
        if ($this->parametre == null) {

            if ($field == null) {
                $param = $this->parametre = $this->Parameters->findById(1)->first();
            } else {
                $param = $this->parametre = $this->Parameters->findById(1)->select([$field])->first();
            }
        }

        if ($field != null) {
            $param =  $this->parametre->{$field};
        }

        $body = $param;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function majNumFromGsm($port = null)
    {   
        include_once ROOT. '/vendor/hyperthese/php-serial/src/PhpSerial.php';
        $this->initializeDevice('\\.\\COM7');

        die();

    }

    public function initializeDevice($port = null)
    {
        $serial = new \phpSerial;
        $serial->deviceSet($port);
        $serial->confBaudRate(115200);
        $serial->confParity("none");
        $serial->confCharacterLength(8);
        $serial->confFlowControl("none");
        $serial->confStopBits(1);

        // Then we need to open it
        $serial->deviceOpen();

        return $serial;
    }

    public function getConfig()
    {
        $body = $this->getParam();
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function sendEmail($data = null)
    {
        // $starttime = \DateTime::createFromFormat('H:i', "21:00");
        // $endtime = \DateTime::createFromFormat('H:i', "04:00");
        // $now = \DateTime::createFromFormat('H:i', date('H:i'));


        // // debug($now > $starttime || $now < $endtime);
        // debug(date('H') > 21 || date('H') <= 04);
        // die();
        // set_time_limit(1);
        
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
        }

        // debug($data);
        // die();
        $options = [
            'fromEmail' => 'contact@moneiko.net',
            'from' => 'Moneiko',
            'subject' => $data['subject'] ?? 'test objet',
            'message' => $data['message'] ?? 'Lorem ipsum',
        ];

        $email = new Email();
        // $options['test'] = true; // en debug, force l'envoi à non si test à true

        $email->setTransport('mainhostinger');
        $transportConfig = $email->getTransport()->getConfig();
        $fromEmail = 'contact@moneiko.net';


        // Temporaire
        // $transport = new DebugTransport();
        // $email->setTransport($transport);
        // $fromEmail = 'contact@moneiko.net';

        $from = $options['from'];
        $additional_headers = ['From' => $fromEmail, 'Content-type' => 'text/html; charset=iso-8859-1', 'MIME-Version' => '1.0', 'Return-Path' => $fromEmail, 'X-Priority' => 3, 'X-Mailer' => "PHP", ];

        $template = !isset($options['template']) ? false : $options['template'];
        $layout = !isset($options['layout']) ? 'default' : $options['layout'];

        $emails = $data['email'] ?? 'andriam.nars@gmail.com';

        if(is_array($emails)){
            $emails = array_unique($emails);
        }

        $email
            ->setTo($emails)
            ->setFrom([$fromEmail => $options['from']])
            ->setSubject($options['subject'])
        ;

        if (!empty($data['bcc'])) {
            $bcc = unserialize($data['bcc']);
            // debug($bcc);
            // die();
            $email->setBcc($bcc);
        }

        $email
            ->setEmailFormat('html')
            ->setTemplate('default')
            ->setLayout('default')
            ->setViewVars($data)
        ;


        // echo ($email->send()['message']);
        // die
        // ;

        $email = $email->send();
        $body = $email;
        // $this->logginto($data, 'data_email');
        $emailSent = $this->EmailsSents->newEntity($data, ['validate' => false]);
        if(!$emailSent->getErrors()) {
            $this->EmailsSents->save($emailSent);
        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
        // debug($email['message']);
        // die();

        
        // debug($email['message']);
        // die();
    }

    // public function checkngrokstatus()
    // {
    //     $response = $this->readUrl('http://127.0.0.1:4040/api/tunnels');
    //     $res = json_decode($response);

    // }

    public function getStatNgrok()
    {
        $winYmlPath = "C:\Users\Loum\AppData\Local\\ngrok\\ngrok.yml";
        $tunnel = null;
        $http = new Client();

        $tunnel = $this->Tunnels->findByIsActive(1)->first();

        $response = $this->readUrl('http://127.0.0.1:4040/api/tunnels');
        $res = json_decode($response);

        $url = '';
        if (!empty($res)) {
            if (isset($res->tunnels[0])) {
                $url = $res->tunnels[0]->public_url;
            }
        }
        $param = $this->getParam();

        $tunnel = $this->renewNgrokToken();
        // $tunnel = $this->renewNgrokHit();

        $now = Chronos::now();
        $resetAt = $now->format('d');
        // if ($resetAt == '02') {
            
        // }

        $body = ['active_tunnel_id' => $tunnel->id, 'nb_conn' => $tunnel->hit, 'resetAt' => $resetAt, 'active_account' => $tunnel->account, 'percent' => $tunnel->get('Pourcentage'), 'url' => $url];
        // $body = ['nb_conn' => $nbHttpConn, 'active_account' => $active_account];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function renewNgrokToken()
    {
        $tunnel = $this->Tunnels->findByIsActive(1)->first();
        $url = '';
        if (!empty($res)) {
            $url = $res->tunnels[0]->public_url;
        }
        $param = $this->getParam();



        if ($tunnel->hit > 20000) {
            
            $tunnel = $this->Tunnels->find()->where(['hit < ' => 20000])->first();
            $this->majNgrokToken($tunnel->id);
            // debug($newTunnel);
            // die();
        }
        
        return $tunnel;
    }

    public function renewNgrokHit()
    {
        if (date('d') == '02') {
            $tunnels = $this->Tunnels->find()->where(['is_active' => 0]);
            $now = Chronos::now();
            foreach ($tunnels as $key => $tunnel) {
                $diff = $now->diffInDays($tunnel->last_update_at);
                if ($diff >= 30) {
                    $data = [
                        'hit' => 0,
                        'last_update_at' => $now,
                    ];
                    $tunnel = $this->Tunnels->patchEntity($tunnel, $data, ['validate' => false]);
                    $this->Tunnels->save($tunnel);
                }
            }
        };
    }

    public function majNgrokToken($tunnelId)
    {
        $url = Router::url(['controller' => 'Tdb', 'action' => 'majUrlNgrok'], true);

        $tunnel = $this->Tunnels->findById($tunnelId)->first();

        $winYmlPath = "C:\Users\Loum\AppData\Local\\ngrok\\ngrok.yml";
        $http = new Client();


        //transform $yamlObject to YAML
        if (file_exists($winYmlPath)) {
            $yaml = Yaml::parseFile($winYmlPath, 0, $debug = null);
            $oldAuthToken = $yaml['authtoken'];
        }

        $ymlConfig = [
            'version' => '2',
            'authtoken' => $oldAuthToken,
            'tunnels' => [
                'default' => [
                    'proto' => 'http',
                    'addr' => (int) 80,
                    'host_header' => 'rewrite'
                ]
            ]
        ];


        $param = $this->getParam();
        $python = $this->Ussd->pythonDefaultPath;
        $cmd = $python."orange/service_ngrok_restart.py 2>&1";

        $newToken = $yaml['authtoken'] = $ymlConfig['authtoken'] = $tunnel->token;
        $newYaml = Yaml::dump($ymlConfig);
        file_put_contents($winYmlPath, $newYaml);
        $r = $this->Ussd->execCli2($cmd);

        $ngRes = $this->readUrl($url);

        if (!empty($ngRes)) {
            $ngRes = json_decode($ngRes);
            $this->Tunnels->updateAll(['is_active' => 0], []);
            $this->Tunnels->updateAll(['is_active' => 1], ['token' => $newToken]);

            if (isset($ngRes->server_ngrok)) {
                $param = $this->Parameters->patchEntity($param, ['current_ngrok_url' => $ngRes->server_ngrok], ['validate' => false]);
            }
        } else {
            $ymlConfig['authtoken'] = $yaml['authtoken'] = $oldAuthToken;
            $old = Yaml::dump($ymlConfig);
            file_put_contents($winYmlPath, $old);
            $this->Ussd->execCli2($cmd);
        }

        $body = ['tunnel_id' => $tunnel->id, 'pourcentage' => $tunnel->get('pourcentage'), 'current_yml' => Yaml::parseFile($winYmlPath, 0, $debug = null)];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function test()
    {

        $timestamp_debut = microtime(true);
            $http = new Client();
            $res = $http->get('http://127.0.0.1:8888/run-script')->body();
        $timestamp_fin = microtime(true);
        $difference_ms = round(($timestamp_fin - $timestamp_debut), 2); 

        debug($res);
        debug($difference_ms);
        die();

        // $timestamp_debut = microtime(true);
        //     $this->Ussd->setPort('com41');
        //     $this->Ussd->run("#144*1*1*0325541393*0325541393*200*2#");
        // $timestamp_fin = microtime(true);

        // $difference_ms = round(($timestamp_fin - $timestamp_debut), 2); 
        
        // debug($res);
        // debug($difference_ms);

        debug(@fopen($url,"r"));
        die();
        $r = $this->Ussd->execCli('D:\ngrok_2.bat');
        echo nl2br($r);
        die();

        $http = new Client();
        // $res = $http->post('http://moneiko.42web.io/email-apis/send-email', [
        //     'email' => 'andriam.nars@gmail.com',
        //     'subject' => 'object',
        //     'message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        // ])->body();

        $res = $http->get('http://moneiko.42web.io/email-apis/send-email')->body();
        echo ($res);
        die();

    }

    public function updateLastAutoReclaAt()
    {
        $now = Chronos::now();
        $param = $this->getParam();
        $param = $this->Parameters->patchEntity($param, [
            'last_auto_recla_executed_at' => $now
        ], ['validate' => false]);

        $param = $this->Parameters->save($param);
        $body = ['param' => $param, 'last_auto_recla_executed_at' => $param->last_auto_recla_executed_at->format('d/m/Y H:i')];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function updateLastReseauAutoReclaAt()
    {
        $now = Chronos::now();
        $param = $this->getParam();
        $param = $this->Parameters->patchEntity($param, [
            'last_auto_recla_erreur_reseau_executed_at' => $now
        ], ['validate' => false]);

        $param = $this->Parameters->save($param);
        $body = ['param' => $param, 'last_auto_recla_erreur_reseau_executed_at' => $param->last_auto_recla_erreur_reseau_executed_at->format('d/m/Y H:i')];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function updateLastOzekiChecking()
    {
        $now = Chronos::now();
        $param = $this->getParam();
        $data = [
            'last_ozeki_checked_at' => $now
        ];
        $param = $this->Parameters->patchEntity($param, $data, ['validate' => false]);
        $param = $this->Parameters->save($param);    
        $body = ['last_ozeki_checked_at' => $param->last_ozeki_checked_at, 'is_autorestart_ozeki_activated' => $param->is_autorestart_ozeki_activated];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkLastOzekiCheckedAt()
    {
        $checkedAt = $this->getParam('last_ozeki_checked_at');;
        $now = Chronos::now();
        $diff = $now->diffInSeconds($checkedAt);
        $body = ['result' => $diff];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkAutoRestartOzekiStatus()
    {
        $is_autorestart_ozeki_activated = $this->getParam('is_autorestart_ozeki_activated');
        $body = ['result' => $is_autorestart_ozeki_activated];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function disableAutoRestartOzekiInServiceNgPy()
    {
        $this->Parameters->updateAll(['is_autorestart_ozeki_activated' => false], ['id' => 1]);
        die();
    }


    public function enableAutoRestartOzekiInServiceNgPy()
    {
        $this->Parameters->updateAll(['is_autorestart_ozeki_activated' => true], ['id' => 1]);
        die();
    }

    /**
     * [getReleves description]
     * @return [type] data = ['html' => '']
     */
    public function readVenteFiats()
    {
        $http = new Client();
        $url = "https://moneiko.net/exchanger/manager/liste-ventes-fiats/1/?montant=&ariary=&type=airtm&mobile_paiement_status=pay%C3%A9&date=";
        
        if (($results = Cache::read('vente_fiat')) === false) {
            $res = $http->get($url)->json;
            $results = $res;
            Cache::write('vente_fiat', $results);
        }

        $this->loadModel('Relances');

        foreach ($results as $key => $result) {

            $mobile = $this->Ussd->checkMobile2($result['numero']);
            $data = [
                'nom' => $result['titulaire'],
                'titulaire' => $result['titulaire'],
                'numero' => $result['numero'],
                'mobile' => $mobile,
                'type' => 'retrait',
            ];

            if (!$this->Relances->find()->where($data)->first()) {
                $relance = $this->Relances->newEntity($data, ['validate' => false]);
                if(!$relance->getErrors()) {
                    $r = $this->Relances->save($relance);
                }
            }
        }
        die();
    }     

    public function check()
    {
        $body = 'ok';
        return $this->response->withType('text/html')->withStringBody($body);
    } 

    public function wakePort()
    {
        $smsPorts = $this->Ports->find()->where([
            'type' => 'envoi',
            // 'mobile' => 'orange'
        ]);

        foreach ($smsPorts as $key => $port) {
            // debug($port);
            $res = $this->Ussd->wakeGsm($port->com);
            debug($res);
        }

        die();
    }

    public function getRelancesFromProd()
    {
        $url = "$this->serveurDisant/exchanger/apis/get-relances";
        $http = new Client();
        $relances = $http->get($url)->json;
        $this->loadModel('RelancesSmss');
        $cm = new ConnectionManager;
        $cm::get('default')->execute('TRUNCATE TABLE `relances_smss`');

        foreach ($relances as $key => $relance) {
            $relancesSms = $this->RelancesSmss->newEntity($relance, ['validate' => false]);
            if(!$relancesSms->getErrors()) {
                $this->RelancesSmss->save($relancesSms);
            }
        }

        $countRelancesSmss = $this->RelancesSmss->find()->count();
        $body = ['result' => $countRelancesSmss];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkCodeNgrokProd()
    {
        $http = new Client();

        $res = ['status' => false];


        if ($this->checkUrl()) {
            $response = $http->get('https://moneiko.net/exchanger/apis/checkLocalServer')->json;

            if ($response['code'] == '200') {
                $res = ['status' => 'success'];
            } else {
                $res = ['status' => 'local_unreachable_from_prod'];
            }

        } else {
            $res = ['status' => 'no_local_connection'];

        }

        $body = $res;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));

    }   

    public function testPost()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $this->logginto($data, 'test_post');
            $body = $data;
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        die('ato');
    }

    public function resetNgrokProgression()
    {
        $tunnels = $this->Tunnels->find()->where(['hit >=' => 16000, 'is_active' => 0]);

        foreach ($tunnels as $key => $tunnel) {
            $tunnelEntity = $this->Tunnels->patchEntity($tunnel, ['hit' => 0], ['validate' => false]);            
            $this->Tunnels->save($tunnelEntity);
        }
        $body = ['result' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function getMembreByNumeroOrangeFailed()
    {
        $url = "http://127.0.0.1/moneyko/exchanger/apis/getMembreByNouveauNumeroOrangeFailed";

        $http = new Client();
        $users = $http->get($url)->json;
        debug($users);

        foreach ($users as $key => $user['id']) {
            
            $urlUpdateMembre = $baseUrl.'/exchanger/apis/updateMembre/'.$user_id;
            
        }

        die();
    }
    

    public function jsontocsv()
    {
        // 1. Lire et décoder le fichier JSON

        $path = ROOT.DS.'notes'.DS.'moneiko.net.json';
        $jsonString = file_get_contents($path); // Remplacer par le chemin de ton fichier JSON
        $data = ($jsonString); // Convertir le JSON en tableau PHP (array)
        debug($data);
        die();

        // Vérifier si la conversion a réussi
        if ($data === null) {
            die('Erreur de lecture ou de conversion du fichier JSON.');
        }

        // 2. Ouvrir ou créer un fichier CSV
        $csvFile = fopen(ROOT.DS.'notes'.DS.'output.csv', 'w'); // Remplacer 'output.csv' par le chemin du fichier CSV souhaité

        // 3. Écrire les en-têtes (si elles existent)
        if (!empty($data)) {
            // Récupérer les clés pour les utiliser comme en-têtes
            $headers = array_keys($data[0]);
            fputcsv($csvFile, $headers);
        }

        // 4. Écrire les données dans le fichier CSV
        foreach ($data as $row) {
            fputcsv($csvFile, $row);
        }

        // 5. Fermer le fichier CSV
        fclose($csvFile);

        echo "Conversion réussie, fichier CSV créé.";

    }

    public function getStat()
    {
        $http = $this->HttpClient;
        $result = $http->get($this->serveurDisant.'/exchanger/apis/getStat');
        $body = ['result' => $result];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function resetCacheManu()
    {
        $nbNonRelgesMvola = $this->Ozekimessageins->find('MvolaNonRegles')->count();
        Cache::write('nb_non_relges_mvola', $nbNonRelgesMvola);
    
        $nbNonRelgesOrange = $this->Ozekimessageins->find('OrangeNonRegles')->count();
        Cache::write('nb_non_relges_orange', $nbNonRelgesOrange);

        $nbNonRelgesAirtel = $this->Ozekimessageins->find('AirtelNonRegles')->count();
        Cache::write('nb_non_relges_airtel', $nbNonRelgesAirtel);
    
        $nbErrorMvola = $this->Ozekimessageins->find('MvolaErrorReseau')->count();
        Cache::write('nb_error_mvola', $nbNonRelgesAirtel);

        $nbErrorOrange = $this->Ozekimessageins->find('OrangeErrorReseau')->count();
        Cache::write('nb_error_orange', $nbNonRelgesAirtel);

        $nbErrorAirtel = $this->Ozekimessageins->find('AirtelErrorReseau')->count();
        Cache::write('nb_error_airtel', $nbNonRelgesAirtel);

        $nbAnnulationsOrange = $this->Ozekimessageins->find('OrangeCanceled')->count();
        Cache::write('orange_canceled', $nbNonRelgesAirtel);

        $nbAnnulationsMvola = $this->Ozekimessageins->find('MvolaCanceled')->count();
        Cache::write('mvola_canceled', $nbNonRelgesAirtel);

        $nbAnnulationsAirtel = $this->Ozekimessageins->find('AirtelCanceled')->count();
        Cache::write('airtel_canceled', $nbNonRelgesAirtel);

        $this->sendNotificationForFailedPaiementToday();

        $body = ['result' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }   

    public function sendNotificationForFailedPaiementToday()
    {
        $nbRappel = 2;
        if ($this->getParam('is_notifications_push_for_failed_paiement_active')) {

            $now = Chronos::now();

            $nonRelgesMvola = $this->Ozekimessageins->find('MvolaNonRegles')->where(['is_notif_droid_push_sent < ' => $nbRappel])->first();
            $mvolaEppelation = Configure::read('eppelations');
            if ($nonRelgesMvola) {
                $mvolaSentAt = Chronos::parse($nonRelgesMvola->senttime);
                if ($now->diffInSeconds($nonRelgesMvola->date_transfert) > 60) {
                    $this->sendPushNotificationForFailedPaiementToMe('Vous avez une nouvelle', ' commande '.$mvolaEppelation.' non réglée');
                    $this->Ozekimessageins->updateAll(['is_notif_droid_push_sent' => ++$nonRelgesMvola->is_notif_droid_push_sent], ['id' => $nonRelgesMvola->id]);
                }
            }

            $nonRelgesOrange = $this->Ozekimessageins->find('OrangeNonRegles')->where(['is_notif_droid_push_sent < ' => $nbRappel])->first();
            if ($nonRelgesOrange) {
                $orangeSentAt = Chronos::parse($nonRelgesOrange->senttime);
                if ($now->diffInSeconds($orangeSentAt) > 60) {
                    $this->sendPushNotificationForFailedPaiementToMe('Vous avez une nouvelle', ' commande Orange money non réglée');
                    $this->Ozekimessageins->updateAll(['is_notif_droid_push_sent' => ++$nonRelgesOrange->is_notif_droid_push_sent], ['id' => $nonRelgesOrange->id]);
                }
            }

            $nonRelgesAirtel = $this->Ozekimessageins->find('AirtelNonRegles')->where(['is_notif_droid_push_sent < ' => $nbRappel])->first();
            if ($nonRelgesAirtel) {
                $airtelSentAt = Chronos::parse($nonRelgesAirtel->senttime);
                if ($now->diffInSeconds($airtelSentAt) > 60) {
                    $this->sendPushNotificationForFailedPaiementToMe('Vous avez une nouvelle', ' commande Airtel Money non réglée');
                    $this->Ozekimessageins->updateAll(['is_notif_droid_push_sent' => ++$nonRelgesAirtel->is_notif_droid_push_sent], ['id' => $nonRelgesAirtel->id]);
                }
            }

        }

        $body = ['result' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));

        // $nbErrorMvola = $this->Ozekimessageins->find('MvolaErrorReseau')->count();
        // $nbErrorOrange = $this->Ozekimessageins->find('OrangeErrorReseau')->count();
        // $nbErrorAirtel = $this->Ozekimessageins->find('AirtelErrorReseau')->count();


    }

    public function sendSMSMerci($numero, $operation_type = "retrait")
    {
        $typeMobile = $this->checkMobile($numero);
        $remerciement = null;

        $messageMerci = $this->getMessageMerci();
        $messageTexte = trim($messageMerci['messageTexte']);
        $messageModele = $messageMerci['messageModele'];

        $this->logginto(compact(
            'numero',
            'messageModele'
        ), 'sms_merci');

        $nbSmsMerciDepotToday = $this->RemerciementSmss->find()->where(['operation_type' => 'depot', 'DATE_FORMAT(created, "%Y-%m-%d") =' => date('y-m-d')])->count();
        if ($operation_type == 'depot' && $nbSmsMerciDepotToday > 70) {
            return false; // on n'evoie plus
        }

        $smsDataToSave = ['numero' => $numero, 'msg_modele' => $messageModele, 'operation_type' => $operation_type];
        // $this->logginto($messageMerci, 'sms_to_send');
        // $this->logginto($smsDataToSave, 'sms_merci');

        if ($typeMobile == 'mvola') {
            $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
        } elseif ($typeMobile == 'orange') {
            $this->Ussd->setPort($this->getParam('port_sms_out_orange'));
        }

        // $m = "Tsy maninona ve raha mba asina 5 * sy comment fankasitrahana ity raha mety ? https://fr.trustpilot.com/review/moneiko.net merci :)";
        // $m = "Misaotra tompoko. Tsy maninona ve raha mba asina kely 5 * sy comment fankasitrahana ity raha mety ? https://fr.trustpilot.com/review/moneiko.net ? Merci bcp :)";
        // tsy mety 
        // $responseSend = $this->Ussd->send($numero, "Mankasitraka tompoko. Raha mba mety dia mba aza adino ny mampafantatra ny fisian'ny site moneiko.net amin'ireo namana sy akaiky, mazotoa tompoko ;)", $flashmode = false);
        // debug($responseSend);
        // die();  

        // Mety
        // $responseSend = $this->Ussd->send($numero, $m, $flashmode = false);
        // debug($this->Ussd->getPort());
        // debug($responseSend);
        // die();

        if (!$this->RemerciementSmss->find()->where($smsDataToSave)->first()) {

            if (!empty($messageTexte)) {
                // die('aoooo');
                $responseSend = $this->Ussd->send($numero, $messageTexte, $flashmode = false);


                // debug($messageTexte);
                // debug($resSend);
                // die();

                $smsDataToSave['msg'] = $messageTexte;
                $remerciement = $this->RemerciementSmss->newEntity($smsDataToSave, ['validate' => false]);
                if (!empty($responseSend)) {
                    $this->logginto($remerciement, 'sms_merci_response');
                }
                // $this->logginto($remerciement, 'sms_merci_entity_new');
                if(!$remerciement->getErrors()) {
                    $remerciement = $this->RemerciementSmss->save($remerciement);
                    // $this->logginto($remerciement, 'sms_merci_entity_saved');
                }
            }
        }

        $body = ['result' => $numero];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function majSmsByPaid($idpanierId)
    {
        // debug($idpanierId);
        // pr($this->Ozekimessageins->findByIdpanierId($idpanierId)->first());
        // die();
        $sms = $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['idpanier_id' => $idpanierId]);

        $this->logginto(compact(
            'smsId'
        ), 'maj_paid');

        $body = ['result' => $sms];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkUssd()
    {
        $body = ['result' => 'error'];
        $this->loadModel('UssdHistorics');
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $ussdHistoric = (bool) $this->UssdHistorics->find()->where([
                'command' => $data['command'],
                'DATE_FORMAT(created, "%Y-%m-%d") =' => date('y-m-d')
            ])->first();
        }

        $body = ['result' => $ussdHistoric];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function restartPc()
    {
        $this->Ussd->restartPc();
        die();
    }
}
?>