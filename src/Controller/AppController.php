<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Cake\Chronos\Chronos;
use Cake\Core\Configure;
use Cake\Cache\Cache;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $noSslParams = ['ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false, 'timeout' => 600];
    public $pushKey = null;
    
    // public $serveurDisant = 'https://moneiko.net';

    public function initialize()
    {
        parent::initialize();
        $this->response = $this->response->withHeader('ngrok-skip-browser-warning', 'true');
        $lienMessenger = $this->lienMessenger = "https://m.me/102686648325815";
        $this->loadModel('Ozekimessageins');
        // $r = $this->Ozekimessageins->find()->where(['MATCH(msg) AGAINST("+Ar +recu +de" IN BOOLEAN MODE)'])->andWhere(['sender' => 'Mvola'])->toArray();
        // $r = $this->Ozekimessageins->find()->where(['msg LIKE' => '%Ar recu de%'])->andWhere(['sender' => 'Mvola'])->limit(500)->toArray();

        // CREATE FULLTEXT INDEX ft_index ON ozekimessageins(msg);
        // "SELECT * FROM ozekimessagein WHERE MATCH(msg) AGAINST('+Ar+recu+de' IN BOOLEAN MODE)";
        // debug($r);
        // die();

        if ($this->pushKey == null) {
            $this->pushKey = $this->getParam('pushkey');
        }

        $this->loadModel('Parameters');

        // debug(file_exists($this->getParam('python_path')));
        // debug($this->getParam('python_path'));
        // die();

        Configure::write('pythonPath', $this->getParam('python_path').' '.ROOT.'/bin/');
        // debug(Configure::read('pythonPath'));
        // die();
        $this->senders = $this->mvolaSenders = Configure::read('mvolaSenders');
        $operateurs = $this->operateurs = Configure::read('operateurs');
        $this->airtelSenders = Configure::read('airtelSenders');
        $this->operateurs_fronts = $operateurs_fronts = Configure::read('operateurs_fronts');
        $this->orangeSenders = Configure::read('orangeSenders');
        $this->loadComponent('Ussd');
        $this->loadModel('Ports');
        $this->loadModel('RemerciementSmss');
        $this->loadModel('Tunnels');

        $this->HttpClient = new \HttpClient();
        
        foreach ($this->operateurs_fronts as $key => $op) {
            ${'portList'.$op['label']} = $this->Ports->find('list', [
                'keyField' => 'numero',
                'valueField' => 'numero'
            ])->where(['mobile' => $op['name']]);
            $this->set(compact('portList'.$op['label']));
        }



        foreach ($this->operateurs_fronts as $key => $op) {
            ${'driverList'.$op['label']} = $this->Ports->find('list', [
                'keyField' => 'letter',
                'valueField' => 'letter'
            ])->where(['mobile' => $op['name'], 'type' => 'envoi']);
            $this->set(compact('driverList'.$op['label']));
        }

        $listNumeroByFreePorts = $this->Ports->find()->where(['statut' => 'ok'])->groupBy('mobile');

        $freeNumeros = [];
        foreach ($listNumeroByFreePorts as $mobile => $port) {
            $freeNumeros[$mobile] = Hash::combine($port, '{n}.numero', '{n}.numero');
        }


        if (empty($this->getParam('num_port_titulaire'))) {
            $portsVerificators = $this->Ports->find()->where(['type in' => ['envoi', 'sms'], 'op_from_port in ' => ['orange', 'telma']])->select(['com', 'numero'])->enableHydration(false);
            $portsVerificators = array_filter($portsVerificators->toArray());

            $randomNum = rand(0, count($portsVerificators)-1);
            $numVerif = $portsVerificators[$randomNum] ?? null;
            $this->Parameters->updateAll(['port_titulaire' => $numVerif['com'], 'num_port_titulaire' => $numVerif['numero']], ['id' => 1]);
        }

        $param = $this->getParam();
        $listTypeNumeros = [];
        foreach ($this->operateurs as $key => $op) {
            $listTypeNumeros['envoi'][$param->{'port_'.$op}] = $param->{'port_'.$op}. ' - ' .$param->{'num_port_envoi_'.$op};
            $listTypeNumeros['reception'][$param->{'port_reception_'.$op}] = $param->{'port_reception_'.$op}. ' - ' .$param->{'num_port_reception_'.$op};
            $listTypeNumeros['sms'][$param->{'port_sms_out_'.$op}] = $param->{'port_sms_out_'.$op}. ' - ' .$param->{'num_port_sms_'.$op};
        }

        if ($param->is_opcache_reseted) {
            if (function_exists('opcache_reset') && extension_loaded('Zend OPcache')) {
                opcache_reset();
                $body = ['status' => 'success'];
            }
        }

        // debug($listNumeroByFreePorts->toArray());
        // debug($listTypeNumeros);


        // $usbLists = $this->Ussd->getUsbList();

        // $this->response->compress();

        // debug(MIIFdDCCBFygAwIBAgIQJ2buVutJ846r13Ci/ITeIjANBgkqhkiG9w0BAQwFADBvMQswCQYDVQQGEwJTRTEUMBIGA1UEChMLQWRkVHJ1c3QgQUIxJjAkBgNVBAsTHUFkZFRydXN0IEV4dGVybmFsIFRUUCBOZXR3b3JrMSIwIAYDVQQDExlBZGRUcnVzdCBFeHRlcm5hbCBDQSBSb290MB4XDTAwMDUzMDEwNDgzOFoXDTIwMDUzMDEwNDgzOFowgYUxCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVyIE1hbmNoZXN0ZXIxEDAOBgNVBAcTB1NhbGZvcmQxGjAYBgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMSswKQYDVQQDEyJDT01PRE8gUlNBIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAkehUktIKVrGsDSTdxc9EZ3SZKzejfSNwAHG8U9/E+ioSj0t/EFa9n3Byt2F/yUsPF6c947AEYe7/EZfH9IY+Cvo+XPmT5jR62RRr55yzhaCCenavcZDX7P0N+pxs+t+wgvQUfvm+xKYvT3+Zf7X8Z0NyvQwA1onrayzT7Y+YHBSrfuXjbvzYqOSSJNpDa2K4Vf3qwbxstovzDo2a5JtsaZn4eEgwRdWt4Q08RWD8MpZRJ7xnw8outmvqRsfHIKCxH2XeSAi6pE6p8oNGN4Tr6MyBSENnTnIqm1y9TBsoilwie7SrmNnu4FGDwwlGTm0+mfqVF9p8M1dBPI1R7Qu2XK8sYxrfV8g/vOldxJuvRZnio1oktLqpVj3Pb6r/SVi+8Kj/9Lit6Tf7urj0Czr56ENCHonYhMsT8dm74YlguIwoVqwUHZwK53Hrzw7dPamWoUi9PPevtQ0iTMARgexWO/bTouJbt7IEIlKVgJNp6I5MZfGRAy1wdALqi2cVKWlSArvX31BqVUa/oKMoYX9w0MOiqiwhqkfOKJwGRXa/ghgntNWutMtQ5mv0TIZxMOmm3xaG4Nj/QN370EKIf6MzOi5cHkERgWPOGHFrK+ymircxXDpqR+DDeVnWIBqv8mqYqnK8V0rSS527EPywTEHl7R09XiidnMy/s1Hap0flhFMCAwEAAaOB9DCB8TAfBgNVHSMEGDAWgBStvZh6NLQm9/rEJlTvA73gJMtUGjAdBgNVHQ4EFgQUu69+Aj36pvE8hI6t7jiY7NkyMtQwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wEQYDVR0gBAowCDAGBgRVHSAAMEQGA1UdHwQ9MDswOaA3oDWGM2h0dHA6Ly9jcmwudXNlcnRydXN0LmNvbS9BZGRUcnVzdEV4dGVybmFsQ0FSb290LmNybDA1BggrBgEFBQcBAQQpMCcwJQYIKwYBBQUHMAGGGWh0dHA6Ly9vY3NwLnVzZXJ0cnVzdC5jb20wDQYJKoZIhvcNAQEMBQADggEBAGS/g/FfmoXQzbihKVcN6Fr30ek+8nYEbvFScLsePP9NDXRqzIGCJdPDoCpdTPW6i6FtxFQJdcfjJw5dhHk3QBN39bSsHNA7qxcS1u80GH4r6XnTq1dFDK8o+tDb5VCViLvfhVdpfZLYUspzgb8c8+a4bmYRBbMelC1/kZWSWfFMzqORcUx8Rww7Cxn2obFshj5cqsQugsv5B5a6SE2Q8pTIqXOi6wZ7I53eovNNVZ96YUWYGGjHXkBrI/V5eu+MtWuLt29G9HvxPUsE2JOAWVrgQSQdso8VYFhH2+9uRv0V9dlfmrPb2LjkQLPNlzmuhbsdjrzch5vRpu/xO28QOG8=);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadModel('Parameters');

        $isLocal = false;
        if ($this->request->getEnv('REMOTE_ADDR') == '127.0.0.1') {
            $isLocal = true;
        }

        // callback màj de l'état des transaction no ilan'ity
        $mode = $this->mode = Configure::read('callback_mode');
        $callbackUrls = Configure::read('callback_url');
        $urlToWeb = $this->urlToWeb = $callbackUrls[$mode]['urlToWeb']; //ussdretrait 
        $urlCallback = $this->urlCallback = $callbackUrls[$mode]['urlCallback'];
        $urlRecheckStatus = $this->urlRecheckStatus = $callbackUrls[$mode]['urlCheckVenteStatus'];
        $urlMajRefTransMobileFromUssdLocal = $this->urlMajRefTransMobileFromUssdLocal = $callbackUrls[$mode]['urlMajRefTransMobileFromUssdLocal'];

        $param = $params = $parameter = $this->parameter = $this->params = $this->param = $this->Parameters->find()->first();
        $serveurDisant = $this->serveurDisant = $parameter['hook_serveur'];

        $this->driverTelma = $driverTelma = str_replace('\\', '', $this->parameter->driver_telma);
        $this->driverOrange = $driverOrange = str_replace('\\', '', $this->parameter->driver_orange);
        $this->driverAirtel = $driverAirtel = str_replace('\\', '', $this->parameter->driver_airtel);
        
        $mvolaSenders = $this->senders;
        $airtelSenders = $this->airtelSenders;
        $orangeSenders = $this->orangeSenders;
        

        $now = Chronos::now();
        $diffInMinutes = $now->diffInMinutes($parameter->last_ussd_at);

        $queryParams = $this->request->getQuery();

        $request = $this->request;
        $requestParam = @$request->params;
        $action = $requestParam['action'];
        $plugin = $requestParam['plugin'];
        $controller = $requestParam['controller'];

        if ($controller == 'Tdb') {


            if (($nbNonRelgesMvola = Cache::read('nb_non_relges_mvola')) === false) {
                $nbNonRelgesMvola = $this->Ozekimessageins->find('MvolaNonRegles')->count();
                Cache::write('nb_non_relges_mvola', $nbNonRelgesMvola);
            }
            
            if (($nbNonRelgesOrange = Cache::read('nb_non_relges_orange')) === false) {
                $nbNonRelgesOrange = $this->Ozekimessageins->find('OrangeNonRegles')->count();
                Cache::write('nb_non_relges_orange', $nbNonRelgesOrange);
            }

            if (($nbNonRelgesAirtel = Cache::read('nb_non_relges_airtel')) === false) {
                $nbNonRelgesAirtel = $this->Ozekimessageins->find('AirtelNonRegles')->count();
                Cache::write('nb_non_relges_airtel', $nbNonRelgesAirtel);
            }
            
            if (($nbErrorMvola = Cache::read('nb_error_mvola')) === false) {
                $nbErrorMvola = $this->Ozekimessageins->find('MvolaErrorReseau')->count();
                Cache::write('nb_error_mvola', $nbNonRelgesAirtel);
            }

            if (($nbErrorOrange = Cache::read('nb_error_orange')) === false) {
                $nbErrorOrange = $this->Ozekimessageins->find('OrangeErrorReseau')->count();
                Cache::write('nb_error_orange', $nbNonRelgesAirtel);
            }

            if (($nbErrorAirtel = Cache::read('nb_error_airtel')) === false) {
                $nbErrorAirtel = $this->Ozekimessageins->find('AirtelErrorReseau')->count();
                Cache::write('nb_error_airtel', $nbNonRelgesAirtel);
            }

            if (($nbAnnulationsOrange = Cache::read('orange_canceled')) === false) {
                $nbAnnulationsOrange = $this->Ozekimessageins->find('OrangeCanceled')->count();
                Cache::write('orange_canceled', $nbNonRelgesAirtel);
            }

            if (($nbAnnulationsMvola = Cache::read('mvola_canceled')) === false) {
                $nbAnnulationsMvola = $this->Ozekimessageins->find('MvolaCanceled')->count();
                Cache::write('mvola_canceled', $nbNonRelgesAirtel);
            } 

            if (($nbAnnulationsAirtel = Cache::read('airtel_canceled')) === false) {
                $nbAnnulationsAirtel = $this->Ozekimessageins->find('AirtelCanceled')->count();
                Cache::write('airtel_canceled', $nbNonRelgesAirtel);
            }
            // debug($nbAnnulationsAirtel);
            $this->setPortGsmVerificationTitulaireToBatch();
        }

        $checkServiceNg = $this->Ussd->checkServiceNg();
        $checkServiceNg = json_decode($checkServiceNg);
        // debug($checkServiceNg);
        $statutOzeki = $checkServiceNg[0]->Ozeki;
        $statutServMon = $checkServiceNg[1]->ServMon;

        $timeServerNow = Chronos::now();

        $numReceptions = $this->Ports->find()->where(['type' => 'reception'])->indexBy('mobile')->toArray();

        $this->Parameters->updateAll(['is_server_ng_started' => ($statutOzeki == 'True' ? true : false)], ['id' => 1]);

        $domain = $this->request->getEnv('HTTP_X_FORWARDED_HOST');
        if (strrpos($domain, 'ngrok') !== false) {
            $tunnel = $this->Tunnels->findByIsActive(1)->first();
            $hit = $tunnel->hit+1;

            $tunnel = $this->Tunnels->patchEntity($tunnel, ['hit' => $hit]);
            $tunnel = $this->Tunnels->save($tunnel);
        }

        $this->set(compact('param', 'nbErrorMvola', 'nbErrorOrange', 'nbErrorAirtel', 'timeServerNow', 'numReceptions', 'listTypeNumeros', 'statutOzeki', 'statutServMon', 'freeNumeros', 'portList', 'operateurs_fronts', 'operateurs', 'action', 'controller', 'urlToWeb', 'urlCallback','mode', 'urlMajRefTransMobileFromUssdLocal', 'nbAnnulationsMvola', 'nbAnnulationsOrange', 'nbAnnulationsAirtel', 'nbNonRelgesMvola', 'nbNonRelgesOrange', 'nbNonRelgesAirtel', 'usbLists', 'queryParams', 'orangeSenders', 'driverAirtel', 'diffInMinutes', 'mvolaSenders', 'airtelSenders', 'isLocal', 'portTelma', 'portTelma', 'params', 'parameter'));
    }

    public function beforeRender(\Cake\Event\Event $event)
    {
        parent::beforeRender($event);
        
    }

    public function deleteCacheStat($mobile = 'mvola')
    {
        $formatMobile = ucfirst($mobile);
        Cache::delete('nb_non_relges_'.$mobile);
        Cache::delete('nb_error_'.$mobile);
        Cache::delete($mobile.'_canceled');
        Cache::delete('nb_error_'.$mobile);

        if (($nbNonRelgesMobile = Cache::read('nb_non_relges'.$mobile)) === false) {
            $nbNonRelgesMobile = $this->Ozekimessageins->find($formatMobile.'NonRegles')->count();
            Cache::write('nb_non_relges'.$mobile, $nbNonRelgesMobile);
        }

        if (($nbErrorMobile = Cache::read('nb_error_'.$mobile)) === false) {
            $nbErrorMobile = $this->Ozekimessageins->find($formatMobile.'ErrorReseau')->count();
            Cache::write('nb_error_'.$mobile, $nbErrorMobile);
        }

        if (($nbAnnulationsMobile = Cache::read($mobile.'_canceled')) === false) {
            $nbAnnulationsMobile = $this->Ozekimessageins->find($formatMobile.'Canceled')->count();
            Cache::write($mobile.'_canceled', $nbAnnulationsMobile);
        }

        return;
    }


    public function setPortGsmVerificationTitulaireToBatch()
    {
        $portTitulaire = $this->getParam('port_titulaire');
        if (!(strrpos(file_get_contents(ROOT.'/bin/titulaire.bat'), $portTitulaire) !== false)) {

            $titulaireBat = "
            @echo off

            call time.bat
            
            set port=$portTitulaire

            cake console sendNotificationsMajNum telma %port% 350 null ASC >> %logfile% 2>>&1 & cake console sendNotificationsMajNum orange %port% 350 null ASC >> %logfile% 2>>&1 & cake console sendNotificationsMajNum airtel %port% 350 null ASC >> %logfile% 2>>&1 

            exit
            ";
            // cake console sendNotificationsMajNum telma %port% 350 null ASC & cake console sendNotificationsMajNum orange %port% 350 null ASC & cake console sendNotificationsMajNum airtel %port% 350 null ASC 

            $titulaireBat = preg_replace("/\ +/", " ",trim($titulaireBat));
            $titulaireBat = preg_replace('/^ +/m', '', $titulaireBat);

            file_put_contents(ROOT.'/bin/titulaire.bat', $titulaireBat);
            
        }
    }

    public function checkIfConnected()
    {
        $connected = @fsockopen("8.8.8.8", 53, $errno, $errstr, 5); // Timeout de 5 secondes
        if ($connected) {
            fclose($connected);
            return true; // Connexion réussie
        } else {
            return false; // Pas de connexion
        }
    }   

    public function checkUrl($url = 'https://google.com')
    {
        return (bool) @fopen($url,"r");

        // $headers = @get_headers($url);
        // if($headers === false) return false;
        // return preg_grep('~^HTTP/\d+\.\d+\s+2\d{2}~',$headers) ? true : false;

        // $http = new Client();
        // return $http->get($url)->code == 200;

        // $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        // $connection =  @socket_connect($socket, "10.197.24.40", 80);
        // return $connection;
    }


    function logginto($string, $file, $erase = false)
    {
        $filename = ROOT.DS.'logs'.DS.$file.'.log';
        new \Cake\Filesystem\File($filename, true, 0777);
        if ($erase == true) {
            file_put_contents($filename, "");
        }
        $file = file_get_contents($filename);
        $newContent = date('Y-m-d H:i:s', time()). ' '. print_r($string, true)."\n";
        $content = $newContent.'' . $file;
        file_put_contents($filename, $content);
    }

    public function getParam($field = null)
    {
        $this->loadModel('Parameters');
        if ($this->parametre == null) {
            $this->parametre = $this->Parameters->findById(1)->first();
        }

        if ($field != null) {
            return $this->parametre->{$field};
        }

        return $this->parametre;
    }

    public function getProdParams()
    {
        $http = new Client();
        return (object) $http->get("https://moneiko.net/exchanger/apis/getParams")->json;
    }

    public function sendPushNotificationToMe($raison, $paiement, $montant_recu, $numero, $solde_actuel = null)
    {
        $key = $this->pushKey;
        $titre = "Vous avez reçu un paiement $paiement succès, ";
        $paiement = ucfirst(strtolower($paiement));
        $desc = "au montant de $montant_recu Ariar, " . date('H:i');


        if ($solde_actuel) {
            // $desc .= ", solde $solde_actuel Ar";
            // $desc .= ', '.date('H:i');
        }

        $http = new Client();
        $url = "http://xdroid.net/api/message";
        $data = [
            'k' => $this->pushKey,
            't' => $titre,
            'c' => $desc
        ];

        if ($this->getParam('is_notifications_push_active')) {
            $res = $http->post($url, $data); // envoi paiement 
            // $this->logginto($data, 'data_push');
        }
        return;
    }

    public function sendSimpleNotificationPush($titre, $msg)
    {
        $key = $this->pushKey;

        $http = new Client();
        $url = "http://xdroid.net/api/message";
        $data = [
            'k' => $this->pushKey,
            't' => $titre,
            'c' => $msg.', '.date('H:i')
        ];

        $res = $http->post($url, $data); // envoi paiement 
        $this->logginto($data, 'data_push');
        return;
    }


    public function sendPushNotificationForFailedPaiementToMe($titre, $msg)
    {
        $key = $this->pushKey;

        $http = new Client();
        $url = "http://xdroid.net/api/message";
        $data = [
            'k' => $this->pushKey,
            't' => $titre,
            'c' => $msg.', '.date('H:i')
        ];

        if ($this->getParam('is_notifications_push_for_failed_paiement_active')) {
            $res = $http->post($url, $data); // envoi paiement 
            $this->logginto($data, 'data_push');
        }
        return;
    }

    public function ago2($time)
    {
        $diff_time = time() - strtotime($time);

        if($diff_time < 1){
            return '0 secondes';
        }
        
        $sec = array(   31556926    => 'an',
            2629743.83  => 'mois',
            86400       => 'jour',
            3600        => 'heure',
            60          => 'min',
            1           => 'sec'
        );

        foreach($sec as $sec => $value){
            $div = $diff_time / $sec;
            if($div >= 1){
                $time_ago = round($div);
                $time_type = $value;
                if($time_ago >= 2 && $time_type != "mois"){
                    $time_type .= "s";
                }
                return '' . $time_ago . ' ' . $time_type;
            }
        }
    }   

    public function ago3($datetime)
    {
        $ago = false;

        if ((Chronos::parse($datetime)->wasWithinLast('1 hour'))) {
            $ago = $this->ago2($datetime);
        } else {
            $ago = Chronos::parse($datetime)->format('d/m, H\\hi');
        }
        
        return $ago;
    }
}
