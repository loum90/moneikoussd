<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Datasource\ModelAwareTrait;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\Chronos\Chronos;
use NumberFormatter;

class UssdComponent extends Component
{
    use ModelAwareTrait;
    
    public function initialize(array $config)
    {
        parent::initialize($config);
        $pythonPath = $this->pythonDefaultPath = Configure::read('pythonPath');
        // debug($pythonPath);
        // die();
        $this->port = 'com13';
        $this->loadModel('Parameters');
        $this->python = $pythonPath;
        $this->pythonPathOrange = $pythonPath.'orange/';
        $this->loadModel('TransactionsHistorics');
        $this->controller = $this->getController();

    }

    public function setPortAndPathByNumeroMobile($mobile)
    {
        $parameter = $this->Parameters->find()->first();
        $portOrange = $parameter->port_orange;
        $portTelma = $parameter->port_telma;
        $portAirtel = $parameter->port_airtel;
        $pythonPath = Configure::read('pythonPath');

        $type = $this->checkMobile($mobile);
        // $this->python = $pythonPath.$type.'/';
        $this->python = $pythonPath.'orange/';

        $port = false;
        if ($type == 'orange') {
            $this->port = $portOrange;
        } elseif (in_array($type, ['mvola', 'telma'])) {
            $this->port = $portTelma;
        } elseif ($type == 'airtel') {
            $this->port = $portAirtel;
        }

        return $this->port;
    }

    public function setPort($port)
    {
        $this->python = $this->pythonPathOrange;
        return $this->port = $port;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function checkMobile($string)
    {
        $mobile = '';
        if (substr($string, 0, 3) === '032' || substr($string, 0, 3) === '037') {
            $mobile = 'orange';
        } elseif (substr($string, 0, 3) === '034' || substr($string, 0, 3) === '038') {
            $mobile = 'mvola';
        } elseif (substr($string, 0, 3) === '033') {
            $mobile = 'airtel';
        } 

        return $mobile;
    }

    public function checkMobile2($string)
    {
        $mobile = '';
        if (substr($string, 0, 3) == '032' || substr($string, 0, 3) === '037') {
            $mobile = 'orange';
        } elseif (substr($string, 0, 3) == '034' || substr($string, 0, 3) == '038') {
            $mobile = 'telma';
        } elseif (substr($string, 0, 3) == '033') {
            $mobile = 'airtel';
        } 

        return $mobile;
    }

    public function checkUssdMobile($string)
    {
        $mobile = '';
        if (strrpos('#144', $string) !== false) {
            $mobile = 'orange';
        } elseif (strrpos('#111', $string) !== false) {
            $mobile = 'mvola';
        }

        return $mobile;
    }

    public function resetGsm()
    {
        $python = $this->python;
        $cmd = $python."wake.py ".$this->port." 2>&1";
        return $this->execCli($cmd);
    }

    public function restartPc()
    {
        $python = $this->python;
        $cmd = $python."restart_windows.py 2>&1";
        // debug($cmd);
        // die();
        return $this->execCli($cmd);
    }

    public function wakeGsm($port = null)
    {
        $python = $this->python.'orange/';
        $cmd = $python."wake.py ".$port." 2>&1";
        return $this->execCli($cmd);
    }

    /**
     * lance la commande gsmmodem-ussd.py com12 #123# 3 1 0325541393
     * @param  [type]  $ussdCode  exemple #123#
     * @param  integer $printMode si 1, le prochain exec bug et ne s'execute pas, c uniquement la dernière commande qui doit être = 1
     * @return [type]  string           
     */
    public function run($ussdPrimary, $ussdReplies = [], $isTest = null)
    {
        $parameter = $this->Parameters->find()->first();
        $now = Chronos::now();
        $diffInMinutes = $now->diffInMinutes($parameter->last_ussd_at);
        $this->waitForOpen($this->port); // évite process occupé
        // $this->waitForOpen($this->port); // évite process occupé

        // if ($diffInMinutes > 5) {
        //     $this->resetGsm($this->port);
        //     sleep(0.5);
        //     $parameter = $this->Parameters->patchEntity($parameter, ['last_ussd_at' => $now], ['validate' => false]);
        //     $this->Parameters->save($parameter);
        // }
        
        // $this->waitForOpen($this->port); // évite process occupé

        $python = $this->python;
        $cmd = $python."gsmmodem-ussd.py ".$this->port." ".$ussdPrimary; // 2>&1 permet de retourner le message s'il y a erreur
        // debug($cmd);
        // die();

        $repliesString = '';
        if (count($ussdReplies)) {
            $repliesString = implode(' ', $ussdReplies);
            $cmd .= ' '.$repliesString;
        }

        $cmd .= " 2>&1";
        // debug($cmd);
        // die();

        $fullUssd = $ussdPrimary.' '.$repliesString;
        // $this->controller->logginto($fullUssd, 'full_ussd', $erase = false);

        return $this->execCli2($cmd);
    }

    public function stopServiceNg()
    {
        $python = $this->python;
        $cmd = $python."orange/service_ng_stop.py 2>&1";
        $res =  $this->execCli2($cmd);

        if (strrpos($res, 'S-Gateway a t arrt.') !== false) {
            return true;
        }

        return false;
    }

    public function checkServiceNg()
    {
        $python = $this->pythonDefaultPath;
        $cmd = $python."orange/check_service_ng.py 2>&1";
        $res =  $this->execCli2($cmd);
        return $res;
    }

    public function generateReportUsbTreeView()
    {
        $python = $this->pythonDefaultPath;
        $cmd = $python."orange/report.py 2>&1";
        debug($cmd);
        $res =  $this->execCli($cmd);
        $res =  $this->execCli2($cmd);
        exec($cmd);
        return $res;
    }

    public function startServiceNg()
    {
        $python = $this->python;
        $cmd = $python."orange/service_ng_start.py 2>&1";
        return $this->execCli2($cmd);
    }

    public function usb($action, $usbLetter)
    {
        $python = $this->python;
        $cmd = $python."orange/".$action."_usb.py $usbLetter 2>&1";
        return $this->execCli2($cmd);
    }

    public function checkServerNg()
    {
        $python = $this->python;
        $cmd = $python."orange/check_service_ng.py 2>&1";
        return $this->execCli2($cmd);
    }

    public function restartGateway()
    {
        $python = $this->python;
        $cmd = $python."orange/service_ng_force_restart.py 2>&1";
        return $this->execCli2($cmd);
    }

    public function readSms($memType = "SM", $mobile) // ou SM,ME,MT
    {
        // $this->waitForOpen($this->port); /// pour evider le bug ressource utilisée par un autre processeur
        
        $python = $this->python;
        $cmd = $python."gsmmodem-sms.py ".$this->port." ".$memType." $mobile 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        return $this->execCli2($cmd);
    }

    function getOrangeSmsDatas($smss)
    {
        $smsInfos = [];
        foreach ($smss as $key => $sms) {
            $smsContent = $sms['content'];

            $extractedDatas = $this->extractOrangeTransactionSms($smsContent);
            if ($extractedDatas['ref']) {
                $smsInfos[] = [
                    'ref' => $extractedDatas['ref'],
                    'montant' => $extractedDatas['montant'],
                    'date' => new Chronos($sms['date']),
                    'numero' => $extractedDatas['numero'] ?? null,
                    'paiement' => 'orange',
                    'solde_actuel' => $extractedDatas['solde_actuel'],
                ];
            }

        }

        return ($smsInfos);
    }

    function saveTransactionsHistorics($smsDatas)
    {
        $count = 0;
        foreach ($smsDatas as $key => $sms) {
            $findedTransaction = $this->TransactionsHistorics->findByRef($sms['ref'])->first();
            if (!$findedTransaction) {
                $transactionsHistoric = $this->TransactionsHistorics->newEntity($sms);
                if (!$transactionsHistoric->getErrors()) {
                    $this->TransactionsHistorics->save($transactionsHistoric);
                    $count ++;
                } else {
                    debug($transactionsHistoric->getErrors());
                }
            }
        }

        return $count;
    }

    public function deleteSms($mobile)
    {
        $python = $this->python;
        // $cmd = $python."gsmmodem-deletesms.py ".$this->port." ".$memType." 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        $cmd = $python."gsmmodem-deletesms.py ".$this->port." $mobile 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        return $this->execCli($cmd);
    }

    public function getMvolaSmsDatas($smss)
    {
        $smsInfos = [];
        $formater = new NumberFormatter("fr_FR", NumberFormatter::DECIMAL);
        foreach ($smss as $key => $sms) {
            $smsContent = $sms['content'];
            $lastString = collection(explode(' ', $smsContent))->last();
            $lastInt = is_numeric($lastString);

            // 1er cas si sms contenant le ref int
            if ($lastInt && strlen($lastString) == 10 && strlen($sms['content']) >= 120) {
                $smsDetail = $this->extractMvolaTransactionSms($smsContent);
                $smsInfos[$key] = [
                    'ref' => $lastString,
                    'date' => new Chronos($sms['date']),
                    'montant' => $smsDetail['montant'] ,
                    'numero' => $smsDetail['numero'] ,
                    'paiement' => 'mvola',
                    'solde_actuel' => $formater->parse($smsDetail['solde_actuel'], NumberFormatter::TYPE_INT32),
                ];

            } else {
            // +ieurs sms, le ref se trouve dans l'un des msg précédents
                $rightSms = $this->getRightMvolaSms(@$smss[$key], @$smss[$key-1], @$smss[$key-2], @$smss[$key+1]);
                if ($rightSms && strlen($lastString) == 10) {
                    $smsDetail = $this->extractMvolaTransactionSms($rightSms);
                    $smsInfos[$key] = [
                        'ref' => $lastString,
                        'date' => new Chronos($sms['date']),
                        'montant' => $smsDetail['montant'] ,
                        'numero' => $smsDetail['numero'] ,
                        'paiement' => 'mvola',
                        'solde' => $smsDetail['solde_actuel']
                    ];
                    if ((float) ($smsDetail['montant']) == 0) {
                        unset($smsInfos[$key]);
                    }
                }
            }

        }

        // debug($smsInfos);
        // die();

        return ($smsInfos);
    }

    public function execCli($cmd)
    {
        $handle = popen($cmd, 'r');
        $read ='';

        while (!feof($handle)) 
        {
            $read .= utf8_encode(fread($handle, 16000));
        }
        pclose($handle);
        return $read;
    }

    public function execCli2($cmd)
    {
        $result = null;
        $process = proc_open($cmd, [1 => ['pipe', 'w']], $pipes, null, null, ['suppress_errors' => true]);

        if (\is_resource($process)) {
            $result = utf8_encode(stream_get_contents($pipes[1]));
            fclose($pipes[1]);
            proc_close($process);
        }

        return trim($result);
    }

    public function extractBetween($message, $text1, $text2 = '')
    {

        if (strlen(trim($text1)) > 0) {
            $part1 = explode(trim($text1), $message)[1] ?? false;
        } else  {
            $part1 = $message;
        }

        if ($text2) {
            $text = trim(explode(trim($text2), $part1)[0]);
        } else {
            $text = trim($part1);
        }
        return $text;
    }

    public function extractTitulaireOrange($content)
    {
        return $this->extractBetween($content, 'AR vers ', ' 032');
    }


    public function extractTitulaireMobileMoneyFromRequestTelma($content)
    {
        return $this->extractBetween($content, 'Ar vers ', '(03');
    }

    public function extractTitulaireMvola($content)
    {
        return $this->extractBetween($content, ' MGA à', ' (03');
    }

    public function getRightMvolaSms($smsCourant, $smsPrecedent, $smsPrecedentPrecedent, $smsSuivant)
    {
        if ((!strrpos($smsPrecedent['content'], 'Vous avez recu') !== false || !strrpos($smsPrecedentPrecedent['content'], 'Vous avez recu') !== false)) {
            if (!strrpos($smsCourant['content'], 'Vous avez transfere') !== false && (strrpos($smsPrecedent['content'], '2/2 ') !== false || strrpos($smsPrecedent['content'], '2/3 ') !== false || strrpos($smsPrecedent['content'], '3/3 ')) !== false) {
                return $smsSuivant['content'];
            }elseif (strrpos($smsPrecedent['content'], 'Vous avez transfere') !== false) {
                return $smsPrecedent['content'];
            } else {
                if (!strrpos($smsCourant['content'], 'Vous avez transfere') !== false && !strrpos($smsPrecedent['content'], 'Vous avez recu') !== false) {
                    return $smsPrecedentPrecedent['content'];
                }
            }
        } else {
            return false;
        }
    }

    public function checkSmsTransfert($sms)
    {
        return (strrpos($sms, 'Vous avez transfere') !== false);
    }

    public function extractMvolaTransactionSms($message)
    {
        $montant = str_replace(' ', '', $this->extractBetween($message, 'Vous avez transfere ', ' Ar a '));
        $numero = $this->extractBetween($message, ' (', ') le ');
        $ref = $this->extractBetween($message, ' Ref: ', '');
        $solde_actuel = $this->extractBetween($message, 'Votre solde est de ', 'Ar.');

        return compact('solde_actuel', 'montant', 'numero', 'ref');
    }

    public function extractOrangeTransactionSms($message)
    {
        $montant = $this->extractBetween($message, 'Votre transfert de ', ' Ar vers le');
        $numero = $this->extractBetween($message, 'Ar vers le ', ' est reussi');
        $ref = trim($this->extractBetween($message, 'Trans id ', '. Orange Money'));
        if (substr($ref, -1) == '.') {
            $ref = substr($ref, 0, -1);
        }
        $solde_actuel = $this->extractBetween($message, 'Nouveau solde: ', ' Ar. Trans');

        return compact('montant', 'numero', 'ref', 'solde_actuel');
    }

    /**
     * retourne le numéro de l'envoyeur et non du destinataire
     * @param  string $numero  [description]
     * @param  string $message [description]
     * @return [type]          [description]
     */
    public function send($numero = "0325541393", $message = "test", $flashmode = true)
    {
        $message = trim($message);
        // $message = filter_var($message, FILTER_SANITIZE_STRING);
        // debug($message);
        // die();

        if ($flashmode == true) {
            $cmd = $this->pythonPathOrange."gsmmodem-sendsms.py ".$this->port." ".$numero." \"$message\" 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        } else {
            $cmd = $this->pythonPathOrange."gsmmodem-sendsms-2.py ".$this->port." ".$numero." \"$message\" 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        }

        $res = $this->execCli2($cmd);
        // debug($res);
        // die();
        // echo ($res);
        // die();
        return $res;
    }

    /**
     * retourne le numéro de l'envoyeur et non du destinataire
     * @param  string $numero  [description]
     * @param  string $message [description]
     * @return [type]          [description]
     */
    public function send2($numero = "0325541393", $message = "test")
    {
        $cmd = $this->pythonPathOrange."send_sms.py ".$this->port." ".$numero." \"$message\" 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        // debug($cmd);
        // die();
        $res = $this->execCli2($cmd);
        // debug($res);
        // die();
        // echo ($res);
        // die();
        return $res;
    }

    public function readAndSaveSmsFromAllMemory($mobile = "032")
    {
        $this->setPortAndPathByNumeroMobile($mobile);

        $sm = $this->readSms("ME", $mobile);
        $smsFromSm = explode("\n", $sm);

        $me = $this->readSms("SM", $mobile);
        $smsFromMe = explode("\n", $me);

        if ($mobile == "034") {
            // $me = $this->readSms("MT");
            // $smsFromMe = explode("\n", $me);
            // $me = $this->readSms("SR");
            // $smsFromMe = explode("\n", $me);
        }

        $all = array_filter(array_merge($smsFromSm ?? [], $smsFromMe ?? []));

        // debug($all);
        // die();

        $smss = [];
        foreach ($all as $key => $sms) {

            $ciblageTexte = in_array($mobile, ['032', '037']) ? 'Votre transfert' : ' ';
            if (strrpos($sms, $ciblageTexte) !== false) {
                $part = explode(' / ', $sms);
                $date = new Chronos($part[1]);
                $smss[] = [
                    'from' => $part[0] ?? null,
                    'date' => $part[1] ? $part[1] : null,
                    'content' => $part[2] ?? null,
                ];
            }
        }

        $smss = collection($smss)->sortBy('date', SORT_ASC, SORT_NATURAL)->toArray();

        if (in_array($mobile, ['032', '037'])) {
            $smsDatas = $this->getOrangeSmsDatas($smss);
        } else {
            $smsDatas = $this->getMvolaSmsDatas($smss);
        }

        // debug($smsDatas);
        // die();
        $nbSmsSaved = $this->saveTransactionsHistorics($smsDatas);


        return $nbSmsSaved;
    }

    public function sendFakeOrangeSms($data, $destination = "0326792722")
    {
        $date = $data['date'];
        $numero = $data['numero'];
        $titulaire = $data['titulaire'];
        $montant = $data['montant'];
        $transId = "PP210516.1335.B".date('dHis');
        $msg = "Votre transfert de $montant Ar vers le $numero est reussi. Frais : 200 Ar .Nouveau solde: 578738 Ar. Trans id $transId. Orange Money vous remercie";
        $response = $this->send($destination, $msg);
        // debug($response);
        // die();
    }

    public function updateTransactionFromSms($data)
    {
        $now = Chronos::now();
        $subMinute = $now->subMinute(3);
        $findedTransaction = $this->TransactionsHistorics->find()->where(['paiement' => 'orange', 'numero' => $data['numero'], 'montant' => $data['montant'], 'date >=' => $subMinute, 'date <' => $now])->first();
        debug($findedTransaction);
    }

    

    public function recursiveUssd($ussdPrimary, $ussdReplies = [], $essai = 1)
    {
        $essais = 5;
        $res = $this->run($ussdPrimary, $ussdReplies);
        if (strrpos($res, 'Exception') !== false) {
            if ($essai <= $essais) {
                $this->recursiveUssd($ussdPrimary, $ussdReplies, ++$essai);
            }
        }


        return $res;

    }

    public function resetUsb($diverLetter = "M:")
    {
        $usbSafetyRemovePath = Configure::read('usbSafetyRemovePath');
        $stopCmd = $usbSafetyRemovePath.'usr.exe stop -d '.$diverLetter;
        $resStopCmd = exec($stopCmd);
        $restartCmd = $usbSafetyRemovePath.'usr.exe return -d '.$diverLetter;
        $resSestartCmd = exec($restartCmd);
        return $resStopCmd."\n".$resSestartCmd;
    }

    public function getUsbGsmList($diverLetter = "M:")
    {
        $usbSafetyRemovePath = Configure::read('usbSafetyRemovePath');
        $listCmd = $usbSafetyRemovePath.'usr.exe list -s';
        $resListCmd = $this->execCli($listCmd);

        $driverLists = explode("\n", $resListCmd);
        $gsmLists = [];
        foreach ($driverLists as $key => $driver) {
            if (strrpos($driver, 'HUAWEI') !== false) {
                $letter = $this->extractBetween(trim($driver), '(', ')');
                $gsmLists[] = $letter;
            }
        }

        return $gsmLists;
    }

    public function checkIfSuccess($string, $typeMobile)
    {
        if ($typeMobile == 'mvola') {
            // Exeption Mvola
            $this->controller->logginto($string, 'mvola/recheck_ussd_res', $erase = false);
            if (strrpos($string, 'cusdMatches[0].group(1)') !== false) {
                $this->controller->logginto('ao', 'mvola/recheck_ussd_res_state', $erase = false);
                return true;
            }
        }
        if (strrpos($string, 'transaction a reussi') !== false || // Mvola
            strrpos($string, 'Transfert initie') !== false || // Orange Money
            strrpos($string, 'Transaction en cours') !== false || // Airtel Money
            strrpos($string, 'Coupon') !== false
        ) {
            return true;
        }

        return false;
    }

    public function checkIfPortOccupe($string)
    {
        if (strrpos('La ressource demandée est en cours', $string) !== false) {
            return true;
        }

        return false;
    }

    public function checkIfIsOpened($port)
    {
        $cmd = $this->python."check.py ".$port." 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        return $this->execCli2($cmd);
    }

    public function cleanString($string)
    {
        return preg_replace( "/\r|\n/", "", $string);
    }

    public function waitForOpen($port)
    {
        $r = $this->checkIfIsOpened($port);
        $r = preg_replace( "/\r|\n/", "", $r);

        // debug($r);
        $i = 0;
        while ($r == 'busy') {

            $res2 = $this->checkIfIsOpened($port);
            $res2 = preg_replace( "/\r|\n/", "", $res2);
            if ($res2 == 'ok') {
                sleep(0.2);
                return;
            }
            if ($i == 18) { // 10 secondes (5 * 2)
                return ('modem trop occupé ou déconnecté');
            }
            sleep(2);
            $i++;
        }
    }

    public function checkPortListDatas()
    {
        $this->checkPortList();
    }

    public function checkPortListShort()
    {
        $cmd = $this->python."orange/port_list_short.py 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        $handle = popen($cmd, 'r');
        $read ='';

        while (!feof($handle)) 
        {
            $read .= trim(utf8_encode(fread($handle, 16000)));
        }
        pclose($handle);
        return $read;
    }
    public function checkPortList()
    {
        $cmd = $this->python."orange/port_list.py 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        $explodedPython = explode(' ', $this->python);
        if (isset($explodedPython[0])) {
            if (!file_exists($explodedPython[0])) {
                pr($explodedPython[0]. ' Le chemin du fichier python.exe est introuvable');
                pr($explodedPython[0]. ' Le chemin a été mise à jour veuillez réactualiser');

                exec("wmic /node:$_SERVER[REMOTE_ADDR] COMPUTERSYSTEM Get UserName", $user);
                $computerInfo = explode('\\', $user[1]);
                $userLogged = $computerInfo[1];
                $path = 'C:\Users\\'.$userLogged.'\\AppData\Local\Programs\Python\Python38\python.exe';
                $this->Parameters->updateAll(['python_path' => $path], ['id' => 1]);
            }
        }
        // die();
        // debug($cmd);
        // die();
        $handle = popen($cmd, 'r');
        $read ='';

        while (!feof($handle)) 
        {
            $read .= trim(utf8_encode(fread($handle, 16000)));
        }
        pclose($handle);
        return $read;
    }

    public function checkUsbList()
    {
        $cmd = $this->python."/orange/usb_list.py 2>&1"; // 2>&1 permet de retourner le message s'il y a erreur
        $handle = popen($cmd, 'r');
        $read ='';

        if ($handle) {
            while (!feof($handle)) 
            {
                $read .= trim(utf8_encode(fread($handle, 16000)));
            }
            pclose($handle);
        }
        return $read;
    }

    public function getUsbList()
    {
        $usbs = $this->checkUsbList();
        $usbs = json_decode($usbs);
        return array_combine($usbs ?? [], $usbs ?? []);
    }

    public function extractMvolaRecuData($message)
    {
        // $montant_recu = str_replace(' ', '', $this->extractBetween($message, 'Vous avez recu ', ' Ar de la'));
        $montant_recu = (float) str_replace(' ', '', $this->extractBetween($message, '1/2 ', ' Ar recu de'));
        $numero = $this->extractBetween($message, ' (', ') le ');
        $ref = $this->extractBetween($message, ' Ref: ', ' 2/2');
        // $titulaire = $this->extractBetween($message, 'de la part de ', ' (03');
        $titulaire = $this->extractBetween($message, 'Ar recu de ', ' (03');
        // $raison = $this->extractBetween($message, 'Raison:', '. Votre solde');
        $raison = $this->extractBetween($message, 'Raison: ', '. Solde');
        $idpanier = substr($raison, 1);
        // $solde_actuel = str_replace(' ', '', $this->extractBetween($message, 'Votre solde est de ', 'Ar.'));
        $solde_actuel = (float) str_replace(' ', '', $this->extractBetween($message, 'Solde : ', 'Ar. Ref'));

        return compact('solde_actuel', 'montant_recu', 'numero', 'ref', 'raison', 'idpanier', 'titulaire');
    }

    public function extractAirtelRecuData($message)
    {
        $montant_recu = str_replace(' ', '', $this->extractBetween($message, 'avez recu Ar ', ' du '));
        $numero = $this->extractBetween($message, 'du ', ' pour');
        $ref = $this->extractBetween($message, 'ID: ', '');
        // debug($message);
        // debug($ref);
        // $raison = $this->extractBetween($message, 'pour ', '. Le solde');
        // $idpanier = substr($raison, 1); // Normal ra A satria NA dia N supprimé
        
        // $solde_actuel = str_replace(' ', '', $this->extractBetween($message, 'votre compte est Ar ', '. Trans ID'));
        $solde_actuel = str_replace(' ', '', $this->extractBetween($message, 'est Ar ', 'ID'));
        $solde_actuel = str_replace('.Trans', '', $solde_actuel);
        $solde_actuel = str_replace('.', '', $solde_actuel);

        return compact('solde_actuel', 'montant_recu', 'numero', 'ref', 'raison', 'idpanier');
    }

    public function extractOrangeRecuData($message)
    {
        $montant_recu = str_replace(' ', '', $this->extractBetween($message, 'recu un transfert de ', 'Ar venant du'));
        $numero = $this->extractBetween($message, 'venant du ', ' Nouveau Solde');
        $ref = $this->extractBetween($message, 'Trans Id: ', '. Orange Money vous');
        $solde_actuel = str_replace(' ', '', $this->extractBetween($message, 'Nouveau Solde: ', 'Ar.'));

        return compact('solde_actuel', 'montant_recu', 'numero', 'ref', 'raison', 'idpanier');
    }

    public function getSoldeFromTransfert($message, $type = "orange")
    {
        if ($type == "orange") {
            $solde = str_replace(' ', '', $this->extractBetween($message, 'Ar .Nouveau solde: ', ' Ar. Trans id'));
        } elseif ($type == "airtel") {
            $solde = str_replace(' ', '', $this->extractBetween($message, 'Votre solde disponible est de Ar ', '. Merci. Trans ID'));
        }
        return compact('solde');
    }

    public function getCurrentNumroOrange()
    {
        $url = Router::url(['controller' => 'Tdb', 'action' => 'presetUssd', "032", 'code' => "#888#"], true);
        $http = new Client();

        $res = $http->get($url)->json;
        $numeroOrange = $this->extractBetween($res['msg'], '261', 'Session fermée');
        $numeroOrange = '0'.str_replace('<br />', '', $numeroOrange);

        return $numeroOrange;
    }

    public function getNetworkName($port)
    {
        $cmd = $this->python."get_network.py $port 2>&1";
        $result = trim(strtolower($this->execCli($cmd)));
        $firstOcur = strpos($result, '+261');
        $prefixNum = substr($result, $firstOcur, 6); // +26133
        $operator = null;
        $this->controller->logginto($result, 'gsm/ussd_get_network_name', $erase = false);
        // debug($prefixNum);
        // die();

        if ($prefixNum == '+26134') {
            $operator = 'telma';
        } 
        elseif($prefixNum == '+26132') {
            $operator = 'orange';
        }
        elseif($prefixNum == '+26133') {
            $operator = 'airtel';
        } 

        return $operator;   
    }

    public function getNumeroFromAt($port)
    {
        $cmd = $this->python."get_self_num.py $port 2>&1";
        // debug($cmd);
        $result = trim(strtolower($this->execCli($cmd)));
        // debug($result);
        // die();
        $firstOcur = strpos($result, 'cnum:');
        // debug($cmd);
        $prefixNum = substr($result, $firstOcur, 19); // +26133
        $prefixNum = trim(str_replace('cnum: "","', '', $prefixNum));
        $result = "0".$prefixNum;
        return $result;
    }

    public function getCurrentNumroTelma()
    {
        $url = Router::url(['controller' => 'Tdb', 'action' => 'presetUssd', "032", 'code' => "#888#"], true);
        $http = new Client();

        $res = $http->get($url)->json;
        $numeroOrange = $this->extractBetween($res['msg'], '261', 'Session fermée');
        $numeroOrange = '0'.str_replace('<br />', '', $numeroOrange);

        return $numeroOrange;
    }


    public function checkExistingByExplode($var1, $var2)
    {
        $var1Exploded = explode(' ', $var1);

        foreach ($var1Exploded as $key => $value) {
            if (strrpos($var2, $value) !== false) {
                return true;
            } elseif ($this->wordMatch($var2, $value) > 90) {
                return true;
            }
        }

        return false;
    }

    function wordMatch($str1, $str2) 
    {
        $strlen1=strlen($str1);
        $strlen2=strlen($str2);
        $max=max($strlen1, $strlen2);

        $splitSize=250;
        if($max>$splitSize)
        {
            $lev=0;
            for($cont=0;$cont<$max;$cont+=$splitSize)
            {
                if($strlen1<=$cont || $strlen2<=$cont)
                {
                    $lev=$lev/($max/min($strlen1,$strlen2));
                    break;
                }
                $lev+=levenshtein(substr($str1,$cont,$splitSize), substr($str2,$cont,$splitSize));
            }
        }

        else {
            $lev=levenshtein($str1, $str2);
        }

        $porcentage= (float) @(-100*$lev/$max+100);
        if($porcentage>75)//Ajustar con similar_text
        similar_text($str1,$str2,$porcentage);

        return $porcentage;
    }

    public function getSn($port)
    {
        $cmd = $this->python."checksn.py $port 2>&1";
        $result = str_replace("\\n", '', $this->execCli($cmd));
        $result = str_replace("\\b", '', $result);
        $result = str_replace("\\r", '', $result);
        $result = str_replace("\\b", '', $result);
        $result = str_replace("'", '', $result);
        $result = str_replace("OK", '', $result);
        $result = str_replace("b", '', $result);
        $result = trim($result);
        return $result;   
    }
}
?>