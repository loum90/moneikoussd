<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Routing\Router;
use App\Controller\SmsController;
use Cake\Chronos\Chronos;

class OneXController extends AppController
{
    public $noSslParams = [
        'redirect' => false,
        'ssl_verify_peer' => false, 
        'ssl_verify_host' => false, 
        'ssl_verify_peer_name' => false
    ];

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadComponent('Ussd');
        $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("032");
        $this->port = $this->parameter->port_telma_sms;
        $type = $this->request->getQuery('type');
        $title = 'SMS - Onex';
        $this->set(compact('title'));
        $this->loadModel('Ozekimessageins');
    }

    public function exectransfer()
    {
        $http = new Client();
        $isServerReachable = (bool) @file_get_contents("http://127.0.0.1:8888/hello.py");

        if ($isServerReachable) {
            $res = $http->get('http://127.0.0.1:8888/exectransfer.py', [], $this->noSslParams)->body();
            echo (utf8_decode($res));
        }
        die();
    }

    public function sendSmsOrangeOnex()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $save = [
                'operator' => 'ONEX',
                'sender' => 'OrangeMoney',
                'msg' => $data['sms_text'],
                'id_1xbet' => $data['id_1xbet'],
                'senttime' => Chronos::now(),
            ];

            $sms = $this->Ozekimessageins->newEntity($save, ['validate' => false]);
            if(!$sms->getErrors()) {
                $this->Ozekimessageins->save($sms);
            }

            return $this->redirect($this->referer());
        }
    }       

    public function index()
    {
        $senders = array_merge($this->senders, $this->airtelSenders, $this->orangeSenders);
        $smss = $this->Ozekimessageins->find()->where([
                "OR" => [
                    ['msg LIKE' => "%Vous avez recu%"]
                ],
                'sender IN' => $senders,
                'operator' => "ONEX",
            ]
        )->order(['senttime' => 'DESC']);

        $options = $this->request->getQuery();
        $statut_1x = @$options['statut_1x'] == null ? 'pending' : $options['statut_1x'];

        if ($statut_1x) {
            $smss->where(['statut_1x' => $statut_1x]);
        }


        $options = $this->request->getQuery();
        $type = $this->request->getQuery('type');
        if ($keyword = @$options['keyword']) {
            if (@$options['search-mobile-sms']) {
                $keyword = str_replace(' ', '', $keyword);  
                $smss->where(['msg LIKE' => "%$keyword%"]);
            } else {
                $smss->where(['msg LIKE' => "%$keyword%"]);
            }
        }

        // debug($options);

        $date = @$options['date'];
        $created_at = @$options['created_at'];
        $expired_at = @$options['expired_at'];

        if ($expired_at && $created_at) {
            $smss->where([
                'receivedtime >=' => $created_at, 
                'receivedtime <=' => $expired_at
            ]);
        } 
        else if ($date) {
            $smss->where(['receivedtime LIKE' => "%$date%"]);
        }

        $nbTotalSms = $this->Ozekimessageins->find()->count();
        $annulations = $this->Ozekimessageins->find()->where([
            'OR' => [
                ['msg LIKE' => "%annulation%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
                ['msg LIKE' => "%annuler%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
            ]
        ])->order(['id' => 'DESC']);

        // debug($smss->toArray());
        $nbDemandeAnnulation = $annulations->count();

        $smss = $this->paginate($smss, ['limit' => 20]);
        $driverTelmaSms = Configure::read('driverTelmaSms');

        $nbPaiementEnCours = $this->Ozekimessageins->find('OneXEnCours', ['senders' => $senders])->count();
        $nbPaiementPaid = $this->Ozekimessageins->find('OneXEnPaid', ['senders' => $senders])->count();


        $this->set(compact('nbPaiementPaid', 'nbPaiementEnCours', 'autrePaiementRecent', 'paiementEnCoursData', 'paiementEnCours', 'nbNonRelgesAirtel', 'nbNonRelgesOrange', 'type', 'options', 'smss', 'annulations', 'driverTelmaSms', 'nbNonRelgesMvola', 'nbTotalSms', 'nbDemandeAnnulation'));
    }

    public function mobcash()
    {
        
    }

    public function getOnexForm()
    {
        $senders = array_merge($this->senders, $this->airtelSenders, $this->orangeSenders);

        $nbPaiementEnCours = $this->Ozekimessageins->find('OneXEnCours', ['senders' => $senders])->count();
        $paiementEnCours = $this->Ozekimessageins->find('OneXEnCours', ['senders' => $senders])->first();
        // debug($paiementEnCours);

        if ($paiementEnCours) {
            $paiementEnCoursData = $this->Ussd->extractOrangeRecuData($paiementEnCours->msg);
            $autrePaiementRecent = $this->Ozekimessageins->find()
                ->where([
                        "OR" => [
                            ['msg LIKE' => "%Vous avez recu%"]
                        ],
                        'sender IN' => $senders,
                        'operator' => "ONEX",
                        'id >' => $paiementEnCours->id
                    ]
                )
                ->order(['senttime' => 'DESC'])
                ->toArray()
            ;
        }

        // debug($autrePaiementRecent);

        $this->set(compact('nbPaiementEnCours', 'autrePaiementRecent', 'paiementEnCoursData', 'paiementEnCours', 'nbNonRelgesAirtel', 'nbNonRelgesOrange', 'type', 'options', 'smss', 'annulations', 'driverTelmaSms', 'nbNonRelgesMvola', 'nbTotalSms', 'nbDemandeAnnulation'));
    }

    public function majPaiement($smsId)
    {
        $body = ['status' => false];
        if ($this->request->is(['post', 'ajax'])) {
            $data = ['statut_1x' => 'paid'];
            
            $ozekimessagein = $this->Ozekimessageins->findById($smsId)->first();   
            $ozekimessagein = $this->Ozekimessageins->patchEntity($ozekimessagein, $data, ['validate' => false]);
            if(!$ozekimessagein->getErrors()) {
                $ozekimessagein = $this->Ozekimessageins->save($ozekimessagein);
                $body = ['status' => 'success', 'sms_id' => $smsId];
            }
        }   

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function onexHookOrange()
    {
        $host = $this->serveurDisant;
        $host = "http://127.0.0.1/moneyko";
        $orangeSenders = $this->orangeSenders;

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();

            if ($data['receiver'] == 'onex') {
                
                $orangeData = $this->Ussd->extractOrangeRecuData($data['msgdata']);

                // couche sécurité
                $transId = $orangeData['ref'];
                // $existingSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$transId%"])->where(['operator' => 'ONEX'])->select(['id', 'msg', 'senttime'])->last();
                // if ($existingSms) {
                //     $this->logginto($orangeData, 'paiement_in_deja_fait', $erase = false);
                //     echo 'Paiement déja fait';
                //     die();
                // }
                

                $sms = new SmsController();
                $result = json_decode($sms->checkOrange($transId, $orangeData['montant_recu'], $orangeData['numero'], $operator = 'ONEX'));
                // pr($orangeData);
                // pr($result);
                // die();


                if ($result->status == true) {
                    $http = new Client();
                    $url = $host.'/exchanger/apis/getMembre';
                    $user = $http->post($url, ['numero_orange' => $orangeData['numero']], $this->noSslParams)->json;
                    $xbet_id_orange = (int) $user['xbet_id_orange'];

                    $ozekimessagein = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$transId%"])->where(['operator' => 'ONEX'])->select(['id', 'msg', 'senttime'])->last();
                    $ozekimessagein = $this->Ozekimessageins->patchEntity($ozekimessagein, ['id_1xbet' => $xbet_id_orange], ['validate' => false]);   
                    $ozekimessagein = $this->Ozekimessageins->save($ozekimessagein);
                    $this->exectransfer();
                    // debug($ozekimessagein);
                    die();
                }
                
                $this->logginto([$data, $orangeData, $result], 'orange_data', $erase = false);
            }
        }

        die();
    }

    public function getImgSrcBySmsPaid($smsId)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        $onexId = $sms->id_1xbet;
        $imgSrc = "img/mobcash_screenshot/sms_id_".$smsId."_onex_id_$onexId.png";

        if (file_exists(WWW_ROOT.$imgSrc)) {
            $body = ['src' => $imgSrc];
        } else {
            $body = ['src' => false];
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function test()
    {
        debug($this->checkIfConnected());
        // debug($this->checkUrl());
        die();
    }
}
?>