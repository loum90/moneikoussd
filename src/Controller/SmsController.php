<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\Chronos\Chronos;
use App\Controller\SmsRecusController;
use DateTime;
use App\Traits\AppTrait;

class SmsController extends AppController
{

    use AppTrait;

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadModel('Paiements');
        $this->loadComponent('Ussd');
        $this->loadModel('Ozekimessageins');
        $this->loadModel('AuthSmss');



        $this->exclusion_sms_merci_depots = Configure::read('exclusion_sms_merci_depots');
    }

    public function test()
    {

        $httpClient = new \HttpClient();

        

        $host = $this->serveurDisant;
        $url = $host.'/exchanger/apis/updatePanier/243741';
        // $res = (new Client())->post($url, ['status' => 'pending'], $this->noSslParams);
        // debug($res->body());
        // $res = $httpClient->sendPostRequestAsync($url, ['status' => 'pending']);
        // debug($res);
        die();

        $this->sendSimpleNotificationPush('Demande d\'achat airtième,', ' via Mvoule pour un montant de 10 000 Ariar');
        // $this->sendSimpleNotificationPush('Quel bordel de merde, bon sang ! merde !', "J ai dit, Merde. Putain.");

        die();
        // debug($this->param);
        // $msg = 'Vous avez recu Ar 137000 du 333179660 pour 6.Votre Soled est Ar 1277618. ID: PP240710.1754.D35028';
        // $msg = 'Vous avez recu Ar 24300 du 330771975 pour peu importe. Le solde de votre compte est Ar 1301918. Trans ID: PP240710.2130.E51649';
        // $r = $this->Ussd->extractAirtelRecuData($msg);
        // debug($r);
        // die();


        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $this->logginto($data, 'test_date', true);
        }
        die();
    }

    /**
     * raison le raison tonga anaty SMS
     * montant = avy any am serveur, le montant initial anle commande
     * raison
     * @param  [type] $raison  [description]
     * @param  [type] $montant [description]
     * @return [type]          [description]
     */
    public function check($raison, $montant)
    {
        $senders = $this->senders;
        $formatedMontant = number_format($montant, 0, ',', ' ');
        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%Raison: $raison%"])->select(['id', 'msg', 'senttime'])->first();

        // debug($sms);
        // die();

        if ($sms != null) {
            $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%de la part de%", 'senttime <' => $sms->senttime])->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
            // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%de la part de%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
            // debug($sms);
            // debug($previousSms);
            // die();

            $groupedPreviousSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$previousSms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $previousSmsData = $this->extractData($groupedPreviousSms);
            // debug($groupedPreviousSms->toArray());
            // debug($previousSmsData);
            // die();
            $groupedSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$sms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $currentSmsData = $this->extractData($groupedSms);
            // pr($groupedSms->toArray());
            // debug($currentSmsData);
            // die();
            // pr($sms);
            // pr($groupedSms ->toArray());
            // die();

            
            $body = ['status' => false, 'msg' => ''];
            
            // pr($montant);
            $this->logginto($montant, 'check/montant', true);
            // $this->logginto($raison, 'check/raison', true);
            // $this->logginto($sms, 'check/sms', true);
            // $this->logginto($previousSms, 'check/previous_sms', true);
            $this->logginto($currentSmsData, 'check/current_sms_data', true);
            // $this->logginto($previousSmsData, 'check/previous_sms_data', true);
            // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
            // die();

            // pr($currentSmsData['montant_recu']);
            // pr($montant);

            // die();
            // if ($currentSmsData['montant_recu'] != $montant) {
            // Si montant reçu supérieur à montant généré dans site, on règle qd même à la base du montant du site 

            // debug($currentSmsData);
            // debug($previousSmsData);
            // die();

            if ($currentSmsData['montant_recu'] < $montant) { 
                $body = ['status' => -2,'numero' => $currentSmsData['numero'], 'msg' => "Activité suspecte"];
            } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+ (float) $previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                $body = ['status' => true,'numero' => $currentSmsData['numero'], 'msg' => 'référence trouvée avec succès'];
            } else {
                $body = ['status' => false, 'msg' => 'Solde incorrect'];
            }

            // pr($body);
            // die();
        } else {
            $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
        }

        // pr($body);
        // die();
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkOrange($transId, $montant, $numero, $operator = null)
    {
        $orangeSenders = $this->orangeSenders;
        // $formatedMontant = number_format($montant, 0, ',', ' ');

        $operatorConds = [];
        if (!is_null($operator)) {
            $operatorConds = ['operator' => $operator];
        }

        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$transId%"])->where($operatorConds)->select(['id', 'msg', 'senttime'])->last();

        if ($sms != null) {
            $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%Vous avez recu un transfert de%", 'senttime <' => $sms->senttime])->where($operatorConds)->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
            // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%de la part de%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
            // debug($transId);
            // debug($sms);
            // debug($previousSms);
            // die();

            $groupedPreviousSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'senttime LIKE' => "%$previousSms->senttime%"])->where($operatorConds)->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $previousSmsData = $this->extractOrangeData($groupedPreviousSms);
            // debug($groupedPreviousSms->toArray());
            // debug($previousSmsData);
            // die();
            $groupedSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'senttime LIKE' => "%$sms->senttime%"])->where($operatorConds)->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $currentSmsData = $this->extractOrangeData($groupedSms);
            // debug($currentSmsData);
            // die();
            // pr($sms);
            // die();
            // pr($sms);
            // pr($groupedSms ->toArray());
            // die();

            
            $body = ['status' => false, 'msg' => ''];
            
            // pr($montant);
            // $this->logginto($montant, 'check/montant', true);
            // $this->logginto($raison, 'check/raison', true);
            // $this->logginto($sms, 'check/sms', true);
            // $this->logginto($previousSms, 'check/previous_sms', true);
            // $this->logginto($currentSmsData, 'check/current_sms_data', true);
            // $this->logginto($previousSmsData, 'check/previous_sms_data', true);
            // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
            // die();

            // pr($currentSmsData['montant_recu']);
            // pr($montant);

            // die();
            // if ($currentSmsData['montant_recu'] != $montant) {
            // Si montant reçu supérieur à montant généré dans site, on règle qd même à la base du montant du site 
            if ($currentSmsData['montant_recu'] < $montant) { 
                $body = ['status' => -2, 'msg' => "Activité suspecte"];
            } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                $body = ['status' => true, 'msg' => 'référence trouvée avec succès'];
            } else {
                $body = ['status' => false, 'msg' => 'Solde incorrect'];
            }

            // pr($body);
            // die();
        } else {
            $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
        }

        // pr($body);
        // die();
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function extractData($groupedSms)
    {
        $smsContent = '';
        foreach ($groupedSms as $key => $sms) {
            // $smsContent .= ' '.substr($sms['msg'], 4);
            $smsContent .= ' '.$sms['msg'];
            // debug($sms);
        }

        return $this->Ussd->extractMvolaRecuData($smsContent);
    }

    public function extractAirtelData($groupedSms)
    {
        $smsContent = '';
        foreach ($groupedSms as $key => $sms) {
            $smsContent .= ' '.substr($sms['msg'], 4);
        }
        return $this->Ussd->extractAirtelRecuData($smsContent);
    }

    public function extractOrangeData($groupedSms)
    {
        $smsContent = '';
        foreach ($groupedSms as $key => $sms) {
            $smsContent .= ' '.substr($sms['msg'], 4);
        }
        return $this->Ussd->extractOrangeRecuData($smsContent);
    }

    // pour Mvola
    // public function webhook()
    // {
    //     $result = ['status' => 'disabled'];

    //     $timestamp_debut = microtime(true);

    //     if ($this->parameter->is_webhook_sms_paiement) {
    //         $host = $this->serveurDisant;
    //         $senders = $this->senders;

    //         if ($this->request->is(['post'])) {
    //             $data = $this->request->getData();
    //             $body = $data;
    //             $this->logginto($data, 'mvola_data');
    //             $mvolaData = $this->Ussd->extractMvolaRecuData($body['msgdata']);

    //             $raison = @$mvolaData['raison'];
    //             $numero = @$mvolaData['numero'];
    //             $montant_recu = @$mvolaData['montant_recu'];

    //             $http = new Client();
    //             if ($raison) {
    //                 $idpanier = (int) substr($raison, 1);

    //                 $urlVerification = $host.'/verification-transaction/'.$idpanier; // ['plugin' => 'Exchanger', 'controller' => 'Support', 'action' => 'shortVerificationTransaction']
    //                 $getVerification = $http->get($urlVerification, $result, $this->noSslParams);
    //                 $resVerification = $getVerification->json;

    //                 // Couche de sécurité pour les paiements entrants

    //                 $dataPaiement = [
    //                     'ref' => $raison,
    //                     'type' => 'mvola',
    //                     'numero' => $numero,
    //                     'montant' => $montant_recu,
    //                     'type_tx' => 'in'
    //                 ];

    //                 $this->logginto($dataPaiement, 'mvola_data_paiement');
    //                 if ($this->Paiements->find()->where($dataPaiement)->first()) {
    //                     $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%$raison%"])->select(['id', 'msg', 'senttime'])->first();
    //                     if ($sms) {
    //                         $this->Ozekimessageins->updateAll(['paiement_status' => 'locked_paiement'], ['id' => $sms->id]);
    //                     }
    //                     if (!empty($trans_id)) {
    //                         // $this->logginto($numero.', '. $montant_recu.', '. $trans_id.', ', 'paiement_in_deja_fait', $erase = false);
    //                     }
    //                     die('Paiement locked');
    //                 }

    //                 // --------------------------------------


    //                 if ($getVerification->code == 200 ) {
    //                     $result = json_decode($this->check($raison, @$resVerification['montant']), true);
    //                     $this->logginto($result, 'mvola/result', $erase = false);
    //                     $url = $host.'/exchanger/pm/pay/'.$idpanier.'?isFromLocal=1';

    //                     $res = $http->post($url, $result, $this->noSslParams); // envoi paiement 
    //                     $this->logginto($res->body(), 'mvola/res', $erase = false);
    //                     // debug($result);
    //                     // echo ($res->body());
    //                     // die;

    //                     // Enregistrement couche sécurité (évite doublon)
    //                     $paiementEntity = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
    //                     if(!$paiementEntity->getErrors()) {
    //                         $this->Paiements->save($paiementEntity);
    //                     }
                        
    //                     // echo ($res->body);
    //                     // die();

    //                     // marquage seulement dans liste
    //                     $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%$raison%"])->select(['id', 'msg', 'senttime'])->first();
    //                     $this->logginto($sms, 'mvola/ozekimessagein', $erase = false);

    //                     if ($sms) {
    //                         if ($res->json['status'] == 'warning') {
    //                             $this->Ozekimessageins->updateAll(['paiement_status' => 'warning', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
    //                         } elseif ($res->json['msg'] == 'already_paid') {
    //                             $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
    //                         } else {
    //                             $this->Ozekimessageins->updateAll(['paiement_status' => $res->json['status'], 'paiement_status_result' => $res->json['msg'], 'date_transfert' => @$res->json['date_transfert']], ['id' => $sms->id]);
    //                         }
    //                     }

    //                     $timestamp_fin = microtime(true);
                        
    //                     $difference_ms = round($timestamp_fin - $timestamp_debut, 2);

    //                     $this->logginto($data['msgdata'].', result hook : '.$res->body.', '.$difference_ms.' (s)', 'sms_recu_mvola');


    //                     // Push Moi Android
    //                     $this->sendPushNotificationToMe($raison, 'Mvola', $montant_recu, $numero, @$mvolaData['solde_actuel']);

    //                     return $this->response->withType('application/json')->withStringBody(json_encode(['res' => $res->json, 'res_body' => $res->body()]));
    //                 }
    //             }
    //         }
    //     }

    //     die();
    // }   


    public function webhook()
    {

        $result = ['status' => 'disabled'];
        // $sms = 'Vous avez recu un transfert de 1000Ar venant du 0327764453 Nouveau Solde: 10500Ar.  Trans Id: PP220712.1414.A01054. Orange Money vous remercie.';
        // $data = $this->Ussd->extractOrangeRecuData($sms);
        // debug($data);
        // die();

        $timestamp_debut = microtime(true);

        if ($this->parameter->is_webhook_sms_paiement) {
            $host = $this->serveurDisant;
            // $host = "http://127.0.0.1/moneyko";
            $senders = $this->senders;
            $http = new Client();

            if ($this->request->is(['post'])) {
                $data = $this->request->getData();
                $body = $data;

                if (strrpos($body['msgdata'], 'Ar recu de') !== false) {
                    
                    $this->logginto($data, 'debug_mvola_data_hook', $erase = false);
                    // die();

                    $receivetAt = DateTime::createFromFormat('d/m/Y H:i:s', $body['recvtime'])->format('Y-m-d H:i:s');
                    $mvolaData = $this->Ussd->extractMvolaRecuData($body['msgdata']);

                    // Couche de sécurité pour les paiements entrants

                    $numero = $mvolaData['numero'];
                    $montant_recu = $mvolaData['montant_recu'];
                    $trans_id = $mvolaData['ref'];
                    $solde_actuel = $mvolaData['solde_actuel'];

                    $dataPaiement = [
                        'ref' => $trans_id,
                        'type' => 'mvola',
                        'numero' => $numero,
                        'montant' => $montant_recu,
                        'type_tx' => 'in'
                    ];

                    $this->logginto($dataPaiement, 'mvola_data_paiement');

                    if ($this->Paiements->find()->where($dataPaiement)->first() || (strrpos(file_get_contents(ROOT.'/logs/securite/mvola_historic_ref.log'), $trans_id) !== false)) {
                        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                        if ($sms) {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'locked_paiement'], ['id' => $sms->id]);
                        }
                        if (!empty($trans_id)) {
                            $this->logginto($numero.', '. $montant_recu.', '. $trans_id.', ', 'paiement_in_deja_fait', $erase = false);
                        }
                        die('Paiement locked');
                    }

                    // die(); // ato rehefa tapahana le reception
                    // --------------------------------------

                    $dataToCheck = [
                        'montant' => $mvolaData['montant_recu'],
                        'numero' => $mvolaData['numero'],
                        'received_at' => $receivetAt,
                        'trans_id' => $mvolaData['ref'],
                    ];

                    $urlVerification = $host.'/exchanger/support/verificationTransactionMvola';

                    $httpClient = new \HttpClient();

                    // $getVerification = $http->post($urlVerification, $dataToCheck, $this->noSslParams);
                    $getVerification = $httpClient->post($urlVerification, $dataToCheck, $this->noSslParams);
                    // $this->logginto($getVerification->body(), 'telma/getVerification', $erase = false);
                    $this->logginto($getVerification, 'telma/getVerification', $erase = false);

                    // debug($getVerification);
                    // echo($getVerification->body());
                    // die();
                    // $resVerification = $getVerification->json;
                    $resVerification = $getVerification;

                    // if ($getVerification->getStatusCode() == 200) {
                    if (isset($getVerification['result'])) {

                        $listeAchatFiat = Configure::read('liste_achat_fiat');
                        $eppelations = Configure::read('eppelations');
                        $mvolaEppelation = $eppelations['mvola'];

                        if (in_array($getVerification['result']['type'], $listeAchatFiat)) {
                            $trans_id = $dataToCheck['trans_id'];
                            $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                            if (!is_null($sms)) {
                                $panier = $getVerification['result'];
                                $typeFiat = $panier['type'];
                                $eppelation = $eppelations[$typeFiat];
                                $this->Ozekimessageins->updateAll(['ptf' => $panier['type'], 'idpanier_id' => $panier['id']], ['id' => $sms->id]);
                                $this->sendSimpleNotificationPush('Vous avez une demande FIAT '.$eppelation, ', via '.$mvolaEppelation.' pour '.$panier['montant'].' Ariar');


                                $url = $host.'/exchanger/apis/updatePanier/'.$panier['id'];
                                // $httpClient->sendPostRequestAsync($url, ['status' => 'pending']);
                                $res = (new Client())->post($url, ['status' => 'pending'], $this->noSslParams);
                                // $this->logginto($res->getBody()->getContents(), 'achat_fiat_manu/mvola_retour');
                                $this->logginto($getVerification['result'], 'achat_fiat_manu/mvola');
                                $this->logginto($res->body(), 'achat_fiat_manu/mvola_res');
                                // debug($panier);
                                // debug($res->body());

                                $body = $panier;
                                return $this->response->withType('application/json')->withStringBody(json_encode($body));

                                die();
                            }
                        }


                        // Enregistrement couche sécurité (évite doublon), ato mitsy io satria le ambony io otran tsy tena maha bloqué azy
                        $this->logginto($trans_id, 'securite/mvola_historic_ref');
                        if (!(strrpos($host, '127.0.0.1') !== false)) {
                            $paiementEntity = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                            if(!$paiementEntity->getErrors()) {
                                $paiementEntity = $this->Paiements->save($paiementEntity);
                            }
                        }

                        $url = $host.'/exchanger/pm/pay?isFromLocal=1'; // moneiko.net ou local
                        $res = $http->post($url, $dataToCheck, $this->noSslParams); // envoi paiement 
                        $body = $res->body();
                        // echo ($res->body());
                        // die();
                        // $this->logginto($body, 'telma/res_1', $erase = false);

                        $trans_id = $mvolaData['ref'];
                        // marquage seulement dans liste
                        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                        $this->logginto($body, 'achat_response/'.$sms->id);

                        // $this->logginto($sms, 'telma/ozekimessagein', $erase = false);
                        // $this->logginto($body, 'telma/res_2', $erase = false);

                        if ($sms) {
                            $this->Ozekimessageins->updateAll(['ptf' => $res->json['ptf'], 'idpanier_id' => $res->json['idpanier_id']], ['id' => $sms->id]);
                            if ($res->json['status'] == 'warning') {
                                $this->Ozekimessageins->updateAll(['paiement_status' => 'warning', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                            } elseif ($res->json['msg'] == 'already_paid') {
                                $this->Ozekimessageins->updateAll(['paiement_status' => 'already_paid', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                            } elseif ($res->json['status'] == 'success') {
                                $this->Ozekimessageins->updateAll(['paiement_status' => $res->json['status'], 'paiement_status_result' => $res->json['msg'], 'date_transfert' => @$res->json['date_transfert']], ['id' => $sms->id]);
                                $this->sendPushNotificationToMe($trans_id, $mvolaEppelation, $montant_recu, $numero, $solde_actuel);
                            } else {
                                $this->sendPushNotificationForFailedPaiementToMe('Un paiement '.$mvolaEppelation, ' au montant de '. $montant_recu .' Ariar semble avoir echoué');
                            }
                        }

                        $timestamp_fin = microtime(true);
                        
                        $difference_ms = round($timestamp_fin - $timestamp_debut, 2);

                        // $this->logginto($data['msgdata'].', result hook : '.$res->body.', '.$difference_ms.' (s)', 'sms_recu_telma');
                        // $this->logginto($res->json, 'sms_res_telma');
                        $this->sendMeSms($numero, $montant_recu, $solde_actuel);

                        if ($this->parameter->is_auto_sms_merci_depot_send_activated) {
                            if (!in_array($numero, $this->exclusion_sms_merci_depots)) {
                                $this->sendSMSMerci($numero, 'depot');
                            }
                        }

                        $this->deleteCacheStat('mvola');


                        return $this->response->withType('application/json')->withStringBody(json_encode(['res' => $res->json, 'res_body' => $res->body()]));
                    } else {
                        $this->logginto($dataToCheck, 'telma/error_null_server_data', $erase = false);
                        // $this->logginto($getVerification->getStatusCode(), 'telma/error_null_server', $erase = false);
                        // $this->logginto($getVerification->body(), 'telma/error_body_server', $erase = false);
                        // $this->logginto($getVerification->getBody()->getContents(), 'telma/error_body_server', $erase = false);
                        echo 'not 200'; die;
                    }
                }


            }
        }

        die();
    }   


    public function checkAirtel($raison, $montant)
    {
        $airtelSenders = $this->airtelSenders;
        $formatedMontant = number_format($montant, 0, ',', ' ');
        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%pour $raison%"])->select(['id', 'msg', 'senttime'])->first();

        if ($sms != null) {
            $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'senttime <' => $sms->senttime])->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
            // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
            // pr($sms);
            // pr($previousSms);
            // die();

            $groupedPreviousSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'senttime LIKE' => "%$previousSms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $previousSmsData = $this->extractAirtelData($groupedPreviousSms);
            // debug($groupedPreviousSms->toArray());
            // debug($previousSmsData);
            // die();
            $groupedSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'senttime LIKE' => "%$sms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
            $currentSmsData = $this->extractAirtelData($groupedSms);
            // pr($sms);
            // die();
            // pr($sms);
            // pr($groupedSms ->toArray());
            // die();

            
            $body = ['status' => false, 'msg' => ''];
            
            // pr($montant);
            // pr($currentSmsData);
            // pr($previousSmsData);
            // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
            // die();

            // pr($currentSmsData['montant_recu']);
            // pr($montant);

            // die();
            if ($currentSmsData['montant_recu'] < $montant) {
                $body = ['status' => -2, 'msg' => "Activité suspecte"];
            } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                $body = ['status' => true, 'msg' => 'référence trouvée avec succès'];
            } else {
                $body = ['status' => false, 'msg' => 'Solde incorrect'];
            }

            // pr($body);
            // die();
        } else {
            $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
        }

        // pr($body);
        // die();
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    // nouveau sans reference pour Airtel
    public function webhookAirtel()
    {
        $result = ['status' => 'disabled'];
        // $sms = 'Vous avez recu un transfert de 1000Ar venant du 0327764453 Nouveau Solde: 10500Ar.  Trans Id: PP220712.1414.A01054. Orange Money vous remercie.';
        // $data = $this->Ussd->extractOrangeRecuData($sms);
        // debug($data);

        // die();

        $timestamp_debut = microtime(true);

        if ($this->parameter->is_webhook_sms_paiement) {
            $host = $this->serveurDisant;
            // $host = "http://127.0.0.1/moneyko";
            $airtelSenders = $this->airtelSenders;
            $http = new Client();

            if ($this->request->is(['post'])) {
                $data = $this->request->getData();
                $body = $data;
                $this->logginto($data, 'debug_airtel_data_hook', $erase = false);
                // die();

                $receivetAt = DateTime::createFromFormat('d/m/Y H:i:s', $body['recvtime'])->format('Y-m-d H:i:s');
                $airtelMoneyData = $this->Ussd->extractAirtelRecuData($body['msgdata']);
                $this->logginto($airtelMoneyData, 'debug_airtel_data_montant', $erase = false);


                // Couche de sécurité pour les paiements entrants

                $numero = $airtelMoneyData['numero'];
                $montant_recu = $airtelMoneyData['montant_recu'];
                $trans_id = $airtelMoneyData['ref'];
                $solde_actuel = $airtelMoneyData['solde_actuel'];

                $dataPaiement = [
                    'ref' => $trans_id,
                    'type' => 'airtel',
                    'numero' => $numero,
                    'montant' => $montant_recu,
                    'type_tx' => 'in'
                ];

                // pr($dataPaiement);
                // die();

                $this->logginto($dataPaiement, 'airtel_data_paiement');
                if ($this->Paiements->find()->where($dataPaiement)->first() || (strrpos(file_get_contents(ROOT.'/logs/securite/airtel_historic_ref.log'), $trans_id) !== false)) {

                    $sms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                    if ($sms) {
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'locked_paiement'], ['id' => $sms->id]);
                    }
                    
                    if (!empty($trans_id)) {
                        $this->logginto($numero.', '. $montant_recu.', '. $trans_id.', ', 'paiement_in_deja_fait', $erase = false);
                    }
                    die('Paiement locked');
                }

                // die(); // ato rehefa tapahana le reception
                // --------------------------------------

                $dataToCheck = [
                    'montant' => $airtelMoneyData['montant_recu'],
                    'numero' => $airtelMoneyData['numero'],
                    'received_at' => $receivetAt,
                    'trans_id' => $airtelMoneyData['ref'],
                ];

                // pr($dataToCheck);
                // die;

                $urlVerification = $host.'/exchanger/support/verificationTransactionAirtel';
                $getVerification = $http->post($urlVerification, $dataToCheck, $this->noSslParams);
                // echo ($getVerification->body());
                // pr ($urlVerification);
                // pr ($getVerification->json);
                // die();
                $resVerification = $getVerification->json;
                // debug($resVerification);
                // die();

                // debug($getVerification->code);
                // die();
                if ($getVerification->code == 200) {

                    if (isset($resVerification['result'])) {
                        $listeAchatFiat = Configure::read('liste_achat_fiat');
                        $eppelations = Configure::read('eppelations');

                        if (in_array($resVerification['result']['type'], $listeAchatFiat)) {

                            $trans_id = $dataToCheck['trans_id'];
                            $sms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                            $panier = $resVerification['result'];
                            $typeFiat = $panier['type'];
                            $eppelation = $eppelations[$typeFiat];
                            $this->Ozekimessageins->updateAll(['ptf' => $panier['type'], 'idpanier_id' => $panier['id']], ['id' => $sms->id]);

                            $this->sendSimpleNotificationPush('Vous avez une demande FIAT '.$eppelation, ', via Airtel Money pour '.$panier['montant'].' Ariar');

                            $url = $host.'/exchanger/apis/updatePanier/'.$panier['id'];
                            $res = (new Client())->post($url, ['status' => 'pending'], $this->noSslParams);
                            $this->logginto($resVerification['result'], 'achat_fiat_manu/airtel');
                            $this->logginto($res->body(), 'achat_fiat_manu/airtel_res');

                            debug($panier);
                            die();
                        }
                    }

                    // Enregistrement couche sécurité (évite doublon) avec clé en unique 
                    $this->logginto($trans_id, 'securite/airtel_historic_ref');
                    if (!(strrpos($host, '127.0.0.1') !== false)) {
                        $paiementEntity = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                        if(!$paiementEntity->getErrors()) {
                            $paiementEntity = $this->Paiements->save($paiementEntity);
                        }
                    }


                    $url = $host.'/exchanger/pm/payAirtel?isFromLocal=1'; // moneiko.net ou local
                    $res = $http->post($url, $dataToCheck, $this->noSslParams); // envoi paiement 
                    $body = $res->body();
                    // $this->logginto($body, 'webhookAirtel/res', $erase = false);

                    // echo ($res->body());
                    // die();

                    $trans_id = $airtelMoneyData['ref'];
                    // marquage seulement dans liste
                    $sms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                    $this->logginto($body, 'achat_response/'.$sms->id);

                    if ($sms) {
                        $this->Ozekimessageins->updateAll(['ptf' => $res->json['ptf'], 'idpanier_id' => $res->json['idpanier_id']], ['id' => $sms->id]);
                        if ($res->json['status'] == 'warning') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'warning', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } elseif ($res->json['msg'] == 'already_paid') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'already_paid', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } elseif ($res->json['status'] == 'success') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => $res->json['status'], 'paiement_status_result' => $res->json['msg'], 'date_transfert' => @$res->json['date_transfert']], ['id' => $sms->id]);
                            $this->sendPushNotificationToMe($trans_id, 'Airtel Money', $montant_recu, $numero, @$airtelMoneyData['solde_actuel']);
                        } else {
                            $this->sendPushNotificationForFailedPaiementToMe('Un paiement Airtel Money', ' au montant de '. $montant_recu .' Ariar semble avoir echoué');
                        }
                    }

                    $timestamp_fin = microtime(true);
                    
                    $difference_ms = round($timestamp_fin - $timestamp_debut, 2);
                    $this->logginto($data['msgdata'].', result hook : '.$res->body.', '.$difference_ms.' (s)', 'sms_recu_orange');
                    $this->logginto($res->json, 'sms_res_orange');

                    $this->sendMeSms($numero, $montant_recu, $solde_actuel);

                    $this->deleteCacheStat('airtel');
                    
                    return $this->response->withType('application/json')->withStringBody(json_encode(['res' => $res->json, 'res_body' => $res->body()]));
                } else {
                    $this->logginto($dataToCheck, 'airtel/error_null_server_data', $erase = false);
                    $this->logginto($getVerification->code, 'airtel/error_null_server', $erase = false);
                    $this->logginto($getVerification->body(), 'airtel/error_body_server', $erase = false);
                    echo 'not 200'; die;
                }


            }
        }

        die();
    }   

    // pour Orange
    public function webhookOrange()
    {
        $result = ['status' => 'disabled'];
        // $sms = 'Vous avez recu un transfert de 1000Ar venant du 0327764453 Nouveau Solde: 10500Ar.  Trans Id: PP220712.1414.A01054. Orange Money vous remercie.';
        // $data = $this->Ussd->extractOrangeRecuData($sms);
        // debug($data);
        // die();

        $timestamp_debut = microtime(true);

        if ($this->parameter->is_webhook_sms_paiement) {
            $host = $this->serveurDisant;
            // $host = "http://127.0.0.1/moneyko";
            $orangeSenders = $this->orangeSenders;
            $http = new Client();

            if ($this->request->is(['post'])) {
                $data = $this->request->getData();
                $body = $data;
                // $this->logginto($data, 'debug_orange_data_hook', $erase = false);
                // die();

                $receivetAt = DateTime::createFromFormat('d/m/Y H:i:s', $body['recvtime'])->format('Y-m-d H:i:s');
                $orangeMoneyData = $this->Ussd->extractOrangeRecuData($body['msgdata']);

                // Couche de sécurité pour les paiements entrants

                $numero = $orangeMoneyData['numero'];
                $montant_recu = $orangeMoneyData['montant_recu'];
                $trans_id = $orangeMoneyData['ref'];
                $solde_actuel = $orangeMoneyData['solde_actuel'];


                $dataPaiement = [
                    'ref' => $trans_id,
                    'type' => 'orange',
                    'numero' => $numero,
                    'montant' => $montant_recu,
                    'type_tx' => 'in'
                ];

                $this->logginto($dataPaiement, 'orange_data_paiement');

                if ($this->Paiements->find()->where($dataPaiement)->first() || (strrpos(file_get_contents(ROOT.'/logs/securite/orange_historic_ref.log'), $trans_id) !== false)) {

                    $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                    if ($sms) {
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'locked_paiement'], ['id' => $sms->id]);
                    }
                    if (!empty($trans_id)) {
                        $this->logginto($numero.', '. $montant_recu.', '. $trans_id.', ', 'paiement_in_deja_fait', $erase = false);
                    }
                    die('Paiement locked');
                }

                // die(); // ato rehefa tapahana le reception
                // --------------------------------------

                $dataToCheck = [
                    'montant' => $orangeMoneyData['montant_recu'],
                    'numero' => $orangeMoneyData['numero'],
                    'received_at' => $receivetAt,
                    'trans_id' => $orangeMoneyData['ref'],
                ];

                // $getVerification = $http->post($urlVerification, $dataToCheck, $this->noSslParams);
                // $this->logginto($getVerification->body(), 'orange/getVerification', $erase = false);

                $httpClient = new \HttpClient();
                $urlVerification = $host.'/exchanger/support/verificationTransactionOrange';
                $getVerification = $httpClient->post($urlVerification, $dataToCheck, $this->noSslParams);
                $this->logginto($getVerification, 'orange/getVerification', $erase = false);

                // debug($getVerification);
                // echo($getVerification->body);
                // die();
                // $resVerification = $getVerification->json;
                $resVerification = $getVerification;

                // if ($getVerification->getStatusCode() == 200) {
                if (isset($getVerification['result'])) {

                    $listeAchatFiat = Configure::read('liste_achat_fiat');
                    $eppelations = Configure::read('eppelations');

                    if (in_array($getVerification['result']['type'], $listeAchatFiat)) {
                        $trans_id = $dataToCheck['trans_id'];
                        $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                        $panier = $getVerification['result'];
                        $this->Ozekimessageins->updateAll(['ptf' => $panier['type'], 'idpanier_id' => $panier['id']], ['id' => $sms->id]);

                        $typeFiat = $panier['type'];
                        $eppelation = $eppelations[$typeFiat];
                        $this->sendSimpleNotificationPush('Vous avez une demande FIAT '.$eppelation, ', via Orange Money pour '.$panier['montant'].' Ariar');
                        $url = $host.'/exchanger/apis/updatePanier/'.$panier['id'];
                        $res = (new Client())->post($url, ['status' => 'pending'], $this->noSslParams);
                        $this->logginto($getVerification['result'], 'achat_fiat_manu/orange');
                        $this->logginto($res->body(), 'achat_fiat_manu/orange_res');
                        debug($panier);
                        die();
                    }

                    // Enregistrement couche sécurité (évite doublon)
                    $this->logginto($trans_id, 'securite/orange_historic_ref');
                    if (!(strrpos($host, '127.0.0.1') !== false)) {
                        $paiementEntity = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                        if(!$paiementEntity->getErrors()) {
                            $paiementEntity = $this->Paiements->save($paiementEntity);
                        }
                    }

                    $url = $host.'/exchanger/pm/payOrange?isFromLocal=1'; // moneiko.net ou local
                    $res = $http->post($url, $dataToCheck, $this->noSslParams); // envoi paiement 
                    $body = $res->body();
                    // $this->logginto($body, 'orange/res_1', $erase = false);

                    // echo ($res->body());
                    // die();

                    

                    $trans_id = $orangeMoneyData['ref'];
                    // marquage seulement dans liste
                    $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                    $this->logginto($body, 'achat_response/'.$sms->id);
                    $this->logginto($sms, 'orange/ozekimessagein', $erase = false);
                    // $this->logginto($res->body(), 'orange/res_2', $erase = false);

                    if ($sms) {
                        $this->Ozekimessageins->updateAll(['ptf' => $res->json['ptf'], 'idpanier_id' => $res->json['idpanier_id']], ['id' => $sms->id]);
                        if ($res->json['status'] == 'warning') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'warning', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } elseif ($res->json['msg'] == 'already_paid') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'already_paid', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } elseif ($res->json['status'] == 'success') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => $res->json['status'], 'paiement_status_result' => $res->json['msg'], 'date_transfert' => @$res->json['date_transfert']], ['id' => $sms->id]);
                            $this->sendPushNotificationToMe($trans_id, 'Orange Money', $montant_recu, $numero, @$orangeMoneyData['solde_actuel']);
                        } else {
                            $this->sendPushNotificationForFailedPaiementToMe('Un paiement Orange Money', ' au montant de '. $montant_recu .' Ariar semble avoir echoué');
                        }
                    }

                    $timestamp_fin = microtime(true);
                    
                    $difference_ms = round($timestamp_fin - $timestamp_debut, 2);

                    // $this->logginto($data['msgdata'].', result hook : '.$res->body.', '.$difference_ms.' (s)', 'sms_recu_orange');
                    // $this->logginto($res->json, 'sms_res_orange');
                    $this->sendMeSms($numero, $montant_recu, $solde_actuel);
                    if ($this->parameter->is_auto_sms_merci_depot_send_activated) {
                        if (!in_array($numero, $this->exclusion_sms_merci_depots)) {
                            $this->sendSMSMerci($numero, 'depot');
                        }
                    }

                    $this->deleteCacheStat('orange');


                    return $this->response->withType('application/json')->withStringBody(json_encode(['res' => $res->json, 'res_body' => $res->body()]));
                } else {
                    $this->logginto($dataToCheck, 'orange/error_null_server_data', $erase = false);
                    // $this->logginto($getVerification->code, 'orange/error_null_server', $erase = false);
                    // $this->logginto($getVerification->body(), 'orange/error_body_server', $erase = false);
                    echo 'not 200'; die;
                }


            }
        }

        die();
    }   

    // pour Orange
    // pour Orange
    public function webhookOrangeTest()
    {
        $result = ['status' => 'disabled'];
        // $sms = 'Vous avez recu un transfert de 1000Ar venant du 0327764453 Nouveau Solde: 10500Ar.  Trans Id: PP220712.1414.A01054. Orange Money vous remercie.';
        // $data = $this->Ussd->extractOrangeRecuData($sms);
        // debug($data);
        // die();

        $timestamp_debut = microtime(true);

        if ($this->parameter->is_webhook_sms_paiement) {
            $host = $this->serveurDisant;
            // $host = "http://127.0.0.1/moneyko";
            $orangeSenders = $this->orangeSenders;
            $http = new Client();

            if ($this->request->is(['post'])) {
                $data = $this->request->getData();
                $body = $data;
                // $this->logginto($data, 'debug_orange_data_hook', $erase = false);
                // die();

                $receivetAt = DateTime::createFromFormat('d/m/Y H:i:s', $body['recvtime'])->format('Y-m-d H:i:s');
                $orangeMoneyData = $this->Ussd->extractOrangeRecuData($body['msgdata']);

                // Couche de sécurité pour les paiements entrants

                $numero = $orangeMoneyData['numero'];
                $montant_recu = $orangeMoneyData['montant_recu'];
                $trans_id = $orangeMoneyData['ref'];

                $dataPaiement = [
                    'ref' => $trans_id,
                    'type' => 'orange',
                    'numero' => $numero,
                    'montant' => $montant_recu,
                    'type_tx' => 'in'
                ];

                // $this->logginto($dataPaiement, 'mvola_data_paiement');
                // if ($this->Paiements->find()->where($dataPaiement)->first()) {
                //     $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                //     $this->Ozekimessageins->updateAll(['paiement_status' => 'locked_paiement'], ['id' => $sms->id]);
                //     if (!empty($trans_id)) {
                //         // $this->logginto($numero.', '. $montant_recu.', '. $trans_id.', ', 'paiement_in_deja_fait', $erase = false);
                //     }
                //     die('Paiement locked');
                // }

                // die(); // ato rehefa tapahana le reception
                // --------------------------------------

                $dataToCheck = [
                    'montant' => $orangeMoneyData['montant_recu'],
                    'numero' => $orangeMoneyData['numero'],
                    'received_at' => $receivetAt,
                    'trans_id' => $orangeMoneyData['ref'],
                ];

                $urlVerification = $host.'/exchanger/support/verificationTransactionOrange';
                $getVerification = $http->post($urlVerification, $dataToCheck, $this->noSslParams);
                // debug($getVerification);
                // echo($getVerification->body);
                // die();
                $resVerification = $getVerification->json;

                if ($getVerification->code == 200) {
                    $url = $host.'/exchanger/pm/payOrange?isFromLocal=1'; // moneiko.net ou local
                    $res = $http->post($url, $dataToCheck, $this->noSslParams); // envoi paiement 
                    // echo ($res ->body);
                    // die();

                    // Enregistrement couche sécurité (évite doublon)
                    // $paiementEntity = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                    // if(!$paiementEntity->getErrors()) {
                    //     $paiementEntity = $this->Paiements->save($paiementEntity);
                    // }

                    $trans_id = $orangeMoneyData['ref'];
                    // marquage seulement dans liste
                    $sms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%$trans_id%"])->select(['id', 'msg', 'senttime'])->first();
                    $this->logginto($sms, 'orange/ozekimessagein', $erase = false);

                    if ($sms) {
                        $this->Ozekimessageins->updateAll(['ptf' => $res->json['ptf'], 'idpanier_id' => $res->json['idpanier_id']], ['id' => $sms->id]);
                        if ($res->json['status'] == 'warning') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'warning', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } elseif ($res->json['msg'] == 'already_paid') {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'already_paid', 'paiement_status_result' => $res->json['msg'], 'date_transfert' => $res->json['date_transfert']], ['id' => $sms->id]);
                        } else {
                            $this->Ozekimessageins->updateAll(['paiement_status' => $res->json['status'], 'paiement_status_result' => $res->json['msg'], 'date_transfert' => @$res->json['date_transfert']], ['id' => $sms->id]);
                        }
                    }

                    $timestamp_fin = microtime(true);
                    
                    $difference_ms = round($timestamp_fin - $timestamp_debut, 2);

                    // $this->logginto($data['msgdata'].', result hook : '.$res->body.', '.$difference_ms.' (s)', 'sms_recu_orange');
                    // $this->logginto($res->json, 'sms_res_orange');
                    return $this->response->withType('application/json')->withStringBody(json_encode($res->json));
                } else {
                    echo 'not 200'; die;
                }


            }
        }

        die();
    }   

    public function checkTelmaSmsFromProd()
    {
        if ($this->request->is(['post'])) {

            $senders = $this->senders;

            $data = $this->request->getData();

            $numero = $data['numero'];
            $montant = $formatedMontant = number_format($data['montant'], 0, ',', ' ');
            $created_at = $data['created_at'];
            $expired_at = $data['expired_at'];

            $sms = $this->Ozekimessageins
                ->find()
                ->where([
                    'sender IN' => $senders,
                    'AND' => [
                        ['msg LIKE' => "%1/2 ".$montant." Ar recu%"],
                        ['msg LIKE' => "%$numero%"],
                    ]
                ])
                ->filter(function ($item) use ($created_at, $expired_at)
                {
                    return $item->senttime > $created_at && $item->senttime < $expired_at;
                })
                // ->toArray()
                ->last()
            ;

            // debug($senders);
            // debug($data);
            // debug($sms);
            // die('ato');

            if ($sms != null) {
                $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%. Raison:%", 'senttime <' => $sms->senttime])->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
                // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'msg LIKE' => "%de la part de%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
                // debug($sms);
                // debug($previousSms);
                // die();

                $groupedPreviousSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'senttime LIKE' => "%$previousSms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                // debug($groupedPreviousSms ->toArray());
                // die();
                $previousSmsData = $this->extractData($groupedPreviousSms);
                // debug($groupedPreviousSms->toArray());
                // debug($previousSmsData);
                // die();
                $groupedSms = $this->Ozekimessageins->find()->where(['sender IN' => $senders, 'senttime LIKE' => "%$sms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                $currentSmsData = $this->extractData($groupedSms);
                // debug($currentSmsData);
                // die();
                // pr($sms);
                // die();
                // pr($sms);
                // pr($groupedSms ->toArray());
                // die();

                
                $body = ['status' => false, 'msg' => ''];
                
                // pr($montant);
                // $this->logginto($montant, 'check/montant', true);
                // $this->logginto($raison, 'check/raison', true);
                // $this->logginto($sms, 'check/sms', true);
                // $this->logginto($previousSms, 'check/previous_sms', true);
                // $this->logginto($currentSmsData, 'check/current_sms_data', true);
                // $this->logginto($previousSmsData, 'check/previous_sms_data', true);
                // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
                // die();

                // pr($currentSmsData['montant_recu']);
                // pr($montant);

                // die();
                // if ($currentSmsData['montant_recu'] != $montant) {
                // Si montant reçu supérieur à montant généré dans site, on règle qd même à la base du montant du site 

                // echo ($currentSmsData['montant_recu']);
                // echo ($montant);
                // echo ($currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
                // die();

                if ($currentSmsData['montant_recu'] < $montant) { 
                    $body = ['status' => -2, 'msg' => "Activité suspecte"];
                } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                    $body = ['status' => true, 'msg' => 'référence trouvée avec succès', 'content' => $sms->msg, 'sms_data' => $currentSmsData];
                } else {
                    $body = ['status' => false, 'msg' => 'Solde incorrect', 'data' => $data, 'currentSmsData' => $currentSmsData, 'previousSmsData' => $previousSmsData, 'groupedSms' => $groupedSms, 'montant' => $montant];
                }

                // pr($body);
                // die();
            } else {
                $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
            }

            // pr($body);
            // die();
            return $this->response->withType('application/json')->withStringBody(json_encode($body));

        }   
        die('ok'); // misy dikany ModemComponent/checkOrangeSmsFromProd
    }

    public function checkOrangeSmsFromProd()
    {
        if ($this->request->is(['post'])) {

            $orangeSenders = $this->orangeSenders;

            $data = $this->request->getData();

            $numero = $data['numero'];
            $montant = $data['montant'];
            $created_at = $data['created_at'];
            $expired_at = $data['expired_at'];

            $sms = $this->Ozekimessageins
                ->find()
                ->where([
                    'sender IN' => $orangeSenders,
                    'msg LIKE' => "%Vous avez recu un transfert de ".$montant."Ar venant du $numero%"])
                ->filter(function ($item) use ($created_at, $expired_at)
                {
                    return $item->senttime > $created_at && $item->senttime < $expired_at;
                })
                // ->toArray()
                ->last()
            ;

            // debug($orangeSenders);
            // debug($data);
            // debug($sms);
            // die('ato');

            if ($sms != null) {
                $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%Vous avez recu un transfert de%", 'senttime <' => $sms->senttime])->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
                // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $orangeSenders, 'msg LIKE' => "%de la part de%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
                // debug($sms);
                // debug($previousSms);
                // die();

                $groupedPreviousSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$previousSms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                $previousSmsData = $this->extractOrangeData($groupedPreviousSms);
                // debug($groupedPreviousSms->toArray());
                // debug($previousSmsData);
                // die();
                $groupedSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$sms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                $currentSmsData = $this->extractOrangeData($groupedSms);
                // debug($currentSmsData);
                // die();
                // pr($sms);
                // die();
                // pr($sms);
                // pr($groupedSms ->toArray());
                // die();

                
                $body = ['status' => false, 'msg' => ''];
                
                // pr($montant);
                // $this->logginto($montant, 'check/montant', true);
                // $this->logginto($raison, 'check/raison', true);
                // $this->logginto($sms, 'check/sms', true);
                // $this->logginto($previousSms, 'check/previous_sms', true);
                // $this->logginto($currentSmsData, 'check/current_sms_data', true);
                // $this->logginto($previousSmsData, 'check/previous_sms_data', true);
                // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
                // die();

                // pr($currentSmsData['montant_recu']);
                // pr($montant);

                // die();
                // if ($currentSmsData['montant_recu'] != $montant) {
                // Si montant reçu supérieur à montant généré dans site, on règle qd même à la base du montant du site 

                // debug($currentSmsData['solde_actuel']);
                // debug($currentSmsData['solde_actuel']);
                // debug($previousSmsData['montant_recu']);

                $this->logginto([
                    'solde_actuel' => $currentSmsData['solde_actuel'],
                    'montant_recu' => $currentSmsData['montant_recu'],
                    'solde_precedent' => $previousSmsData['solde_actuel'],
                ], 'orange/check_sms_data', true);

                // debug($previousSmsData['solde_actuel']);

                // debug($currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);

                if ($currentSmsData['montant_recu'] < $montant) { 
                    $body = ['status' => -2, 'msg' => "Activité suspecte"];
                } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                    $body = ['status' => true, 'msg' => 'référence trouvée avec succès', 'content' => $sms->msg, 'sms_data' => $currentSmsData];
                } else {
                    $body = ['status' => false, 'msg' => 'Solde incorrect'];
                }

                // pr($body);
                // die();
            } else {
                $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
            }

            // pr($body);
            // die();
            return $this->response->withType('application/json')->withStringBody(json_encode($body));

        }   
        die('ok'); // misy dikany ModemComponent/checkOrangeSmsFromProd
    }

    public function checkAirtelSmsFromProd()
    {
        if ($this->request->is(['post'])) {

            $airtelSenders = $this->airtelSenders;

            $data = $this->request->getData();

            $numero = $data['numero'];
            $montant = $data['montant'];
            $created_at = $data['created_at'];
            $expired_at = $data['expired_at'];

            $sms = $this->Ozekimessageins
                ->find()
                ->where([
                    'sender IN' => $airtelSenders,
                    'msg LIKE' => "%Vous avez recu Ar ".$montant." du $numero%"])
                ->filter(function ($item) use ($created_at, $expired_at)
                {
                    return $item->senttime > $created_at && $item->senttime < $expired_at;
                })
                // ->toArray()
                ->last()
            ;

            // debug($airtelSenders);
            // debug($data);
            // debug($sms);
            // die('ato');

            if ($sms != null) {
                $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 'senttime <' => $sms->senttime])->order(['id' => 'DESC'])->select(['id', 'msg', 'senttime'])->first();
                // $previousSms = $this->Ozekimessageins->find()->where(['sender IN' => $airtelSenders, 'msg LIKE' => "%de la part de%", 'id' => 93])->select(['id', 'msg', 'senttime'])->first();
                // debug($sms);
                // debug($previousSms);
                // die();

                $groupedPreviousSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$previousSms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                $previousSmsData = $this->extractAirtelData($groupedPreviousSms);
                // debug($groupedPreviousSms->toArray());
                // debug($previousSmsData);
                // die();
                $groupedSms = $this->Ozekimessageins->find()->where(['senttime LIKE' => "%$sms->senttime%"])->select(['msg'])->orderAsc('msg')->enableHydration(false);
                $currentSmsData = $this->extractAirtelData($groupedSms);
                // debug($currentSmsData);
                // die();
                // pr($sms);
                // die();
                // pr($sms);
                // pr($groupedSms ->toArray());
                // die();

                
                $body = ['status' => false, 'msg' => ''];
                
                // pr($montant);
                // $this->logginto($montant, 'check/montant', true);
                // $this->logginto($raison, 'check/raison', true);
                // $this->logginto($sms, 'check/sms', true);
                // $this->logginto($previousSms, 'check/previous_sms', true);
                // $this->logginto($currentSmsData, 'check/current_sms_data', true);
                // $this->logginto($previousSmsData, 'check/previous_sms_data', true);
                // var_dump($currentSmsData['solde_actuel'] == $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']);
                // die();

                // pr($currentSmsData['montant_recu']);
                // pr($montant);

                // die();
                // if ($currentSmsData['montant_recu'] != $montant) {
                // Si montant reçu supérieur à montant généré dans site, on règle qd même à la base du montant du site 
                if ($currentSmsData['montant_recu'] < $montant) { 
                    $body = ['status' => -2, 'msg' => "Activité suspecte"];
                } elseif ($currentSmsData['solde_actuel'] >= $currentSmsData['montant_recu']+$previousSmsData['solde_actuel']) { // solde courant >= montant reçu + solde précedent
                    $body = ['status' => true, 'msg' => 'référence trouvée avec succès', 'content' => $sms->msg, 'sms_data' => $currentSmsData];
                } else {
                    $body = ['status' => false, 'msg' => 'Solde incorrect'];
                }

                // pr($body);
                // die();
            } else {
                $body = ['status' => false, 'msg' => 'aucune référence trouvée'];
            }

            // pr($body);
            // die();
            return $this->response->withType('application/json')->withStringBody(json_encode($body));

        }   
        die('ok'); // misy dikany ModemComponent/checkAirtelSmsFromProd
    }

    /**
     * http://127.0.0.1/moneiko-ussd/sms/axife
     * @return [type] [description]
     */
    public function axife()
    {
        $http = new \Cake\Http\Client();
        $pythonPath = Configure::read('pythonPath').'orange/';
        $cmd = $pythonPath."test2.py 2>&1";
        pr($pythonPath."test2.py 2>&1");
        $cmd = "D:\\ngrok.bat";
        pr($cmd);
        // debug(file_exists($cmd));
        // debug($this->Ussd->execCli2($cmd));
        // shell_exec($cmd);
        // $cmd = "C:\\Cmder\\Cmder.exe";
        // $output = (passthru($cmd));
        // $output = (exec($cmd));
        // debug($output);
        // exec("C:\Windows\system32\\notepad.exe");
        // exec('"C:\Windows\system32\\notepad.exe"');
        // debug(file_exists("C:\Program Files (x86)\Axife Mouse Recorder DEMO\\AxMRec.exe"));
        // exec("C:\Program Files (x86)\Axife Mouse Recorder DEMO\\AxMRec.exe");
        // passthru('"C:\\axife\\AxMRec.exe"');
        // passthru($cmd);
        die();
    }

    function ex()
    {
        $command = "C:/Windows/System32/notepad.exe";
        shell_exec('SCHTASKS /F /Create /TN _law /TR "' . $command . '"" /SC DAILY /RU 
        INTERACTIVE');
        shell_exec('SCHTASKS /RUN /TN "_law');
        shell_exec('SCHTASKS /DELETE /TN "_law" /F');

        die();
    }

    public function payLockedOrange()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->orangeSenders, 'paiement_status' => 'locked_paiement'])->order(['senttime' => 'DESC'])->limit(10);
        $subMin = Chronos::now()->subMinutes(30);
        $smss = $smss->where(['senttime >= ' => $subMin]);
        $smsRecus = new SmsRecusController();
        $result = false;
        $results = [];

        foreach ($smss as $key => $sms) {
            $r = $smsRecus->searchIdPanierOrange('orange', $sms->id, 'array');
            $panier_id = $r['panierOnServer']->id;
            $sms_id = $sms->id;
            
            if ($r['panierOnServer']->status != 4) {
                $http = new Client();
                $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeOrange', 'panier_id' => $panier_id, 'sms_id' => $sms_id]);
                $results = $result = $http->get($url);
                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
                $this->logginto($result->json, 'paiement_orange_locked', $erase = false);
            }

            elseif ($r['panierOnServer']->status == 4) {
                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
            }

        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_airtel_locked', $erase = false);
        }
        $body = ['status' => (bool) $result];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payUnpaidOrange()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['is_delayed' => 0, 'sender in' => $this->orangeSenders, 'paiement_status IS' => NULL])->order(['senttime' => 'DESC'])->limit(10);
        // $subMin = Chronos::now()->subMinutes(60);
        $subMin = Chronos::now()->subDays(2);
        $smss = $smss->where(['senttime >= ' => $subMin]);
        $smsRecus = new SmsRecusController();
        $result = false;
        $results = [];

        foreach ($smss as $key => $sms) {

            $r = $smsRecus->searchIdPanierOrange('orange', $sms->id, 'array');

            if (isset($r['panierOnServer']->id)) {
                $orangeMoneyData = $this->Ussd->extractOrangeRecuData($sms['msg']);

                if (
                    $r['panierOnServer']->montant == $orangeMoneyData['montant_recu'] &&
                    $r['panierOnServer']->numero_orange == $orangeMoneyData['numero']
                ) {

                    $panier_id = $r['panierOnServer']->id;
                    $sms_id = $sms->id;

                    // debug($orangeMoneyData);
                    
                    // if ($r['panierOnServer']->status != 4) {
                    //     $http = new Client();
                    //     $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeOrange', 'panier_id' => $panier_id, 'sms_id' => $sms_id]);
                    //     $results = $result = $http->get($url);
                    //     $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'is_delayed' => 1], ['id' => $sms->id]);
                    //     $this->logginto($result->json, 'paiement_orange_delayed', $erase = false);
                    //     sleep(15);
                    // }

                    // elseif ($r['panierOnServer']->status == 4) {
                    //     $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
                    // }

                }
            }

        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_airtel_locked', $erase = false);
        }
        $body = ['status' => (bool) $result];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }       

    public function payLockedAirtel()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->airtelSenders, 'paiement_status' => 'locked_paiement'])->order(['senttime' => 'DESC'])->limit(10);
        $subMin = Chronos::now()->subMinutes(10);
        $smss = $smss->where(['senttime >= ' => $subMin]);
        $smsRecus = new SmsRecusController();
        $result = false;
        $results = [];

        foreach ($smss as $key => $sms) {
            $r = $smsRecus->searchIdPanierAirtel('airtel', $sms->id, 'array');
            $panier_id = $r['panierOnServer']->id;
            $sms_id = $sms->id;
            
            if ($r['panierOnServer']->status != 4) {
                $http = new Client();
                $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeAirtel', 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);
                $results[] = $result = $http->get($url);
                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
            }
        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_airtel_locked', $erase = false);
        }
        $body = ['results' => (bool) $results];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function markSmsAsPaidOrange()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->orangeSenders, 'paiement_status IS' => NULL])->order(['senttime' => 'ASC'])->limit(1);
        $subMin = Chronos::now()->subMinutes(10);
        $smss = $smss->where(['senttime >= ' => $subMin]);

        $smsRecus = new SmsRecusController();
        $results = [];
        $http = new Client();
        $tempsDeTolerance = 7;

        foreach ($smss as $key => $sms) {
            $findedPanier = $smsRecus->searchIdPanierOrange('orange', $sms->id, 'array');

            if (isset($findedPanier['panierOnServer'])) {
                if (isset($findedPanier['panierOnServer']->id)) {
                        
                    $panier_id = $findedPanier['panierOnServer']->id;
                    $panierOnServer = $findedPanier['panierOnServer'];
                    $isStatusSuccess = in_array(strtoupper($panierOnServer->status), ['4', '100', 'SUCCESS']);
                    if ($isStatusSuccess) {
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_simple_as_paid' => 1], ['id' => $sms->id]);
                    }
                }
            }
        }

        die();

    }

    public function markSmsAsPaidMvola()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders, 'paiement_status IS' => NULL])->order(['senttime' => 'ASC'])->limit(1);
        $subMin = Chronos::now()->subMinutes(10);
        $smss = $smss->where(['senttime >= ' => $subMin]);

        // debug($smss ->toArray()); die;

        $smsRecus = new SmsRecusController();
        $results = [];
        $http = new Client();
        $tempsDeTolerance = 7;

        foreach ($smss as $key => $sms) {
            $findedPanier = $smsRecus->searchIdPanierMvola('mvola', $sms->id, 'array');

            if (isset($findedPanier['panierOnServer'])) {
                if (isset($findedPanier['panierOnServer']->id)) {
                        
                    $panier_id = $findedPanier['panierOnServer']->id;
                    $panierOnServer = $findedPanier['panierOnServer'];
                    $isStatusSuccess = in_array(strtoupper($panierOnServer->status), ['4', '100', 'SUCCESS']);
                    if ($isStatusSuccess) {
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_simple_as_paid' => 1], ['id' => $sms->id]);
                    }
                }
            }
        }

        die();

    }

    public function payLockedMvola()
    {
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders, 'paiement_status' => 'locked_paiement'])->order(['senttime' => 'DESC'])->limit(10);
        $subMin = Chronos::now()->subMinutes(10);
        $smss = $smss->where(['senttime >= ' => $subMin]);
        $smsRecus = new SmsRecusController();
        $result = false;
        $results = [];

        foreach ($smss as $key => $sms) {
            $findedPanier = $smsRecus->searchIdPanierMvola('mvola', $sms->id, 'array');
            $panier_id = $findedPanier['panierOnServer']->id;
            $sms_id = $sms->id;
            
            if ($findedPanier['panierOnServer']->status != 4) {
                $http = new Client();
                $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeMvola', 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);
                $results[] = $result = $http->get($url);
                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
            }
        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_mvola_locked', $erase = false);
        }
        $body = ['results' => (bool) $results];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * Efa azo activena
     * @return [type] [description]
     */
    public function payErrSynchronisedMvola()
    {
        if (!$this->getParam('is_auto_recla_erreur_reseau')) {
            die('stop error script');
        }
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders, 'paiement_status IS' => NULL, 'is_check_sent_done' => 0])->order(['senttime' => 'DESC'])->limit(1);
        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        // debug($smss ->toArray());
        // die();

        $smsRecus = new SmsRecusController();
        $results = [];
        $http = new Client();
        $tempsDeTolerance = 7;

        foreach ($smss as $key => $sms) {
            $findedPanier = $smsRecus->searchIdPanierMvola('mvola', $sms->id, 'array');

            if (isset($findedPanier['panierOnServer'])) {
                if (isset($findedPanier['panierOnServer']->id)) {
                        
                    $panier_id = $findedPanier['panierOnServer']->id;
                    $panierOnServer = $findedPanier['panierOnServer'];

                    if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {
                        $receivedtime = Chronos::parse($sms->receivedtime);
                        $panierDate = Chronos::parse($panierOnServer->created_date);

                        $this->Ozekimessageins->updateAll(['is_check_sent_done' => 1, 'mark_err_synch_as_paid' => 1], ['id' => $sms->id]); 

                        $isCorrect = $receivedtime->lte($panierDate) && $panierDate->diffInSeconds($receivedtime) <= $tempsDeTolerance;

                        if ($isCorrect) {
                            $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeMvola', 'panier_id' => $panier_id, 'sms_id' => $sms->id], true);
                            $results[] = $result = $http->get($url)->json;

                            if ($result['status'] == 'success') {
                                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]); 
                            }
                        } else {
                            $this->logginto(compact('panierOnServer', 'receivedtime', 'panierDate'), 'paiement_vola_error_synchronised');
                        }

                        break;
                    }

                }
            } else {
                die('Panier non trouvé');
            }
        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_mvola_error_synchronised', $erase = false);
        }

        // debug($results);

        $body = $results;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payErrSynchronisedOrange()
    {
        if (!$this->getParam('is_auto_recla_erreur_reseau')) {
            die('stop error script');
        }
        $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->orangeSenders, 'paiement_status IS' => NULL, 'is_check_sent_done' => 0])->order(['senttime' => 'DESC'])->limit(1);
        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        // debug($smss ->toArray());
        // die();


        $smsRecus = new SmsRecusController();
        $results = [];
        $http = new Client();
        $tempsDeTolerance = 7;

        foreach ($smss as $key => $sms) {
            $findedPanier = $smsRecus->searchIdPanierOrange('orange', $sms->id, 'array');

            if (isset($findedPanier['panierOnServer'])) {
                if (isset($findedPanier['panierOnServer']->id)) {
                        
                    $panier_id = $findedPanier['panierOnServer']->id;
                    $panierOnServer = $findedPanier['panierOnServer'];

                    if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                        $receivedtime = Chronos::parse($sms->receivedtime);
                        $panierDate = Chronos::parse($panierOnServer->created_date);

                        // debug($panierOnServer);
                        // die();
                        $this->Ozekimessageins->updateAll(['is_check_sent_done' => 1, 'mark_err_synch_as_paid' => 1], ['id' => $sms->id]); 

                        $isCorrect = $receivedtime->lte($panierDate) && $panierDate->diffInSeconds($receivedtime) <= $tempsDeTolerance;
                        // debug($panierDate->diffInSeconds($receivedtime));
                        // debug($isCorrect);
                        // die('ato');

                        $this->logginto(compact('sms', 'panierOnServer', 'receivedtime', 'panierDate'), 'paiement_orange_data_synchronised');

                        if ($isCorrect) {
                            $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeOrange', 'panier_id' => $panier_id, 'sms_id' => $sms->id], true);
                            $results[] = $result = $http->get($url)->json;

                            // debug($result);

                            if ($result['status'] == 'success') {
                                $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]); 
                            }
                        } else {
                            $this->logginto(compact('panierOnServer', 'receivedtime', 'panierDate'), 'paiement_orange_error_synchronised');
                        }

                        break;
                    }
                    

                }
            }
        }

        if (!empty($results)) {
            $this->logginto($results, 'paiement_orange_error_synchronised');
        }

        // debug($results);

        $body = $results;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payFailedMvola()
    {
        if (!$this->getParam('is_auto_recla_general')) {
            die('stop script general auto recla');
        }
        // $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders, 'is_check_sent_done' => 0, 'paiement_status NOT IN' => ['SUCCESS', 4]])->order(['senttime' => 'DESC'])->limit(1);
        $smss = $this->Ozekimessageins->find('Recus')->where([
            'sender in' => $this->senders, 
            'OR' => [
                ['paiement_status IS' => NULL],
                ['paiement_status' => 'error'],
            ],
            'is_check_recla_paid_done' => 0])
        ->order(['senttime' => 'DESC'])->limit(1);
        // $smss = $this->Ozekimessageins->find('Recus')->where(['id' => 166837])->order(['senttime' => 'DESC'])->limit(1);


        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        $sms = null;
        if ($smss->count() > 0) {
            $sms = $smss->first();
            $mvolaData = $this->Ussd->extractMvolaRecuData($sms['msg']);
            if ($mvolaData['montant_recu'] >= $this->param->montant_max_auto_recla) {
                $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                $body = ['amount' => 'amount too large to deal with complaint'];
                return $this->response->withType('application/json')->withStringBody(json_encode($body));
            };
            // debug($sms);
        }

        $body = [];
        $results = [];

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $response = $smsRecus->searchIdPanierMvola('mvola', $sms->id, 'array');
            if (isset($response['panierOnServer']->id)) {
                $panierOnServer = $response['panierOnServer'];
                $nbAssociatedSmss = $response['nbAssociatedSmss'];
                $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
                $panier_id = $response['panierOnServer']->id;
                $sms_id = $sms->id;
                
                // debug($panierOnServer->status);
                // debug($sms_id);
                // die();

                if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                    $http = new Client();

                    $type = $panierOnServer->type;
                    $usdRounded = round($panierOnServer->usd);
                    $compte = isset($panierOnServer->datas['compte']) ? $panierOnServer->datas['compte'] : $panierOnServer->datas['asset'];
                    $ptf = ucfirst(strtolower($type));
                    $methode = 'check'.$ptf.'HistorickByIdpanierId';
                    // debug($methode);
                    // debug($panier_id);
                    // die();
                    $resultPaymentsDones = $smsRecus->{$methode}($panier_id, $asArray = 1); // retourne une collection()
                    $resultPaymentsDones = collection(json_decode($resultPaymentsDones, true));
                    $nbResultPaymentsDones = $resultPaymentsDones->count();
                    // debug($resultPaymentsDones ->toArray());

                    // debug($resultPaymentsDones ->toArray());
                    // debug($resultPaymentsDones->count());
                    // debug($nbAssociatedSmss);
                    // debug($panierOnServer);
                    // die();

                    $resultMatch = null;
                    if ($resultPaymentsDones->count() > 0) {

                        if ($panierOnServer->type == 'faucetpay') {
                            $resultMatch = $resultPaymentsDones->match(['compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        } else {
                            $resultMatch = $resultPaymentsDones->match(['usd_rounded' => $usdRounded, 'compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        }
                    }

                    // On vérifie si il y a eu des paiements associé au PTF de cette transaction
                    if ($nbResultPaymentsDones >= $nbAssociatedSmss) { // raha efa nisy envoi dia aleo tsy traitena
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $this->deleteCacheStat('mvola');
                    } else {
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeMvola', 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);
                        $results[] = $result = $http->get($url);
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_transfer_as_paid' => 1], ['id' => $sms->id]);
                    }

                    $this->logginto(compact('panierOnServer', 'resultPaymentsDones'), 'panier_paiement_mvola_failed');
                }

                if (!empty($results)) {
                    $this->logginto($results, 'paiement_mvola_failed', $erase = false);
                }
                $body = ['results' => (bool) $results];
            }
                
        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payFailedOrange()
    {
        if (!$this->getParam('is_auto_recla_general')) {
            die('stop script general auto recla');
        }

        // $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->orangeSenders, 'is_check_recla_paid_done' => 0, 'paiement_status NOT IN' => ['SUCCESS', 4]])->order(['senttime' => 'DESC'])->limit(1);
        $smss = $this->Ozekimessageins->find('Recus')->where([
            'sender in' => $this->orangeSenders, 
            'OR' => [
                ['paiement_status IS' => NULL],
                ['paiement_status' => 'error'],
            ],
            'is_check_recla_paid_done' => 0])
        ->order(['senttime' => 'DESC'])->limit(1);
        // $smss = $this->Ozekimessageins->find('Recus')->where(['id' => 166837])->order(['senttime' => 'DESC'])->limit(1);

        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        $sms = null;
        if ($smss->count() > 0) {
            $sms = $smss->first();
            $orangeData = $this->Ussd->extractOrangeRecuData($sms['msg']);
            if ($orangeData['montant_recu'] >= $this->param->montant_max_auto_recla) {
                $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                $body = ['amount' => 'amount too large to deal with complaint'];
                return $this->response->withType('application/json')->withStringBody(json_encode($body));
            };
            // debug($sms);
        }

        // debug($smss ->toArray());
        // die();
        $body = [];
        $results = [];

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $response = $smsRecus->searchIdPanierOrange('orange', $sms->id, 'array');

            if (isset($response['panierOnServer']->id)) {
                // debug($response);
                // die();
                $panierOnServer = $response['panierOnServer'];
                $nbAssociatedSmss = $response['nbAssociatedSmss'];
                $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
                $panier_id = $response['panierOnServer']->id;
                $sms_id = $sms->id;
                
                // debug($panierOnServer);
                // debug($sms_id);
                // die();

                if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                    $http = new Client();

                    $type = $panierOnServer->type;
                    $usdRounded = round($panierOnServer->usd);
                    $compte = isset($panierOnServer->datas['compte']) ? $panierOnServer->datas['compte'] : $panierOnServer->datas['asset'];
                    $ptf = ucfirst(strtolower($type));
                    $methode = 'check'.$ptf.'HistorickByIdpanierId';
                    // debug($methode);
                    // debug($panier_id);
                    // die();
                    $resultPaymentsDones = $smsRecus->{$methode}($panier_id, $asArray = 1); // retourne une collection()
                    $resultPaymentsDones = collection(json_decode($resultPaymentsDones, true));
                    // debug($resultPaymentsDones);
                    // die();
                    $nbResultPaymentsDones = $resultPaymentsDones->count();

                    // debug($resultPaymentsDones ->toArray());
                    // debug($resultPaymentsDones->count());
                    // debug($nbAssociatedSmss);
                    // debug($panierOnServer);
                    // die();

                    $resultMatch = null;
                    if ($resultPaymentsDones->count() > 0) {

                        if ($panierOnServer->type == 'faucetpay') {
                            $resultMatch = $resultPaymentsDones->match(['compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        } else {
                            $resultMatch = $resultPaymentsDones->match(['usd_rounded' => $usdRounded, 'compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        }
                    }

                    // debug($nbResultPaymentsDones >= $nbAssociatedSmss);
                    // die();

                    // On vérifie si il y a eu des paiements associé au PTF de cette transaction
                    if ($nbResultPaymentsDones >= $nbAssociatedSmss) { // raha efa nisy envoi dia aleo tsy traitena
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $this->deleteCacheStat('orange');
                    } else {
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeOrange', 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);
                        $results[] = $result = $http->get($url)->body();
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_transfer_as_paid' => 1], ['id' => $sms->id]);
                    }

                    $this->logginto(compact('panierOnServer', 'resultPaymentsDones'), 'panier_paiement_orange_failed');
                }

                if (!empty($results)) {
                    $this->logginto($results, 'paiement_orange_failed', $erase = false);
                }

                $body = ['results' => (bool) $results, 'panier' => $panierOnServer];
            }
        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payFailedAirtel()
    {
        if (!$this->getParam('is_auto_recla_general')) {
            die('stop script general auto recla');
        }
        // $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->airtelSenders, 'is_check_recla_paid_done' => 0, 'paiement_status NOT IN' => ['SUCCESS', 4]])->order(['senttime' => 'DESC'])->limit(1);
        $smss = $this->Ozekimessageins->find('Recus')->where([
            'sender in' => $this->airtelSenders, 
            'OR' => [
                ['paiement_status IS' => NULL],
                ['paiement_status' => 'error'],
            ],
            'is_check_recla_paid_done' => 0])
        ->order(['senttime' => 'DESC'])->limit(1);
        // $smss = $this->Ozekimessageins->find('Recus')->where(['id' => 166837])->order(['senttime' => 'DESC'])->limit(1);

        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        $sms = null;
        if ($smss->count() > 0) {
            $sms = $smss->first();
            $airtelData = $this->Ussd->extractAirtelRecuData($sms['msg']);
            if ($airtelData['montant_recu'] >= $this->param->montant_max_auto_recla) {
                $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                $body = ['amount' => 'amount too large to deal with complaint'];
                return $this->response->withType('application/json')->withStringBody(json_encode($body));
            };
            // debug($sms);
        }


        // debug($sms);
        // $dataSms = $this->Ussd->extractAirtelRecuData($sms->msg);
        // debug($dataSms);
        // die();

        // debug($smss ->toArray());
        // die();
        $body = [];
        $results = [];

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $response = $smsRecus->searchIdPanierAirtel('airtel', $sms->id, 'array');

            if (isset($response['panierOnServer']->id)) {

                // debug($response);
                // die();
                $panierOnServer = $response['panierOnServer'];
                $nbAssociatedSmss = $response['nbAssociatedSmss'];
                $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
                $panier_id = $response['panierOnServer']->id;
                $sms_id = $sms->id;
                
                // debug($panierOnServer->status);
                // debug($sms_id);
                // die();

                if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                    $http = new Client();

                    $type = $panierOnServer->type;
                    $usdRounded = round($panierOnServer->usd);
                    $compte = isset($panierOnServer->datas['compte']) ? $panierOnServer->datas['compte'] : $panierOnServer->datas['asset'];
                    $ptf = ucfirst(strtolower($type));
                    $methode = 'check'.$ptf.'HistorickByIdpanierId';
                    // debug($methode);
                    // debug($panier_id);
                    // die();
                    $resultPaymentsDones = $smsRecus->{$methode}($panier_id, $asArray = 1); // retourne une collection()
                    $resultPaymentsDones = collection(json_decode($resultPaymentsDones, true));
                    $nbResultPaymentsDones = $resultPaymentsDones->count();
                    // debug($resultPaymentsDones ->toArray());

                    // debug($resultPaymentsDones ->toArray());
                    // debug($resultPaymentsDones->count());
                    // debug($nbAssociatedSmss);
                    // debug($panierOnServer);
                    // die();

                    $resultMatch = null;
                    if ($resultPaymentsDones->count() > 0) {

                        if ($panierOnServer->type == 'faucetpay') {
                            $resultMatch = $resultPaymentsDones->match(['compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        } else {
                            $resultMatch = $resultPaymentsDones->match(['usd_rounded' => $usdRounded, 'compte' => $compte, 'date_short' => $shortDatePanierOnServer])->first();
                        }
                    }

                    // debug($nbResultPaymentsDones >= $nbAssociatedSmss);
                    // die();

                    // On vérifie si il y a eu des paiements associé au PTF de cette transaction
                    if ($nbResultPaymentsDones >= $nbAssociatedSmss) { // raha efa nisy envoi dia aleo tsy traitena
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $this->deleteCacheStat('airtel');
                    } else {
                        $this->Ozekimessageins->updateAll(['is_check_recla_paid_done' => 1], ['id' => $sms->id]);
                        $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeAirtel', 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);
                        $results[] = $result = $http->get($url);
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_transfer_as_paid' => 1], ['id' => $sms->id]);
                    }

                    $this->logginto(compact('panierOnServer', 'resultPaymentsDones'), 'panier_paiement_airtel_failed');
                }

                if (!empty($results)) {
                    $this->logginto($results, 'paiement_airtel_failed', $erase = false);
                }
                
                $body = ['results' => (bool) $results];

            }

        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function payError($mobile = "mvola")
    {
        if (!$this->getParam('is_auto_recla_erreur_reseau')) {
            die('stop error script');
        }

        $ucFirstMobile = ucfirst($mobile);
        // debug($this->{$mobile.'Senders'});
        // $smss = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders, 'is_check_sent_done' => 0, 'paiement_status NOT IN' => ['SUCCESS', 4]])->order(['senttime' => 'DESC'])->limit(1);
        $smss = $this->Ozekimessageins->find('Recus')->where([
            'sender in' => $this->{$mobile.'Senders'}, 
            'paiement_status IN' => ['error', 'timeout'],
            'paiement_status_result LIKE' => "%réseau inaccessible%",
            'is_check_sent_done' => 0
        ])
        ->order(['senttime' => 'DESC'])->limit(1);
        // $smss = $this->Ozekimessageins->find('Recus')->where(['id' => 166837])->order(['senttime' => 'DESC'])->limit(1);

        $subMin = Chronos::now()->subMinutes($this->getParam('timeout_auto_recla'));
        $smss = $smss->where(['senttime >= ' => $subMin]);

        $sms = null;
        if ($smss->count() > 0) {
            $sms = $smss->first();
            // debug($sms);
        }

        // $methode = "extract".$ucFirstMobile."RecuData";
        // $dataSms = $this->Ussd->$methode($sms->msg);
        // die();


        // die();

        // debug($smss ->toArray());
        // die();
        $body = [];
        $results = [];

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $searchIdPanierMobile = 'searchIdPanier'.$ucFirstMobile;
            $response = $smsRecus->{$searchIdPanierMobile}($mobile, $sms->id, 'array');
            // debug($response);
            // die();
            $panierOnServer = $response['panierOnServer'];
            $nbAssociatedSmss = $response['nbAssociatedSmss'];
            $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
            $panier_id = $response['panierOnServer']->id;
            $sms_id = $sms->id;
            
            // debug($panierOnServer);
            // debug($panierOnServer->status);
            // debug($sms_id);
            // die();

            if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                $receivedtime = Chronos::parse($sms->receivedtime);
                $panierDate = Chronos::parse($panierOnServer->created_date);

                $this->Ozekimessageins->updateAll(['is_check_sent_done' => 1], ['id' => $sms->id]); 

                $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommande'.$ucFirstMobile, 'panier_id' => $panier_id, 'sms_id' => $sms->id], true);
                $http = new Client();
                $results[] = $result = $http->get($url)->json;
                // debug($result);

                if ($result['status'] == 'success') {
                    $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]); 
                    $this->deleteCacheStat($mobile);
                }
                $this->logginto(compact('panierOnServer', 'receivedtime', 'panierDate'), 'error_'.$mobile.'_data');

            }

            if (!empty($results)) {
                $this->logginto($results, 'paiement_'.$mobile.'_error', $erase = false);
            }
            $body = ['results' => $results];
        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * Ancien à base de raison
     * @return [type] [description]
     */
    // public function payFailedMvola()
    // {
    //     $smss = $this->Ozekimessageins->find()->where([
    //         // 'id' => 104788, 
    //         'sender in' => $this->senders, 
    //         'msg LIKE' => "%Vous avez recu%", 
    //         'operator !=' => 'ONEX', 
    //         'or' => [
    //             ['paiement_status' => 'error'],
    //             ['paiement_status IS' => NULL]
    //         ],
    //         'coalesce(paiement_status_result, "") !=' => 'Numero profil ne correspond pas',
    //     ])->order(['senttime' => 'DESC'])->limit(5);
    //     $subMin = Chronos::now()->subMinutes(60);
    //     $smss = $smss->where(['senttime >= ' => $subMin]);
    //     $results = [];

    //     foreach ($smss as $key => $sms) {
    //         $http = new Client();
    //         $mvolaData = $this->Ussd->extractMvolaRecuData($sms->msg);
    //         $panier_id = $mvolaData['idpanier'];
    //         $nb_essai_envoi = $sms->nb_essai_envoi;

    //         if ($nb_essai_envoi <= 1) {
    //             $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommandeMvola', 'panier_id' => $panier_id, 'sms_id' => $sms->id], true);
    //             // debug($url);
    //             // die();
    //             $results[$sms->id] = $result = $http->get($url, [])->json;
    //             // debug($result);
    //             if ($result['status'] == 'success') {
    //                 $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
    //             } else {
    //                 $incremented = $sms->nb_essai_envoi + 1;
    //                 $msg = @$result['msg'] ?? '';
    //                 $this->Ozekimessageins->updateAll(['nb_essai_envoi' => $incremented, 'paiement_status_result' => $msg], ['id' => $sms->id]);
    //             }
    //             sleep(5);
    //         } 
    //     }

    //     if (!empty($results)) {
    //         $this->logginto($results, 'payed_failed_mvola');
    //     }
    //     $body = ['results' => $results];
    //     return $this->response->withType('application/json')->withStringBody(json_encode($body));
    // }

    /**
     * Venant du site en prod, par exemple OTP, maj numero mobile, etc
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function sendSms($userId = null)
    {
        // #144#*2*1*2*1*2*1*3# be sms 250 Ar
        if ($this->request->is(['post', 'put', 'patch', 'get'])) {
            $data = $this->request->getData();

            // pr($data);
            // die('ici');

            $numero = $data['numero'];
            $msg = $data['msg'];
            $msg = $this->normalizeChars($data['msg']);

            $mobile = $this->Ussd->checkMobile2($numero);
            $param = $this->getParam();

            if (isset($data['port'])) {
                $this->Ussd->setPort($data['port']); // Si test Postman
            } else {
                $this->Ussd->setPort($this->getParam('port_sms_out_'.$mobile));
            }

            // pr($data);
            // pr($this->Ussd->getPort());
            // die();

            $isTelmaAutoRechargeActivated = $param->{'is_recharge_auto_forfait_telma'};
            $isOrangeAutoRechargeActivated = $param->{'is_recharge_auto_forfait_orange'};

            $cas = null;
            if ($isTelmaAutoRechargeActivated == true && $isOrangeAutoRechargeActivated == false) {
                $cas = '1 er cas';
                $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
            } elseif ($isTelmaAutoRechargeActivated == true && $isOrangeAutoRechargeActivated == true) {
                $cas = '2 eme cas';
                if (in_array($mobile, ['telma', 'airtel'])) {
                    $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
                    $cas = '3 eme cas';;
                } else {
                    $this->Ussd->setPort($this->getParam('port_sms_out_orange'));
                    $cas = '4 eme cas';
                }
            }

            // if ($mobile == 'orange' || $mobile == 'airtel') {
            //     $this->Ussd->setPort($this->getParam('port_sms_out_orange'));
            // } elseif ($mobile == 'telma') {
                // $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
            // }

            // if ($mobile == 'airtel') {
            //     if ($param->nb_sms_to_telma > 0) { // _to_ = tout operator
            //         $this->Ussd->setPort($this->getParam('port_sms_out_telma'));
            //     } else if($param->nb_sms_to_orange > 0) {
            //         $this->Ussd->setPort($this->getParam('port_sms_out_orange'));
            //     } else {
            //         $this->Ussd->setPort($this->getParam('port_sms_out_airtel'));
            //     }
            // }

            // $host = $this->serveurDisant;
            // $http = new Client();
            // $urlAlert = $host.'/exchanger/apis/alertSmsLow/'.$mobile;

            // if ($mobile == 'telma' && $param->nb_sms_to_telma < 125) {
            //     $http->get($urlAlert);
            // } elseif ($mobile == 'orange' && $param->nb_sms_to_orange < 125) {
            //     $http->get($urlAlert);
            // }
                

            // debug($this->getParam('port_sms_out_'.$mobile));
            // debug($this->Ussd->getPort());
            // debug($this->Ussd->port);
            // debug($mobile);

            // pr($this->Ussd->getPort());
            // die();

            // pr($numero);
            // pr($msg);
            // die();
            
            $port = $this->Ussd->getPort();
            $this->logginto(compact('cas', 'numero', 'mobile', 'msg', 'port'), 'data_sms');
            $res = $this->Ussd->send($numero, $msg);
            // $this->logginto($numero. ' : '. $msg, 'sms/login', $erase = false);

            // debug($res);
            // die();
            // debug($res);

            if ($userId) {
                $data['user_id'] = $userId;
            }

            $AuthSms = $this->AuthSmss->newEntity($data, ['validate' => false]);
            if(!$AuthSms->getErrors()) {
                $this->AuthSmss->save($AuthSms);
            }
            // echo trim($res);
            echo trim($res);
        }

        die();

    }



    public function sendMeSms($numero, $montantRecu, $soldeActuel)
    {
        $this->Ussd->setPort('com70');
        $mobile = ucfirst($this->checkMobile2($numero));
        // $this->Ussd->send('0321702119', "Achat $mobile $montantRecu Ar, solde $soldeActuel Ar", $flashmode = false);
        return;
    }   


    // public function testEnvoiSms()
    // {
    //     $this->Ussd->setPort('com70');
    //     $this->Ussd->send('0321702119', "Achat sy, montant zao Ar", $flashmode = false);
    //     die('ao');
    // }

    public function paySingleError($smsId)
    {
        $body = [];
        $result = [];



        // sleep(1);
        // $result = ['status' => 'success', 'mobile' => 'mvola'];
        // $body = $result;
        // return $this->response->withType('application/json')->withStringBody(json_encode($body));

        // sleep(7);
        // throw new \Cake\Http\Exception\ServiceUnavailableException();
        // die();


        $isReclaFromJs = $this->request->getQuery('is_recla_from_js');

        if ($isReclaFromJs) {
            $sms = $this->Ozekimessageins->findById($smsId)->where(['is_check_sent_done' => 0])->first();
            $this->Ozekimessageins->updateAll(['is_check_sent_done' => 1], ['id' => $sms->id]);
        } else {
            $sms = $this->Ozekimessageins
                ->findById($smsId)
                ->first()
            ;
        }

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $mobile = $sms->get('Mobile');
            $ucFirstMobile = ucfirst($mobile);
            $searchIdPanierMobile = 'searchIdPanier'.$ucFirstMobile;
            $response = $smsRecus->{$searchIdPanierMobile}($mobile, $sms->id, 'array');
            $panierOnServer = @$response['panierOnServer'];

            if (isset($panierOnServer->id)) {
                
                $panierOnServer = $response['panierOnServer'];
                $nbAssociatedSmss = $response['nbAssociatedSmss'];

                $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
                $panier_id = $response['panierOnServer']->id;
                $sms_id = $sms->id;
                
                // debug($panierOnServer);
                // debug($panierOnServer->status);
                // debug($sms_id);
                // die();

                if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                    $receivedtime = Chronos::parse($sms->receivedtime);
                    $panierDate = Chronos::parse($panierOnServer->created_date);

                    $type = $panierOnServer->type;
                    $usdRounded = round($panierOnServer->usd);
                    $ptf = ucfirst(strtolower($type));
                    $methode = 'check'.$ptf.'HistorickByIdpanierId';

                    // METY HO BLOQUÉ ERREUR 503 ato

                    // sleep(2);
                    $resultPaymentsDones = $smsRecus->{$methode}($panier_id, $asArray = 1); // retourne une collection()
                    $resultPaymentsDones = collection(json_decode($resultPaymentsDones, true));
                    $nbResultPaymentsDones = $resultPaymentsDones->count();

                    // debug($nbResultPaymentsDones);
                    // debug($nbAssociatedSmss);

                    $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommande'.$ucFirstMobile, 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);

                    // On vérifie si il y a eu des paiements associé au PTF de cette transaction
                    if ($nbResultPaymentsDones < $nbAssociatedSmss) { // raha efa nisy envoi dia aleo tsy traitena
                        // debug($url);
                        $http = new Client();
                        $results[] = $result = $http->get($url)->body();
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_transfer_as_paid' => 1], ['id' => $sms->id]);

                        $httpResult = json_decode($result, true);
                        $result = ['status' => $httpResult['status'], 'result' => $result, 'nbResultPaymentsDones' => $nbResultPaymentsDones, 'nbAssociatedSmss' => $nbAssociatedSmss, 'mobile' => $mobile];

                    } else {
                        $this->deleteCacheStat($mobile);
                        $result = ['status' => 'nb_sms_sup', 'nbResultPaymentsDones' => $nbResultPaymentsDones, 'nbAssociatedSmss' => $nbAssociatedSmss, 'mobile' => $mobile];
                    } 
                } else {
                    $result = ['status' => 'order_already_paid', 'status_on_server' => $panierOnServer->status, 'mobile' => $mobile];
                }

            } else {
                $result = ['status' => 'order_not_found', 'msg' => 'Idpanier pour SMS ID '.$smsId.' non trouvé', 'mobile' => $mobile];
            }

        } else {
            $result = ['status' => 'sms_not_found'];
        }
        
        $body = $result;
        $this->logginto(compact('sms', 'result'), 'panier_paiement_'.$mobile.'_single_recla');

        $this->loadModel('LogReclas');

        $logRecla = $this->LogReclas->newEntity(['content' => serialize($result), 'ozekimessagein_id' => $sms->id], ['validate' => false]);
        if(!$logRecla->getErrors()) {
            $this->LogReclas->save($logRecla);
        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function getSmsId($smsId)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();

        $mobile = $sms->get('Mobile');
        $extractMethode = 'extract'.ucfirst($mobile).'RecuData';

        $data = $this->Ussd->{$extractMethode}($sms->msg);

        $sms->data = $data;

        $body = $sms;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function paySingleErrorTest($smsId)
    {
        $body = [];
        $result = [];

        $sms = $this->Ozekimessageins->findById($smsId)->first();

        if ($sms != null) {
            $smsRecus = new SmsRecusController();
            $result = false;

            $mobile = $sms->get('Mobile');
            $ucFirstMobile = ucfirst($mobile);
            $searchIdPanierMobile = 'searchIdPanier'.$ucFirstMobile;
            $response = $smsRecus->{$searchIdPanierMobile}($mobile, $sms->id, 'array');
            $panierOnServer = @$response['panierOnServer'];

            if (isset($panierOnServer->id)) {
                
                $panierOnServer = $response['panierOnServer'];
                $nbAssociatedSmss = $response['nbAssociatedSmss'];

                $shortDatePanierOnServer = Chronos::parse($panierOnServer->created)->format('Y-m-d');
                $panier_id = $response['panierOnServer']->id;
                $sms_id = $sms->id;
                
                // debug($panierOnServer);
                // debug($panierOnServer->status);
                // debug($sms_id);
                // die();

                if (!in_array($panierOnServer->status, ['SUCCESS', 4])) {

                    $receivedtime = Chronos::parse($sms->receivedtime);
                    $panierDate = Chronos::parse($panierOnServer->created_date);

                    $type = $panierOnServer->type;
                    $usdRounded = round($panierOnServer->usd);
                    $ptf = ucfirst(strtolower($type));
                    $methode = 'check'.$ptf.'HistorickByIdpanierId';

                    // METY HO BLOQUÉ ERREUR 503 ato

                    // sleep(2);
                    $resultPaymentsDones = $smsRecus->{$methode}($panier_id, $asArray = 1); // retourne une collection()
                    $resultPaymentsDones = collection(json_decode($resultPaymentsDones, true));
                    $nbResultPaymentsDones = $resultPaymentsDones->count();

                    // debug($nbResultPaymentsDones);
                    // debug($nbAssociatedSmss);

                    $url = Router::url(['controller' => 'SmsRecus', 'action' => 'reglerCommande'.$ucFirstMobile, 'panier_id' => $panier_id, 'sms_id' => $sms_id], true);

                    // On vérifie si il y a eu des paiements associé au PTF de cette transaction
                    if ($nbResultPaymentsDones < $nbAssociatedSmss) { // raha efa nisy envoi dia aleo tsy traitena
                        // debug($url);
                        $http = new Client();
                        // $results[] = $result = $http->get($url)->body();
                        $this->Ozekimessageins->updateAll(['paiement_status' => 'success', 'marked_transfer_as_paid' => 1], ['id' => $sms->id]);

                        $httpResult = json_decode($result, true);
                        $result = ['status' => 'success'];

                    } else {
                        // $this->deleteCacheStat($mobile);
                        $result = ['status' => 'nb_sms_sup', 'nbResultPaymentsDones' => $nbResultPaymentsDones, 'nbAssociatedSmss' => $nbAssociatedSmss];
                    } 
                } else {
                    $result = ['status' => 'order_already_paid', 'status_on_server' => $panierOnServer->status];
                }

            } else {
                $result = ['status' => 'order_not_found', 'msg' => 'Idpanier pour SMS ID '.$smsId.' non trouvé'];
            }

        } else {
            $result = ['status' => 'sms_not_found'];
        }
        
        $body = $result;
        $this->logginto(compact('sms', 'result'), 'panier_paiement_'.$mobile.'_single_recla');

        $this->loadModel('LogReclas');

        $logRecla = $this->LogReclas->newEntity(['content' => serialize($result), 'ozekimessagein_id' => $sms->id], ['validate' => false]);
        if(!$logRecla->getErrors()) {
            $this->LogReclas->save($logRecla);
        }

        // $body = ['test' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function voirLogReclas($ozekimessagein_id)
    {
        $this->loadModel('LogReclas');
        $logReclas = $this->LogReclas->findByOzekimessageinId($ozekimessagein_id);
        $this->set(compact('logReclas'));
    }

    /**
     * Compte Betwinner
     * 380771761 (joueur simple anà)
     * 996453173 (ity le fanaovana rechargement caisse)
     * 
     *  User ID: narsantsoaa 
        Password: JTp6zs1k 
        Workplace code: 1291341 
        hash : 1e6016d8154a0746bf93e3ea2b9c490b640bf431e49156d98b0a83d13ae61443
        Antananarivo - Moneiko Mada
     */
    public function webhookBetwinner()
    {
        $http = new Client();

        $endpoint = "https://partners.servcul.com/CashdeskBotAPI";

        $userID = "narsantsoaa";
        $password = "JTp6zs1k";
        $workplaceCode = "1291341";

        $lng = "ru";

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $dt = $date->format('Y.m.d H:i:s');

        $userid= "narsantsoaa";
        $summa = 100;


        // Check cashpoint balance:
        $cashdeskid = "1291341"; // (КРМ - workplace code, cash register number)
        $cashierpass = "JTp6zs1k";
        $hash = "1e6016d8154a0746bf93e3ea2b9c490b640bf431e49156d98b0a83d13ae61443";


        $sha256 = hash('sha256', "hash=$hash&cashdeskid=$cashdeskid&dt=$dt"); // hash={0}&cashdeskid={1}&dt={2}
        $md5 = md5("dt=$dt&cashierpass=$cashierpass&cashdeskid=$cashdeskid"); // dt={0}&cashierpass={1}&cashdeskid={2}
        $signature = hash("sha256", $sha256.$md5);

        $confirm = md5("$cashdeskid:$hash");


        $url = $endpoint."/Cashdesk/$cashdeskid/Balance?confirm=$confirm&dt=$dt";


        $res = $http->get($url);
        debug($res->json);
        die();



        $client = new \GuzzleHttp\Client(['verify' => false]);

        // Effectuer la requête GET avec l'en-tête "sign"
        $response = $client->request('GET', $url, [
            'headers' => [
                'sign' => $signature
            ],
        ]);

        // Afficher la réponse
        // debug($response->getBody());

        die();















        die();


        

        // debug($sha256);
        // debug($md5);
        die();
    }


    // public function webhookBetwinner()
    // {
    //     $userID = "narsantsoaa";
    //     $password = "JTp6zs1k";
    //     $workplaceCode = "1291341";
    //     $endpoint = "https://partners.servcul.com/CashdeskBotAPI";

    //     $lng = "ru";

    //     $date = new \DateTime('now', new \DateTimeZone('UTC'));
    //     $dt = $date->format('Y.m.d H:i:s');

    //     $userid= "narsantsoaa";
    //     $summa = 100;

    //     $cashdeskid = "1291341"; // (КРМ - workplace code, cash register number)
    //     $cashierpass = "JTp6zs1k";
    //     $hash = "1e6016d8154a0746bf93e3ea2b9c490b640bf431e49156d98b0a83d13ae61443";

    //     $sha256 = hash('sha256', "hash=$hash&cashdeskid=$cashdeskid&dt=$dt"); // hash={0}&cashdeskid={1}&dt={2}
    //     $md5 = md5("dt=$dt&cashierpass=$cashierpass&cashdeskid=$cashdeskid"); // dt={0}&cashierpass={1}&cashdeskid={2}

    //     $signature = hash("sha256", $sha256.$md5);
    //     $confirm = md5("$userid:$hash");


    //     $http = new Client();
    //     $url = $endpoint."/Cashdesk/{$cashdeskid}/Balance?confirm=$confirm&dt=$dt";


    //     $res = $http->get($url, [], [
    //         'headers' => [
    //             'sign' => $signature
    //         ]
    //     ]);

    //     echo($res->body());

    //     // debug($sha256);
    //     // debug($md5);
    //     die();
    // }
}
?>