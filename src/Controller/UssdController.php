<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Chronos\Chronos;
use Cake\Http\Client;
use App\Controller\TdbController;
use App\Traits\AppTrait;

class UssdController extends AppController
{
    use AppTrait;
    
    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadComponent('Ussd');
        $this->loadModel('SentUssds');
        $this->loadModel('Paiements');
        $this->loadModel('RemerciementSmss');
        // $mode = $this->mode = "prod"; // local ou prod
        $mode = $this->mode = Configure::read('callback_mode');; // local ou prod
        $callbackUrls = Configure::read('callback_url');

        // callback màj de l'état des transaction no ilan'ity
        $urlToWeb = $this->urlToWeb = $callbackUrls[$mode]['urlToWeb']; //ussdretrait 
        $urlCallback = $this->urlCallback = $callbackUrls[$mode]['urlCallback'];
        
        $this->client = $http = new Client();
        // $response = $this->run("#321#", ["3", "1", "0325541393"]);

    }

    public function index()
    {
        $timestamp_debut = microtime(true);
        // ----------- fin màj de l'état des transaction no ilan'ity -----------

        // récupère la 1ere transaction marqué non payée 
        // et marque à payer dans le callback dans le prochain script
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $transactionATraiter = (object) $data['ipn']; // $this->Utility->sendRetraitParUssd($venteEntity) // exemple VentePmEntity
            $this->logginto("$transactionATraiter->client_account, $transactionATraiter->ariary Ar, $transactionATraiter->paiement, $transactionATraiter->titulaire", 'vente_ipn');
        } else {
            return $this->redirect(['controller' => 'Tdb', 'action' => 'index', 'orange']);
        }

        if (!isset($transactionATraiter->id)) {
            die('<h3>Aucune transaction à traiter pour le moment</h3>');
        } else {

            $isUssdSent = (bool) $transactionATraiter->is_ussd_sent;
            $mobile_paiement_id = $transactionATraiter->mobile_paiement_id;
            $mobile_paiement_status = $transactionATraiter->mobile_paiement_status;

            $date = $transactionATraiter->date ?? $transactionATraiter->created->format('');
            $numero = $transactionATraiter->numero;
            $titulaire = $transactionATraiter->titulaire;
            $paiement = $transactionATraiter->paiement;
            $montant = round($transactionATraiter->ariary);
            $prefixMobile = $this->checkMobilePrefix($numero);

            $type = "";
            $ptf_ref = "TX";
            $orange_montant_limite_ariary = Configure::read('orange_montant_limite_ariary');
            $mvola_montant_limite_ariary = Configure::read('mvola_montant_limite_ariary');
            $airtel_montant_limite_ariary = Configure::read('airtel_montant_limite_ariary');

            $driverTelma = $this->driverTelma;
            $driverOrange = $this->driverOrange;

            $model = '';

            // pr($transactionATraiter);
            // die();

            // Doit être pareil que _getFactureId()
            if (isset($transactionATraiter->pm_paiement_id)) {
                $type = "perfect";
                $model = "VentePerfects";
                $ptf_ref = "PM-".$transactionATraiter->id;
                // $ptf_ref = "PM".$transactionATraiter->id;
            } elseif (isset($transactionATraiter->payeer_paiement_id)) {
                $type = "payeer";
                $model = "VentePayeers";
                $ptf_ref = "PY-".$transactionATraiter->id;
                // $ptf_ref = "PY".$transactionATraiter->id;
            } elseif (isset($transactionATraiter->ac_transfer)) {
                $type = "advcash";
                $model = "VenteAdvcashs";
                $ptf_ref = "ADV-".$transactionATraiter->id;
                // $ptf_ref = "ADV".$transactionATraiter->id;
            } elseif (isset($transactionATraiter->generated_adress)) {
                $type = "crypto";
                $model = "CryptoOrders";
                $ptf_ref = ($transactionATraiter->asset).'-'.$transactionATraiter->id;
                // $ptf_ref = ($transactionATraiter->asset).'-'.$transactionATraiter->id;
            } elseif (isset($transactionATraiter->devise)) {
                $type = "fiat";
                $model = "VenteFiats";
                $ptf_ref = strtoupper(substr($transactionATraiter->type, 0, 2)).'-'.$transactionATraiter->id; // vente _getSkrillRefFront()
                // $ptf_ref = strtoupper(substr($transactionATraiter->type, 0, 2)).$transactionATraiter->id; // vente _getSkrillRefFront()
            } elseif (isset($transactionATraiter->fp_paiement_id)) {
                $type = "faucet";
                $model = "VenteFaucetpays";
                $ptf_ref = 'FP-'.($transactionATraiter->asset).'-'.$transactionATraiter->id;
                // $ptf_ref = strtoupper(substr($transactionATraiter->type, 0, 2)).$transactionATraiter->id; // vente _getSkrillRefFront()
            }

            // debug($transactionATraiter);
            // die();
            $this->logginto($transactionATraiter, 'test_fiat', $erase = false);
            // die();

            $message = "";
            $message = "Retrait $type $montant Ar au $numero - $titulaire, le $date.";
            $messageClient = "Moneiko vous remercie pour ce retrait $type $montant Ar au $numero - $titulaire, le $date.";

            // die('maintenance..');
            // test Local voir moneiko.net/ussd-retrait
            // if ($isUssdSent == false /*&& $mobile_paiement_status != "payé" && !$mobile_paiement_id*/) { // tokony itovy amle ussdcontroller ary am site ligne 48
            if ($isUssdSent == false && $mobile_paiement_status != "payé") { // tokony itovy amle ussdcontroller ary am site ligne 48

                $dataTransaction = compact('date', 'numero', 'titulaire', 'montant');
                // lance la requête pour l'envoi 
                $usedPort = $this->Ussd->setPortAndPathByNumeroMobile($numero);
                $typeMobile = $this->Ussd->checkMobile($numero);
                $isSentSuccessfull = false;
                $resUssd = '';

                $dataPaiement = [
                    'ref' => $ptf_ref,
                    'type' => $typeMobile,
                    'numero' => $numero,
                    'montant' => $montant,
                    'type_tx' => 'out'
                ];

                // $this->logginto($dataPaiement, 'data_paiement');
                // die();

                // autre couche de sécurité locale
                // if (!$this->Paiements->find()->where($dataPaiement)->first()) {
                if (true) {

                    if ($typeMobile == 'orange') {
                        // $this->Ussd->resetUsb($driverOrange);

                        // $resUssd = $this->Ussd->run("#123#", ["3", "1", "0325541393"]);

                        if ($montant <= $orange_montant_limite_ariary) { 
                            // $resUssd = $this->Ussd->run("#144#", ["1", "1", $numero, $numero, $montant, "2", "2958"]); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                            // $resUssd = $this->Ussd->run("#144*1*1*$numero*$numero*$montant*2#", ["2958", "1"]); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23

                            $transfertCommand = "#144*1*1*$numero*$numero*$montant*2*2958#";
                            if ($this->allowTransfertIfMoreMinutesMoreThan($transfertCommand)) {
                                
                                $resUssd = $this->Ussd->run("#144*1*1*$numero*$numero*$montant*2#", ["2958"]); // n'execute pas le double transfert
                                // $resUssd = substr($resUssd, 0, -162);

                                $paiement = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                                if(!$paiement->getErrors()) {
                                    $this->Paiements->save($paiement);
                                }

                                $this->logginto($numero.', '.@$transactionATraiter->montant.' usd, '. $montant.', '. $titulaire.', '. $resUssd, 'orange/ussd_response', $erase = false);
                                sleep(5);
                                $this->Ussd->readAndSaveSmsFromAllMemory($prefixMobile); // MAJ SMS VERS BDD Pour la sécurité
                                $tdb = new TdbController();
                                $tdb->majRefMobileVersSite($paiement, true);
                                $isSentSuccessfull = strrpos($resUssd, 'initie') !== false;
                                $this->saveUsssdHistorics($commandUssd = $transfertCommand, $responseUssd = $resUssd, $typeUssd = "orange");

                            } else {
                                $resUssd = "$transfertCommand commande déjà executée récemment";
                            }
                        } else {
                            $resUssd = "Montant hors limite";
                        }

                        if (strrpos($resUssd, 'ATZ') !== false) {
                            $this->Ussd->resetUsb($driverOrange);
                        } 

                    }
                    elseif ($typeMobile == 'airtel') {
                        // $this->Ussd->resetUsb($driverOrange);
                        // $resUssd = $this->Ussd->run("#123#", ["3", "1", "0325541393"]);

                        $numero = ltrim($numero, '0'); // pour supprimer le 0 au début du numéro airtel

                        if ($montant <= $airtel_montant_limite_ariary) { 

                            $transfertCommand = "*436*2# 1*1*$numero*$numero*$montant*$ptf_ref# 2958";
                            if ($this->allowTransfertIfMoreMinutesMoreThan($transfertCommand)) {

                                // $resUssd = $this->Ussd->run("*436*2#", ["1*1*$numero*$numero*$montant*$ptf_ref", "2958"]); // verion avy dia mitohy
                                $resUssd = $this->Ussd->run("*436*2#", ["1", "1", $numero, $numero, $montant, $ptf_ref, "2958"]); // version le misy appuyez sur ok
                                // $resUssd = $this->Ussd->cleanTransfert($resUssd);
                                // $resUssd = substr($resUssd, 0, -162);

                                $paiement = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                                if(!$paiement->getErrors()) {
                                    $this->Paiements->save($paiement);
                                }

                                $this->logginto($numero.', '.@$transactionATraiter->montant.' usd, '. $montant.', '. $titulaire.', '. $resUssd, 'airtel/ussd_response', $erase = false);
                                sleep(5);
                                $this->Ussd->readAndSaveSmsFromAllMemory($prefixMobile); // MAJ SMS VERS BDD Pour la sécurité
                                $tdb = new TdbController();
                                $tdb->majRefMobileVersSite($paiement, true);
                                $isSentSuccessfull = strrpos($resUssd, 'initie') !== false;
                                $this->saveUsssdHistorics($commandUssd = $transfertCommand, $responseUssd = $resUssd, $typeUssd = "airtel");

                            } else {
                                $resUssd = "$transfertCommand commande déjà executée récemment";
                            }

                        } else {
                            $resUssd = "Montant hors limite";
                        }

                        if (strrpos($resUssd, 'ATZ') !== false) {
                            $this->Ussd->resetUsb($driverOrange);
                        } 

                    } elseif (in_array($typeMobile, ['mvola'])) {
                        // $this->Ussd->resetUsb($driverTelma);
                        // sleep(2);
                        if ($montant <= $mvola_montant_limite_ariary) {

                            $transfertCommand = "#111*1*2*$numero*$montant*2*$ptf_ref# 2958";
                            if ($this->allowTransfertIfMoreMinutesMoreThan($transfertCommand)) {
                                $compte = $transactionATraiter->client_account;
                                $ussd = "#111*1*2*$numero*$montant*3# 2*$ptf_ref";
                                $this->logginto($ussd, 'mvola/ussd', $erase = false);
                                // $ussd = "#111*1*2*$numero*$montant*2*$ptf_ref#";
                                $resUssd = $this->Ussd->run($ussd, ["2958"]);
                                // $resUssd = "Votre traiteraansaction a reussi, pour enregistrer";
                                $resUssd = str_replace(", Entrer le nom correspondant ou ignorer :", "", $resUssd);
                                // $resUssd = $this->Ussd->run("#111#", ["1", "3", "2", $numero, $montant, $ptf_ref, "2958"]);
                                // $this->Ussd->run("#111*1*2*2*2958#");

                                $paiement = $this->Paiements->newEntity($dataPaiement, ['validate' => false]);
                                // if(!$paiement->getErrors()) {
                                //     $this->Paiements->save($paiement);
                                // }

                                $this->logginto($numero.', '. $montant.' Ar, '. $titulaire.', '. $resUssd, 'mvola/ussd_response', $erase = false);
                                // die('ato');
                                
                                // Pour vérifier si les envois ont bien été excutés
                                $fraisMvola = $this->getTarifMvolaEnvoi($montant);
                                $montantAvecFrais = $montant+$fraisMvola;
                                $ussdData = [
                                    'ref' => $ptf_ref,
                                    'vente_id' => $transactionATraiter->id,
                                    'numero' => $numero,
                                    'montant' => $montant,
                                    'frais_mvola' => $fraisMvola,
                                    'montant_avec_frais' => $montantAvecFrais,
                                    'res_ussd' => $resUssd,
                                    'type' => $typeMobile,
                                    'model' => $model,
                                    'date' => date('Y-m-d'),
                                ];

                                // $this->logginto($ussdData, 'mvola/ussd_data', $erase = false);

                                $sentUssd = $this->SentUssds->newEntity($ussdData, ['validate' => false]);
                                $this->Parameters->updateAll(['last_solde_mvola' => $this->parameter['last_solde_mvola']-$montantAvecFrais], ['id' => 1]);
                                if(!$sentUssd->getErrors()) {
                                    $this->SentUssds->save($sentUssd);
                                }

                                $historicUssd = $this->saveUsssdHistorics($commandUssd = $transfertCommand, $responseUssd = $resUssd, $typeUssd = "telma");

                                if (strrpos($resUssd, 'ATZ') !== false) {
                                    $this->Ussd->resetUsb($driverTelma);
                                }
                            } else {
                                $resUssd = "$transfertCommand commande déjà executée récemment ";
                            }

                        } else {
                            $resUssd = "Montant hors limite";
                        }

                        // debug($resUssd);
                        // die();
                        // $resUssd = $this->Ussd->run("#111#", ["1", "2", $numero, $montant, $ptf_ref, "2958"]);
                        // debug($resUssd);
                        // die();
                        $isSentSuccessfull = strrpos($resUssd, 'reussi') !== false;
                    } else {
                        $resUssd = 'Mobile non pris en compte';
                        die();
                    }

                    $isSentSuccessfull = true;
                    $timestampFinUssd = microtime(true);


                    // if (!empty($resUssd)) {

                    // Envoi email et màj raison (txn_id) sur la transaction callback
                    $url = $this->urlCallback.$transactionATraiter->id.'/'.$type; // exemple : moneiko.net/exchanger/ussdMajRetrait/55
                    // $this->logginto($url, 'url_check_callback', $erase = false);


                    if ($resultState = $this->Ussd->checkIfSuccess($resUssd, $typeMobile)) {
                        // $this->logginto($resultState.': '.$resUssd, 'test_check_state', $erase = false);
                        $res = $this->client->post($url, ['ussd_response' => $resUssd, 'ptf_ref' => $ptf_ref], ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false]);
                        $response = $res->json;
                        // $this->logginto($url. ' : ' . $response, 'response_callback', $erase = false);
                        $this->logginto(compact('url', 'response'), 'response_callback_debug', $erase = false);

                        if (!$res->isOk()) {
                            sleep(2.5);
                            $response = $this->client->post($url, ['ussd_response' => $resUssd, 'ptf_ref' => $ptf_ref], ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false])->json;
                        }

                        // $this->logginto("$result->client_account, $result->ariary Ar, $result->paiement, $result->titulaire, $result->orange_txnid", 'debug_callback', $erase = false);

                        if (@$response['result'] != null) {
                            $result = (object) $response['result']; // compte = VentePerfect entity par exemple

                            $timestamp_fin = microtime(true);
                            $differenceMsUssd = round($timestampFinUssd - $timestamp_debut); 

                            if ($this->parameter->is_auto_sms_merci_retrait_send_activated) {
                                $this->sendSMSMerci($numero);
                            }
                            $difference_ms = round($timestamp_fin - $timestamp_debut); 

                            $this->logginto("$result->type, $result->ariary Ar, $result->paiement, $result->titulaire, $result->orange_txnid, $differenceMsUssd/$difference_ms secondes", 'callback', $erase = false);
                        }
                    } else {
                        $res = $this->client->post($url, ['ussd_response' => $resUssd, 'ptf_ref' => $ptf_ref], ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false]);
                        $this->logginto($resUssd, 'check_state_error', $erase = false);
                        $this->logginto($res->json, 'res_error', $erase = false);
                        // debug($resUssd);
                    }
                    // echo '<br>'. $messageTexte;
                } else {
                    $this->logginto($numero.', '. $montant.', '. $ptf_ref.', ', 'paiement_deja_fait', $erase = false);
                }
            }
        }


        die();
    }

    public function allowTransfertIfMoreMinutesMoreThan($command = null)
    {
        $this->loadModel('UssdHistorics');
        // $command = "#111*1*2*0344082230*101200*2*FP-USDT-4488# 2958";
        $findedHistoricUssd = $this->UssdHistorics->find()->where([
            'command' => $command
        ])->last();

        $now = Chronos::now();
        $isAllowed = false;

        if (!$findedHistoricUssd) {
            $isAllowed = true;
        }
        else {
            $minutesDiff = $findedHistoricUssd->created->diffInMinutes($now);
            $isAllowed = $minutesDiff >= 10;
        }

        return $isAllowed;
    }

    public function saveUsssdHistorics($command, $response, $type)
    {
        $this->loadModel('UssdHistorics');

        $port = $this->Ussd->setPortAndPathByNumeroMobile((string) $type);
        $numero = $this->Ports->findByCom($port)->first()->numero;

        $ussdData = [
            'command' => $command,
            'response' => $response,
            'type' => $type,
            'numero' => $numero,
        ];

        $this->logginto($ussdData, 'ussd_data', $erase = false);

        $ussdHistoric = $this->UssdHistorics->newEntity($ussdData, ['validate' => false]);

        if(!$ussdHistoric->getErrors()) {
            $ussdHistoric = $this->UssdHistorics->save($ussdHistoric);
            $this->logginto($ussdHistoric, 'saved_ussd_data', $erase = false);
        }
        // debug($ussdHistoric);
        // die;

        return $ussdHistoric;
    }
 

    public function test($num = "0329965433")
    {

        $resUssd = "Votre transaction a reussi";
        $transfertCommand = "#111*1*2*0343551503*76400*2*BI-17244# 2958";
        // $historicUssd = $this->saveUsssdHistorics($commandUssd = $transfertCommand, $responseUssd = $resUssd, $typeUssd = "telma");
        if ($this->allowTransfertIfMoreMinutesMoreThan($transfertCommand)) {
            die('Transfert permis');
        } else {
            die('Transfert non permis');
        }
        die();
        $this->sendSMSMerci($num);
        // $this->Ussd->setPort('com65');
        // $message = "Misaotra tompoko mba azo asina kely 5 * sy com fankasitrahana ve ity raha mba mety ? https://fr.trustpilot.com/review/moneiko.net ? Merci bcp :)";
        // $this->Ussd->send($numero = "0341035571", $message, $flashmode = false);
        die();
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $resUssd = $data['resUssd'];
            $typeMobile = $data['typeMobile']; 
            $result = $this->Ussd->checkIfSuccess($resUssd, $typeMobile);
            debug($resUssd);
            debug($result);
        }

        die();
    }

    public function checkMobilePrefix($numero)
    {
        return substr($numero, 0, 3);
    }


    public function readSms()
    {
        $smss = $this->Ussd->readAndSaveSmsFromAllMemory();
        debug($smss);
        die();
    }

    public function testAccessibiliteFromProd($mobile)
    {
        $this->Ussd->setPortAndPathByNumeroMobile($mobile);
        $res = '';

        if ($mobile == "032") {
            $res = $this->Ussd->run('#144#');
        } else {
            $res = $this->Ussd->run('#111#');
        }
        echo $res;
        die();
    }

    public function getTarifMvolaEnvoi($montant)
    {
        $tarifsMvola = Configure::read('tarifMvola');
        $frais = 0;
        foreach ($tarifsMvola as $key => $tarif) {
            $tInf = $tarif[0];
            $tSup = $tarif[1];
            // 5000
            if ($montant >= $tInf && $montant <= $tSup) {
                $frais = $key;
            }
        }

        return $frais;
    }



    public function testState()
    {
        $portDebug = $this->Ussd->setPortAndPathByNumeroMobile("034");
        $result = $this->Ussd->waitForOpen($portDebug);
        debug($result);
        die();
    }

    /**
     * /numero_ho_verifiena/com_anle_numport_titulaire/operateur_anle_numero_ho_verifiena
     */
    public function getTitulaireFromNumero($numero, $comPort = "COM36", $mobile = 'orange')
    {
        $this->Ussd->setPort($comPort);
        $typeMobile = $this->Ussd->checkMobile($numero);
        $body = ['status' => (bool) false, 'nom_titulaire' => '', 'response' => ''];

        $operateurNumPortTitulaire = $this->Ussd->checkMobile2($this->getParam('num_port_titulaire'));

        if ($operateurNumPortTitulaire == 'orange') {
            if ($this->Ussd->checkMobile2($numero) == $mobile) {

                $nummero = trim($numero);
                if ($mobile == "orange") {
                    $ussd = "#144#*1*1*$numero*$numero*500*2#";
                    $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    $nomTitulaire = $this->Ussd->extractTitulaireOrange($resUssd);
                } elseif($mobile == "telma") {
                    $ussd = "#144#*1*3*$numero*500# 2";
                    $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    $nomTitulaire = $this->Ussd->extractTitulaireMvola($resUssd);
                } elseif($mobile == "airtel") {
                    $ussd = "#144#*1*2*$numero*500# 2";
                    $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    $nomTitulaire = $this->Ussd->extractTitulaireMvola($resUssd);
                }
                
                $body = ['status' => (bool) $nomTitulaire, 'nom_titulaire' => $nomTitulaire, 'response' => $resUssd, 'ussd' => $ussd];
            }
        } elseif($operateurNumPortTitulaire == 'telma') {
            if ($this->Ussd->checkMobile2($numero) == $mobile) {
                $nummero = trim($numero);
                $ussd = "#111*1*2*$numero*500*123*2*123#";
                $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                $nomTitulaire = $this->Ussd->extractTitulaireMobileMoneyFromRequestTelma($resUssd);
                
                $body = ['status' => (bool) $nomTitulaire, 'nom_titulaire' => $nomTitulaire, 'response' => $resUssd, 'ussd' => $ussd];
            }

        } elseif($operateurNumPortTitulaire == 'airtel') {
            if ($this->Ussd->checkMobile2($numero) == $mobile) {
                $nummero = trim($numero);
                if ($mobile == "orange" || $mobile == "telma") {
                    if ($mobile == 'orange') {
                        $ussd = "*436*2# 2*1*$numero*$numero*500*2";
                    } elseif($mobile == 'telma') {
                        $ussd = "*436*2# 3*1*$numero*$numero*500*2";
                    } else {
                        $this->logginto($mobile, 'check_mobile');
                    }
                    $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    // $resUssd = "Veuillez entrer votre mot de passe pour confirmer l'envoi de Ar 500 au 325541393 NARISANDY NIRINTSOA ANDRIAMAMPIANINA."; // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    $nomTitulaireBrut = $this->Ussd->extractBetween($resUssd, 'envoi de Ar ', '');
                    $firstPositionAu = strpos($nomTitulaireBrut, ' au 3')+13;
                    $nomTitulaire = trim(str_replace('.', '', str_replace(substr($nomTitulaireBrut, 0, $firstPositionAu), '', $nomTitulaireBrut)));
                } elseif($mobile == "airtel") {
                    $ussd = "*436*2# 1*1*$numero*$numero*500*2";
                    $resUssd = $this->Ussd->run($ussd); // tel:%23%20144*1*1*0322814109*0322814109*42000*2*2958%20%23
                    // $resUssd = "Veuillez entrer votre mot de passe pour l'envoi de Ar 500 a BAKOLYHARISOA RATSIMBAZAFY (331153918) comme : 2@ :";
                    $nomTitulaireBrut = $this->Ussd->extractBetween($resUssd, 'envoi de Ar ', ' (3');
                    $firstPositionA = strpos($nomTitulaireBrut, 'a')+1;
                    $nomTitulaire = trim(str_replace(substr($nomTitulaireBrut, 0, $firstPositionA), '', $nomTitulaireBrut));
                }

                $body = ['status' => (bool) $nomTitulaire, 'nom_titulaire' => $nomTitulaire, 'response' => $resUssd, 'ussd' => $ussd];
            }

        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function rechargeForfait($mobile)
    {
        $comPort = $this->getParam('port_sms_out_'.$mobile);
        $this->Ussd->setPort($comPort);

        $body = ['status' => (bool) false, 'nom_titulaire' => '', 'response' => ''];

        if ($mobile == "orange") {

            // $ussd = "#144*2*1*2*1*2*1*3#"; 
            $ussd = "#144*2*1*2*1*2*3*2#";

        } elseif($mobile == "telma") {

            $ussd = "#111*1*1*4*3*2#"; // forfait 24h sms 250 Ariary Telma

        } elseif($mobile == "airtel") {

            $ussd = "*436# 7 5 2";

        }

        $ussd.= ' 2958';




        $now = Chronos::now();

        $data['last_recharge_forfait_'.$mobile] = $now;

        $parameter = $this->Parameters->patchEntity($this->getParam(), $data, ['validate' => false]);
        $parameter = $this->Parameters->save($parameter);

        $resUssd = $this->Ussd->run($ussd);
        $body = ['status' => (bool) true, 'port' => $comPort,'ussd' => $ussd, 'response' => $resUssd,  ];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * Recursive
     */
    public function majAndConsultationForfait($mobile, $essai = 1)
    {
        $essais = 3;

        $isSaved = false;
        $comPort = $this->getParam('port_sms_out_'.$mobile);
        $this->Ussd->setPort($comPort);
        $parameter = $this->getParam();

        $body = ['status' => (bool) false, 'nom_titulaire' => '', 'response' => ''];

        if (empty($comPort)) {
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        if ($mobile == "telma") {

            $ussd = "#359#";
            $infoForfait = $resUssd = !empty($comPort) ? $this->Ussd->run($ussd) : 'Port Vide';

            if (strrpos($resUssd, 'Mes offres') !== false) {
                $infoForfait = $this->Ussd->run('#359# 1 1 1');
                $nbSmsRestantUo = (int) $this->Ussd->extractBetween($infoForfait, 'SMS restants: ', ' SMS Telma et');
                $nbSmsRestantTo = (int) $this->Ussd->extractBetween($infoForfait, 'SMS Telma et ', ' SMS autres opérateurs');
                $data = ['nb_sms_uo_telma' => $nbSmsRestantUo, 'nb_sms_to_telma' => $nbSmsRestantTo];
            } else {
                $data = ['nb_sms_uo_telma' => 0, 'nb_sms_to_telma' => 0];
            }

        } elseif($mobile == "orange") {

            $ussd = "#321#";
            $infoForfait = !empty($comPort) ? $this->Ussd->run($ussd) : 'Port Vide';
            $nbSmsRestantUo = (int) $this->Ussd->extractBetween($infoForfait, '', 'SMS orange');
            $nbSmsRestantTo = (int) $this->Ussd->extractBetween($infoForfait, 'orange, ', 'SMS toutop');
            $data = ['nb_sms_uo_orange' => $nbSmsRestantUo, 'nb_sms_to_orange' => $nbSmsRestantTo];
            $this->logginto($data, 'data_info_orange');
            $this->logginto($infoForfait, 'ress_info_orange');

        } elseif($mobile == "airtel") {

            $ussd = '*999#';
            $infoForfait = !empty($comPort) ? $this->Ussd->run($ussd) : 'Port Vide';
            $nbSmsRestantUo = (int) $this->Ussd->extractBetween($infoForfait, 'SMS: ', '.Bonus');
            $data = ['nb_sms_uo_airtel' => $nbSmsRestantUo];
            if (strrpos($infoForfait, 'SMS') !== false) {
                $status = true;
            }
        }

        if (
            strrpos($infoForfait, 'ATZ') !== false ||
            strrpos($infoForfait, 'object is not subscriptabl') !== false
        ) {
            $this->logginto($infoForfait, 'info_forfait_'.$mobile);
            $this->Ussd->resetUsb($this->getDriverByMobile($mobile));
            if ($essai <= $essais) {
                $this->majAndConsultationForfait($mobile, ++$essai);
            }
        }


        $now = Chronos::now();
        $data['last_checked_forfait_at_'.$mobile] = $now;
        $parameter = $this->Parameters->patchEntity($parameter, $data, ['validate' => false]);
        $this->Parameters->save($parameter);

        $this->logginto($infoForfait, 'info_forfait_full_'.$mobile);
        
        $body = ['status' => (bool) $isSaved, 'port' => $comPort,'ussd' => $ussd, 'firstInfo' => @$resUssd, 'response' => @$infoForfait, 'data' => $data];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    /**
     * rechargeSmsAuto/mobile dans moneiko.js
     * @param  string $mobile [description]
     * @return [type]         [description]
     */
    public function rechargeSmsAuto($mobile = 'telma')
    {
        $result = ['result' => null, 'msg' => 'Aucun forfait '.ucfirst($mobile).' rechargé'];

        $dateDeRenouvellementArrived = $this->getParam('last_recharge_forfait_'.$mobile)->addDay(1)->isPast();
        $dateDeRenouvellementArrived7Jours = $this->getParam('last_recharge_forfait_'.$mobile)->addDay(7)->isPast();
        $isAutoRechargeActivated = $this->getParam('is_recharge_auto_forfait_'.$mobile);


        if ($mobile == 'telma' && $isAutoRechargeActivated && $dateDeRenouvellementArrived) {

            $result = $this->majAndConsultationForfait($mobile);
            $data = json_decode($result)->data;
            $nbSmsRestant = $data->{'nb_sms_uo_'.$mobile};

            if (is_int($nbSmsRestant) && $nbSmsRestant < 10) {
                $this->rechargeForfait($mobile);
                $result = ['result' => 'success', 'msg' => ucfirst($mobile).' rechargé'];
            }


        } elseif ($mobile == 'orange' && $isAutoRechargeActivated && $dateDeRenouvellementArrived7Jours) {


            $result = $this->majAndConsultationForfait($mobile);
            $data = json_decode($result)->data;
            $nbSmsRestant = $data->{'nb_sms_uo_'.$mobile};

            if (is_int($nbSmsRestant) && $nbSmsRestant < 10) {


                $this->rechargeForfait($mobile);
                $result = ['result' => 'success', 'msg' => ucfirst($mobile).' rechargé'];
            }
            
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function majAndConsultationForfaitAll()
    {
        $ress = [];
        if (date('H') > 05 && date('H') <= 23) {
            foreach ($this->operateurs as $key => $op) {
                $ress[] = json_decode($this->majAndConsultationForfait($op));
            };
            $this->logginto($ress, 'ussd/majAndConsultationForfaitAll');
        }

        $body = $ress;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * à mettre dans un cron journalier, systeme scheduler
     * DÉSACTIVÉ Pour le moment
     * @return [type] [description]
     */
    public function rechargeForfaitAuto()
    {
        $http = new Client();

        $responses = [];
        foreach ($this->operateurs as $key => $mobile) {
            if ($this->getParam('is_recharge_auto_forfait_'.$mobile)) {
                $lastMajAt = $this->getParam('last_recharge_forfait_'.$mobile);
                if ($lastMajAt->isYesterday()) {
                    if ($this->Ussd->checkIfIsOpened($this->getParam('port_sms_out_'.$mobile)) == 'ok') {
                        $responses[] = $res = $this->rechargeForfait($mobile);
                        $this->logginto($res, 'gsm/recharge_forfait_auto', $erase = false);
                    }
                }
            }
        }

        $body = ['results' => $responses];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }
}
?>