<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Chronos\Chronos;
use Cake\Routing\Router;
use Cake\Log\Log;
use Cake\Http\Client;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
use App\Traits\AppTrait;

class TdbController extends AppController
{
    use AppTrait;

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadComponent('Ussd');
        $this->loadModel('Smss');
        $this->loadModel('TransactionsHistorics');
        $this->loadModel('Ozekimessageins');
        $this->loadModel('SentUssds');
        $this->loadModel('AuthSmss');
        $this->loadModel('UssdHistorics');
        $this->loadModel('Ports');


        // callback màj de l'état des transaction no ilan'ity
        $mode = $this->mode = Configure::read('callback_mode');
        $numOranges = $this->numOranges = Configure::read('num_orange');
        $this->operateurs = $operateurs = Configure::read('operateurs');
        $callbackUrls = Configure::read('callback_url');
        $urlToWeb = $this->urlToWeb = $callbackUrls[$mode]['urlToWeb']; //ussdretrait 
        $urlCallback = $this->urlCallback = $callbackUrls[$mode]['urlCallback'];
        $urlRecheckStatus = $this->urlRecheckStatus = $callbackUrls[$mode]['urlCheckVenteStatus'];
        $urlMajRefTransMobileFromUssdLocal = $this->urlMajRefTransMobileFromUssdLocal = $callbackUrls[$mode]['urlMajRefTransMobileFromUssdLocal'];

        $this->client = $http = new Client();

        $this->portOrange = $portOrange = $this->parameter->port_orange;
        $this->portTelma = $portTelma = $this->parameter->port_telma;
        $this->portAirtel = $portAirtel = $this->parameter->port_airtel;

        $driverTelma = $this->driverTelma;
        $driverOrange = $this->driverOrange;

        $mobiles = Configure::read('mobile');   

        $this->set(compact('mobiles', 'operateurs', 'operateurs_fronts', 'numOranges', 'parameter', 'driverTelma','portTelma', 'portTelma', 'driverOrange', 'usbLists'));
    }

    public function index($paiement = "orange")
    {
        $options = $this->request->getQuery();
        $tables = ConnectionManager::get('default')->execute('show tables');

        $numero = $options['numero'] ?? '';
        $montant = $options['montant'] ?? ''; 
        $ref = $options['ref'] ?? '';
        $date = $options['date'] ?? date('Y-m-d');

        $transactionsHistorics = $this->TransactionsHistorics->find();
        if ($paiement) {
            $transactionsHistorics->where(['paiement' => $paiement]);
        }

        if ($numero) {
            $transactionsHistorics->where(['numero' => $numero]);
        }

        if ($montant) {
            $transactionsHistorics->where(['montant' => $montant]);
        }

        if ($ref) {
            $transactionsHistorics->where(['ref' => $ref]);
        } 

        if ($date) {
            $transactionsHistorics->where(['date LIKE' => "%$date%"]);
        }   

        $limitSolde = Configure::read('limit_solde');

        $transactionsHistorics = $this->paginate($transactionsHistorics, ['limit' => 50, 'order' => ['id' => 'DESC']]);

        $lastSmsMvola = $this->Ozekimessageins->find('Recus')->where(['sender in' => $this->senders])->order(['senttime' => 'DESC'])->first();
        $smsMvolaData = $this->Ussd->extractMvolaRecuData($lastSmsMvola->msg);
        $soldeMvola = $smsMvolaData['solde_actuel'];
        $soldeMvolaPercent = round(($soldeMvola/$limitSolde)*100)*0.94; // 0.94 correspond à l'hauteur css pour garder les petits écarts
        $soldeMvolaPercent = $soldeMvolaPercent < 94 ? $soldeMvolaPercent : 94;
        
        $lastSmsOrange = $this->Ozekimessageins->find("Recus")->where(['sender in' => $this->orangeSenders])->order(['senttime' => 'DESC'])->first();
        $smsOrangeData = $this->Ussd->extractOrangeRecuData($lastSmsOrange->msg);
        $soldeOrange = (int) @$smsOrangeData['solde_actuel'];
        $soldeOrangePercent = round(($soldeOrange/$limitSolde)*100)*0.94; // 0.94 correspond à l'hauteur css pour garder les petits écarts
        $soldeOrangePercent = $soldeOrangePercent < 94 ? $soldeOrangePercent : 94;
        
        $lastSmsAirtel = $this->Ozekimessageins->find("Recus")->where(['sender in' => $this->airtelSenders])->order(['senttime' => 'DESC'])->first();
        $smsAirtelData = $this->Ussd->extractAirtelRecuData($lastSmsAirtel->msg);
        // debug($smsAirtelData);
        // die();
        $soldeAirtel = $smsAirtelData['solde_actuel'];
        $soldeAirtelPercent = (float) round(((float)$soldeAirtel/(float)$limitSolde)*100)*0.94; // 0.94 correspond à l'hauteur css pour garder les petits écarts
        $soldeAirtelPercent = $soldeAirtelPercent < 94 ? $soldeAirtelPercent : 94;

        // die();

        // $lastRappelMoiSmsOrange = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Veuillez me rappeler%"])->last();

        $infos_forfait_sms = $this->getParam('infos_forfait_sms');
        $infoSms = '';
        if (strrpos($infos_forfait_sms, 'airtel') !== false) {
            $infoSms = $this->Ussd->extractBetween($infos_forfait_sms, 'SMS: ', '.Bonus').' SMS Airtel';
        }

        // Stats 2FA SMS

        $today = date('Y-m-d');
        $authSmssTelma = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$today.'%', 'numero LIKE' => '034%']);
        $authSmssOrange = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$today.'%', 'numero LIKE' => '032%']);
        $authSmssAirtel = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$today.'%', 'numero LIKE' => '033%']);

        $now = Chronos::now();
        $hier = $now->subDays(1);
        $authSmssTelmaSub1 = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$hier->format('Y-m-d').'%', 'numero LIKE' => '034%']);
        $authSmssOrangeSub1 = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$hier->format('Y-m-d').'%', 'numero LIKE' => '032%']);
        $authSmssAirtelSub1 = $this->AuthSmss->find()->where([/*'user_id !=' => '3383',*/ 'created LIKE' => '%'.$hier->format('Y-m-d').'%', 'numero LIKE' => '033%']);
        $listTunnels = $this->Tunnels->find()->order(['id' => 'ASC']);

        $this->set(compact('listTunnels', 'hier', 'authSmssTelmaSub1', 'authSmssOrangeSub1', 'authSmssAirtelSub1', 'authSmssTelma', 'authSmssOrange', 'authSmssAirtel', 'infoSms', 'lastRappelMoiSmsOrange', 'soldeMvolaPercent', 'soldeOrangePercent', 'soldeAirtelPercent', 'limit_solde', 'lastSmsMvola', 'lastSmsOrange', 'lastSmsAirtel', 'smsMvolaData', 'smsOrangeData', 'smsAirtelData', 'transactionsHistorics', 'options', 'paiement', 'tables'));
    }

    public function sendAllSmsTransactionInBdd($mobile = "032")
    {
        $paiement = $this->Ussd->checkMobile($mobile);
        $nbSmsSaved = $this->Ussd->readAndSaveSmsFromAllMemory($mobile);
        if ($nbSmsSaved) {
            $this->Flash->set("$nbSmsSaved transaction(s) enregistrée(s) avec succès", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        } else {
            $this->Flash->set("Aucune transaction enregistrée", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-danger']]);
        }
        return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function vidageSmsModem($mobile = "032")
    {
        // $paiement = $this->Ussd->checkMobile($mobile);
        $this->Ussd->setPortAndPathByNumeroMobile($mobile);
        $nbSmsDeleted = $this->Ussd->deleteSms($mobile);
        $body = ['status' => true];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
        // $this->Flash->set("Sms effacés $mobile", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        // return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function viderTransactions($paiement)
    {
        $this->TransactionsHistorics->deleteAll(['paiement' => $paiement]);
        $this->Flash->set("transaction(s) enregistrée(s) vidées avec succès", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function majRefMobileVersSite($paiement, $calledFromClass = false)
    {
        $transactionsHistorics = $this->TransactionsHistorics->find()->where(['is_traitee' => 0, 'paiement' => $paiement, 'date LIKE' => '%'.date('Y-m-d').'%']);
        $count = 0;

        $url = $this->urlMajRefTransMobileFromUssdLocal; // ussdMajTransactionRefMobile
        // $url = "http://127.0.0.1/moneyko/ussd-maj-transaction-ref-mobile";
        // debug($url);
        // die();
        foreach ($transactionsHistorics as $key => $transactionsHistoric) {

            $data = $transactionsHistoric->toArray();
            $data['date'] = $transactionsHistoric->date->format('Y-m-d H:i:s');
            // debug($data);
            // die();
            $res = $this->client->post($url, $data, ['timeout' => 120]);
            // $res = $res->body;
            // echo ($res);
            // die();
            // // debug($data);
            // // echo ($res);
            $res = $res->json;
            if (isset($res['id'])) {
                $count++;
                $transactionsHistoric = $this->TransactionsHistorics->patchEntity($transactionsHistoric, ['type' => $res['type'], 'is_traitee' => 1], ['validate' => false]);
                $this->TransactionsHistorics->save($transactionsHistoric);
            }
        }

        $this->reCheckUssdRetraitStatuts($paiement);

        if (!$this->request->is(['ajax']) && $calledFromClass == false) {
            if ($count) {
                $this->Flash->set("$count transaction(s) mise à jour avec succès", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
            } else {
                $this->Flash->set("Aucun ref transaction mis à jour", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-warning']]);
            }
            
            return $this->redirect(['action' => 'index', $paiement]); 
        } else {
            $body = ['count' => $count];
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        }

        die();
    }

    // envoyé vers recheckVenteStatus() moneyko site
    function reCheckUssdRetraitStatuts($paiement)
    {
        $sentUssds = $this->SentUssds->find()->where(['checked' => 0, 'date' => date('Y-m-d'), 'type' => $paiement])->order(['id' => 'DESC']);
        foreach ($sentUssds as $key => $sentUssd) {

                $model = $sentUssd->model;
                $ussdResponse = $sentUssd->ussdResponse;
                $venteID = $sentUssd->vente_id;
                $url = $this->urlRecheckStatus.'/'.$sentUssd->model;

                $data = [
                    'ussd_response' => $sentUssd->res_ussd,
                    'vente_id' => $sentUssd->vente_id,
                    'orange_txnid' => $sentUssd->ref,
                    'mobile_paiement_status' => "payé"
                ];

                $http = new Client();

                if ($sentUssd->vente_id) {
                    
                    $res = $http->post($url, $data, ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false]);
                    $res = $res->json;
                    if (isset($res['id'])) {
                        $this->SentUssds->updateAll(['checked' => 1], ['id' => $sentUssd->id]);
                        $this->logginto($res, 'recheck', $erase = false);
                    }

                }

        }

        return;
    }

    /**
     * 034 telma ou 032 orange
     * @return [type] json
     */
    public function execUssd($type = '')
    {
        $setType = $this->Ussd->checkMobile2($type);
        $body = ['status' => false, 'msg' => ''];

        if ($this->request->is(['post'])) {

            $this->loadModel('UssdHistorics');
            $data = $this->request->getData();

            $command = $data['command'];
            $ussdParts = $this->formatUssd($command);

            $port = $this->Ussd->setPortAndPathByNumeroMobile((string) $type);
            $numero = $this->Ports->findByCom($port)->first()->numero;
            $res = $this->Ussd->run($ussdParts['ussdPrimary'], $ussdParts['parts']);

            // debug($ussdParts['parts']);
            // debug($res);
            // die();

            // if (strrpos($command, '2958') !== false) {
                $ussdData = [
                    'type' => $setType,
                    'command' => $command,
                    'numero' => $numero,
                    'response' => $res ?? nl2br($res),
                ];


                $ussdHistoric = $this->UssdHistorics->newEntity($ussdData, ['validate' => false]);
                if(!$ussdHistoric->getErrors()) {
                    $this->UssdHistorics->save($ussdHistoric);
                }
            // }

            if (strrpos($res, 'ATZ') !== false) {
                $this->Ussd->resetUsb($this->getDriverByMobile($setType));
            }
            
            $body = ['status' => true, 'msg' => nl2br($res)];
        }

       return $this->response->withType('application/json')->withStringBody(json_encode($body));   
    }

    /**
     * 034 telma ou 032 orange
     * @return [type] json
     */
    public function execUssdByPort()
    {
        $body = ['status' => false, 'msg' => ''];

        if ($this->request->is(['post'])) {

            $this->loadModel('UssdHistorics');
            $data = $this->request->getData();

            $command = $data['command'];
            $port = $data['port'];
            $ussdParts = $this->formatUssd($command);

            $this->Ussd->setPort($port);
            $res = $this->Ussd->run($ussdParts['ussdPrimary'], $ussdParts['parts']);

            
            $body = ['status' => true, 'msg' => nl2br($res)];
        }

       return $this->response->withType('application/json')->withStringBody(json_encode($body));   
    }

    public function listUssdHistorics()
    {
        $this->loadModel('UssdHistorics');
        $ussdHistorics = $this->UssdHistorics->find();
        $ussdHistorics = $this->paginate($ussdHistorics, ['limit' => 50, 'order' => ['id' => 'DESC']]);
        $this->set(compact('ussdHistorics'));
    }

    public function formatUssd($command)
    {
        $parts = explode(' ', $command);
        $ussdPrimary = trim($parts[0]);
        unset($parts[0]);

        return compact('ussdPrimary', 'parts');
    }

    public function deleteBackup($table, $paiement)
    {
        $tables = ConnectionManager::get('default')->execute('DROP TABLE '.$table.';');
        $this->Flash->set("Suppression effectuée", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function restoreBackup($table, $paiement)
    {
        $tables = ConnectionManager::get('default')->execute('DROP TABLE IF EXISTS transactions_historics');
        $sql = "CREATE TABLE transactions_historics LIKE $table;
        INSERT INTO transactions_historics SELECT * FROM $table;";
        ConnectionManager::get('default')->execute($sql);
        $this->Flash->set("Restauration $table effectuée", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function createBackup($paiement)
    {
        $nbTables = ConnectionManager::get('default')->execute('show tables');
        $niemeNouveauTable = count($nbTables);
        $nouvellTable = "backup_$niemeNouveauTable";

        $sql = "CREATE TABLE $nouvellTable LIKE transactions_historics;
        INSERT INTO $nouvellTable SELECT * FROM transactions_historics;";
        $nbTables = ConnectionManager::get('default')->execute($sql);
        $this->Flash->set("Nouvelle sauvegarde créée", ['element' => 'alert', 'key' => 'default', 'params' => ['class' => 'alert alert-success']]);
        return $this->redirect(['action' => 'index', $paiement]); 
    }

    public function debugSmsModem($mobile = "032")
    {
        $allSms = $this->formatSmsAsArray($mobile);
        $this->set(compact('allSms'));
    }

    public function formatSmsAsArray($mobile, $essai = 0)
    {
        $essais = 1;
        $port = $this->Ussd->setPortAndPathByNumeroMobile((string) $mobile);
        $this->Ussd->waitForOpen($port);
        $smsMe = $this->Ussd->readSms('ME', $mobile);
        // debug($smsMe);
        // die();

        // Récursive
        // if (strrpos($smsMe, 'ATZ') !== false) {
        //     if ($essai < $essais) {
        //         // $resp = $this->resetGsmMobile($mobile);
        //         $this->Ussd->resetGsm();
        //         if (in_array($mobile, ['032'])) {
        //             // $this->logginto($mobile, 'gsm/debug_atz', $erase = false);
        //             return $this->formatSmsAsArray($mobile, ++$essai);
        //         }
        //     }
        // }

        $smsFromMe = explode("\n", $smsMe);
        
        $smsSm = $this->Ussd->readSms('SM', $mobile);
        // debug($smsSm);
        // die();
        $smsFromSm = explode("\n", $smsSm);


        $allSms = array_filter(array_merge($smsFromSm ?? [], $smsFromMe ?? []));
        // debug($smsFromSm);
        // debug($smsFromMe);
        // die();
            
        $smss = [];
        foreach ($allSms as $key => $sms) {
            if (strrpos($sms, 'gsmmodem-sms.py') !== false) {
                $this->vidageSmsModem($mobile);
                break;
            };
            if (strrpos($sms, ' / ') !== false) {
                $part = explode(' / ', $sms);
                $date = new Chronos($part[1]);
                $smss[] = [
                    'from' => $part[0] ?? null,
                    'date' => $date->format('d/m/Y H:i:s'),
                    'date_formated' => $date,
                    'content' => mb_convert_encoding(($part[2]) ?? '', 'UTF-8'),
                ];
            }
        }

            // die();

        $smss = collection($smss)->sortBy('date', SORT_ASC, SORT_NATURAL)->toArray();
        return $smss;
    }

    public function saveAllSms($mobile)
    {
        $allSms = $this->formatSmsAsArray($mobile); 
        // debug($allSms);
        // die();        
        $isSaved = false;
        if ($this->request->getEnv('REMOTE_ADDR') == '127.0.0.1') {
            foreach ($allSms as $key => $sms) {
                if (!$this->Smss->findByContent($sms['content'])->select(['id'])->first()) {

                    $mobileType = 'orange';
                    if (in_array($mobile, ['032', 'OrangeMoney'])) {
                        $mobileType = "orange";
                    } elseif (in_array($mobile, ['034', 'Mvola', 'Telma'])) {
                        $mobileType = "mvola";
                    } elseif (in_array($mobile, ['033', 'AirtelMoney', 'Airtel'])) {
                        $mobileType = "airtel";
                    }

                    $data = [
                        'date' => $sms['date_formated'],
                        'content' => $sms['content'],
                        'numero' => $sms['from'],
                        'mobile' => $mobileType,
                    ];
                    // debug($data);
                    $isSaved = (bool) $this->Smss->save($this->Smss->newEntity($data));
                }
            }
        }

        // die();

        if ($isSaved) {
            $this->vidageSmsModem($mobile);
        }

        $body = ['status' => true, 'is_saved' => $isSaved];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    function listhistoriquesSms($mobile)
    {
        $now = Chronos::now();
        $subDays = $now->subDays(1);
        $smss = $this->Smss->findByMobile($mobile)->where(['date >=' => $subDays]);
        $this->set(compact('smss', 'mobile'));        
    }

    public function resetTelma()
    {
        $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("034");
        $rand = rand(0,1);
        if ($rand == 0) {
            $resUssd = $this->Ussd->run("*120#");
        } else {
            $resUssd = $this->Ussd->run("#359#");
        }
        $body = ['status' => true, 'msg' => $resUssd];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function test3()
    {
        // $pincode_email_auth = rand(100000, 999999);
        // debug($pincode_email_auth);
        die();
        // $r = $this->Ussd->getTitulaireFromNumero('0341035571', 'com48', 'telma');
        // debug($r);
        die();
    }

    public function generateCompPortManConfig()
    {
        $r = $this->Ussd->generateReportUsbTreeView();
        debug($r);
        die();
    }

    public function generateComPortManConfig()
    {
        $f = file_get_contents(COMPORTMAN."ComPortInfoBusTypes_report.txt");
        $ports = explode('----------------------- COM Port', $f);
        $config = '';
        $numSection = 10;
        $ini = COMPORTMAN."ComPortMan.ini";
        foreach ($ports as $key => $port) {
            $filter = $this->Ussd->extractBetween($port, '-----------------------', '---------------- Port');
            if (strrpos($filter, 'UI Interface') !== false) {
                $configs = explode("\n", $filter);
                $deviceIdConfig = $this->Ussd->extractBetween($filter, 'DeviceID           : ', 'Parent DeviceID');
                $portConfig = $this->Ussd->extractBetween($filter, 'PortName           : ', 'KernelName');
                $config .= "[ComPorts$numSection]\nDeviceID=$deviceIdConfig\nPortName=$portConfig\n\r";
                ++$numSection;
            }
        }

        $enteteConfig = file_get_contents(CONFIG.'entete_cpmconfig.txt');
        $finalConfig = $enteteConfig."\n".$config;
        
        if (file_exists($ini)) {
            file_put_contents($ini, $finalConfig);

            $generateFile = file_get_contents($ini);

            if (strrpos($generateFile, 'ComPorts') !== false) {
                echo $ini ." généré avec succès"."<br><br>";
                $exec = $this->Ussd->execCli(COMPORTMAN.'_stop.cmd');
                echo($exec).'<br><br>';
                $exec = $this->Ussd->execCli(COMPORTMAN.'_start.cmd');
                echo($exec).'<br><br>';
            }
        } else {
            echo $ini . "n'existe pas";
        }
        // $yourArray = file("C:\Program Files\ComPortMan\ComPortInfoBusTypes_report.txt", FILE_IGNORE_NEW_LINES);
        // debug($yourArray);

        die();
    }

    public function enableTendaEthernet($ethernet = "tenda_cable")
    {
        $string = $this->Ussd->execCli2('netsh interface show interface "'.$ethernet.'"');
        $string = mb_convert_encoding($string, 'UTF-8', 'UTF-8');
        $etat = ($this->cleanChar(explode(':', $string)[2]));

        if ($etat == "Dsactivtatdeconnexion") { // Activtatdeconnexion
            $string = $this->Ussd->execCli2('netsh interface set interface "'.$ethernet.'" enable');
        }

        die();
    }

    public function checkphp()
    {
        phpinfo();
        die();
    }

    public function getEstAchatJ()
    {
        $now = Chronos::now();
        $difference = $now->diffInMinutes(Chronos::parse('7:00'));
        $totalHeureJ = 13;
        $diffHeureDecimal = $difference/60;
        // debug($diffHeureDecimal);
        $coefMult = $totalHeureJ/$diffHeureDecimal;
        // debug($coefMult);
        $nb = $this->Ozekimessageins->find('Recus')->where(['DATE_FORMAT(senttime, "%Y-%m-%d") =' => date('y-m-d')])->count();

        if (date('H') < 20) {
            $estSmsJ = round($coefMult*$nb);
            // debug($estSmsJ);

            $lastEst = $this->request->getSession()->read('last_est_j');

            if ($lastEst == $estSmsJ) {
                $variation = 'fa-minus';
            } elseif ($lastEst < $estSmsJ) {
                $variation = 'fa-angle-up text-success';
            } else {
                $variation = 'fa-angle-down text-danger';
            }

            
            $body = ['nb' => $estSmsJ, 'last_est' => $lastEst, 'est_sms_j' => $estSmsJ, 'variation' => $variation, 'current_nb' => $nb];

            if ($lastEst != $estSmsJ) {
                $this->request->getSession()->write('last_est_j.variation', $variation);
            }
        } else {
            $body = ['nb' => $nb, 'variation' => 'fa-minus', 'current_nb' => $nb];
        }


        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function testSendPushApi()
    {
        $this->sendSimpleNotificationPush('Vous avez un nouveau message !', ' Ceci est un test venant du serveur local distant');
        $body = ['result' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function test()
    {
        
        die();
        $content = "You have received funds Vous avez reçu 10.55 USD sur votre compte CR5704273 à Deriv. The transaction was completed by RAZAFITSOAVINA Tolojanahary Fanjaniaina Narindra, CR6302343.                The products offered on our website are complex derivative products that carry a significant risk of potential loss. CFDs are complex instruments with a high risk of losing money rapidly due to leverage. You should consider whether you understand how these products work and whether you can afford to take the high risk of losing your money. For company information on the senders of this email, please click here. Centre d'Aide    |    Conditions générales    |    Confidentialité et sécurité";
        $test = $this->Ussd->extractBetween($content, 'The transaction was completed by ', 'offered on our');
        $test = $this->Ussd->extractBetween($content, ', ', '.                The products');
        debug($test);
        die;
        $python = $this->python;
        $cmd = $python."chrome.py".$this->port." 2>&1";


        die();

        $userAgent = $this->request->getHeaderLine('User-Agent');
        if (strrpos($userAgent, 'GPTBot') !== false) {
            die('access denied');
        }
        die('ato');
        
        // $last = $this->Ports->find()->last();
        // Log::write('info', 'SELECT * FROM test', ['scope' => ['queries']]);
        die();
        // $loggers = Log::configured();
        // debug($loggers);
        // die();
        debug('ato');
        debug(Log::getConfig('sqlLogger'));
        die();
        $last = $this->Ports->find()->last();
        debug($last);
        die();
        $lastEst = 100;
        $estSmsJ = 99;

        $lastEst = $this->request->getSession()->read('last_est_j_test');


        if ($lastEst == $estSmsJ) {
            $variation = 'fa-minus';
        } elseif ($lastEst < $estSmsJ) {
            $variation = 'fa-angle-up text-success';
        } else {
            $variation = 'fa-angle-down text-danger';
        }
        
        $body = ['nb' => $estSmsJ, 'variation' => $variation];

        if ($lastEst != $estSmsJ) {
            $this->request->getSession()->write('last_est_j_test.variation', $variation);
        }

        debug($body);

        die();

        $now = Chronos::now();
        $difference = $now->diffInMinutes(Chronos::parse('8:00'));
        $totalHeureJ = 12;
        $diffHeureDecimal = $difference/60;
        debug($diffHeureDecimal);
        $coefMult = $totalHeureJ/$diffHeureDecimal;
        debug($coefMult);
        $nb = $this->Ozekimessageins->find('Recus')->where(['DATE_FORMAT(senttime, "%Y-%m-%d") =' => date('y-m-d')])->count();
        $estSmsJ = round($coefMult*$nb);
        debug($estSmsJ);
        die();
        // new \Cake\Cache\Engine\MemcacheEngine;
        // die;

        // $memcache = new \Memcache();
        // $connection = $memcache->connect('127.0.0.1', 11211);
        // die();
        $memcache = memcache_connect('localhost', 11211);

        if ($memcache) {
            $memcache->set("str_key", "
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                ");
            $memcache->set("num_key", 123);

            $object = new \StdClass;
            $object->attribute = 'test';
            $memcache->set("obj_key", $object);

            $array = Array('assoc'=>123, 345, 567);
            $memcache->set("arr_key", $array);

            var_dump($memcache->get('str_key'));
            var_dump($memcache->get('num_key'));
            var_dump($memcache->get('obj_key'));
        }
        else {
            echo "Connection to memcached failed";
        }


        $stats = $memcache->getStats();

        echo "<pre>";
        print_r($stats);
        echo "</pre>";

        die();
        $domain = $this->request->getEnv('HTTP_X_FORWARDED_HOST');
        if (strrpos($domain, 'ngrok') !== false) {
            $tunnel = $this->Tunnels->findByIsActive(1)->first();
            $hit = $tunnel->hit+1;

            $tunnel = $this->Tunnels->patchEntity($tunnel, ['hit' => $hit]);
            $tunnel = $this->Tunnels->save($tunnel);
            debug($tunnel);
        }
        die();
        $this->sendSMSMerci('0325541393', 'depot');
        die;
        // $string = $this->Ussd->execCli2('tasklist | find "ngrok"');
        $string = $this->Ussd->execCli2("D:\\test.bat");
        echo $string;
        die();
        $string = $this->Ussd->execCli2('netsh interface show interface "tenda_cable"');
        $string = mb_convert_encoding($string, 'UTF-8', 'UTF-8');
        $etat = ($this->cleanChar(explode(':', $string)[2]));

        if ($etat != "Activtatdeconnexion") {
            $string = $this->Ussd->execCli2('netsh interface set interface "tenda_cable" enable');
        }

        die();
        $timestamp_debut = microtime(true);
        
            // $r = $this->Ussd->execCli(USBSAFELYREMOVE.'usr.exe wholocks -d E:');

            $r = exec(USBSAFELYREMOVE.'usr.exe wholocks -d E:');
            
        $timestamp_fin = microtime(true);
        
        $difference_ms = round(($timestamp_fin - $timestamp_debut), 2); debug($difference_ms);

        debug($r);

        die();
        $var = "0331100908";
        debug($var[0]);
        $var = ltrim($var, '0');
        debug($var);
        die();

        $timestamp_debut = microtime(true);
        
            $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("032");
            // $r1 = $this->Ussd->waitForOpen($usedPort); /// pour evider le bug ressource utilisée par un autre processeur
            // $resUssd = $this->Ussd->run("#144#", ['2', '1', '2', '1', '4', '3'], ['test']);
            // $resUssd = $this->Ussd->execCliTest($this->Ussd->python."wake.py com26 2>&1");
            $resUssd = $this->Ussd->resetGsm();
            debug($resUssd);
            
        $timestamp_fin = microtime(true);
        
        $difference_ms = round(($timestamp_fin - $timestamp_debut), 2);
        debug($difference_ms);
        die();


        // $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("034");
        // $resUssd = $this->Ussd->run("#111*1*3*2*0347085930*100*blabla#", ['2958']);
        // debug($resUssd);
        // die();

        // $path = ROOT.'/bin/reset_usb.bat';
        // // $r = $this->Ussd->execCli($path);
        // // debug($r);
        exec('"C:\\Program Files (x86)\\USB Safely Remove\\" forcedstop -d M:');
        exec('"C:\\Program Files (x86)\\USB Safely Remove\\" forcedreturn -d M:');
        die();
    }

    public function test2()
    {
        $usedPort = $this->Ussd->setPortAndPathByNumeroMobile("034");
        $r1 = $this->Ussd->waitForOpen($usedPort); /// pour evider le bug ressource utilisée par un autre processeur
        $resUssd = $this->Ussd->run("#111#", ["1"]);
        debug($resUssd);
        die();
    }

    public function resetUsb($diverLetter)
    {
        $this->Ussd->resetUsb($diverLetter);
        $body = ['status' => 'success', 'msg' => $diverLetter.':'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function resetGsmMobile($mobile)
    {
        $type = $this->Ussd->checkMobile2($mobile);
        $resp = $this->Ussd->resetUsb($this->getDriverByMobile($type));

        $result = [
            'status' => strrpos($resp, 'successfully') !== false,
            'msg' => $resp
        ];
        return $result;
    }

    public function presetUssd($mobile, $essai = 0, $code = null)
    {
        $essais = 1;
        $code = $this->request->getQuery('code');
        $setType = $this->Ussd->checkMobile2($mobile);

        $port = $this->Ussd->setPortAndPathByNumeroMobile((string) $mobile);
        $this->Ussd->waitForOpen($port);
        $numero = $this->Ports->findByCom($port)->first()->numero;

        $ussdParts = $this->formatUssd($code);
        $res = $this->Ussd->run($ussdParts['ussdPrimary'], $ussdParts['parts']);

        

        if (strrpos($res, 'ATZ') !== false) {
            if ($essai <= $essais) {
                $this->Ussd->resetGsm();
                if (in_array($mobile, ['032'])) {
                    return $this->presetUssd($mobile, ++$essai);
                }
            }
        }

        if (strrpos($res, '# example response: +CFUN: 1') !== false && $mobile == "033") {
            if (strrpos($res, 'ATZ') !== false) {
                $this->Ussd->resetUsb($this->getDriverByMobile("airtel"));
            }

            if ($essai < $essais ) {
                $this->presetUssd($mobile, ++$essai);
            }
        }

        $body = ['msg' => nl2br($res)];

        if (strrpos($code, '#*6*1') !== false) {
            $currentSolde = $this->parameter->last_solde_mvola;
            $solde = $this->Ussd->extractBetween(nl2br($res), 'Votre solde MVola est de ', ' Ar. Ref:');
            $solde = (int) str_replace(' ', '', $solde);
            $diffSolde = round($solde - $currentSolde);
            $class = $diffSolde > 0 ? 'text-danger' : 'text-success';
            $body = ['msg' => 'Solde Mvola : '.$solde.' Ar, diff : <span class="'.$class.'">'.$diffSolde.' Ar</span> <br>'];
        }

        $ussdData = [
            'type' => $this->Ussd->checkMobile2($mobile),
            // 'command' => str_replace(2958, '', $code),
            'command' => $code,
            'numero' => $numero,
            'response' => $res?? nl2br($res)
        ];

        $ussdHistoric = $this->UssdHistorics->newEntity($ussdData, ['validate' => false]);
        if(!$ussdHistoric->getErrors()) {
            $this->UssdHistorics->save($ussdHistoric);
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function histoCallback($file)
    {
        echo str_replace(["\r", "\n"], ['', '<hr class="my-2">'], file_get_contents(LOGS.DS.$file));
        die();
    }
    
    public function histoCallback2($dir, $file)
    {
        $this->renameHistoCallback($dir, $file);
        echo str_replace(["\r", "\n"], ['', '<hr class="my-2">'], file_get_contents(LOGS.DS.$dir.DS.$file));
        die();
    }

    public function renameHistoCallback($dir, $file)
    {
        if (file_exists(LOGS.DS.$dir.DS.$file)) {
            $path = LOGS.DS.$dir.DS.$file;
            $fileInstance = new File($path);

            if ($fileInstance->size() > 400000) { // 400 ko eo eo
                rename(LOGS.DS.$dir.DS.$file, LOGS.DS.$dir.DS.$file.'.'.date('Y-m-d-H-i-s'));
                $file = new File(LOGS.DS.$dir.DS.$file, true, 0777);
            }

        }
        $body = ['status' => 'ok'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function editParameter()
    {
        $body = ['result' => ''];
        $parameter = $this->Parameters->find()->first();
        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->getData();
            $parameter = $this->Parameters->patchEntity($parameter, $data);
            if (!$parameter->getErrors()) {
                $this->Parameters->save($parameter);
                $body = ['result' => 'success'];
            } else{
                $body = ['result' => $parameter->getErrors()];
            }
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function portList()
    {
        $this->Ussd->checkPortList();
        $ports = json_decode($this->getPortListDatas());
        $comPorts = [];
        
        if ($this->Ports->find()->count() > 0) {
            $comPorts = $this->Ports->find()->indexBy('com')->toArray();
            $arrayPorts = $this->Ports->find('list', [
                'keyField' => 'numero',
                'valueField' => 'com',
            ])->toArray();
        }

        $nbPorts = (int) count((array) $ports);
        $param = $this->getParam();
        $missingsPorts = [];
        if (count((array) $ports)) {
            $missingsPorts = $this->Ports->find()->where(['com not in' => $ports]);
        }

        $this->set(compact('checkServiceNg', 'ports', 'nbPorts', 'comPorts', 'arrayPorts', 'param', 'missingsPorts'));
    }

    public function resetUsb2($gsmLetter)
    {
        $ex = $this->Ussd->usb('stop', $gsmLetter);
        $ex .= ' '. $this->Ussd->usb('return', $gsmLetter);

        $body = ['result' => true, 'msg' => $ex];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    public function forceResetAllComPort()
    {
        $gsmList = $this->Ussd->getUsbGsmList();
        $nbGsmList = count($gsmList);
        $traitees = 0;

        $debugWhoLocks = "\n";
        $ex = '';
        $missingsPortLogs = [];
        foreach ($gsmList as $key => $gsmLetter) {

            $this->Ussd->stopServiceNg();

            // $checkGsm = exec(USBSAFELYREMOVE.'usr.exe wholocks -d '.$gsmLetter);
            // $checkGsm = $this->Ussd->usb('check', $gsmLetter);

            // $debugWhoLocks .= $gsmLetter. ', '.$checkGsm."\n";

            // if ((strrpos($checkGsm, 'Perhaps the device is ready for ejecting now') !== false)) {
                $ex = $this->Ussd->usb('stop', $gsmLetter);
                // sleep(1);
                $ex .= ' '. $this->Ussd->usb('return', $gsmLetter);

                $logsUsr[] = $ex;

                if ($key+1 < $nbGsmList) {
                    // sleep(1.5);
                }
            // }

            $this->logginto($logsUsr, 'gsm/force_reset_all_com_port', $erase = true);


        }

        pr($logsUsr);
        die();
    }

    public function getPortList()
    {

        $ports = explode("<br />", nl2br($this->Ussd->checkPortList()));
        $listPorts = [];
        foreach ($ports as $port) {
            if (strrpos($port, 'UI Interface') !== false) {
                $class = (strrpos($port, strtoupper($this->portOrange)) !== false) || (strrpos($port, strtoupper($this->portTelma)) !== false) || (strrpos($port, strtoupper($this->portAirtel)) !== false) ? 'text-white' : 'text-secondary';
                $listPorts[] = $port;
            }
        }


        $body = $listPorts;
        return $this->response->withType('application/json')->withStringBody(json_encode($body, JSON_PRETTY_PRINT));
    }

    public function getPortListDatas()
    {
        $ports = $this->Ussd->checkPortListShort();
        // debug($ports);
        // die;
        $ports = trim(rtrim(str_replace("\n", '', $ports), ';'));
        $dataPorts = explode(';', $ports);
        $dataPorts = array_map('trim', $dataPorts);
        $dataPorts = array_filter($dataPorts);
        $dataPorts = array_combine($dataPorts, $dataPorts);
        $body = $dataPorts;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function getPortNumListDatas()
    {
        $body = $this->Ports->find()->select(['com_numero' => "CONCAT(com, ' - ', numero)"])->enableAutoFields(true)->find('list', [
            'keyField' => 'com',
            'valueField' => 'com_numero'
        ]);
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function cleanPortsTable()
    {
        $cm = new ConnectionManager;
        $cm::get('default')->execute('TRUNCATE TABLE `ports`');
        $body = ['status' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function stopServiceNg()
    {
        $res = $this->Ussd->stopServiceNg();
        $this->logginto($res, 'gsm/stopedserviceng', $erase = true);

        $body = ['status' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function startServiceNg()
    {
        $this->Ussd->startServiceNg();
        $body = ['status' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    //http://127.0.0.1/moneiko-ussd/tdb/getNumeroFromAt/com39
    public function getOpNetwork($port)
    {
        $this->Ussd->setPort($port);
        $opFromPort = $this->Ussd->getNetworkName($port);
        debug($opFromPort);
        die();
        
    }
    
    public function getNumeroFromAt($port)
    {
        $this->Ussd->setPort($port);
        $opFromPort = $this->Ussd->getNumeroFromAt($port);
        debug($opFromPort);
        die();
        
    }

    /**
     * rempli les numéros aux ports selon ussd
     * @param  [type] $comPort [description]
     * @return [type]          [description]
     */
    public function remapPortToNum($comPort)
    {

        $this->Ussd->setPort($comPort);

        $numero = null;
        $port = null;
        $res = null;
        $resUssd = null;
        
        if (json_decode($this->checkPort($comPort))->result == 'ok') {

            $opFromPort = $this->Ussd->getNetworkName($comPort);
            // debug($comPort);
            // debug($opFromPort);
            // die();

            if ($opFromPort == 'telma') {
                $resUssd = $this->Ussd->run('*120#');
                $res = $this->Ussd->cleanString($resUssd);
                $result = $this->Ussd->extractBetween($res, 'Votre numero est', 'Session fermée par le');

                // $result = "261321702119";
                $numero = $result;
            } elseif ($opFromPort == 'orange') {
                $resUssd = $this->Ussd->run('#888#');
                $res = $this->Ussd->cleanString($resUssd);
                $result = $this->Ussd->extractBetween($res, 'MSISDN:', 'Session fermée par le');

                // $result = "261321702119";
                $numero = "0".substr($result, 3, 10);
            } elseif ($opFromPort == 'airtel') {

                $result = $this->Ussd->getNumeroFromAt($comPort);
                // debug($result);
                // die();
                $resUssd = $result;
                // $resUssd = $this->Ussd->run('*123#');
                $res = $this->Ussd->cleanString($resUssd);
                // $result = $this->Ussd->extractBetween($res, '', '. Fahana tavela');

                // $result = "261321702119";
                $numero = $result;
            }

            $this->logginto($comPort.' '.$resUssd, 'gsm/remap_port_to_num', $erase = false);

            if (isset($numero)) {

                $data = [
                    'op_from_port' => $opFromPort,
                    'res_ussd' => $resUssd,
                    'com' => $comPort,
                    'numero' => $numero,
                    'mobile' => $this->Ussd->checkMobile2($numero),
                    'modified' => Chronos::now()
                ];


                $port = $this->Ports->findByCom($comPort)->first();

                if ($port) {
                    $port = $this->Ports->patchEntity($port, $data, ['validate' => false]);
                } else {
                    $port = $this->Ports->newEntity($data);
                }

                $this->Ports->save($port);
            }
            $body = ['result' => (bool) $port, 'numero' => $numero, 'res' => $res, 'msg' => '<br>'.$res];

        } else {
            $body = ['result' => (bool) false, 'numero' => '', 'res' => '', 'msg' => 'Port occupé'];
        }


        return $this->response->withType('application/json')->withStringBody(json_encode($body));

        die;
    }

    /**
     * Affecte les ports dans paramétrage selon les types (envoi, sms, reception) et les numéros
     * Modifie les fichiers config du ozeki ngs
     * @return [type] [description]
     */
    public function remapRoutage()
    {

        $ports = $this->Ports->find()->first();
        $body = ['status' => null];

        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->getData();

            foreach ($this->operateurs as $op) {
                if (!empty($data['num_port_sms_'.$op])) { // SMS
                    $portByNumero = $this->Ports->findByNumero($data['num_port_sms_'.$op])->first();
                    if ($portByNumero) {
                        $data['port_sms_out_'.$op] = $portByNumero->com;
                        $this->Ports->updateAll(['type' => 'sms'], ['id' => $portByNumero->id]);
                    }
                }


                if (!empty($data['num_port_envoi_'.$op])) { // ENVOI
                    $portByNumero = $this->Ports->findByNumero($data['num_port_envoi_'.$op])->first();
                    if ($portByNumero) {
                        $data['port_'.$op] = $portByNumero->com;
                        $this->Ports->updateAll(['type' => 'envoi'], ['id' => $portByNumero->id]);
                    }
                }

                if (!empty($data['num_port_reception_'.$op])) { //RECEPTION
                    $portByNumero = $this->Ports->findByNumero($data['num_port_reception_'.$op])->first();
                    if ($portByNumero) {
                        $data['port_reception_'.$op] = $portByNumero->com;
                        $this->Ports->updateAll(['type' => 'reception'], ['id' => $portByNumero->id]);
                        $this->majConfigOzekiNg($portByNumero->com, $portByNumero->numero);
                    }
                }

                if (!empty($data['num_port_titulaire'])) {
                    $portByNumero = $this->Ports->findByNumero($data['num_port_titulaire'])->first();
                    if ($portByNumero) {
                        $data['port_titulaire'] = $portByNumero->com;
                    }
                }
            }

            $dataNoTitulaires = array_diff_key($data, [
                "port_titulaire" => '', 
                "num_port_titulaire" => '',
                'is_recharge_auto_forfait_telma' => '',
                'is_recharge_auto_forfait_orange' => '',
                'is_recharge_auto_forfait_airtel' => '',
                'is_webhook_sms_paiement' => '',
            ]);
            // debug($this->countArrayValue($dataNoTitulaires));
            // pr($dataNoTitulaires);
            // die();
            // if ($this->countArrayValue($dataNoTitulaires)) {
            //     $body = ['status' => 'samevaluesoccur'];
            // } else{
                // pr($data);
                // die();
                $params = $this->Parameters->patchEntity($this->params, $data, ['validate' => false]);
                $params = $this->Parameters->save($params);
                $body = ['status' => 'success'];
            // }
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    
    public function checkPort($comPort)
    {
        $this->Ussd->setPortAndPathByNumeroMobile(032);
        $res = $this->Ussd->checkIfIsOpened($comPort);
        $this->Ports->updateAll(['statut' => $res], ['com' => $comPort]);
        $body = ['result' => $res];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    // public function checkPort($comPort)
    // {
    //     $port = $this->Ports->findByCom($comPort)->first();
    //     $this->Ussd->setPortAndPathByNumeroMobile(032);
    //     $res = $this->Ussd->checkIfIsOpened($comPort);

    //     $port = $this->Ports->patchEntity($port, ['statut' => $res], ['validate' => false]);
    //     $this->Ports->save($port);
    //     $body = ['result' => $res];
    //     return $this->response->withType('application/json')->withStringBody(json_encode($body));
    // }


    public function checkRemapRoutage($op = "telma")
    {
        $port = $this->Ports->find()->where(['mobile' => $op, 'type' => 'reception'])->first();
        $result = json_decode($this->checkPort($port->com)->getBody());
        $paramData = $this->getParam()->toArray();
        $http = new Client();

        if ($result->result == 'ok') {
            $this->stopServiceNg();
            $response = $http->post('http://127.0.0.1/moneiko-ussd/tdb/remapRoutage', $paramData);
            $result = $response->json;
            $this->startServiceNg();
        };
        die();
    }

    // Maj Ngrok Url prod
    public function majUrlNgrok()
    {

        $http = new Client();

        // debug($http->get($url)->getHeaders());

        // $url = "nabtqxron.coms"; //your url goes in this place instead of nabtron.com

        // if (@fopen($url,"r")) {
        //     die('accessible');
        // }

        // die();
        $response = $this->readUrl('http://127.0.0.1:4040/api/tunnels');

        $res = ['status' => false];

        if ($response) {
            $res = json_decode($response);


            if (isset($res->tunnels[0])) {
                
                $url = $res->tunnels[0]->public_url;

                $httpUrl = 'https://'.explode("//", $url)[1];
                $data = ['server_ngrok' => $httpUrl];

                // pr($data);
                // die();

                // $res = $http->post('http://127.0.0.1/moneyko/exchanger/support/majUrlNgrokFromLocal', $data)->json;
                // if ($this->checkUrl()) {
                    $res = $http->post('https://moneiko.net/exchanger/support/majUrlNgrokFromLocal', $data)->json;
                    // debug($res);
                    // die();
                // } else {
                    // $res = ['status' => 'no connection'];
                // }
                // die();
            }
            
        }

        $body = $res;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function remapLettre()
    {

        // $completedLetterPorts = $this->Ports->find()->where(['coalesce(letter, "") =' => ''])->combine('id', 'letter')->toArray();
        // debug($completedLetterPorts);
        // die();

        // $r = $this->Ussd->resetUsb('I:,P:');
        // debug($r);
        // die();
        $gsmList = [
            // 'Q:', 
            // 'V:', 
            // 'H:', 
            // 'R:,S:'
            // 'L:,N:'
        ];

        $gsmList = $this->Ussd->getUsbGsmList();
        // debug($gsmList);
        // die();
        $nbGsmList = count($gsmList);
        $traitees = 0;

        $debugWhoLocks = "\n";
        $missingsPortLogs = [];
        foreach ($gsmList as $key => $gsmLetter) {

            $this->Ussd->stopServiceNg();

            if (!$this->Ports->findByLetter($gsmLetter)->first()) {
                // $checkGsm = exec(USBSAFELYREMOVE.'usr.exe wholocks -d '.$gsmLetter);
                // $checkGsm = $this->Ussd->usb('check', $gsmLetter);

                // $debugWhoLocks .= $gsmLetter. ', '.$checkGsm."\n";

                // if ((strrpos($checkGsm, 'Perhaps the device is ready for ejecting now') !== false)) {
                    // $ex = exec(USBSAFELYREMOVE.'usr.exe stop -d '.$gsmLetter);
                    $ex = $this->Ussd->usb('stop', $gsmLetter);

                    // sleep(1);
                    // $ex = $this->Ussd->usb('return', $gsmLetter); // décommenter lors d'un debug

                    $ports = json_decode($this->getPortListDatas());
                    $missingsPort = $this->Ports->find()->where(['com not in' => $ports])->first();
                    if ($missingsPort) {
                        $missingsPortLogs[] = json_encode($missingsPort);
                        $this->Ports->updateAll(['letter' => $gsmLetter], ['id' => $missingsPort->id]);
                        // sleep(1);
                        $res = $this->Ussd->usb('return', $gsmLetter);
                        echo ($res.' '.$missingsPort->com.' to '.$gsmLetter);

                        ++$traitees;
                    }

                    if ($key+1 < $nbGsmList) {
                        // sleep(1.5);
                    }
                // }
            }

            $this->logginto($debugWhoLocks, 'gsm/wholocks', $erase = true);
            $this->logginto($missingsPortLogs, 'gsm/missings_port_logs', $erase = true);
            $this->logginto($gsmList, 'gsm/gsm_list', $erase = true);


        }

        if ($traitees == 0) {
            echo 'Aucun';
            sleep(6);
        } else {
            echo $debugWhoLocks;
        }
        
        die();

    }

    public function checkStateUrl()
    {
        $response = $this->readUrl('http://127.0.0.1:4040/api/tunnels');
        $res = json_decode($response);
        $httpUrl = $res->tunnels[0]->public_url;
        $urlLocal = 'https://'.explode("//", $httpUrl)[1];
        
        
        $urlDistant = $this->getProdParams()->server_ngrok;
        // debug($urlLocal);
        // debug($urlDistant);
        // die();
        $body = ['status' => $urlLocal == $urlDistant];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }



    public function cleanPayment()
    {
        $today = date('Y-m-d');
        $smss = $this->Ozekimessageins
            ->find()
            ->where(['sender' => 'MVola', 'senttime LIKE' => "%$today%",'msg LIKE' => "%Vous avez recu%"])
            ->where([
                'OR' => [
                    ['paiement_status' => 'error'],
                    ['paiement_status IS' => NULL],
                ]
            ])
        ;

        $type = 'mvola';

        $smss = $smss->formatResults(function ($query) use($type)
        {
            return $query->each(function ($item) use($type)
            {
                if ($type == 'mvola') {
                    $smsData = $this->Ussd->extractMvolaRecuData($item->msg);
                } elseif ($type == 'airtel') {
                    $smsData = $this->Ussd->extractAirtelRecuData($item->msg);
                }

                $item->montant_recu = @$smsData['montant_recu'];
                $item->solde_actuel = @$smsData['solde_actuel'];
                $item->idpanier = (int) @$smsData['idpanier'];
                $item->raison = (int) @$smsData['raison'];
                return $item;
            });
        });

        $messagesIds = $smss->extract('idpanier')->toArray();
        
        $serveurDisant = $this->serveurDisant;
        // $serveurDisant = 'http://127.0.0.1/moneyko';
        
        $http = new Client();
        $body = ['status' => false];

        if ($smss->count()) {
            $res = $http->post($serveurDisant.'/exchanger/support/cleanPayment', ['ids' => $messagesIds]);

            if ($res->code == 200) {
                $paniers = $res->json;
                if ($paniers != null) {
                    $txnids = collection($paniers)->match(['status' => 4])->extract('txnid')->toArray();

                    foreach ($txnids as $key => $txnid) {
                        $sms = $this->Ozekimessageins
                            ->find()
                            ->where(['senttime LIKE' => "%$today%",'msg LIKE' => "%Raison: $txnid%"])
                            ->first()
                        ;

                        if ($sms) {
                            $this->Ozekimessageins->updateAll(['paiement_status' => 'success'], ['id' => $sms->id]);
                        }


                        $body = ['status' => true];
                    }
                }
            }
        }


        return $this->response->withType('application/json')->withStringBody(json_encode($body));

    }

    public function majLastSoldeMvola()
    {
        $http = new Client();
        $url = Router::url(['action' => 'presetUssd', '034', 'code' => "#111*1# #*6*1 2958"], true);
        // debug($url);
        // die();
        // debug($http->get($url, ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false])->body());
        // die();
        $res = $http->get($url, ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false])->json['msg'];
        $solde = (int) str_replace(" ", "", $this->Ussd->extractBetween($res, 'Solde Mvola : ', ' Ar, diff'));


        $res = 0;
        if (is_integer($solde) && $solde > 0) {
            
            $data = ['last_solde_mvola' => $solde, 'last_maj_solde_mvola_at' => Chronos::now()];
            $parameter = $this->Parameters->patchEntity($this->parameter, $data, ['validate' => false]);
            if(!$parameter->getErrors()) {
                $res = (bool) $this->Parameters->save($parameter);

                $soldeMvolaAjuste = round($solde/1.015, 0);
                $soldeMvolaAjuste = round($soldeMvolaAjuste/100)*100;

                if ($soldeMvolaAjuste > 3000000) {
                    $soldeMvolaAjuste = 3000000;
                }
                $http->post("https://moneiko.net/exchanger/support/updateParams", ['solde_mvola' => $soldeMvolaAjuste])->json;
            }
            
        }
        $body = ['msg' => $res ? "Solde courant : $solde Ariary" : 'error', 'solde_mvola' => $solde];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkStateDiffNum($mobile = '034')
    {
        $result = ['status' => false];
        if ($mobile == '032') {
            $numeroOrange = $this->Ussd->getCurrentNumroOrange();

            $prodParam = $this->getProdParams();
            $result = [
                'status' => ($prodParam->numero_depot_orange != $numeroOrange) ? 'success' : false
            ];
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function transferMvolaToOrange()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $numero = $data['numero'];
            $montant = $data['montant_transfert'];
            // $ussd = "#111# 1*2*$numero*$montant*ecolage 2958";
            $ussd = "#111# 1*2*$numero*$montant*ecolage 2958";
            $http = new Client();
            $url = Router::url(['action' => 'presetUssd', '034', 'code' => $ussd], true);
            $res = $http->get($url)->json['msg'];
            $this->logginto(['ussd' => $ussd, 'res' => $res], 'transfert_mvola_orange', $erase = false);
            // echo ($ussd) . '<br>';
            echo ($res);
        }
        die();
    }

    public function test500()
    {
        echo 'lol'; die;
        // throw new \Cake\Http\Exception\InternalErrorException('Erreur 404: Page non trouvée');
    }

    public function checkServerNg()
    {
        $res = $this->Ussd->checkServerNg() == 'true' ? true : false;
        $activeTunnel = $this->Tunnels->findByIsActive(1)->first();
        $body = ['status' => $res, 'hit' => $activeTunnel->hit, 'rate_hit_tunnel' => round($activeTunnel->hit/19500, 2), 'percent' => $activeTunnel->get('Pourcentage')];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function restartServiceNg()
    {
        $res = $this->Ussd->restartGateway();
        sleep(5);
        $res = $this->Ussd->startServiceNg();

        $body = ['status' => 'success'];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }


    /**
     * Restart le Gateway
     * @param  [type] $mode $mode = 1 si nuit
     * @return [type]       [description]
     */
    public function restartGateway($mode = null)
    {
        if ($mode == null) {
            $res = $this->Ussd->restartGateway();

            $result = false;
            if (strrpos($res, 'Le service OZEKI NG-SMS-Gateway a d') !== false) {
                $result = true;
            }

            $body = ['status' => $result];
            return $this->response->withType('application/json')->withStringBody(json_encode($body));
        } elseif ($mode == 1) {
            $lastSmsOrangemoney = $this->Ozekimessageins->find('Recus')->where(['sender IN' => $this->orangeSenders])->order(['senttime' => 'DESC'])->first();
            $now = Chronos::now();

            $diff = $now->diffInMinutes($lastSmsOrangemoney->date_transfert);
            if ((date('H') > 8 && date('H') < 19) && $diff > 30) {
                return $this->restartGateway();
            }
            die('not in time interval');
        }
    }

    public function checkHealth($operator = "Orangemoney")
    {
        $ops = ['Orangemoney'];
        $now = Chronos::now();
        $body = ['status' => false];
        // $body = ['status' => 'restarted', 'diffSecs' => 20];


        $http = new Client();
        $result = $http->get("https://moneiko.net/exchanger/apis/getParams", [], $this->noSslParams)->json;

        if (isset($result['numero_orange_retrait'])) {
            $orangeDepotNum = $result['numero_depot_orange'];
            $url = Router::url(['action' => 'presetUssd', '032', 'code' => "#123# 3 1 $orangeDepotNum"], true);
            $res = $http->get($url, ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false])->json['msg'];

            sleep(12);
            $lastRappelMoiOrange = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Veuillez me rappeler%"])->last();
            $senttimeLastSms = Chronos::parse($lastRappelMoiOrange->senttime);
            
            $diffSecs = $now->diffInSeconds($senttimeLastSms);
            // debug($now->format('H:i:s'));
            // debug($senttimeLastSms->format('H:i:s'));
            // debug($lastRappelMoiOrange);
            // debug($diffSecs);
            // die();

            if ($diffSecs > 20) {
                $this->restartGateway();
                $body = ['status' => 'restarted', 'diffSecs' => $this->ago3($lastRappelMoiOrange->senttime)];
            }

        }
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function getLastRappelMoi($op = 'orange')
    {
        $lastRappelMoiOrange = $this->Ozekimessageins->find()->where(['msg LIKE' => "%Veuillez me rappeler%"])->last();
        $body = ['status' => '200', 'ago' => $this->ago3($lastRappelMoiOrange->senttime)];

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function switchport($opName)
    {
        $param = $this->getParam();
        $isExchange = $param->{'is_exchange_activated_'.$opName} == 0 ? 1 : 0;
        $param = $this->Parameters->updateAll(['is_exchange_activated_'.$opName => $isExchange], ['id' => 1]);
        $body = ['status' => 'success', 'result' => $isExchange];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }   

    public function startOrStopServerNg()
    {
        $param = $this->getParam();
        if ($param->is_server_ng_started == true) {
            $this->Ussd->stopServiceNg();
            $isExchange = 0;
        } else {
            $this->Ussd->startServiceNg();
            $isExchange = 1;
        }
        $param = $this->Parameters->updateAll(['is_server_ng_started' => $isExchange], ['id' => 1]);
        $body = ['status' => 'success', 'result' => $isExchange];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function remapDriverEnvoi()
    {
        $data = [];
        foreach ($this->operateurs as $op) {
            $port = $this->Ports->findByMobile($op)->where(['type' => 'envoi'])->first();
            $data['driver_'.$op] = $port->letter;
        }

        // debug($data);

        // die();

        $params = (bool) $this->Parameters->updateAll($data, ['id' => 1]);
        // debug($params);
        // die();
        $body = ['status' => $params == true ? 'success' : 'failed', 'data' => $data];

        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkProgressPortLetter()
    {
        $nbPorts = $this->Ports->find()->count();
        $nbPortsRemplis = $this->Ports->find()->where(['coalesce(letter, "") !=' => ''])->count();
        $progress = round((($nbPortsRemplis/$nbPorts)*100), 0);
        $body = ['progress' => $progress];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function findPortByNum($numero)
    {   
        $port = $this->Ports->findByNumero($numero)->first();
        $body = ['port' => $port->com];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function definirNumeroTransactionVersProd()
    {
        $param = $this->getParam();
        $res = ['status' => ''];
        $resSave = ['status' => ''];
        $http = new Client();
        $isComplete = false;
        foreach ($this->operateurs as $key => $op) {

            $dataList['config_list_num_'.$op] = [
                $param->{'num_port_envoi_'.$op} => $param->{'num_port_envoi_'.$op},
                $param->{'num_port_reception_'.$op} => $param->{'num_port_reception_'.$op}
            ];

            if (empty($param->{'num_port_envoi_'.$op}) || empty($param->{'num_port_reception_'.$op})) {
                $res = ['status' => 'error', 'msg' => 'un numéro vide détecté'];
                break;
            }

            $isComplete = true;
        }


        $datasNumDepots = [];
        if ($isComplete) {
            // $resData = $http->post("http://127.0.0.1/moneyko/exchanger/apis/updateParams", $dataList)->json;

            // debug($this->checkUrl("https://moneiko.net"));
            // debug($http->get("https://moneiko.net")->getHeaders());
            // die();
            // debug(get_headers("https://moneiko.net/exchanger/apis/getParams"));

            // $handle = curl_init("https://moneiko.net");
            //   curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            //   curl_setopt($handle, CURLOPT_NOBODY, true);
            //   curl_exec($handle);
             
            //   $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            //   curl_close($handle);
             
            //   if ($httpCode >= 200 && $httpCode < 300) {
            //     die('ok');
            //   }
            //   else {
            //     die('false be');
            //   }



            // debug($http->get('https://moneiko.net/exchanger/apis/getParams')->json);
            // die();
            if ($this->checkUrl($this->serveurDisant."/exchanger/apis/updateParams")) {
                $resData = $http->post($this->serveurDisant."/exchanger/apis/updateParams", $dataList)->json;
                $resSave = ['status' => 'success', 'datas' => $resData];

                if (!empty($resData['config_list_num_telma']) && 
                    !empty($resData['config_list_num_orange']) && 
                    !empty($resData['config_list_num_airtel'])
                ) {
                    foreach ($this->operateurs as $key => $op) {
                        $datasNumDepots['numero_depot_'.$op] = $param->{'num_port_reception_'.$op};
                    }

                    $resSave['datas_num_depots'] = $datasNumDepots;
                }
            } else {
                $resSave = ['status' => 'url innaccessible'];
            }

        }

        $resData = $http->post($this->serveurDisant."/exchanger/apis/updateParams", $datasNumDepots)->json;

        $body = $resSave;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function synchronizeLocalServerTime()
    {
        $servParamUrl = "https://moneiko.net/exchanger/apis/getParams";
        $http = new Client();

        if ($this->checkUrl($servParamUrl)) {
            $serverParam = $http->get($servParamUrl, [], $this->noSslParams)->json;
            $this->Ussd->execCli2('time '.$serverParam['heureactuelserveur']);
        }

        $body = ['status' => true];
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function checkLocalServerTime()
    {
        $servParamUrl = "https://moneiko.net/exchanger/apis/getParams";
        $http = new Client();
        $serverParam = $http->get($servParamUrl, [], $this->noSslParams)->json;
        $serverTime = Chronos::parse($serverParam['heureactuelserveur']);
        $localTime = Chronos::now();

        $diffSeconds = $serverTime->diffInDays($localTime);

        if ($diffSeconds > 0) {
            $this->Ussd->execCli2('time '.$serverParam['heureactuelserveur']);
        }
        die();
    }

    public function traitementrelance($port = 'com41', $mobile = 'orange')
    {
        $this->loadModel('Relances');

        // $membres = json_decode(file_get_contents(ROOT.'/patch/mpanao_retrait.json'), true);
        // foreach ($membres as $key => $membre) {
        //     if (!$this->Relances->findByNumero($membre['numero'])->first()) {
        //         $relance = $this->Relances->newEntity($membre);
        //         $this->Relances->save($relance);
        //     }
        // }


        $relances = $this->Relances->find();

        $relances->select(['mobile',
          'count' => $relances->func()->count('*')
        ])
         ->group('mobile');

        // foreach ($relances as $row) {
        //    debug($row);
        // }


        $relances = $this->Relances->find()->where(['mobile' => $mobile, 'is_sent' => 0]);
        // debug($relances ->toArray());

        foreach ($relances as $key => $relance) {

            $string = $relance->nom;
            $pieces = explode(' ', $string);
            $lastName = ucfirst(strtolower($this->normalizeChars(array_pop($pieces))));

            // echo $relance->nom;


            // $lastName = 
            $msg = 'Bjr '.$lastName.'. Saika mba ampilaza fotsiny hoe efa miverina mandeha moneiko.net, au cas ou. Ny tompony. Misaotra tompoko.';
            $msg = $this->normalizeChars($msg);
            // $this->Relances->updateAll(['is_sent' => 1], ['id' => $relance->id]);
        }

        die();
    }

    public function updateSms($smsId)
    {
        $sms = $this->Ozekimessageins->findById($smsId)->first();
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            // $data['receivedtime'] = $data['senttime'];
            $sms = $this->Ozekimessageins->patchEntity($sms, $data, ['validate' => false]);
            $sms = $this->Ozekimessageins->save($sms);
        }
        $body = $sms;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function testUrl()
    {
        $url = "https://moneiko.net";
        // debug($this->checkUrl());
        // die();

        // Initialiser cURL
        $ch = curl_init($url);

        // Configurer les options
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        // Exécuter la requête
        $response = curl_exec($ch);

        // Séparer les en-têtes de la réponse
        list($headers, $content) = explode("\r\n\r\n", $response, 2);

        // Afficher les en-têtes
        echo $headers;

        // Fermer la session cURL
        curl_close($ch);

        // debug($this->checkUrl("https://moneiko.net"));
        die();
    }

    public function checkTitulaireNom($nom_titulaire = null, $nom_complet = null)
    {
        $nom_titulaire = 'BURTON ELIH WEGA';
        $nom_complet = 'RAZAFINDRAMANGA burton elih wega';

        $nom_complet = strtoupper($this->normalizeChars($nom_complet));
        $normalizedNomComplet = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet)));
        $nom_titulaire = strtoupper($this->normalizeChars($nom_titulaire));

        $explodedNomTitulaire = explode(' ', $nom_titulaire);
        $explodedNomComplet = explode(' ', $nom_complet);
        $intersectedResultFromFicheUser = array_filter(array_intersect($explodedNomTitulaire, $explodedNomComplet));
        $scores = $this->checkCoherence($nom_complet, $nom_titulaire);

        $result = ['status' => null];

        if (
            (
                (strrpos($nom_complet, $nom_titulaire) !== false) || 
                (strrpos($normalizedNomComplet, $nom_titulaire) !== false) ||
                $intersectedResultFromFicheUser ||
                $this->checkExistingByExplode($nom_complet, $nom_titulaire)
            ) 
            && 
            ($scores->maxScore1 > 70 || $scores->maxScore2 > 80)
        ) {
            $result = ['status' => 'success', 'nom_titulaire' => $nom_titulaire, 'nom_complet' => $nom_complet, 'scores' => $scores];
        } else {
            $result = ['status' => 'failed'];
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    public function majSolde($mobile)
    {
        $host = $this->serveurDisant;
        $result = ['result' => false];

        if ($mobile == 'orange') {
            $mobile = 'mobile';
        }
        $http = new Client();

        if ($this->request->is(['post', 'ajax'])) {
            $data = $this->request->getData();
            if ($data['montant'] > 0) {
                $montant = $data['montant'];
                $url = $host."/exchanger/apis/updateParams";
                $now = Chronos::now();
                $save = ['solde_'.$mobile => $montant, /*'date_maj_'.$mobile => $now*/];
                $result = $http->post($url, $save)->json;
            }
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

    /**
     * Payeer ou Pm
     * @param  [type] $ptf [description]
     * @return [type]      [description]
     */
    public function majSoldeUsd($ptf)
    {
        $host = $this->serveurDisant;
        $result = ['result' => false];

        if ($ptf == 'payeer') {
            
        }

        $http = new Client();

        if ($this->request->is(['post', 'ajax'])) {
            $data = $this->request->getData();
            if ($data['montant'] > 0) {

                if ($ptf == 'payeer') {
                    $key = 'py';
                } else {
                    $key = $ptf;
                }

                $save = [
                    'reserve_'.$key => round($data['reserve'], 2),
                ];

                $montant = $data['montant'];
                $portefeuille ;

                $url = $host."/exchanger/apis/updateParamsAndResetSolde/".$ptf;
                $now = Chronos::now();
                $result = $http->post($url, $save, $this->noSslParams)->json;
            }
        }

        $body = $result;
        return $this->response->withType('application/json')->withStringBody(json_encode($body));
    }

}
?>