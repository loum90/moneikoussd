<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SentUssds Model
 *
 * @method \App\Model\Entity\SentUssd get($primaryKey, $options = [])
 * @method \App\Model\Entity\SentUssd newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SentUssd[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SentUssd|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SentUssd|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SentUssd patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SentUssd[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SentUssd findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SentUssdsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sent_ussds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('ref')
            ->maxLength('ref', 50)
            ->requirePresence('ref', 'create')
            ->notEmpty('ref');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 15)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->decimal('montant')
            ->requirePresence('montant', 'create')
            ->notEmpty('montant');

        $validator
            ->scalar('res_ussd')
            ->maxLength('res_ussd', 255)
            ->allowEmpty('res_ussd');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('model')
            ->maxLength('model', 50)
            ->allowEmpty('model');

        $validator
            ->date('date')
            ->allowEmpty('date');

        return $validator;
    }
}
