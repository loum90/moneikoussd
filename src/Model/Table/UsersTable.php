<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Filesystem\File;


/**
 * Users Model
 *
 * @property \App\Model\Table\AuthSmssTable|\Cake\ORM\Association\HasMany $AuthSmss
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('AuthSmss', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasMany('VentePerfects', [
            'className' => 'Exchanger.VentePerfects',
            'foreignKey' => 'user_id'
        ]);

        $this->hasMany('UsersIps', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 500)
            ->allowEmpty('password');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 10)
            ->allowEmpty('mobile');

        $validator
            ->scalar('titulaire')
            ->maxLength('titulaire', 255)
            ->allowEmpty('titulaire');

        $validator
            ->boolean('is_c2c')
            ->requirePresence('is_c2c', 'create')
            ->notEmpty('is_c2c');

        $validator
            ->scalar('numero_orange')
            ->maxLength('numero_orange', 11)
            ->allowEmpty('numero_orange');

        $validator
            ->scalar('numero_telma')
            ->maxLength('numero_telma', 11)
            ->allowEmpty('numero_telma');

        $validator
            ->scalar('numero_airtel')
            ->maxLength('numero_airtel', 11)
            ->allowEmpty('numero_airtel');

        $validator
            ->integer('is_numero_airtel_verified')
            ->requirePresence('is_numero_airtel_verified', 'create')
            ->notEmpty('is_numero_airtel_verified');

        $validator
            ->scalar('nom_complet')
            ->maxLength('nom_complet', 250)
            ->allowEmpty('nom_complet');

        $validator
            ->date('date_naissance')
            ->allowEmpty('date_naissance');

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 250)
            ->allowEmpty('adresse');

        $validator
            ->allowEmpty('num_cin');

        $validator
            ->scalar('cin_recto')
            ->maxLength('cin_recto', 250)
            ->notEmpty('cin_recto');

        $validator
            ->scalar('cin_verso')
            ->maxLength('cin_verso', 250)
            ->notEmpty('cin_verso');

        $validator
            ->scalar('cin_selfie')
            ->requirePresence('cin_selfie', 'create')
            ->maxLength('cin_selfie', 250)
            ->notEmpty('cin_selfie');

        $validator
            ->scalar('verification_status')
            ->allowEmpty('verification_status');

        $validator
            ->requirePresence('step_completed', 'create')
            ->notEmpty('step_completed');

        $validator
            ->scalar('raison_kyc_echec')
            ->allowEmpty('raison_kyc_echec');

        $validator
            ->scalar('numero_mobile')
            ->maxLength('numero_mobile', 15)
            ->allowEmpty('numero_mobile');

        $validator
            ->boolean('is_cin_recto')
            ->requirePresence('is_cin_recto', 'create')
            ->notEmpty('is_cin_recto');

        $validator
            ->boolean('is_cin_verso')
            ->requirePresence('is_cin_verso', 'create')
            ->notEmpty('is_cin_verso');

        $validator
            ->boolean('is_cin_selfie')
            ->requirePresence('is_cin_selfie', 'create')
            ->notEmpty('is_cin_selfie');

        $validator
            ->boolean('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->boolean('is_kyc_rappel')
            ->requirePresence('is_kyc_rappel', 'create')
            ->notEmpty('is_kyc_rappel');

        $validator
            ->dateTime('last_payout')
            ->allowEmpty('last_payout');

        $validator
            ->scalar('note_kyc')
            ->allowEmpty('note_kyc');

        $validator
            ->scalar('emploi')
            ->maxLength('emploi', 50)
            ->allowEmpty('emploi');

        $validator
            ->scalar('source_revenus')
            ->maxLength('source_revenus', 50)
            ->allowEmpty('source_revenus');

        $validator
            ->scalar('revenu')
            ->maxLength('revenu', 50)
            ->allowEmpty('revenu');

        $validator
            ->scalar('type_ppe')
            ->maxLength('type_ppe', 15)
            ->allowEmpty('type_ppe');

        $validator
            ->requirePresence('nb_kyc', 'create')
            ->notEmpty('nb_kyc');

        $validator
            ->scalar('xbet_id_telma')
            ->maxLength('xbet_id_telma', 11)
            ->allowEmpty('xbet_id_telma');

        $validator
            ->scalar('xbet_id_orange')
            ->maxLength('xbet_id_orange', 11)
            ->allowEmpty('xbet_id_orange');

        $validator
            ->scalar('xbet_id_airtel')
            ->maxLength('xbet_id_airtel', 11)
            ->allowEmpty('xbet_id_airtel');

        $validator
            ->requirePresence('is_quotas_reached', 'create')
            ->notEmpty('is_quotas_reached');

        $validator
            ->dateTime('last_profil_modified_at')
            ->requirePresence('last_profil_modified_at', 'create')
            ->notEmpty('last_profil_modified_at');

        $validator
            ->dateTime('last_telma_modified_at')
            ->requirePresence('last_telma_modified_at', 'create')
            ->notEmpty('last_telma_modified_at');

        $validator
            ->dateTime('last_orange_modified_at')
            ->requirePresence('last_orange_modified_at', 'create')
            ->notEmpty('last_orange_modified_at');

        $validator
            ->dateTime('last_airtel_modified_at')
            ->requirePresence('last_airtel_modified_at', 'create')
            ->notEmpty('last_airtel_modified_at');

        $validator
            ->scalar('last_connected_ip')
            ->maxLength('last_connected_ip', 100)
            ->allowEmpty('last_connected_ip');

        $validator
            ->dateTime('last_connected_at')
            ->requirePresence('last_connected_at', 'create')
            ->notEmpty('last_connected_at');

        $validator
            ->scalar('last_connected_browser')
            ->maxLength('last_connected_browser', 500)
            ->allowEmpty('last_connected_browser');

        $validator
            ->dateTime('last_connexion_attempt_at')
            ->requirePresence('last_connexion_attempt_at', 'create')
            ->notEmpty('last_connexion_attempt_at');

        $validator
            ->requirePresence('nb_attempt_connexion', 'create')
            ->notEmpty('nb_attempt_connexion');

        $validator
            ->integer('is_numero_telma_verified')
            ->requirePresence('is_numero_telma_verified', 'create')
            ->notEmpty('is_numero_telma_verified');

        $validator
            ->requirePresence('is_numero_orange_verified', 'create')
            ->notEmpty('is_numero_orange_verified');

        $validator
            ->integer('is_numero_telma_finished')
            ->requirePresence('is_numero_telma_finished', 'create')
            ->notEmpty('is_numero_telma_finished');

        $validator
            ->requirePresence('is_numero_orange_finished', 'create')
            ->notEmpty('is_numero_orange_finished');

        $validator
            ->integer('is_numero_airtel_finished')
            ->requirePresence('is_numero_airtel_finished', 'create')
            ->notEmpty('is_numero_airtel_finished');

        $validator
            ->scalar('titulaire_numero_orange')
            ->maxLength('titulaire_numero_orange', 255)
            ->allowEmpty('titulaire_numero_orange');

        $validator
            ->scalar('titulaire_numero_telma')
            ->maxLength('titulaire_numero_telma', 255)
            ->allowEmpty('titulaire_numero_telma');

        $validator
            ->scalar('titulaire_numero_airtel')
            ->maxLength('titulaire_numero_airtel', 255)
            ->allowEmpty('titulaire_numero_airtel');

        $validator
            ->integer('step_modification_nom')
            ->requirePresence('step_modification_nom', 'create')
            ->notEmpty('step_modification_nom');

        $validator
            ->integer('counter_faux_titulaire_numero_telma')
            ->requirePresence('counter_faux_titulaire_numero_telma', 'create')
            ->notEmpty('counter_faux_titulaire_numero_telma');

        $validator
            ->integer('counter_faux_titulaire_numero_orange')
            ->requirePresence('counter_faux_titulaire_numero_orange', 'create')
            ->notEmpty('counter_faux_titulaire_numero_orange');

        $validator
            ->integer('counter_faux_titulaire_numero_airtel')
            ->requirePresence('counter_faux_titulaire_numero_airtel', 'create')
            ->notEmpty('counter_faux_titulaire_numero_airtel');

        $validator
            ->integer('counter_allow_sendpinsms_modifnum_numero_telma')
            ->requirePresence('counter_allow_sendpinsms_modifnum_numero_telma', 'create')
            ->notEmpty('counter_allow_sendpinsms_modifnum_numero_telma');

        $validator
            ->integer('counter_allow_sendpinsms_modifnum_numero_orange')
            ->requirePresence('counter_allow_sendpinsms_modifnum_numero_orange', 'create')
            ->notEmpty('counter_allow_sendpinsms_modifnum_numero_orange');

        $validator
            ->integer('counter_allow_sendpinsms_modifnum_numero_airtel')
            ->requirePresence('counter_allow_sendpinsms_modifnum_numero_airtel', 'create')
            ->notEmpty('counter_allow_sendpinsms_modifnum_numero_airtel');

        $validator
            ->dateTime('last_sent_pin_numero_orange_at')
            ->requirePresence('last_sent_pin_numero_orange_at', 'create')
            ->notEmpty('last_sent_pin_numero_orange_at');

        $validator
            ->dateTime('last_sent_pin_numero_telma_at')
            ->requirePresence('last_sent_pin_numero_telma_at', 'create')
            ->notEmpty('last_sent_pin_numero_telma_at');

        $validator
            ->dateTime('last_sent_pin_numero_airtel_at')
            ->requirePresence('last_sent_pin_numero_airtel_at', 'create')
            ->notEmpty('last_sent_pin_numero_airtel_at');

        $validator
            ->boolean('is_2fa_telma')
            ->requirePresence('is_2fa_telma', 'create')
            ->notEmpty('is_2fa_telma');

        $validator
            ->boolean('is_2fa_orange')
            ->requirePresence('is_2fa_orange', 'create')
            ->notEmpty('is_2fa_orange');

        $validator
            ->boolean('is_2fa_airtel')
            ->requirePresence('is_2fa_airtel', 'create')
            ->notEmpty('is_2fa_airtel');

        $validator
            ->boolean('is_paiement_check_titulaire_kyc')
            ->requirePresence('is_paiement_check_titulaire_kyc', 'create')
            ->notEmpty('is_paiement_check_titulaire_kyc');

        $validator
            ->integer('nb_counter_alert_auth_telma')
            ->requirePresence('nb_counter_alert_auth_telma', 'create')
            ->notEmpty('nb_counter_alert_auth_telma');

        $validator
            ->integer('nb_counter_alert_auth_orange')
            ->requirePresence('nb_counter_alert_auth_orange', 'create')
            ->notEmpty('nb_counter_alert_auth_orange');

        $validator
            ->integer('nb_counter_alert_auth_airtel')
            ->requirePresence('nb_counter_alert_auth_airtel', 'create')
            ->notEmpty('nb_counter_alert_auth_airtel');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    
    public function findSimple(Query $query, array $options)
    {
        $query->where([
            'Users.is_c2c' => 0,
        ]);
        return $query;
    }

    public function findIsVerified(Query $query, array $options)
    {
        $query->where([
            'Users.is_deleted' => 0,
            'Users.step_completed' => 3,
        ]);
        return $query;
    }


    /*$validator->add('name', 'custom', [
        'rule' => [$this, 'noneSpecialCaracters'],
        'message' => $incorrect.': caractères alphanumériques uniquement',
        'provider' => 'custom'
    ]);*/
    public function noneSpecialCaracters($value, $provider)
    {
        $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
        return preg_match($pattern, $value) ? false : true;
    }

    public function findCourbeBrut(Query $query, array $options)
    {
        $annee_mois = @$options['annee_mois'];

        $users = $this->find();

        if (isset($options['where'])) {
            $users->where($options['where']);
        }

        $users = $users->orderAsc('Users.id')->where(['is_deleted' => 0,])->select([
            'id', 
            'annee_mois' => 'DATE_FORMAT(Users.created, "%Y-%m")', 
            'nb' => 'count(Users.id)', 
            'date_creation' => "DATE_FORMAT(created, '%Y-%m-%d')"
        ]);

        if ($annee_mois) {
            $users = $users->having([
                'annee_mois' => $annee_mois,
            ]);
        }


        $users = $users->group('date_creation')->indexBy('date_creation');

        return $users;
    }

    public function findCourbeMoisBrut(Query $query, array $options)
    {
        $annee_mois = @$options['annee_mois'];

        $users = $this->find();

        if (isset($options['where'])) {
            $users->where($options['where']);
        }

        $users = $users->orderAsc('Users.id')->where(['is_deleted' => 0,])->select([
            'id', 
            'annee_mois' => 'DATE_FORMAT(Users.created, "%Y-%m")', 
            'cumule' => 'count(Users.id)', 
        ]);

        if ($annee_mois) {
            $users = $users->having([
                'annee_mois' => $annee_mois,
            ]);
        }
        
        $users = $users->group('annee_mois')->indexBy('annee_mois');

        return $users;
    } 

    // In src/Models/Table/ExampleTable.php
    /**
     * Find neighbors method
     */
    public function findNeighbors(Query $query, array $options) {
        $id = $options['id'];
        $previous = $this->find()
            // ->select('id')
            ->order(['id' => 'DESC'])
            ->where(['id <' => $id])
            ->first();
        $next = $this->find()
            // ->select('id')
            ->order(['id' => 'ASC'])
            ->where(['id >' => $id])
            ->first();
        return ['prev' => $previous['id'], 'next' => $next['id']];
    }

    public function findStateCins(Query $query, array $options)
    {
        $query
            ->find('Simple')
            ->where([
                'or' => [
                    ['wordcount(nom_complet)' => 1],
                    ['is_modified_local' => true]
                ]
                // 'coalesce(Users.cin_recto, "") !=' => '',
            ])
            ->order(['id' => 'DESC'])
            ->map(function ($user)
            {
                $isCinFileExists = (new File(WWW_ROOT.$user->get('CinRectoPath')))->exists();
                $statut = 0;
                if (strlen($user->cin_recto) > 0 && $isCinFileExists) {
                    $statut = 1; // fichier cin existe en prod et en local
                } elseif (strlen($user->cin_recto) > 0 && !$isCinFileExists) {
                    $statut = 2; // fichier cin existe en prod et mais n'existe pas en local
                } else {
                    $statut = 3; // fichier cin n'existe pas en prod et n'existe pas en local
                }
                $user->statut = $statut;
                return $user;
            })
        ;
        return $query;
    }
}
