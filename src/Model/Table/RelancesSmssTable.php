<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RelancesSmss Model
 *
 * @method \App\Model\Entity\RelancesSms get($primaryKey, $options = [])
 * @method \App\Model\Entity\RelancesSms newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RelancesSms[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RelancesSms|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RelancesSms|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RelancesSms patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RelancesSms[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RelancesSms findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RelancesSmssTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('relances_smss');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('type')
            ->maxLength('type', 20)
            ->allowEmpty('type');

        $validator
            ->boolean('is_sent')
            ->requirePresence('is_sent', 'create')
            ->notEmpty('is_sent');

        $validator
            ->scalar('numero_telma')
            ->maxLength('numero_telma', 10)
            ->allowEmpty('numero_telma');

        $validator
            ->scalar('numero_orange')
            ->maxLength('numero_orange', 10)
            ->allowEmpty('numero_orange');

        $validator
            ->scalar('numero_airtel')
            ->maxLength('numero_airtel', 10)
            ->allowEmpty('numero_airtel');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
