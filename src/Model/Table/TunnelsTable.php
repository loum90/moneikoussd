<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tunnels Model
 *
 * @method \App\Model\Entity\Tunnel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tunnel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tunnel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tunnel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tunnel|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tunnel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tunnel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tunnel findOrCreate($search, callable $callback = null, $options = [])
 */
class TunnelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tunnels');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('account')
            ->maxLength('account', 25)
            ->allowEmpty('account');

        $validator
            ->scalar('token')
            ->maxLength('token', 500)
            ->allowEmpty('token');

        $validator
            ->integer('hit')
            ->requirePresence('hit', 'create')
            ->notEmpty('hit');

        $validator
            ->boolean('is_active')
            ->requirePresence('is_active', 'create')
            ->notEmpty('is_active');

        $validator
            ->dateTime('last_update_at')
            ->requirePresence('last_update_at', 'create')
            ->notEmpty('last_update_at');

        return $validator;
    }
}
