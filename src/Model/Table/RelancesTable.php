<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Relances Model
 *
 * @method \App\Model\Entity\Relance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Relance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Relance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Relance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Relance|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Relance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Relance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Relance findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RelancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('relances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 250)
            ->allowEmpty('nom');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 15)
            ->allowEmpty('numero');

        $validator
            ->scalar('titulaire')
            ->maxLength('titulaire', 255)
            ->allowEmpty('titulaire');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 50)
            ->allowEmpty('mobile');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        return $validator;
    }
}
