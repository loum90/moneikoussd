<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Chronos\Chronos;

/**
 * Ozekimessageins Model
 *
 * @method \App\Model\Entity\Ozekimessagein get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ozekimessagein newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ozekimessagein[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ozekimessagein|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ozekimessagein|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ozekimessagein patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ozekimessagein[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ozekimessagein findOrCreate($search, callable $callback = null, $options = [])
 */
class OzekimessageinsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ozekimessagein');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->senders = Configure::read('mvolaSenders');
        $this->airtelSenders = Configure::read('airtelSenders');
        $this->orangeSenders = Configure::read('orangeSenders');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('sender')
            ->maxLength('sender', 255)
            ->allowEmpty('sender');

        $validator
            ->scalar('receiver')
            ->maxLength('receiver', 255)
            ->allowEmpty('receiver');

        $validator
            ->scalar('msg')
            ->allowEmpty('msg');

        $validator
            ->scalar('senttime')
            ->maxLength('senttime', 100)
            ->allowEmpty('senttime');

        $validator
            ->scalar('receivedtime')
            ->maxLength('receivedtime', 100)
            ->allowEmpty('receivedtime');

        $validator
            ->scalar('operator')
            ->maxLength('operator', 100)
            ->allowEmpty('operator');

        $validator
            ->scalar('msgtype')
            ->maxLength('msgtype', 160)
            ->allowEmpty('msgtype');

        $validator
            ->scalar('reference')
            ->maxLength('reference', 100)
            ->allowEmpty('reference');

        return $validator;
    }

    public function findToday(Query $query, array $options)
    {
        $query
            ->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%",'msg LIKE' => "%Vous avez recu%"])
            ->order(['id' => 'DESC'])
        ;
        return $query;
    }

    public function findOneMinuteAgo(Query $query, array $options)
    {
        $oneMinuteAgo = Chronos::now()->subMinutes(1);

        $query
            ->where(['Ozekimessageins.senttime <' => $oneMinuteAgo])
        ;
        return $query;
    }

    public function findMvolaNonRegles(Query $query, array $options)
    {
        $nbNonRelgesMvola = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->senders, 'msg LIKE' => "%Ar recu de%",
            'OR' => [
                ['paiement_status not in' => ['success', 'locked_paiement', 'already_paid']],
                ['paiement_status IS' => NULL]
            ]
        ])->order(['senttime' => 'DESC']);
        return $nbNonRelgesMvola;
    }


    public function findAirtelNonRegles(Query $query, array $options)
    {
        $nbNonRelgesAirtel = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->airtelSenders, 'msg LIKE' => "%Vous avez recu Ar%", 
            'OR' => [
                ['paiement_status not in' => ['success', 'locked_paiement', 'already_paid']],
                ['paiement_status IS' => NULL]
            ]
        ]);
        return $nbNonRelgesAirtel;
    }

    public function findOrangeNonRegles(Query $query, array $options)
    {
        $nbNonRelgesOrange = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->orangeSenders, 'msg LIKE' => "%Vous avez recu%",
            'OR' => [
                ['paiement_status not in' => ['success', 'locked_paiement', 'already_paid']],
                ['paiement_status IS' => NULL]
            ]
        ]);

        return $nbNonRelgesOrange;
    }

    public function findOrangeCanceled(Query $query, array $options)
    {
        $results = $this->find()->where([
            'sender IN' => $this->orangeSenders,
            'OR' => [
                ['msg LIKE' => "%annulation%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
                ['msg LIKE' => "%annuler%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
            ],
            'operator !=' => 'ONEX'
        ])->order(['id' => 'DESC']);

        return $results;
    }

    public function findMvolaCanceled(Query $query, array $options)
    {
        $results = $this->find()->where([
            'sender IN' => $this->senders,
            'OR' => [
                ['msg LIKE' => "%annulation%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
                ['msg LIKE' => "%annuler%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
            ],
            'operator !=' => 'ONEX'
        ])->order(['id' => 'DESC']);

        return $results;
    }

    public function findAirtelCanceled(Query $query, array $options)
    {
        $results = $this->find()->where([
            'sender IN' => $this->airtelSenders,
            'OR' => [
                ['msg LIKE' => "%annulation%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
                ['msg LIKE' => "%annuler%", 'senttime LIKE' => "%".date('Y-m-d')."%"],
            ],
            'operator !=' => 'ONEX'
        ])->order(['id' => 'DESC']);

        return $results;
    }

    public function findOneXEnCours(Query $query, array $options)
    {
        $senders = $options['senders'];
        $query
            ->where([
                "OR" => [
                    ['msg LIKE' => "%Vous avez recu%"]
                ],
                'sender IN' => $senders,
                'operator' => "ONEX",
                'statut_1x' => 'pending',
            ]
        )->order(['senttime' => 'DESC'])
        ;
        return $query;
    }

    public function findOneXEnPaid(Query $query, array $options)
    {
        $senders = $options['senders'];
        $query
            ->where([
                "OR" => [
                    ['msg LIKE' => "%Vous avez recu%"]
                ],
                'sender IN' => $senders,
                'operator' => "ONEX",
                'statut_1x' => 'paid',
            ]
        )->order(['senttime' => 'DESC'])
        ;
        return $query;
    }

    public function findRecus(Query $query, array $options)
    {
        $query
            ->where(['OR' => [
                ['msg LIKE' => "%Vous avez recu%"],
                ['msg LIKE' => '%recu de%'],
            ]])
            ->where(['operator !=' => 'ONEX'])
        ;
        return $query;
    }

    public function findAutres(Query $query, array $options)
    {
        $ops = $options['ops'];
        $query
            ->where(['sender not in' => $ops, 
                'and' => [
                    ['msg not like' => '%A062B49AA46641F377%'],
                    ['msg not like' => '%Internet%'],
                    ['msg not like' => '%Bonus%'],
                    ['msg not like' => '%Tsindrio%'],
                    ['msg not like' => '%KADOA%'],
                    ['msg not like' => '%PROMO%'],
                    ['msg not like' => '%*436*%'],
                    ['msg not like' => '%*App*%'],
                    ['msg not like' => '%Avance%'],
                    ['msg not like' => '%Net Mlay%'],
                    ['msg not like' => '%DOUBLE%'],
                    ['msg not like' => '%QUIZ MLAY%'],
                    ['msg not like' => '%C1363%'],
                    ['msg not like' => '%CC3268%'],
                ]
            ])->order(['receivedtime' => 'DESC']);
        ;
        return $query;
    }



    public function findMvolaErrorReseau(Query $query, array $options)
    {
        $result = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->senders, 'msg LIKE' => "%Ar recu de%",
            'OR' => [
                ['paiement_status' => 'error'],
            ],
            'paiement_status_result LIKE' => "%réseau inaccessible%",
        ])->order(['senttime' => 'DESC']);
        
        return $result;
    }

    public function findOrangeErrorReseau(Query $query, array $options)
    {
        $result = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->orangeSenders, 'msg LIKE' => "%Ar recu de%",
            'OR' => [
                ['paiement_status' => 'error'],
            ],
            'paiement_status_result LIKE' => "%réseau inaccessible%",
        ])->order(['senttime' => 'DESC']);
        
        return $result;
    }

    public function findAirtelErrorReseau(Query $query, array $options)
    {
        $result = $this->find()->where(['operator !=' => 'ONEX', 'senttime LIKE' => "%".date('Y-m-d')."%", 'sender IN' => $this->airtelSenders, 'msg LIKE' => "%Ar recu de%",
            'OR' => [
                ['paiement_status' => 'error'],
            ],
            'paiement_status_result LIKE' => "%réseau inaccessible%",
        ])->order(['senttime' => 'DESC']);
        
        return $result;
    }
}
