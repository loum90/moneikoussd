<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Smss Model
 *
 * @method \App\Model\Entity\Sms get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sms newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sms[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sms|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sms|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sms patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sms[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sms findOrCreate($search, callable $callback = null, $options = [])
 */
class SmssTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('smss');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('content')
            ->allowEmpty('content');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 50)
            ->allowEmpty('numero');

        return $validator;
    }
}
