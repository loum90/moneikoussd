<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UssdHistorics Model
 *
 * @method \App\Model\Entity\UssdHistoric get($primaryKey, $options = [])
 * @method \App\Model\Entity\UssdHistoric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UssdHistoric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UssdHistoric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UssdHistoric|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UssdHistoric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UssdHistoric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UssdHistoric findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UssdHistoricsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ussd_historics');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('command')
            ->maxLength('command', 500)
            ->allowEmpty('command');

        $validator
            ->scalar('response')
            ->allowEmpty('response');

        return $validator;
    }
}
