<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class LogReclasTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('log_reclas');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Ozekimessageins', [
                'className' => 'Ozekimessageins',
                'foreignKey' => 'ozekimessagein_id',
            ]
        );

    }

    public function validationDefault(Validator $validator)
    {
        return $validator;
    }
}