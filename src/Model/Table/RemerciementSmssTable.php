<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RemerciementSmss Model
 *
 * @method \App\Model\Entity\RemerciementSms get($primaryKey, $options = [])
 * @method \App\Model\Entity\RemerciementSms newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RemerciementSms[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RemerciementSms|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemerciementSms|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemerciementSms patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RemerciementSms[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RemerciementSms findOrCreate($search, callable $callback = null, $options = [])
 */
class RemerciementSmssTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('remerciement_smss');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 30)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        return $validator;
    }
}
