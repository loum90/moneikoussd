<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ports Model
 *
 * @method \App\Model\Entity\Port get($primaryKey, $options = [])
 * @method \App\Model\Entity\Port newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Port[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Port|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Port|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Port patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Port[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Port findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PortsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ports');
        $this->setDisplayField('numero');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('mobile')
            ->allowEmpty('mobile');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 12)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->scalar('com')
            ->maxLength('com', 20)
            ->allowEmpty('com');

        return $validator;
    }
}
