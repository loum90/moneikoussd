<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransactionsHistorics Model
 *
 * @method \App\Model\Entity\TransactionsHistoric get($primaryKey, $options = [])
 * @method \App\Model\Entity\TransactionsHistoric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TransactionsHistoric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TransactionsHistoric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TransactionsHistoric|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TransactionsHistoric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionsHistoric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionsHistoric findOrCreate($search, callable $callback = null, $options = [])
 */
class TransactionsHistoricsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('transactions_historics');
        $this->setDisplayField('ref');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 15)
            ->allowEmpty('numero');

        $validator
            ->scalar('paiement')
            ->requirePresence('paiement', 'create')
            ->notEmpty('paiement');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('ref')
            ->maxLength('ref', 50)
            ->requirePresence('ref', 'create')
            ->notEmpty('ref')
            ->add('ref', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['ref']));

        return $rules;
    }
}
