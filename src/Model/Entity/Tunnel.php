<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tunnel Entity
 *
 * @property int $id
 * @property string $account
 * @property string $token
 * @property int $hit
 * @property bool $is_active
 * @property \Cake\I18n\FrozenTime $last_update_at
 */
class Tunnel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];

    // protected $_virtual = ['pourcentage'];

    public function _getPourcentage()
    {
        $ratio = round(($this->hit/20000) * 100, 2);
        return $ratio > 100 ? 100 : $ratio;
    }
}
