<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Traits\AppTrait;

class User extends Entity
{
    use AppTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' =>   false,
        '*' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];


    public function _getMobileFormat()
    {
        return $this->formatNumber($this->mobile);
    }

    public function _getCinRectoPath()
    {
        return !empty($this->cin_recto) ? 'img'.DS.'CINS_Moneiko'.DS.$this->cin_recto : '';
    }

    public function _getCinVersoPath()
    {
        return !empty($this->cin_verso) ? 'uploads/cin/'.$this->cin_verso : '';
    }

    public function _getCinSelfiePath()
    {
        return !empty($this->cin_selfie) ? 'uploads/cin/'.$this->cin_selfie : '';
    }

    public function _getStatutProgression()
    {
        if ($this->step_completed == 0) {
            $statut = '<span class="text-secondary">Aucune information</span>';
        } elseif ($this->step_completed == 1) {
            $statut = 'Informations de base remplies';
        } elseif ($this->step_completed == 2) {
            $statut = '<span class="text-warning">En attente de validation</span>';
        } elseif ($this->step_completed == 3) {
            $statut = 'Vérifié';
        }
        return $statut;
    }

    public function _getIsVerified()
    {
        return $this->step_completed == 3;
    }


    public function _getTelmaProgression()
    {
        if ($this->is_numero_telma_verified == 1) {
            
        }
    }

    public function _getIs2faMarquageVide()
    {
        return !$this->is_2fa_telma && !$this->is_2fa_orange && !$this->is_2fa_airtel;
    }

    public function _getProgresionTelmaFormated()
    {
        $html = "";
        
        if ($this->is_numero_telma_verified == 1) {
            $html = '<span class="pointer badge badge-success"> Valide <span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_telma_verified == 2)  {
            $html = '<span data-toggle="tooltip" data-placement="bottom" title="Par mesure de sécurité, le paiement de cette commande n\'est pas autorisé. Si vous persistez, cette commande sera gelée !" class="pointer badge badge-danger"> Invalide <span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_telma_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"> Suspendu <span class="fa fa-clock-o"></span> </span>';
        }
        else  {
            $html = '<span class="text-secondary">En cours</span> <span class="fa fa-clock-o text-secondary"></span>';
        }

        return $html;
    }

    public function _getProgresionOrangeFormated()
    {
        $html = "";
        
        if ($this->is_numero_orange_verified == 1) {
            $html = '<span class="pointer badge badge-success"> Valide <span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_orange_verified == 2)  {
            $html = '<span data-toggle="tooltip" data-placement="bottom" title="Le paiement de cette commande n\'est pas autorisé, si vous persistez, cette commande sera gelée !" class="pointer badge badge-danger"> Invalide <span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_orange_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"> Suspendu <span class="fa fa-clock-o"></span> </span>'; }
        else  {
            $html = '<span class="text-secondary">En cours</span> <span class="fa fa-clock-o text-secondary"></span>';
        }

        return $html;
    }

    public function _getProgresionAirtelFormated()
    {
        $html = "";

        if ($this->is_numero_airtel_verified == 1) {
            $html = '<span class="pointer badge badge-success"> Valide <span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_airtel_verified == 2)  {
            $html = '<span data-toggle="tooltip" data-placement="bottom" title="Le paiement de cette commande n\'est pas autorisé, si vous persistez, cette commande sera gelée !" class="pointer badge badge-danger"> Invalide <span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_airtel_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"> Suspendu <span class="fa fa-clock-o"></span> </span>';
        }
        else  {
            $html = '<span class="text-secondary">En cours</span> <span class="fa fa-clock-o text-secondary"></span>';
        }

        return $html;
    }


    public function _getProgresionTelmaPastille()
    {
        $html = "";
        
        if ($this->is_numero_telma_verified == 1) {
            $html = '<span class="pointer badge badge-success"><span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_telma_verified == 2)  {
            $html = '<span class="pointer badge badge-danger"> <span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_telma_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"><span class="fa fa-clock-o"></span> </span>';
        }
        else  {
            $html = '<span class="pointer badge badge-secondary"><span class="fa fa-clock-o"></span></span>';
        }

        return $html;
    }

    public function _getProgresionOrangePastille()
    {
        $html = "";
        
        if ($this->is_numero_orange_verified == 1) {
            $html = '<span class="pointer badge badge-success"><span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_orange_verified == 2)  {
            $html = '<span class="pointer badge badge-danger"><span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_orange_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"><span class="fa fa-clock-o"></span> </span>'; }
        else  {
            $html = '<span class="pointer badge badge-secondary"><span class="fa fa-clock-o"></span></span>';
        }

        return $html;
    }

    public function _getProgresionAirtelPastille()
    {
        $html = "";

        if ($this->is_numero_airtel_verified == 1) {
            $html = '<span class="pointer badge badge-success"><span class="fa fa-check-circle"></span></span>';
        }
        elseif($this->is_numero_airtel_verified == 2)  {
            $html = '<span class="pointer badge badge-danger"><span class="fa fa-times-circle"></span></span>';
        }
        elseif($this->is_numero_airtel_verified == 3)  {
            $html = '<span class="pointer badge badge-warning"><span class="fa fa-clock-o"></span> </span>';
        }
        else  {
            $html = '<span class="pointer badge badge-secondary"><span class="fa fa-clock-o"></span></span>';
        }

        return $html;
    }
}
