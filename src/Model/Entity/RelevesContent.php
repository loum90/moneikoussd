<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RelevesContent Entity
 *
 * @property int $id
 * @property string $html
 * @property int $created
 */
class RelevesContent extends Entity
{
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];
}
