<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RelancesSms Entity
 *
 * @property int $id
 * @property string $email
 * @property string $type
 * @property bool $is_sent
 * @property \Cake\I18n\FrozenTime $created
 * @property string $numero_telma
 * @property string $numero_orange
 * @property string $numero_airtel
 */
class RelancesSms extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public function _getPrenom()
    {
        $mots = explode(" ", trim($this->nom));

        // Récupérer le dernier mot (le dernier élément du tableau)
        return ucfirst(strtolower(end($mots)));
    }

    public function checkMobile2($string)
    {
        $mobile = '';
        if (substr($string, 0, 3) == '032' || substr($string, 0, 3) === '037') {
            $mobile = 'orange';
        } elseif (substr($string, 0, 3) == '034' || substr($string, 0, 3) == '038') {
            $mobile = 'telma';
        } elseif (substr($string, 0, 3) == '033') {
            $mobile = 'airtel';
        } 

        return $mobile;
    }

    public function _getNumero()
    {
        $numero = null;

        if ($this->numero_orange) {
            $numero = $this->numero_orange;
        } elseif ($this->numero_telma) {
            $numero = $this->numero_telma;
        } elseif ($this->numero_airtel) {
            $numero = $this->numero_airtel;
        }

        return $numero;
    }

    public function _getMobile()
    {
        return $this->checkMobile2($this->_getNumero());
    }

}
