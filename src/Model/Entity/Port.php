<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Port Entity
 *
 * @property int $id
 * @property string $mobile
 * @property string $numero
 * @property string $com
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Port extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true,
    ];

    public function _getClassHtml()
    {
        
        $html = "";
        
        if ($this->mobile == 'telma') {
            $html = 'text-success';
        }
        elseif($this->mobile == 'orange')  {
            $html = 'text-warning';
        }
        elseif($this->mobile == 'airtel')  {
            $html = 'text-danger';
        }

        return $html;
    }
}
