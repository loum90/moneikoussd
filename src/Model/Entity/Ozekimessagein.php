<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Chronos\Chronos;

/**
 * Ozekimessagein Entity
 *
 * @property int $id
 * @property string $sender
 * @property string $receiver
 * @property string $msg
 * @property string $senttime
 * @property string $receivedtime
 * @property string $operator
 * @property string $msgtype
 * @property string $reference
 */
class Ozekimessagein extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];

    public function _getType()
    {
        $smsTypes = Configure::read('smsType');
        if (strrpos($this->msg, 'pour') !== false) { // Airtel
            $raison = $this->extractBetween($this->msg, 'pour ', '. Le solde');;
        } else { // Mvola
            $raison = $this->extractBetween($this->msg, "Raison: ", ". Votre solde");
        }
        return @$smsTypes[$raison[0]];
    }

    public function extractBetween($message, $text1, $text2 = '')
    {
        $part1 = explode($text1, $message)[1] ?? false;
        if ($text2) {
            $text = trim(explode($text2, $part1)[0]);
        } else {
            $text = $part1;
        }
        return $text;
    }

    public function _getClassStatut()
    {
        $class = "";
        if ($this->paiement_status == 'success') {
            $class = "text-success";
        } elseif ($this->paiement_status == "warning") {
            $class = "text-danger";
        } elseif($this->paiement_status == "already_paid") {
            $class = "text-info";
        }elseif($this->paiement_status == "locked_paiement") {
            $class = "text-secondary";
        } elseif($this->paiement_status == "error" && $this->paiement_status_result == "Numero profil ne correspond pas") {
            $class = "text-danger-2";
        } 
        elseif($this->paiement_status == "error" && (strrpos('Solde incorrect', $this->paiement_status_result) !== false)) {
            $class = "text-danger-3";
        } 
        elseif((strrpos('Erreur de transfert depuis le wallet', $this->paiement_status_result) !== false)) {
            $class = "text-danger-3";
        }
        else {
            $class = "text-warning";
        }

        return $class;
    }

    public function _getTooltipStatut()
    {
        $text = "";
        if ($this->paiement_status == 'success') {
            $text = "Paiement succès";
        } elseif ($this->paiement_status == "warning") {
            $text = "Montant incorrect";
        } elseif($this->paiement_status == "already_paid") {
            $text = "Paiement déjà effectué";
        }elseif($this->paiement_status == "locked_paiement") {
            $text = "Paiement bloqué, déjà existant";
        }elseif($this->paiement_status == "error" && $this->paiement_status_result == "Numero profil ne correspond pas") {
            $text = "Erreur de numéro profil";
        } 
        elseif($this->paiement_status == "error" && (strrpos('Solde incorrect', $this->paiement_status_result) !== false)) {
            $class = "Solde incorrect";
        } 
        elseif((strrpos('Erreur de transfert depuis le wallet', $this->paiement_status_result) !== false)) {
            $text = "Erreur de transfert depuis le wallet";
        }
        else {
            $text = "Problème de paiement";
        }

        return $text;
    }


    public function _getMobile()
    {
        $mvolaSenders = $this->senders = Configure::read('mvolaSenders');
        $orangeSenders = $this->orangeSenders = Configure::read('orangeSenders');
        $airtelSenders = $this->airtelSenders = Configure::read('airtelSenders');

        $mobile = '';
        
        if (in_array($this->sender, $mvolaSenders)) {
            $mobile = 'mvola';
        }
        elseif (in_array($this->sender, $orangeSenders)) {
            $mobile = 'orange';
        }
        elseif (in_array($this->sender, $airtelSenders)) {
            $mobile = 'airtel';
        }

        return $mobile;
    }

    public function _getDiff()
    {
        $now = Chronos::now();
        $diff = $now->diffInMinutes(Chronos::parse($this->senttime));
        return $diff;
    }
}
