<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Relance Entity
 *
 * @property int $id
 * @property string $nom
 * @property string $numero
 * @property string $titulaire
 * @property string $mobile
 * @property \Cake\I18n\FrozenTime $created
 * @property bool $is_sent
 * @property string $type
 */
class Relance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true, 
    ];
}
