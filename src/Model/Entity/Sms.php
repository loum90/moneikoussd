<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sms Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $date
 * @property string $content
 * @property string $numero
 */
class Sms extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];

    public function _getExtractMontant() 
    {
        $datas = [];
        if (strrpos($this->content, 'Votre transfert') !== false) {
            $transfert = $this->extractBetween($this->content, 'Votre transfert de ', ' Ar vers le ');
            $frais = trim(str_replace(':', '', $this->extractBetween($this->content, 'Frais', ' Ar')));
            $nouveauSolde = $this->extractBetween($this->content, 'olde: ', ' Ar. Trans');
            $datas = [
                'transfertAvecFrais' => @($transfert+$frais),
                'nouveauSolde' => $nouveauSolde
            ];
        }
        return $datas;
    }

    public function extractBetween($message, $text1, $text2 = '')
    {
        $part1 = explode($text1, $message)[1] ?? false;
        if ($text2) {
            $text = trim(explode($text2, $part1)[0]);
        } else {
            $text = $part1;
        }
        return $text;
    }
}
