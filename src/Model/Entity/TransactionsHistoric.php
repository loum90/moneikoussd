<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TransactionsHistoric Entity
 *
 * @property int $id
 * @property int $numero
 * @property int $paiement
 * @property int $ref
 * @property \Cake\I18n\FrozenTime $date
 */
class TransactionsHistoric extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];

    public function _getMobileColor()
    {
        $class = '';

        if ($this->paiement == 'vanilla') {
            $class = 'text-info';
        } elseif ($this->paiement == 'mvola') {
            $class = 'text-success';
        } elseif ($this->paiement == 'orange') {
            $class = 'text-warning';
        }

        return $class;
    }
}
