<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AuthSms Entity
 *
 * @property int $id
 * @property string $msg
 * @property string $numero
 * @property string $expediteur
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\User $user
 */
class AuthSms extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'msg' => true,
        'numero' => true,
        'expediteur' => true,
        'user_id' => true,
        'created' => true,
        'user' => true
    ];
}
