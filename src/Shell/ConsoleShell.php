<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\UssdComponent;
use Symfony\Component\Yaml\Yaml;
use Cake\Chronos\Chronos;
use App\Traits\AppTrait;
use Cake\Filesystem\Folder;
use Cake\Routing\Router;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ConsoleShell extends Shell
{

    use AppTrait;

    public function initialize(array $config = []):void
    {
        parent::initialize($config);
        $this->loadModel('Parameters');
        $this->loadModel('Ports');


        $baseUrl = $this->getParam('hook_serveur'); // 'http://127.0.0.1/moneyko';
        $this->urlNotificateMembre = $urlNotificateMembre = $baseUrl.'/exchanger/apis/sendNotifications/';

        Configure::write('pythonPath', $this->getParam('python_path').' '.ROOT.'/bin/');
        // debug(Configure::read('pythonPath'));
        // die();

        $this->httpConfig = ['timeout' => 180, 'ssl_verify_peer' => false, 'ssl_verify_host' => false, 'ssl_verify_peer_name' => false];
        $this->Ussd = new UssdComponent(new ComponentRegistry());
    }

    public function autoMajTunnel()
    {
        $this->loadModel('Tunnels');
        $currentId = $this->Tunnels->find()->where(['is_active' => 1])->first()->id;

        $nextEntity = $this->Tunnels->find()
            ->where(['id >' => $currentId])
            ->orderAsc('id')
            ->first();


        $nextId = $nextEntity ? $nextEntity->id : null;

        if ($nextEntity == null) {
            $nextId = $this->Tunnels->find()->first()->id;
        }

        $this->out('Prochain Token ID : '. $nextId);

        $res = $this->majNgrokToken($nextId);
    }

    public function majNgrokToken($tunnelId)
    {
        $this->loadModel('Tunnels');
        $url = BASEURL.Router::url(['controller' => 'Tdb', 'action' => 'majUrlNgrok'], true);

        $tunnel = $this->Tunnels->findById($tunnelId)->first();

        $winYmlPath = "C:\Users\Loum\AppData\Local\\ngrok\\ngrok.yml";
        $http = new Client();

        //transform $yamlObject to YAML
        if (file_exists($winYmlPath)) {
            $yaml = Yaml::parseFile($winYmlPath, 0, $debug = null);
            $oldAuthToken = $yaml['authtoken'];
        }

        $param = $this->getParam();
        $python = $this->Ussd->pythonDefaultPath;
        $cmd = $python."orange/service_ngrok_restart.py 2>&1";

        $newToken = $yaml['authtoken'] = $tunnel->token;
        $newYaml = Yaml::dump($yaml);
        file_put_contents($winYmlPath, $newYaml);
        $r = $this->Ussd->execCli2($cmd);

        if (strrpos($r, 'Le service ngrok') !== false) {
            $this->out('Redémarrage Ngrok effectué succès');
        }

        $ngRes = $this->readUrl($url);
        $this->out('New token : '.$newToken);

        if (!empty($ngRes)) {

            $ngRes = json_decode($ngRes);
            $this->Tunnels->updateAll(['is_active' => 0], []);
            $this->Tunnels->updateAll(['is_active' => 1], ['token' => $newToken]);

            if (isset($ngRes->server_ngrok)) {
                $param = $this->Parameters->patchEntity($param, ['current_ngrok_url' => $ngRes->server_ngrok], ['validate' => false]);
            }
        } else {


            // 
            $yaml['authtoken'] = $oldAuthToken;
            $old = Yaml::dump($yaml);
            file_put_contents($winYmlPath, $old);
            $this->Ussd->execCli2($cmd);
        }

        $body = ['tunnel_id' => $tunnel->id, 'current_yml' => Yaml::parseFile($winYmlPath, 0, $debug = null)];

        $response = $this->readUrl('http://127.0.0.1:4040/api/tunnels');
        $res = json_decode($response);
        $httpUrl = $res->tunnels[0]->public_url;
        $urlLocal = 'https://'.explode("//", $httpUrl)[1];

        $this->out('urlLocal : '. $urlLocal);
        $urlDistant = $this->getProdParams()->server_ngrok;
        $this->out('urlDistant : '. $urlDistant);

        if ($urlLocal == $urlDistant) {
            $this->out('Processus effectué avec succès');
        } else {
            $this->out('Processus échoué');
        }

        return $body;
    }

    public function getProdParams()
    {
        $http = new Client();
        return (object) $http->get("https://moneiko.net/exchanger/apis/getParams")->json;
    }

    function logginto($string, $file, $erase = false)
    {
        $filename = ROOT.DS.'logs'.DS.$file.'.log';
        new \Cake\Filesystem\File($filename, true, 0777);
        if ($erase == true) {
            file_put_contents($filename, "");
        }
        $file = file_get_contents($filename);
        $newContent = date('Y-m-d H:i:s', time()). ' '. print_r($string, true)."\n";
        $content = $newContent.' ' . $file;
        file_put_contents($filename, $content);
    }

    /**
     * Recursive vérification numero global
     * success Message de succès. Texte vert.
    — error Messages d’Erreur. Texte rouge.
    — warning Messages d’avertissement. Texte jaune.
    — info Messages d’informations. Texte cyan.
    — comment Texte supplémentaire. Texte bleu.
     * exemple andriam.nars
     * cake console sendNotificationsMajNum orange COM30 3383|null 1 5 // recursive mode
     * cake console sendNotificationsMajNum orange COM30 1 3383 // foreach mode
     * @param  string $numero $num
     * @return [type]         videg
     */
    // public function sendNotificationsMajNum($mobile = "orange", $comPort = 'COM36', $userId = null, $essai = 1) // recursive mode
    // λ cake console sendNotificationsMajNum orange com130 350 null
    public function sendNotificationsMajNum($mobile = "orange", $comPort = 'COM36', $nbMembres = 1,  $userId = null, $order = 'ASC') // foreach mode
    {
        $this->Parameters->updateAll(['statut_port_verificateur' => 'success'], ['id' => 1]);

        $essais = 50; // nombre de boucle
        $folder = new Folder();
        $folder->create(ROOT.'/bin/titulaire_log/'.date('Ymd'));

        $http = new Client();
        $now = Chronos::now();
        $numero = "numero_".$mobile;
        $paramSiteLocal = $this->getParam();
        $numPortTitulaire = $paramSiteLocal->num_port_titulaire;

        if (empty($numPortTitulaire)) {
            $this->out('Vérificateur titulaire vide');
            die();
        }

        // $userId = $userId == 'null' ? null : $userId;

        // 1r etape
        $this->info('---------------'.$mobile.'----------------');
        $this->out('Vérification le '.date('d/m/Y H:i'));
        $baseUrl = 'http://127.0.0.1/moneyko';
        $baseUrl = 'https://moneiko.net';
        $this->out('<info>BASE URL :</info> ' . $baseUrl.', COM : '.$comPort. ' Vérificateur : '.$numPortTitulaire);


        $paramsSiteProd = (object) $http->get($baseUrl.'/exchanger/apis/getParams/', [], $this->httpConfig)->json;
        $this->out('-------------------1---------------------');

        // $this->out($fields);
        
        // 2m etape
        $unverifiedUsers = $http->get($baseUrl.'/exchanger/apis/getMembresStatuts/'.$mobile, [], $this->httpConfig)->json;

        foreach ($unverifiedUsers as $key => $unverifiedUser) {
            $this->out($key .' : '. $unverifiedUser);
        }
        $this->out('-------------------2---------------------');

        $fields = ['id', 'nom_complet', 'email', 'counter_allow_sendpinsms_modifnum_'.$numero, 'last_'.$mobile.'_modified_at', $numero];
        $getMembresByNumeroUrl = $baseUrl.'/exchanger/apis/getMembresByNumero/'.$mobile.'/'.$nbMembres.'/'.$userId.'/'.$order;
        // debug($fields);
        // debug($getMembresByNumeroUrl);
        // die();
        $usersEntities = $http->post($getMembresByNumeroUrl, $fields, $this->httpConfig)->json;
        // debug($getMembresByNumeroUrl);
        // debug($usersEntities);
        // die();
        $urlNotificateMembre = $baseUrl.'/exchanger/apis/sendNotifications/';


        // $usersEntities = $http->post($baseUrl.'/exchanger/apis/getUnverifiedMember/'.$mobile.'/'.$userId)->json;
        // $userEntity = $usersEntities['currentUser'];

        if ($usersEntities != null) {
            foreach ($usersEntities as $essai => $userEntity) {

                if (!is_null($userEntity)) {

                    $userNumeroMobile = $userEntity[$numero];
                    // debug($userEntity);
                    // die();

                    $user_id = $userEntity['id'];
                    $counterModifNum = $userEntity['counter_allow_sendpinsms_modifnum_'.$numero];
                    $lastMobileModifiedAt = Chronos::parse($userEntity['last_'.$mobile.'_modified_at']);
                    $lastModifInCurrentMonth = $lastMobileModifiedAt > $now->startOfMonth()  && $lastMobileModifiedAt < $now->endOfMonth();

                    $urlUpdateMembre = $baseUrl.'/exchanger/apis/updateMembre/'.$user_id;


                    if ($this->Ussd->checkMobile2($userNumeroMobile) == $mobile) {
                        $urlTitulaireVerification = 'http://127.0.0.1/moneiko-ussd/ussd/getTitulaireFromNumero/'.$userNumeroMobile.'/'.$comPort.'/'.$mobile;
                        // echo ($user_id);
                        // echo ($urlTitulaireVerification);
                        // die();
                        $result = $http->get($urlTitulaireVerification, [], ['timeout' => 120])->json;
                        // echo ($result);
                        // die();
                        $isAucunCompteMobileMoney = $this->checkSiAucunCompteMobileMoney($result); 
                        // // echo ($user);
                        // // die();
                        $nom_complet = strtoupper($this->normalizeChars($userEntity['nom_complet']));
                        $normalizedNomComplet = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet)));
                        $nom_titulaire = strtoupper($this->normalizeChars($result['nom_titulaire']));


                        if (!empty($nom_titulaire) || $isAucunCompteMobileMoney) {
                            $this->out('Iteration : '.($essai+1));
                            $this->out('ID User : '.$user_id);
                            $this->out('ORDER : '.$order);
                            $this->out('Port : '.$comPort);
                            $this->out('Numéro : '.$userNumeroMobile);
                            $this->out('Nom complet (serveur) : '.$nom_complet);
                            $this->out('Nom titulaire (code) : '.(!empty($nom_titulaire) ? $nom_titulaire : 'Aucun compte Mobile Money souscrit'));
                            // $this->out('Response : '.$result['response']);

                            $explodedNomTitulaire = explode(' ', $nom_titulaire);
                            $explodedNomComplet = explode(' ', $nom_complet);
                            $intersectedResultFromFicheUser = array_filter(array_intersect($explodedNomTitulaire, $explodedNomComplet));
                            $scores = $this->checkCoherence($nom_complet, $nom_titulaire);
                            
                            if (
                                (
                                    (strrpos($nom_complet, $nom_titulaire) !== false) || 
                                    (strrpos($normalizedNomComplet, $nom_titulaire) !== false) ||
                                    $intersectedResultFromFicheUser ||
                                    $this->checkExistingByExplode($nom_complet, $nom_titulaire)
                                ) 
                                && 
                                ($scores->maxScore1 > 70 || $scores->maxScore2 > 80)
                            ) {

                                // CORRESPOND
                                $dataSuccess = [
                                    'is_'.$numero.'_verified' => 1, 
                                    'is_'.$numero.'_finished' => 1,
                                    'titulaire_'.$numero => $nom_titulaire
                                ];

                                if (strlen($nom_complet) > 1) {
                                    $dataSuccess['step_modification_nom'] = 1;
                                }

                                // debug($dataSuccess);
                                $userUpdated = $http->post($urlUpdateMembre, $dataSuccess, $this->httpConfig)->json;
                                $notification = $http->post($urlNotificateMembre, ['msg' => 'Nous vous informons que le numéro '.$userEntity[$numero].'a été vérifié et validé avec succès par notre système et est désormais opérationnel pour toutes les transactions d\'achat ou de dépôt.', 'user_id' => $userEntity['id']], $this->httpConfig)->json;
                                $this->success('Statut : Correspond');

                            } else {
                                
                                // NE CORRESPOND PAS

                                $dataToUpdate = [
                                    'is_'.$numero.'_verified' => 2, // numéro modifié à invalide
                                    'is_'.$numero.'_finished' => 1, 
                                    'titulaire_'.$numero.'' => !empty($nom_titulaire) ? $nom_titulaire : 'Aucun compte Mobile Money souscrit',
                                    'last_'.$mobile.'_modified_at' => $lastMobileModifiedAt->subDays($paramsSiteProd->nb_jours_modifiable_num)->format('Y-m-d H:i:s'), // 7 tokony hitovy amle config ary am prod app_config nbJoursModifiableNum, le delai jour fanovana numero
                                ];

                                if ($counterModifNum > 2 && $lastModifInCurrentMonth) {
                                    $dataToUpdate['is_'.$numero.'_verified'] = 3; // numéro modifié à suspendu
                                    $http->post($urlNotificateMembre, ['msg' => "Vous avez effectué des modifications de numéros trop fréquentes récemment, les paiements à partir de votre numéro ".ucfirst($mobile)." sur le site sont temporairement suspendus par mesure de sécurité cf Art CGU 5.3", 'user_id' => $user_id], $this->httpConfig)->json;
                                }

                                $userUpdated = $http->post($urlUpdateMembre, $dataToUpdate, $this->httpConfig)->json;


                                $emailUrl = $baseUrl.'/exchanger/apis/sendEmailToVerifyNumber/';
                                $dataToSend = [
                                    'email' => $userEntity['email'],
                                    'numero' => $userEntity[$numero],
                                ];
                                $http->post($emailUrl, $dataToSend, $this->httpConfig)->json;
                                // $this->out($dataToSend);
                                
                                $notification = $http->post($urlNotificateMembre, ['msg' => 'Nous avons détecté que le propriétaire du numéro '.$userEntity[$numero].' ne correspond pas au profil de votre carte d\'identité nationale. Nous vous prions de mettre à jour votre numéro '.ucfirst($mobile).' avec votre nom complet (nom et prénom(s) cf CIN) en cliquant <a target="_blank" href="https://moneiko.net/modifier-numero-'.$mobile.'">ici</a>, cf. Art GCU 3.1!', 'user_id' => $userEntity['id']], $this->httpConfig)->json;
                                $this->out('Statut : <warning>Ne correspond pas, demande de mise à jour infos (Nom complet et numéro)</warning>');
                                if (!is_array($notification)) {
                                    $this->out('Notification : '. $notification);
                                }

                                
                                // NE CORRESPOND PAS OU AUCUN COMPTE MOBILE MONEY
                                // $checkInfosUrl = $baseUrl.'/exchanger/apis/checkTitulaireByNumeroByVentes/'.$user_id.'/'.$userNumeroMobile.'/'.$nom_titulaire;
                                // $checkInfosUrlResult = $http->get($checkInfosUrl)->json;
                                // if ($checkInfosUrlResult['is_intersected_names'] == false) {
                                // } else {

                                //     // CORRESPOND MAIS PAR VENTES
                                //     $userUpdated = $http->post($urlUpdateMembre, [
                                //         'nom_complet' => $checkInfosUrlResult['nom_complet_maj'],
                                //         'is_'.$numero.'_verified' => 1, 
                                //         'is_'.$numero.'_finished' => 1, 
                                //         'titulaire_'.$numero.'' => $nom_titulaire,
                                //     ])->json;
                                //     $this->out('Statut : <warning>Ne correspond pas mais modification du nom ajusté à partir des infos de ventes</warning>');

                                // }                        
                            }
                            $userUrl = $baseUrl.'/exchanger/manager/forcelogin/'.$user_id;
                            $this->out('Lien user : '.$userUrl);
                        } else {

                            // GESTION AUCUN COMPTE OU EXPIRÉ
                            if ($isAucunCompteMobileMoney) {
                                $this->out('<error>Pas de compte mobile money '.ucfirst($mobile).' : '.$userNumeroMobile.'</error>');
                            } else {
                                $this->out('<error>GSM Bug</error>');
                                $this->out('ID User : '.$user_id);
                                $this->out('<error>'.$urlTitulaireVerification.'</error>');
                                $this->Parameters->updateAll(['statut_port_verificateur' => 'error'], ['id' => 1]);
                                $this->generateFreePortComVerificateur();
                            }
                        }
                    } else {
                        $userUpdated = $http->post($urlUpdateMembre, [
                            'is_'.$numero.'_verified' => 2, // numéro modifié à invalide
                            'is_'.$numero.'_finished' => 1, 
                            'titulaire_'.$numero.'' => 'Numéro incorrect',
                            'last_'.$mobile.'_modified_at' => $now->subDays(30)->format('Y-m-d H:i:s'),
                        ], $this->httpConfig)->json;
                        $this->out(print_r('ID User : '.$user_id, true));
                        $this->out('Statut : <warning>Numéro opérateur incorrect</warning>');
                    }

                    $this->info('------------------Done------------------');

                    // if (!is_null($usersEntities['nextUser'])) {
                    //     $nextUserId = $usersEntities['nextUser']['id'];
                    //     if ($essai < $essais) {
                    //         $this->sendNotificationsMajNum($mobile, $comPort, $nextUserId, ++$essai);
                    //     }
                    // } else {
                    //     $this->abort('Plus de next User');
                    // }
                    
                }
            }
        } else {
            $this->out('<warning>Aucun utilisateur</warning>');
        }

        $this->out('fin');
        die();
    }

    public function generateFreePort()
    {
        $this->generateFreePortComVerificateur();
    }

    public function checkSiAucunCompteMobileMoney($result)
    {
        $isAucunCompteMobileMoney =
            (strrpos($result['response'], 'transfert a echoue') !== false) || 
            (strrpos($result['response'], 'bénéficiaire que vous avez saisi est différent') !== false) || // Orange io
            (strrpos($result['response'], 'votre bénéficiaire ne peut pas recevoir ce transfert') !== false) || // Mvola io
            (strrpos($result['response'], 'Nous vous prions de verifier le numero') !== false) // Airtel io
        ; 

        return $isAucunCompteMobileMoney;
    }

    public function sendAllNotificationMajNum($com = '130')
    {
        $operateurs = Configure::read('operateurs');

        foreach ($operateurs as $key => $operateur) {
            $this->sendNotificationsMajNum($operateur, $com, 350, null);
        }

    }

    /**
     * Récursive exemple
     * @param  [type]  $mobile [description]
     * @param  integer $essai  [description]
     * @param  [type]  $userId [description]
     * @return [type]          [description]
     */
    public function runMe($mobile, $essai = 0, $userId = null)
    {
        $baseUrl = 'http://127.0.0.1/moneyko';
        $http = new Client();

        $this->out($essai);
        $user = $http->post($baseUrl.'/exchanger/apis/getUnverifiedMember/'.$mobile.'/'.$userId)->json;
        $this->out($user['currentUser']['nom_complet']);

        $nextUser = $user['nextUser'];
        $this->out($user['nextUser']['nom_complet']);
        $this->hr();

        $essais = 25;

        $nextUserId = $user['nextUser']['id'];
        if ($essai < $essais) {
            $this->runMe($mobile, ++$essai, $nextUserId);
        }
    }

    public function getPortList()
    {
        $http = new Client();
        
        $portLists = $http->get('http://127.0.0.1/moneiko-ussd/tdb/getPortList');
        // $this->out($portLists->json);

        foreach ($portLists->json as $key => $port) {
            $com = $this->Ussd->extractBetween(trim($port), ' (', ')');
            $coms[] = $com;
        }
        $this->out($coms);
    }

    public function verificationTitulaireTelma($userId = null)
    {
        $http = new Client();
        $baseUrl = 'http://127.0.0.1/moneyko';
        $baseUrl = 'https://moneiko.net';
        $numero = "numero_telma";
        $now = Chronos::now();
        $senders = Configure::read('mvolaSenders');
        $mobile = 'telma';
        $this->out('<info>BASE URL :</info> ' . $baseUrl);

        $this->out('-------------------1---------------------');
        // $unverifiedUsers = $http->get($baseUrl.'/exchanger/apis/getMembresStatuts/'.$mobile)->json;

        // foreach ($unverifiedUsers as $key => $unverifiedUser) {
        //     $this->out($key .' : '. $unverifiedUser);
        // }
        // $this->out('-------------------2---------------------');

        // die();

        $urlNotificateMembre = $baseUrl.'/exchanger/apis/sendNotifications/';
        $getMembresByNumeroUrl = $baseUrl.'/exchanger/apis/getMembresByTelma/'.$userId;

        $usersEntities = $http->post($getMembresByNumeroUrl)->json;
        if (!is_null($usersEntities)) {
            foreach ($usersEntities as $essai => $user) {
                $user_id = $user['id'];
                $userNumeroMobile = $user[$numero];
                $numeroTelma = trim($user['numero_telma']);
                $this->loadModel('Ozekimessageins');
                $sms = $this->Ozekimessageins->find()->where(['msg LIKE' => "%$numeroTelma%", 'operator !=' => 'ONEX', 'sender in' => $senders])->first();
                
                if ($sms != null) {
                    // pr($user);
                    // pr($sms);

                    $dataMvola = $this->Ussd->extractMvolaRecuData($sms->msg);
                    // debug($sms->id);
                    // die();

                    $idpanierUrl = $baseUrl.'/exchanger/apis/findIdpanier/'.$dataMvola['idpanier'];
                    $idpanier = $http->post($idpanierUrl)->json;
                    $userFromIdpanier = $idpanier['user']['nom_complet'];
                    $userIdFromPanier = $idpanier['user']['id'];
                    $isUserEmpty = is_null($idpanier['user']);
 
                    $nom_complet_from_idpanier = trim(strtoupper($this->normalizeChars($userFromIdpanier)));
                    $nom_titulaire = trim(strtoupper($this->normalizeChars($dataMvola['titulaire'])));
                    $nom_complet = trim(strtoupper($this->normalizeChars($user['nom_complet'])));
                    $normalizedNomCompletFromPanier = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet_from_idpanier)));
                    $normalizedNomComplet = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet)));

                    
                    $this->out('Iteration : '.($essai+1));
                    $this->out(print_r('ID User : '.$user_id, true));
                    $this->out(print_r('ID Panier : '.$idpanier['id'], true));
                    $this->out('Numéro : '.$userNumeroMobile);
                    $this->out('Nom complet (serveur) : '.$nom_complet);
                    $this->out('Nom complet (lié panier serveur) : '.(!empty($nom_complet_from_idpanier) ? $nom_complet_from_idpanier : '<error>Vide</error>'));
                    $this->out('Nom titulaire ('.$userNumeroMobile.') : '.(!empty($nom_titulaire) ? $nom_titulaire : 'Aucun compte Mobile Money souscrit'));

                    // $unverifiedUsers = $http->get($baseUrl.'/exchanger/apis/getMembresStatuts/'.$mobile)->json;

                    // foreach ($unverifiedUsers as $key => $unverifiedUser) {
                    //     $this->out($key .' : '. $unverifiedUser);
                    // }

                    // debug($dataMvola);
                    // debug($idpanier);
                    // debug($nom_complet_from_idpanier);
                    // debug($nom_titulaire);
                    // debug($nom_complet);

                    $explodedNomTitulaire = explode(' ', $nom_titulaire);

                    // Explode à partir du nom fiche user de l'idpanier
                    $explodednormalizedNomCompletFromPanier = explode(' ', $nom_complet_from_idpanier);
                    $intersectedResultFromFichePanier = array_filter(array_intersect($explodedNomTitulaire, $explodednormalizedNomCompletFromPanier));

                    // Explode à partir du nom fiche user
                    $explodedNomComplet = explode(' ', $nom_complet);
                    $intersectedResultFromFicheUser = array_filter(array_intersect($explodedNomTitulaire, $explodedNomComplet));

                    $urlUpdateMembre = $baseUrl.'/exchanger/apis/updateMembre/'.$user_id;
                    if (
                        (strrpos($nom_complet_from_idpanier, $nom_titulaire) !== false) || 
                        (strrpos($normalizedNomCompletFromPanier, $nom_titulaire) !== false) || 
                        // $intersectedResultFromFicheUser ||
                        $intersectedResultFromFichePanier
                    ) {

                        // Correspond
                        $userUpdated = $http->post($urlUpdateMembre, [
                            'is_'.$numero.'_verified' => 1, 
                            'is_'.$numero.'_finished' => 1]
                        )->json;
                        $this->success('Statut : Correspond');

                        $notification = $http->post($urlNotificateMembre, ['msg' => '', 'user_id' => $user_id])->json;

                    } else {

                        if (!$isUserEmpty) {

                            // Ne correspond pas

                            //  1 - Mise à jour profil numero
                            $dataToUpdate = [
                                'is_'.$numero.'_verified' => 2, // numéro modifié à invalide
                                'is_'.$numero.'_finished' => 1, 
                                'titulaire_'.$numero.'' => $nom_titulaire,
                                // 'last_'.$mobile.'_modified_at' => $now->subDays(30)->format('Y-m-d H:i:s'), Tsy reinitialisena tsony 
                            ];

                            $userUpdated = $http->post($urlUpdateMembre, $dataToUpdate)->json;

                            $emailUrl = $baseUrl.'/exchanger/apis/sendEmailToVerifyNumber/';
                            $dataToSend = [
                                'email' => $user['email'],
                                'numero' => $user[$numero],
                            ];
                            $http->post($emailUrl, $dataToSend)->json;
                            // $this->out($dataToSend);
                            
                            $urlNotificateMembre = $baseUrl.'/exchanger/apis/sendNotifications/';
                            $msg = 'Nous avons détecté que le propriétaire du numéro '.$user[$numero].' ne correspond pas à votre profil. Nous vous prions de mettre à jour votre numéro Telma avec votre nom complet (nom et prénom(s) cf CIN) en cliquant <a target="_blank" href="https://moneiko.net/modifier-numero-'.$mobile.'">ici</a>, cf. Art GCU 3.1!';
                            $this->logginto($msg, 'email/verification');


                            $notification = $http->post($urlNotificateMembre, ['msg' => $msg, 'user_id' => $userIdFromPanier])->json;
                            $this->out('Statut : <warning>Ne correspond pas, demande de mise à jour infos (Nom complet et numéro)</warning>');
                            if (!is_array($notification)) {
                                $this->out('Notification : '. $notification);
                            }
                            
                        } else {
                            $this->out('<warning>Statut lié à la commande du membre introuvable</warning>');
                        }
                                    
                    }
                    $userUrl = $baseUrl.'/exchanger/manager/forcelogin/'.$user_id;
                    $this->out('Lien user : '.$userUrl);

                    // pr($nom_titulaire);
                    // pr($nom_complet);

                    // die;
                }
                $this->out('------------------Done-------------------');
            }


        }
    }

    public function mapUsbGsm()
    {

        // $r = $this->Ussd->resetUsb('I:,P:');
        // debug($r);
        // die();
        // $gsmList = $this->Ussd->getUsbGsmList();
        $gsmList = [
            'Q:', 
            'V:', 
            // 'H:', 
            // 'R:,S:'
            // 'L:,N:'
        ];
        $nbGsmList = count($gsmList);

        foreach ($gsmList as $key => $gsmLetter) {

            $checkGsm = exec(USBSAFELYREMOVE.'usr.exe wholocks -d '.$gsmLetter);

            if (strrpos($checkGsm, 'Perhaps the device is ready for ejecting now') !== false) {
                $ex = exec(USBSAFELYREMOVE.'usr.exe forcedstop -d '.$gsmLetter);
                sleep(1);
                $ports = json_decode($this->getPortListDatas());
                $missingsPort = $this->Ports->find()->where(['com not in' => $ports])->first();
                debug($missingsPort);
                if ($missingsPort) {
                    $this->Ports->updateAll(['letter' => $gsmLetter], ['id' => $missingsPort]);
                }
                sleep(1);
                $res = exec(USBSAFELYREMOVE.'usr.exe forcedreturn -d '.$gsmLetter);
                debug($ex.' '.$res);
            }

            if ($key+1 < $nbGsmList) {
                sleep(1.5);
            }
        }
        
        die();
    }

    /**
     * manager/listUsersActif() dans la prod
     */
    public function statrelancesms()
    {
        $this->loadModel('Relances');

        // $membres = json_decode(file_get_contents(ROOT.'/patch/mpanao_retrait.json'), true);
        // foreach ($membres as $key => $membre) {
        //     if (!$this->Relances->findByNumero($membre['numero'])->first()) {
        //         $relance = $this->Relances->newEntity($membre);
        //         $this->Relances->save($relance);
        //     }
        // }
        // die();

        $relancesTotal = $this->Relances->find();
        $relancesTotal->select(['mobile', 'count' => $relancesTotal->func()->count('*')])->group('mobile');
        $relancesUnsent = $this->Relances->find()->where(['is_sent' => 0]);
        $relancesUnsent->select(['mobile', 'count' => $relancesUnsent->func()->count('*')])->group('mobile');
        $relancesSent = $this->Relances->find()->where(['is_sent' => 1]);
        $relancesSent->select(['mobile', 'count' => $relancesSent->func()->count('*')])->group('mobile');

        foreach ($relancesTotal as $groupe) {
            $this->out('Total '.$groupe->mobile.' : '.$groupe->count);
        }

        $this->hr();

        foreach ($relancesUnsent as $groupe) {
            $this->out('Total unsent '.$groupe->mobile.' : '.$groupe->count);
        }

        $this->hr();

        foreach ($relancesSent as $groupe) {
            $this->out('Total sent '.$groupe->mobile.' : '.$groupe->count);
        }


        $this->hr();

        die();
    }

    public function lancerelancesms($limit = 99999)
    {
        $this->loadModel('Relances');
        $relances = $this->Relances->find()->where(['is_sent' => 0, 'type' => 'retrait']);
        $i = 1;
        foreach ($relances as $key => $relance) {

            if ($i <= $limit) {
                $string = $relance->nom;
                $numero = $relance->numero;
                $mobile = $relance->mobile;
                $pieces = explode(' ', $string);
                $lastName = ucfirst(strtolower($this->normalizeChars(array_pop($pieces))));

                $this->Ussd->setPort($this->getParam('port_sms_out_'.$mobile));

                // echo $relance->nom;

                // $lastName = 
                // debug(date('H'));
                if (date('H') > 06 && date('H') < 18) {
                    $salutation = 'Bjr';
                } else {
                    $salutation = 'Bsr';
                }

                // $numero = "0325541393";
                $contenu = "Saika ilaza fotsiny hoe efa mande ny fanovana retrait AIRTM instantane ato amin'ny moneiko, C 4450 Ar. merci :)";
                $msg = $salutation.' '.$lastName.'. '.$contenu;

                $msg = $this->normalizeChars($msg);
                $this->out($i.' eme sms');
                $this->out($numero);
                $this->out($msg);
                $this->hr();

                $res = $this->Ussd->send($numero, $msg, $flashmode = false);
                sleep(2);
                $suite = "Raha nahafapo anao ny nanao retrait tato amin moneiko.net/retrait-airtm, dia misokatra ho anao foana izany :), Admin Sandy Moneiko. Bonne journee";
                $this->out($suite);
                $res = $this->Ussd->send($numero, $suite, $flashmode = false);
                $this->Relances->updateAll(['is_sent' => 1], ['id' => $relance->id]);

                sleep(2);


                ++$i;
                // die();
            }
        }

        die();
    }

    public function sendRelanceSms(/*$limit = 99999*/ $option = null)
    {
        $this->loadModel('RelancesSmss');
        $relances = $this->RelancesSmss->find()->where([
            'is_sent' => 0,
            'numero_telma IS NOT' => NULL,
            'numero_orange IS NOT' => NULL,
            'numero_airtel IS NOT' => NULL,
        ]);
        $countTelma = $relances->match(['is_sent' => 0, 'mobile' => 'telma'])->count();
        $countOrange = $relances->match(['is_sent' => 0, 'mobile' => 'orange'])->count();
        $countAirtel = $relances->match(['is_sent' => 0, 'mobile' => 'airtel'])->count();
        
        if ($option == 'stat') {
            $this->out('Stats :');
            $this->out('Nb Telma: ' . ' '. $countTelma);
            $this->out('Nb Orange: ' . ' '. $countOrange);
            $this->out('Nb Airtel: ' . ' '. $countAirtel);
            $this->hr();
            die();
        }

        if (in_array($option, ['telma', 'orange', 'airtel'])) {
            $relances = $relances->match(['is_sent' => 0, 'mobile' => $option]);
        }

        $i = 1;
        foreach ($relances as $key => $relance) {

            // if ($i <= $limit) {
                $lastName = $relance->get('Prenom');
                $numero = $relance->get('Numero');
                $mobile = $relance->get('Mobile');

                $this->Ussd->setPort($this->getParam('port_sms_out_'.$mobile));

                // echo $relance->nom;

                // $lastName = 
                // debug(date('H'));
                if (date('H') > 06 && date('H') < 18) {
                    $salutation = 'Bjr';
                } else {
                    $salutation = 'Bsr';
                }

                // $numero = "0325541393";
                $contenu = $this->getParam('msg_relance');
                $msg = $salutation.' '.$lastName.'. '.$contenu;

                if (strlen($msg) > 160) {
                    $msg = $salutation.' '.$contenu;
                }


                $msg = $this->normalizeChars($msg);
                $this->out($i.' eme sms');
                $this->out($numero);
                $this->out($msg);
                $this->out(strlen($msg).' mots');
                $this->hr();

                // $res = $this->Ussd->send($numero, $msg, $flashmode = false);
                // $this->Relances->updateAll(['is_sent' => 1], ['id' => $relance->id]);

                // sleep(2);


                ++$i;
                die();
            // }
        }

        die();
    }

    public function rechargeForfaitSms($mobile = "telma")
    {
        // 'is_recharge_auto_forfait_telma' => '',
        // 'is_recharge_auto_forfait_orange' => '',
        // 'is_recharge_auto_forfait_airtel' => '',

        $param = $this->getParam();

        if ($param->{'is_recharge_auto_forfait_'.$mobile}) {
            $portTelmaSms = $param->port_sms_out_telma;
            if (!empty($portTelmaSms)) {
                $url = BASEURL.Router::url(['controller' => 'Ussd', 'action' => 'rechargeForfait', $mobile]);
                $http = new Client();
                $res = $http->get($url)->json;
                $this->logginto($res, 'console/rechargeForfaitSms');
                $this->out($res);
            }
        } else {
            $this->out('Option recharge auto sms désactivé '.$mobile);
        }
    }
    
    // public function sendNotificationsMajNum($mobile = "orange", $comPort = 'COM36', $userId = null, $essai = 1) // recursive mode
    // λ cake console sendNotificationsMajNum orange com130 350 null
    public function reverificationsNumeroKyc() // foreach mode
    {
        // debug($this->urlNotificateMembre);
        // die();

        $essais = 50; // nombre de boucle
        $folder = new Folder();
        $folder->create(ROOT.'/bin/titulaire_log/'.date('Ymd'));

        $http = new Client();
        $now = Chronos::now();
        $paramSiteLocal = $this->getParam();
        $numPortTitulaire = $paramSiteLocal->num_port_titulaire;
        $comPort = $paramSiteLocal->port_titulaire;

        if (empty($numPortTitulaire)) {
            $this->out('Vérificateur titulaire vide');
            die();
        }


        // $userId = $userId == 'null' ? null : $userId;

        // 1r etape
        $this->out('Vérification le '.date('d/m/Y H:i'));

        $baseUrl = $this->getParam('hook_serveur'); // 'http://127.0.0.1/moneyko';
        // $baseUrl = 'https://moneiko.net';
        $this->out('<info>BASE URL :</info> ' . $baseUrl.', COM : '.$comPort. ' Vérificateur : '.$numPortTitulaire);


        $verifications = (object) $http->get($baseUrl.'/exchanger/apis/getReverificationsNumerosKyc/', [], $this->httpConfig)->json;


        foreach ($verifications as $key => $verification) {

            $mobile = $verification['mobile'];
            $userEntity = (object) $verification['user'];

            if (isset($userEntity->id)) {
                
                $this->out('User ID : '.$userEntity->id.', '.$userEntity->nom_complet);
                $user_id = $userEntity->id;
                $userNumeroMobile = $numero = $userEntity->{'numero_'.$mobile};


                $urlTitulaireVerification = 'http://127.0.0.1/moneiko-ussd/ussd/getTitulaireFromNumero/'.$userNumeroMobile.'/'.$comPort.'/'.$mobile;
                $result = $http->get($urlTitulaireVerification, [], ['timeout' => 120])->json;
                $isAucunCompteMobileMoney = $this->checkSiAucunCompteMobileMoney($result); 

                $nom_complet = strtoupper($this->normalizeChars($userEntity->nom_complet));
                $normalizedNomComplet = strtoupper(str_replace(" ", "", $this->normalizeChars($nom_complet)));
                $nom_titulaire = strtoupper($this->normalizeChars($result['nom_titulaire']));

                $urlUpdateMembre = $baseUrl.'/exchanger/apis/updateMembre/'.$user_id;


                if (!empty($nom_titulaire) || $isAucunCompteMobileMoney) {
                    // debug(!empty($nom_titulaire));
                    // debug($isAucunCompteMobileMoney);
                    // debug(!empty($nom_titulaire) || $isAucunCompteMobileMoney);
                    // die();

                    $resultChecked = $this->checkFinalNumero2($nom_titulaire, $nom_complet);
                    // debug($resultChecked);
                    // die();
                    if ($resultChecked['result'] == true) {

                        // CORRESPOND

                        // 1 Mise à jour compte numéro
                        $dataSuccess = [
                            'is_'.$numero.'_verified' => 1, 
                            'is_'.$numero.'_finished' => 1,
                            'titulaire_'.$numero => $nom_titulaire
                        ];
                        $userUpdated = $http->post($urlUpdateMembre, $dataSuccess, $this->httpConfig)->json;
                        $this->success('Statut : Màj statut numéro OK');
                        
                        // 2 Mise à jour statut verification KYC / Numéro
                        $dataVerification = [
                            'is_traitee' => 1, 
                            'is_correct' => 1,
                            'nouveau_titulaire' => $nom_titulaire,
                            'score_1' => $resultChecked['scores']->maxScore1,
                            'score_2' => $resultChecked['scores']->maxScore2,
                            'score_next_1' => $resultChecked['score_next']->maxScore1,
                            'score_next_2' => $resultChecked['score_next']->maxScore2,
                        ];
                        $urlUpdateVerification = $baseUrl.'/exchanger/apis/updateVerification/'.$verification['id'];
                        $verificationUpdated = $http->post($urlUpdateVerification, $dataVerification, $this->httpConfig)->json;
                        $this->success('Statut : Màj Verification '.$numero.' OK');

                        // 3 Envoi notification
                        $notification = $http->post($this->urlNotificateMembre, ['msg' => 'Suite à votre demande de re-vérification, votre numéro '.ucfirst($mobile).' est désormais vérifié, vous êtes autorisé(e) à effectuer des opérations de dépôts sur le site !', 'user_id' => $userEntity->id], $this->httpConfig)->json;

                        // 4 Envoi email
                        $completeUrl = $baseUrl.'/exchanger/apis/sendEmailForNumeroVerificationSuccess/'.$verification['id'];
                        $email = $http->get($baseUrl.'/exchanger/apis/sendEmailForNumeroVerificationSuccess/'.$verification['id'], [], $this->httpConfig)->json;
                        $this->out($completeUrl);
                        // $this->out($email['message']);


                        // Envoi email pour dire que la vérification est réussie                    
                        
                    } else {

                        // NE CORRESPOND PAS

                        //  1 - Mise à jour profil numero
                        
                        $dataToUpdate = [
                            'is_'.$numero.'_verified' => 2, // numéro modifié à KYC invalide
                            'is_'.$numero.'_finished' => 1, 
                            'titulaire_'.$numero.'' => !empty($nom_titulaire) ? $nom_titulaire : 'Aucun compte Mobile Money souscrit',
                        ];

                        $userUpdated = $http->post($urlUpdateMembre, $dataToUpdate, $this->httpConfig)->json;
                        // $this->out('Ne correspond pas');

                        // 2 Mise à jour statut verification KYC / Numéro
                        $dataVerification = [
                            'is_traitee' => 1, 
                            'is_correct' => 0,
                            'nouveau_titulaire' => $nom_titulaire,
                            'score_1' => $resultChecked['scores']->maxScore1,
                            'score_2' => $resultChecked['scores']->maxScore2,
                            'score_next_1' => $resultChecked['score_next']->maxScore1,
                            'score_next_2' => $resultChecked['score_next']->maxScore2,
                        ];
                        $urlUpdateVerification = $baseUrl.'/exchanger/apis/updateVerification/'.$verification['id'];
                        $verificationUpdated = $http->post($urlUpdateVerification, $dataVerification, $this->httpConfig)->json;
                        $this->out('<error>Statut : Màj statut numéro '.$numero.' Invalide</error>');
                        $this->success('Statut : Màj Verification OK');


                        // 3 Envoi notification
                        $notification = $http->post($this->urlNotificateMembre, ['msg' => 'Suite à votre demande de re-vérification, votre numéro '.ucfirst($mobile).' ne semble pas appartenir à l\'identité du propriétaire de votre compte moneiko ou alors notre système n\'a pas pu reconnaître la concordance des informations d\'identité de votre carte SIM et de votre pièce d\'identité! Veuillez <a href="'.$this->getLinkMessenger().'">nous contactez</a> pour vérifier et mettre à jour ce numéro afin de pouvoir effectuer les opérations d\'achat (dépôt) sur le site !', 'user_id' => $userEntity->id], $this->httpConfig)->json;

                        // 4 Envoi email
                        $completeUrl = $baseUrl.'/exchanger/apis/sendEmailForNumeroVerificationFailed/'.$verification['id'];
                        $this->out($completeUrl);
                        $email = $http->get($completeUrl, [], $this->httpConfig)->json;
                        // $this->out($email['message']);


                        // Envoi email pour dire que ça ne correspond pas

                    }
                } else {
                    die('ouin');
                }

                // die();
            }
        }
        
    }

    public function checkAndFixCurruptTable()
    {
        $cm = new \Cake\Datasource\ConnectionManager;
        $tables = $cm::get('default')->getSchemaCollection()->listTables();

        foreach ($tables as $key => $table) {
            $sql = "CHECK TABLE `$table`";
            $checked = $cm::get('default')->execute($sql);
            $result = $checked->fetch();
            if ($result[2] == "warning") {
                $cm::get('default')->execute("REPAIR TABLE `$table`");
                $this->logginto("REPAIR TABLE `$table`", 'repair_table');
            }
        }

        // $patch = file(ROOT.'/patch/patch.sql');
        // foreach ($patch as $key => $line) {
        //     debug($line);
        // }
        // die();
        // $cm::get('default')->execute($patch);

        $this->out('fin');
    }

}
