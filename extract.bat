@echo off

set "fileToCheck=D:\wamp\www\moneiko-ussd\wget.exe"
set "thresholdSize=5000000"  REM Adjust the threshold size as needed
set "zipFile=D:\wamp\www\moneiko-ussd\wget.zip"
set "extractTo=D:\wamp\www\moneiko-ussd"

for %%I in ("%fileToCheck%") do (
    set "fileSize=%%~zI"
)

if %fileSize% gtr %thresholdSize% (
    echo Taille du fichier %fileSize%
    echo Fichier est plus grand que %thresholdSize% bytes. Extraction executée !
    7za x -aoa "%zipFile%" -o"%extractTo%"
) else (
    echo Taille du fichier %fileSize%
    echo Le fichier n'est pas plus grand que %thresholdSize% bytes. Extraction non exécutée !
)

rem pause
