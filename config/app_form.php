<?php 

return [
    'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
    'inputContainer' => '<div class="form-group">{{content}}</div>',
    'select' => '<select name="{{name}}" class="form-control" {{attrs}}>{{content}}</select>',
    'button' => '<div class="form-group"><button{{attrs}} class="btn btn-primary">{{text}}</button></div>',
    'selectMultiple' => '<select name="{{name}}[]" class="form-control select2_multiple" multiple="multiple"{{attrs}}>{{content}}</select>',
    // 'checkbox' => '<input type="checkbox" name="{{name}}" class="flat" value="{{value}}"{{attrs}}>',
    'label' => '<label{{attrs}} class="control-label w-100">{{text}}</label>',
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}" class="custom-control-input" {{attrs}}>',
    'nestingLabel' => '{{hidden}}<label{{attrs}} class="custom-control custom-checkbox">{{input}}<span class="custom-control-label">{{text}}</span></label>',        
    'radioWrapper' => '{{label}}',
    'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    'error' => '<div class="alert alert-danger mt-4"><i class="fa fa-1x fa-exclamation-triangle"></i> {{content}}</div>',
];