<?php 
return [
    'usbSafetyRemovePath' => '"C:\\Program Files (x86)\\USB Safely Remove\\"',
    // 'pythonPath' => 'C:\Users\Loum\AppData\Local\Programs\Python\Python38\python.exe '.ROOT.'/bin/',
    'driverOrange' => 'I:',
    'driverTelma' => 'N:',
    'driverTelmaSms' => 'I:',
    'callback_mode' => 'prod', // local ou prod // MODE URL ANTSOINA REFA MANAO MAJ anle transaction
    'callback_url' => [
        'prod' => [
            'urlToWeb' => "https://moneiko.net/ussd-retrait",
            'urlCallback' => "https://moneiko.net/ussd-maj-retrait/",
            'urlMajRefTransMobileFromUssdLocal' => "https://moneiko.net/ussd-maj-transaction-ref-mobile",
            'urlCheckVenteStatus' => "https://moneiko.net/ussd-recheck",
        ],
        'local' => [
            'urlToWeb' => "http://127.0.0.1/moneyko/ussd-retrait",
            'urlCallback' => "http://127.0.0.1/moneyko/ussd-maj-retrait/",
            'urlMajRefTransMobileFromUssdLocal' => "http://127.0.0.1/moneyko/ussd-maj-transaction-ref-mobile",
            'urlCheckVenteStatus' => "http://127.0.0.1/moneyko/ussd-recheck",
        ]
    ],
    'orange_montant_limite_ariary' => 4000000, // Ariary
    'mvola_montant_limite_ariary' => 4000000, // Ariary,
    'airtel_montant_limite_ariary' => 4000000, // Ariary,
    'smsType' => [
        '1' =>  'btc',
        '2' =>  'payeer',
        '3' =>  'advcash',
        '4' =>  'pm',
    ],
    'tarifMvola' => [
        50 => [100, 5000],
        100 => [5001, 10000],
        200 => [10001, 25000],
        400 => [25001, 50000],
        800 => [50001, 100000],
        1500 => [100001, 500000],
        2500 => [500001, 1000000],
        3000 => [1000000, 10000000]
    ],
    'telma_numero' => [
        '0347085930' => '0347085930', '0341035571' => '0341035571'
    ],
    'mvolaSenders' => ['MVola', '+261341035571', '+261347085930'],
    'airtelSenders' => ['AirtelMoney', '+261337590891', '+261338524425', '+261334093330'],
    'orangeSenders' => ['OrangeMoney', '+261325541393', '+261321490606'],
    'limit_solde' => '3000000',
    'num_orange' => [
        '0325541393' => '0325541393',
        '0321490606' => '0321490606',
        '0327232084' => '0327232084',
        '0329965466' => '0329965466',
        '0329965433' => '0329965433',
        '0321702119' => '0321702119',
    ],
    'assets' => [
        'BTC' => 'BTC',
        'BCH' => 'BCH',
        'ETH' => 'ETH',
        'LTC' => 'LTC',
        'DOGE' => 'DOGE',
        'TRX' => 'TRX',
        'USDT' => 'USDT',
        'BNB' => 'BNB',
        'DASH' => 'DASH',
    ],
    'operateurs' => ['telma', 'orange', 'airtel'],
    'operateurs_fronts' => [
        [
            'name' => 'telma',
            'label' => 'Telma',
            'class' => 'text-success'
        ],
        [
            'name' => 'orange',
            'label' => 'Orange',
            'class' => 'text-warning'
        ],
        [
            'name' => 'airtel',
            'label' => 'Airtel',
            'class' => 'text-danger'
        ],
    ],
    'mobile' => [
        [
            'name' => 'mvola',
            'label' => 'mvola',
            'class' => 'text-success'
        ],
        [
            'name' => 'orange',
            'label' => 'Orange',
            'class' => 'text-warning'
        ],
        [
            'name' => 'airtel',
            'label' => 'Airtel',
            'class' => 'text-danger'
        ],
    ],
    'exclusion_sms_merci_depots' => ['0347436670', '0349895029'],
    'liste_achat_fiat' => [
        'skrill',
        'binance',
        'airtm',
        'betwinner',
        '1xbet',
        'megaparis',
        '1win',
        'deriv',
        'neteller',
        'okx',
        'redotpay',
    ],
    'eppelations' => [
        'airtm' => 'airtième',
        'skrill' => 'skrill',
        'binance' => 'baillnince',
        '1xbet' => 'One Xbet',
        'betwinner' => 'Bètwinneur',
        'megaparis' => 'mégapari',
        '1win' => 'one win',
        'deriv' => 'dérive',
        'okx' => 'okx',
        'redotpay' => 'rédotpay',
        'mvola' => 'Mvoul'
    ],
];