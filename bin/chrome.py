import pygetwindow as gw
import pyautogui
import time
import pyperclip
from PIL import ImageGrab
from datetime import datetime
import keyboard
import pytesseract
import socket

def check_internet_connection():
    try:
        # Essai de connexion au serveur Google DNS
        socket.create_connection(("8.8.8.8", 53), timeout=5)
        print('Connection to socket succeded')
        return True
    except OSError:
        print('Connection to socket failed')
        return False

# if check_internet_connection():
if True:
    
    # Spécifier le chemin de tesseract
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

    # Obtenir la date et l'heure actuelles
    now = datetime.now()

    # Formater la date en 'Ymd-His'
    date_courante = now.strftime("%Y%m%d-%H%M%S")

    # Trouver toutes les fenêtres ouvertes contenant "Chrome" dans leur titre
    chrome_windows = [window for window in gw.getAllTitles() if 'Chrome' in window]

    if chrome_windows:
        # Obtenir la première fenêtre Chrome trouvée
        chrome_window = gw.getWindowsWithTitle(chrome_windows[0])[0]
            
        # Restaurer la fenêtre si elle est minimisée
        if chrome_window.isMinimized:
            chrome_window.restore()
        
        # Mettre la fenêtre au premier plan (comme Alt+Tab)
        chrome_window.activate()
        
        # Optionnel : Maximiser la fenêtre si besoin
        chrome_window.maximize()
        
        # Attendre un peu pour s'assurer que la fenêtre est bien activée
        time.sleep(1)
        
        # Simuler un raccourci clavier Ctrl + 2 pour basculer sur le deuxième onglet
        # pyautogui.hotkey('ctrl', '2')  # Utilise 'ctrl' pour Windows/Linux, 'command' pour macOS

        # Local : Faire focus sur 2e onglet chrome 
        pyautogui.moveTo(320, 20)
        pyautogui.click()

        
        # Copier ID Utilisateur
        time.sleep(1)
        pyautogui.moveTo(530, 413)
        pyautogui.click()

        # focus sur mobcash
        time.sleep(1)
        pyautogui.moveTo(1500, 820)
        pyautogui.click()
        pyautogui.hotkey('esc')

        # Local : Copier l'ID 
        time.sleep(1)
        pyautogui.moveTo(1560, 270)
        pyautogui.click()
        onex_id = pyperclip.paste()
        print(f"ID {onex_id}")

        # Pointer et coller ID utilisateur sur mobcash
        time.sleep(1)
        pyautogui.moveTo(1350, 480)
        pyautogui.click()
        pyautogui.hotkey('ctrl', 'a')
        pyautogui.hotkey('ctrl', 'v')

        # cliquer OK après avoir entrer le ID utilisateur
        time.sleep(1)
        pyautogui.moveTo(1885, 594)
        pyautogui.click()
        time.sleep(2)

        # Local : Copier montant
        time.sleep(1)
        pyautogui.moveTo(525, 533)
        pyautogui.click()
        copiedmontant = pyperclip.paste()
        print(f"Montant : {copiedmontant}")

        # Mobcash : Coller montant sur mobcash
        time.sleep(1)
        pyautogui.moveTo(1360, 715)
        pyautogui.click()
        pyautogui.hotkey('ctrl', 'a')
        pyautogui.hotkey('ctrl', 'v')

        # Mobcash : Valider ID & Montant Mobcash
        time.sleep(1)
        pyautogui.moveTo(1861, 838)
        pyautogui.click()

        # Local : Copier SMS ID
        time.sleep(1)
        pyautogui.moveTo(525, 650)
        pyautogui.click()
        copiedsmsid = pyperclip.paste()
        print(f"SMSID : {copiedsmsid}")

        # Local : Faire focus sur Mobcacsh pour la capture d'ecran
        mobcashWindow = [window2 for window2 in gw.getAllTitles() if 'Edge' in window2]

        # Mettre mobcash en focus
        mobcashWindow = gw.getWindowsWithTitle(mobcashWindow[0])[0]

        if mobcashWindow:
            time.sleep(1)
            mobcashWindow.restore()
            mobcashWindow.activate()

        # Obtenir la fenêtre active
        time.sleep(1)
        window = gw.getActiveWindow()

        if window:
            # Capturer la position et la taille de la fenêtre
            bbox = (window.left, window.top, window.right, window.bottom)
            
            # Prendre la capture d'écran de la zone de la fenêtre
            screenshot = ImageGrab.grab(bbox)

            # Utiliser pytesseract pour faire l'OCR et extraire le texte
            # extracted_text = pytesseract.image_to_string(screenshot, lang = 'fra')
            
            # Sauvegarder l'image
            # capturefile = "mobcash_screenshot/date_courante_"+date_courante+"_onex_id_"+onex_id+"_sms_id_"+copiedsmsid+".png";
            capturefile = "../webroot/img/mobcash_screenshot/sms_id_"+copiedsmsid+"_onex_id_"+onex_id+".png";
            # capturefile = "mobcash_screenshot/date_courante.png";
            screenshot.save(capturefile, "JPEG", quality = 100)
            print("Capture d'écran enregistrée avec succès! "+capturefile)
            # print("Resultat final : "+extracted_text)

        else:
            print("Aucune fenêtre chrome active trouvée.")

        # Cliquer sur Valider finalement
        time.sleep(1)
        pyautogui.moveTo(130, 730)
        pyautogui.click()

        # On vide le presse papier
        # ------------------------
        
        print(f"Basculé Cashier : {chrome_window.title}")
    else:
        print("Aucune fenêtre Chrome n'est ouverte.")