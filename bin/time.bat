:: adapted from http://stackoverflow.com/a/10945887/1810071
@echo off
rem for /f "skip=1" %%x in ('wmic os get localdatetime') do if not defined MyDate set MyDate=%%x
rem for /f %%x in ('wmic path win32_localtime get /format:list ^| findstr "="') do set %%x
rem set fmonth=00%Month%
rem set fday=00%Day%
rem set today=%Year%-%fmonth:~-2%-%fday:~-2%
rem echo %today%


:: timestamp YYYYMMDD_HHMMSS
@echo off
for /f "delims=" %%a in ('wmic OS Get localdatetime  ^| find "."') do set dt=%%a
set datetime1=%dt:~0,8%
set datetime2=%dt:~8,6%
set "logfile=titulaire_log\%datetime1%\%datetime2%.log"

md titulaire_log\%datetime1%
rem echo %datetime%


rem @echo off
rem setlocal enableDelayedExpansion
rem set "months= 01:Jan 02:Feb 03:Mar 04:Apr 05:May 06:Jun 07:Jul 08:Aug 09:Sep 10:Oct 11:Nov 12:Dec"
rem set "ts="
rem for /f "skip=1 delims=" %%A in ('wmic os get localdatetime') do if not defined ts set "ts=%%A"
rem set "yyyy=%ts:~0,4%"
rem set "mm=%ts:~4,2%"
rem set "dd=%ts:~6,2%"
rem for /f %%A in ("!months:*%mm%:=!") do set "mon=%%A"
rem set "logfile=C:\log\log_%dd%%mon%%yyyy%.log"
rem echo %logfile%
