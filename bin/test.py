import pygetwindow as gw
import pyautogui
import time
import pyperclip
from PIL import ImageGrab
from datetime import datetime
import keyboard
import pytesseract
from PIL import Image
from PIL import ImageOps, ImageEnhance

# Spécifie le chemin vers l'exécutable Tesseract (sur Windows)
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Local : Faire focus sur Mobcacsh pour la capture d'ecran
mobcashWindow = [window2 for window2 in gw.getAllTitles() if 'Edge' in window2]

# Mettre mobcash en focus
mobcashWindow = gw.getWindowsWithTitle(mobcashWindow[0])[0]

if mobcashWindow:
    time.sleep(1)
    mobcashWindow.restore()
    mobcashWindow.activate()

# Obtenir la fenêtre active
time.sleep(1)
window = gw.getActiveWindow()

if window:
    # Capturer la position et la taille de la fenêtre
    bbox = (window.left, window.top, window.right, window.bottom)
    
    # Prendre la capture d'écran de la zone de la fenêtre
    # screenshot = ImageGrab.grab(bbox)

    # Spécifie le chemin de l'image
    # image_path = 'mobcash_screenshot/Screenshot_72.png'
    image_path = 'mobcash_screenshot/date_courante.png'
    

    # Ouvrir l'image avec Pillow
    image = Image.open(image_path)

    # Convertir l'image en niveaux de gris
    gray_image = ImageOps.grayscale(image)

    # Améliorer le contraste
    enhancer = ImageEnhance.Contrast(gray_image)
    contrasted_image = enhancer.enhance(2)

    # Appliquer l'OCR avec pytesseract pour l'image en anglais
    extracted_text = pytesseract.image_to_string(gray_image, lang='eng')

    # Utiliser pytesseract pour faire l'OCR et extraire le texte
    # extracted_text = pytesseract.image_to_string(screenshot, lang = 'eng')
    
    # Sauvegarder l'image
    capturefile = "mobcash_screenshot/date_courante.png";
    # screenshot.save(capturefile, "JPEG", quality = 40)
    print("Capture d'écran enregistrée avec succès! "+capturefile)
    print("Resultat final : "+extracted_text)

else:
    print("Aucune fenêtre active trouvée.")