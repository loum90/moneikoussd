const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    headless: "new",
    devtools: false,
  });
  const page = await browser.newPage();

  // Navigate to the URL
  await page.goto(process.argv[2]);

  // Wait for a short time to let JavaScript execute (you may need to adjust this)
  await page.waitForTimeout(300);

  // Get the HTML content after JavaScript execution
  const html = await page.content();

  // Output the HTML content
  console.log(html);

  await browser.close();
})();