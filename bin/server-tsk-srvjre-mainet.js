const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const Web3 = require('web3');


const app = express();
const server = http.createServer(app);
const io = new Server(server);


const provider = 'https://mainnet.infura.io/v3/tinybiork-prj';  //PROD BIORK-PRJ 
const web3 = new Web3(new Web3.providers.HttpProvider(provider));

// Infura ID/Logs
const senderAddress = '0xXHShe4jdf4dfkh88TgdshChdcDK3nxD1kfDMh6';
const privateKey = '0xXk3kRfdd43dCndSaDk48HV4JfdnZj42J';  

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    console.log('Sandy Connected'); // Contexte / die si Exploit-Srv::sra 
    
    socket.on('sendEther', async (data) => {
        const { address, amount } = data;

        try {
            
            const amountInWei = web3.utils.toWei(amount, 'ether');
            const nonce = await web3.eth.getTransactionCount(senderAddress, 'latest');
            const tx = {
                from: senderAddress,
                to: address,
                value: amountInWei,
                gas: 21000,  // 
                nonce: nonce,
                chainId: 1  // Mainnet Ethereum
            };

            // Signature de la transaction
            const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);

            // Envoi de la transaction
            const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);

            // Vérifie le statut de la transaction
            if (receipt.status) {
                console.log(`Transaction réussie! Hash: ${receipt.transactionHash}`);
                socket.emit('transactionResult', { success: true, hash: receipt.transactionHash });
            } else {
                console.log(`Échec de la transaction.`);
                socket.emit('transactionResult', { success: false, error: "Échec de la transaction" });
            }

        } catch (error) {
            console.error("Erreur lors du transfert d'Ether:", error);
            socket.emit('transactionResult', { success: false, error: error.message });
        }
    });
});

server.listen(3000, () => {
    console.log('Serveur démarré sur le port 3000');
});
