import psutil
import win32serviceutil
import time
import pygetwindow as gw
import pyautogui
import subprocess

# ON ARRETE TOUS LES SERVIECES WAMP
services = ["wampapache64", "wampmariadb64", "wampmysqld64"]

for service in services:
    try:
        print(f"Arrêt du service {service} en cours...")
        win32serviceutil.StopService(service)
        # Attendre quelques instants pour s'assurer que le service est arrêté
        time.sleep(2)
        print(f"Service {service} arrêté.")
    except Exception as e:
        print(f"Erreur lors de l'arrêt du service {service} : {e}")

# ON ARRETE WAMPSERVER

def chercher_et_fermer_wampmanager():
    for proc in psutil.process_iter(['pid', 'name']):
        try:
            name = proc.info['name'].lower()
            if "wampmanager" in name:  # Accepte wampmanager.exe ou wampmanager64.exe
                pid = proc.info['pid']
                print(f"Arrêt de {name} (PID {pid})...")
                proc.terminate()
                proc.wait(timeout=5)  # Attend 5 secondes avant de forcer
                print(f"{name} (PID {pid}) arrêté.")
                return
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            continue
    print("Aucun processus wampmanager trouvé.")

chercher_et_fermer_wampmanager()

# Recherche de la fenêtre Google Chrome
windows = gw.getWindowsWithTitle("Google Chrome")
if windows:
    # Choisit la première fenêtre trouvée
    chrome_window = windows[0]
    # Active la fenêtre
    chrome_window.activate()
    time.sleep(0.5)  # Petite pause pour s'assurer que la fenêtre est bien active
    # Récupère les coordonnées du coin supérieur droit
    x, y = chrome_window.topright
    # Ajuste la position pour cliquer précisément sur la croix
    pyautogui.click(x - 10, y + 10)
else:
    print("Aucune fenêtre Chrome trouvée.")

# Redémarrer l'ordinateur
try:
    subprocess.run("shutdown /r /t 0", shell=True, check=True)
except subprocess.CalledProcessError as e:
    print("Erreur lors du redémarrage :", e)