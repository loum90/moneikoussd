# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-sendsms com13 0325541393 'test'
# line = rxBuffer[:-readTermLen].decode('unicode-escape'), rectifier serial_comms.py ligne 107 par celle-ci au lieu de decode()
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/serial_comms.py

from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms, SentSms
from PDUUSSDConverter import converter
from pprint import pprint

import json
import multiprocessing
import time
import PDUUSSDConverter
import serial
import sys
import logging
import json
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com61'
NUMERO = cliArgs[2] if len(cliArgs) > 1 else '0325541393' # AT+CPMS="ME","SM","MT"
MSG = cliArgs[3] if len(cliArgs) > 1 else 'test' # AT+CPMS="ME","SM","MT"
BAUDRATE = 115200
PIN = None  # SIM card PIN (if any)

def main(n = False):

    modem = GsmModem(PORT, BAUDRATE)
    modem.smsTextMode = True
    modem.connect(PIN)
    modem.write('AT+CSMP=17,167,0,16\n\r', timeout=5, expectedResponseTermSeq='> ') # envoi message flash, enlever si normal
    modem.processStoredSms(True)
    response = modem.sendSms(NUMERO, MSG)
    # pprint(vars(response))
    print(response.number)
    # modem.write(chr(27), timeout=5, expectedResponseTermSeq='> ') # envoi message flash, enlever si normal
    modem.close()

if __name__ == '__main__':
    # main()

    p = multiprocessing.Process(target=main, name="main", args=(10,))
    p.start()

    # Wait 10 seconds for foo
    time.sleep(2)

    # If thread is active
    if p.is_alive():
        # Terminate foo
        p.terminate()

    # Cleanup
    p.join()