import http.server
import socketserver
import subprocess
import json
import shlex

PORT = 8888

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/run-script':
            self.run_script()
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write(b'Not Found')

    def run_script(self):
        try:
            # Path to your script
            # script_path = 'gsmmodem-ussd.py com61 #144*1*1*0325541393*0325541393*200*2#'
            script_path = 'service_ng_stop.py'
            
            # Execute the script and capture the output
            result = subprocess.run(script_path, shell=True, capture_output=True, text=True, check=True)
            
            # Strip the output
            output = result.stdout.strip()
            
            # Send response
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            
            # Send the HTML response with the script output
            html_response = f"{output}"
            self.wfile.write(html_response.encode('utf-8'))
        except Exception as e:
            self.send_response(500)
            self.end_headers()
            self.wfile.write(bytes(f'Error: {str(e)}', 'utf-8'))

with socketserver.TCPServer(("", PORT), MyHandler) as httpd:
    print(f"Serving at port {PORT}")
    httpd.serve_forever()

# C:\Users\Loum\AppData\Local\Programs\Python\Python38\python.exe
# D:\wamp\www\moneiko-ussd\bin\orange\server.py