import psutil
import subprocess
import time

import json


def checkIfProcessRunning(processName):

    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


# print('Ozek', checkIfProcessRunning('OzekiNG')),
# print('ServMon', checkIfProcessRunning('servmon'))


Ozeki = str(checkIfProcessRunning('OzekiNG'))
servmon = str(checkIfProcessRunning('servmon'))

json_data = '[ {"Ozeki": "'+Ozeki+'"}, {"ServMon": "'+servmon+'"}]'

json_object = json.loads(json_data)

json_formatted_str = json.dumps(json_object, indent=2)

print(json_formatted_str)