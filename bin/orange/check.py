from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from PDUUSSDConverter import converter

import logging
import PDUUSSDConverter
import sys
import time
import serial
import re

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com79' # exemple com13
BAUDRATE = 115200
PIN = None  # SIM card PIN (if any)
NBPORT = re.search(r'\d+',PORT).group() # extract l'entier 
from serial import SerialException

try:
    ser = serial.Serial(port=PORT)
    print('ok')
except SerialException:
    print ('busy')