import serial
import sys
import time


# utilisé dans ussdComponent
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com26' # exemple com13
BAUDRATE = 115200 # 115200

def wakeGsm(essai = 1): 
    
    essais = 2
    ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)

    ser.write(('AT+CSQ\r').encode()) # check l'état de veille
    time.sleep(0.5)

    ser.write(('AT+CFUN=1\r').encode()) # check l'état de veille
    time.sleep(0.5)
    
    ser.close()
    # time.sleep(0.1)

    if (essai < essais):
        essai = essai + 1   
        wakeGsm(essai)

if __name__ == '__main__':
    wakeGsm()