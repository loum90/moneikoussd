import serial
import sys
import time
import re

# import importlib
from subprocess import call

# importlib.import_module("gsmmodem-ussd")

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'COM61' # exemple com13
NUMERO = cliArgs[2] if len(cliArgs) > 1 else '0325541393' # AT+CPMS="ME","SM","MT"
MSG = cliArgs[3] if len(cliArgs) > 1 else 'Salut le stylo ako avereno fa ilaiko' # AT+CPMS="ME","SM","MT"

BAUDRATE = 115200 # 115200

# call(["python", "get_network.py", PORT])
# call(["python", "gsmmodem-ussd.py", PORT, "*123#"])

def read(string, fetch = False):
    at = string
    at = at.encode()
    ser.write(at)
    msg = ser.readline().decode('utf-8').strip()
    if fetch == True :
        return print(msg)
    else :
        return msg

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)

# print('AT+cmgs="'+NUMERO+'"\n\r')
# exit()

line = read('AT+CMGF=1\n\r')
time.sleep(.5)
line = read('AT+CSMP=17,167,0,16\n\r', True) # mode flash sms
# line = read('AT+CSMP=17,167,0,0\n\r', True) # mode normal
# time.sleep(.75)
# line = read(chr(26))
time.sleep(.5)
line = read('AT+CMGS=\"'+NUMERO+'\"\n\r', True)
time.sleep(.5)
line = read(MSG, True)
time.sleep(.5)
line = read(chr(26))
time.sleep(.5)
line = read(chr(27))
time.sleep(.5)
# line = read("\n\r")
# time.sleep(.5)

ser.close()


