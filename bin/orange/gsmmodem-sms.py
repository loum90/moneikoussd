# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-new
# line = rxBuffer[:-readTermLen].decode('unicode-escape'), rectifier serial_comms.py ligne 107 par celle-ci au lieu de decode()
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/serial_comms.py

from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from PDUUSSDConverter import converter
from pprint import pprint

import PDUUSSDConverter
import serial
import sys
import logging
import json
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com26'
MEMTYPE = cliArgs[2] if len(cliArgs) > 1 else 'ME' # AT+CPMS="ME","SM","MT"
MOBILE = cliArgs[3] if len(cliArgs) > 1 else '032' # 032 ou 034 : le modem telma mibug vao misy anle wekeGsm
BAUDRATE = 115200
PIN = None  # SIM card PIN (if any)


def wakeGsm(essai = 1): 

    if MOBILE in ["034", "033"] == False:
        essais = 2
        ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=3)
        ser.write(("AT\r").encode())
        ser.write(("ATZ\r").encode())
        ser.close()

        if (essai < essais):
            essai = essai + 1
            wakeGsm(essai)
        

def handleSms(sms):
    # print(u'== SMS message received ==\nFrom: {0}\nTime: {1}\nMessage:\n{2}\n'.format(sms.number, sms.time, sms.text))
    if sms.number :
        # pprint(vars(sms))
        print(u'{0} / {1} / {2}'.format(sms.number, sms.time.strftime('%Y-%m-%d %H:%M:%S'), sms.text))
    else :
        print(False)
def main():

    modem = GsmModem(PORT, BAUDRATE, smsReceivedCallbackFunc=handleSms)
    modem.smsTextMode = False
    modem.connect(PIN)
    # logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

    # modem.processStoredSms(True)
    smss = modem.listStoredSms(status=Sms.STATUS_ALL, memory=MEMTYPE, delete=False); 
    print(str(len(smss)))
    for i in range(len(smss)):
        sms = smss[i]
        handleSms(sms)
    modem.close()

if __name__ == '__main__':
    wakeGsm()
    main()