import psutil
import subprocess
import time

def checkIfProcessRunning(processName):

    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


if checkIfProcessRunning('OzekiNG'):
    subprocess.Popen(['net', 'stop', 'OzekiNG']) # start/stop/pause/continue
    time.sleep(3)

if checkIfProcessRunning('OzekiNG'):
    subprocess.Popen(['net', 'stop', 'OzekiNG']) # start/stop/pause/continue
    time.sleep(3)


if checkIfProcessRunning('servmon'):
    subprocess.call(["taskkill","/F","/IM","servmon.exe"])
    time.sleep(1)
if checkIfProcessRunning('servmon'):
    subprocess.call(["taskkill","/F","/IM","servmon.exe"])
    time.sleep(1)