import socket

def check_internet_connection():
    try:
        # Essai de connexion au serveur Google DNS
        socket.create_connection(("8.8.8.8", 53), timeout=5)
        return True
    except OSError:
        return False

if check_internet_connection():
    print('True')
else:
    print('False')