import serial
import sys
import time
import re

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'COM62' # exemple com13
BAUDRATE = 115200 # 115200

def read(string, fetch = False):
    at = string
    at = at.encode()
    ser.write(at)
    msg = ser.readline().decode('utf-8').strip()
    if fetch == True :
        return print(msg)
    else :
        return msg

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)

# time.sleep(1)
read('AT+CSCA?\r')
time.sleep(.25)
read('AT\r', True)
time.sleep(.25)
# read('AT+CSCA?\r', True)
ser.close()

# b'AT+CSCA?\r'
# b'\r\n+CSCA: "+261331110006",145\r\n\r\n'
# [Finished in 2.1s]