from multiprocessing import Process, Lock
import time
import os

def f():
    time.sleep(2)
    exit()

if __name__ == '__main__':

    timeout_function = float(10) # 9 seconds for max function time

    p1=Process(target=f, args=())   #Here you can change from 6 to 3 for instance, so you can watch the behavior
    start=time.time()
    p1.start()

    while p1.is_alive():

        timeout=time.time()

        if timeout-start > timeout_function:
            p1.terminate()
            print("process terminated")
            
        print("watching, time passed: "+str(timeout-start) )

        time.sleep(1)
