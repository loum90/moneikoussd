import serial as ser
import serial.tools.list_ports as prtlst

from infi.devicemanager import DeviceManager
dm = DeviceManager()
dm.root.rescan()

devices = dm.all_devices


for i in devices:
    try:
        print (('{} : address: {}, bus: {}, location: {}').format(i.friendly_name, i.address, i.bus_number, i.location))
    except Exception:
        pass