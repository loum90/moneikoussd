import serial.tools.list_ports
from pprint import pprint
# https://www.delftstack.com/howto/python/list-serial-ports-in-python/
for serialPort in serial.tools.list_ports.comports():
    if "PC UI" in serialPort.description :
        print(serialPort) 