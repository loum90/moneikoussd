import serial
import sys

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'com9' # exemple com13
BAUDRATE = 115200 # 115200

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)


# before writing anything, ensure there is nothing in the buffer
if ser.inWaiting() > 0:
    ser.flushInput()

# set the timeout to something reasonable, e.g., 1 s
ser.timeout = 1.

# send the commands:
ser.write(("ATZ\r").encode())

# read the response, guess a length that is more than the message
msg = ser.read(1024)
print (msg)