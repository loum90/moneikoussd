import os, termios, time
master, slave = os.openpty()

def loop():
    while True:
        data = os.read(master, 1)
        print(data)  # <--- never gets here
        time.sleep(.1)

from threading import Thread
thread = Thread(target=loop, daemon=True)
thread.start()

os.write(slave, b'123')
termios.tcdrain(slave)  # <--- gets stuck here
time.sleep(3)  # allow thread to read all data
