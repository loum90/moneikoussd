import wmi

model = "Huawei"


disks                      = wmi.WMI().Win32_DiskDrive()
drives_to_partitions       = wmi.WMI().Win32_DiskDriveToDiskPartition()
paritions_to_logical_disks = wmi.WMI().Win32_LogicalDiskToPartition()

drive_letter_name       = None
cf_drive_partition_name = None
drive_device_id         = None

for disk in disks:
    print(disk)
