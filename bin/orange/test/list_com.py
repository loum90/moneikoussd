import serial
import json
from serial.tools import list_ports

if __name__ == "__main__":
    for port in list_ports.comports():
        if "USB" in port.hwid:
            print(json.dumps(port, default=lambda obj: vars(obj), indent=4))
            # print(f"Name: {port.name}")
            # print(f"Description: {port.description}")
            # print(f"hwid: {port.hwid}")
            # print(f"Location: {port.location}")
            # print(f"Product: {port.product}")
            # print(f"Manufacturer: {port.manufacturer}")
            # print(f"ID: {port.pid}")
            print("----------")