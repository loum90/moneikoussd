import serial
import sys
import time
import re

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'COM21' # exemple com13
BAUDRATE = 115200 # 115200

def read(string, fetch = False):
    at = string
    at = at.encode()
    ser.write(at)
    msg = ser.read(64)
    if fetch == True :
        return print(msg)
    else :
        return msg

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)
# line = read('AT+CFUN=0\r', True)
# time.sleep(1)
# line = read('AT+CFUN=1,1\r', True)
# time.sleep(1)
# 
line = read('ATZ\r', True)
time.sleep(1)
line = read('ATE0\r', True)
time.sleep(1)
line = read('AT+CFUN?\r', True)
time.sleep(1)
# line = read('AT+CUSD=1,"231C0E3702",15', True)
ser.close()