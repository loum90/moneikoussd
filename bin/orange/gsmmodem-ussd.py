# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-new
# pip install PDUUSSDConverter
# exemple gsmmodem-ussd.py com13 #123# 3 1 0325541393, rappel moi
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/modem.py ligne 978 timeout changer si ussd bug
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/modem.py ligne 432 timeout changer si erreur ATZ configuration

from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from pprint import pprint
from PDUUSSDConverter import converter

import logging
from subprocess import call
import PDUUSSDConverter
import sys
import multiprocessing
from multiprocessing import Process
import signal
from contextlib import contextmanager
import time
import serial
import re
import os
from subprocess import Popen
from serial import SerialException


# ligne 432, 950, 983,
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
ussdPrimary = cliArgs[2] if len(cliArgs) > 1 else '#144#'

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com41' # exemple com13
BAUDRATE = 115200 # 115200
USSD_STRING = converter.text_to_pdu(ussdPrimary)
PIN = None  # SIM card PIN (if any)
NBPORT = re.search(r'\d+',PORT).group() # extract l'entier 

isOrange = False;
isAirtel = False;

if ("#123" in ussdPrimary) or ("#144" in ussdPrimary):
    isOrange = True

if ("*436" in ussdPrimary):
    isAirtel = True

def retry(attempt , attempts):

    if attempt < attempts:
        attempt = attempt + 1
        main(attempt)
    else: 
        print('Modem bug')
    quit()

def wakeGsm(essai = 1): 
    if ("*436" in ussdPrimary or "*123#" in ussdPrimary or "#144" in ussdPrimary ) == False: # désactivé pour airtel money
        essais = 2
        ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)
        ser.write(("AT\r").encode())
        ser.write(("ATZ\r").encode())
        ser.close()
        # time.sleep(0.1)

        if (essai < essais):
            essai = essai + 1   
            wakeGsm(essai)

def replyUssd(response, ussd):
   response = response.reply(converter.text_to_pdu(ussd))
   msg = response.message;
   msgText = converter.pdu_to_text(msg)
   return msgText

def main(attempt = 1):


    attempts = 3

    modem = GsmModem(PORT, BAUDRATE, 10)
    # logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
    # modem.smsTextMode = False
    modem.connect(PIN)
    # pprint(modem)
    # exit()
    # modem.waitForNetworkCoverage(10)
    ussdCommand = ' '.join(cliArgs)
    # print('Envoi USSD : {0}'.format(ussdCommand)+'\n')
    response = modem.sendUssd(USSD_STRING) # response type: gsmmodem.modem.Ussd

    # Force le modem à démarrer l'ussd vers le 1er menu pour Mvola seulement
    # bug du modem si commande avec envoi mvola + lancé 2 fois de suite, la 2nde affiche la réponse en mémoire du 1er, donc pas de menu 
    # Solution si "Rappelle" ne figure pas dans le message, on recommence le script

    firstMsg = converter.pdu_to_text(response.message)
    # print(firstMsg)
    # response.cancel()
    # modem.close()
    # exit()

    if isOrange == False: # SI TELMA
        if ("Echec de la transaction." in firstMsg) == True :
            print('Echec de la transaction')
            quit()
        elif (("#111#" in ussdPrimary) and ("Rappelle" in firstMsg) == False): # "Rappelle" est un mot clé dans le 1er menu de #111#
            # print('(bug)')
            modem.close()
            retry(attempt, attempts)
        elif (("#111*1*3*2" in ussdPrimary) and ("ransfer" in firstMsg) == False): # "ransfer" est un mot clé dans le 1er menu de #111#
            # print('(bug)')
            modem.close()
            retry(attempt, attempts)
    else : # SI ORANGE
        if ("ransfer" in firstMsg) == False and ("#144" in ussdPrimary): # "Transfert" est un mot clé dans le 1er menu de #111#
            # print('(bug)')
            # exit()
            
            if ("Choix non disponible" in firstMsg) == True:
                response.cancel()
                modem.close()
                retry(attempt, attempts)
    # ------------------------ fin vérification bug -------------------------

    nbCmd = len(cliArgs)
    if (nbCmd < 4): # si il n'y a pas de choix de réponse ussd pour la 1ère commande, on affiche le contenu du 1er ussd, equivalent isset($cliArgs[3]) en php

        msg = response.message;
        msgText = converter.pdu_to_text(msg)


        if "session" in msgText:
            reply = replyUssd(response, '2')
            print(converter.pdu_to_text(reply))
            response.cancel()
        else :
            print(msgText)

    # response.sessionActive veut dire que le ussd donne le choix sur la fenêtre suivante
    # Si airtel et qu'on entre *436*7# Suivi et que ce 1er code donne En cours de traitement appuyer sur OK on doit attendre un peu (qq secondes)
    # car à ce moment response.sessionActive est désactivé dès qu'on quitte la fenêtre 

    if isAirtel == True:
        if response.sessionActive == False:
            print(firstMsg)
            print('--------------------------------------------')
            if (('En cours' in firstMsg) or ('sur ok' in firstMsg)) :
                time.sleep(5)
                response.sessionActive = True # forcena true satria ary ambany hitohy

    # Gestion des réponses Ussd
    if response.sessionActive:

        nbCliArgs = len(cliArgs)-1
        for key, ussdReply in enumerate(cliArgs):

            if key > 2: # on affiche à partir du 3 ème (1er code ussd ex #144, #111, *436) paramètres pour les réponses ussd
                if nbCliArgs == key : # pour le dernier choix effectué
                    replyMsg = replyUssd(response, str(ussdReply));
                    # response.reply(converter.text_to_pdu(ussdReply))
                    print(replyMsg)
                    # print('---------------------Last-----------------------')
                    # print('arg ', key, ', code dernier : ' , ussdReply, ', réponse : ' , replyMsg);

                    if isAirtel == True:
                        if (('En cours' in replyMsg) or ('sur ok' in replyMsg)) :
                            time.sleep(5)
                            response.cancel()

                else :

                    replyMsg = replyUssd(response, str(ussdReply));
                    print(replyMsg)

                    if isAirtel == True:
                        # print('arg ', key, ', code intermediaire : ' , ussdReply, ', réponse : ' , replyMsg);
                        if (('En cours' in replyMsg) or ('sur ok' in replyMsg)) :
                            time.sleep(5)


                    if ('COUPON' in replyMsg) or ('ticket' in replyMsg) :
                        print("Arret - Coupon recquis")
                        quit()

                    if isOrange == False:
                        if 'Echec' in replyMsg: # si on a echec, c'est à dire qu'on a + de choix, dc on arrete le script sinon si le on force de choisir le modem bug
                            quit()
                        # time.sleep(0.5)
                    print('--------------------------------------------')
            else :

                # print(firstMsg)

                # print('arg ', key, ', code : ' , ussdReply, ', : ' , '1er menu *436# 1, 2, 3, ...');

                if isAirtel == True:
                    if (('En cours' in firstMsg) or ('sur ok' in firstMsg)) :
                        time.sleep(5)
                        response.cancel()

                    if firstMsg == 'null':
                        response.cancel()
                        
        response.cancel()
    else:

        if isAirtel == True:

            if (('En cours' in replyMsg) or ('sur ok' in replyMsg)) :
                time.sleep(5)
                response.cancel()

            if firstMsg == 'null':
                response.cancel()
        else :
            print('Session fermée par le réseau GSM.')

    modem.close()


def checkPort(PORT):
    try:
        serial.Serial(port=PORT)
        return 'free'
    except SerialException:
        return 'busy'

def waitForOpened():

    start = time.time()
    while checkPort(PORT) == 'busy':
        end = time.time()
        spent_time = round((end - start), 0)
        # print(spent_time)
        if checkPort(PORT) == 'free' or spent_time > 20:
            return;

def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

class TimeoutException(Exception): pass

if __name__ == '__main__':
    waitForOpened()
    # wakeGsm()
    main()
    # timeout_function = 25 # 9 seconds for max function time

    # checkNum = ['*120#', '#888#', '*999#', '*123#'];
    # if ussdPrimary in checkNum:
    #     timeout_function = 9 # 9 seconds for max function time

    # p1=Process(target=main, args=())   #Here you can change from 6 to 3 for instance, so you can watch the behavior
    # start=time.time()
    # p1.start()

    # while p1.is_alive():

    #     timeout=time.time()

    #     if timeout-start > timeout_function:
    #         p1.terminate()

    #     time.sleep(.5)