# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-new
# gsmmodem-deletesms.py com13 SM

from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from PDUUSSDConverter import converter
from pprint import pprint

import PDUUSSDConverter
import serial
import sys
import logging
import json
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com29'
MOBILE = cliArgs[2] if len(cliArgs) > 1 else '032' # 032 ou 034 : le modem telma mibug vao misy anle wekeGsm
# MEMTYPE = cliArgs[2] if len(cliArgs) > 1 else 'sm' # AT+CPMS="ME","SM","MT"
BAUDRATE = 115200
PIN = None  # SIM card PIN (if any)

def wakeGsm(essai = 1): 

    if MOBILE in ["034", "033"] == False:
        essais = 5
        ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=3)
        ser.write(("AT\r").encode())
        # ser.write(("ATZ\r").encode())
        ser.close()

        if (essai < essais):
            essai = essai + 1
            wakeGsm(essai)

def main():

    modem = GsmModem(PORT, BAUDRATE)
    modem.smsTextMode = True
    modem.connect(PIN)
    modem.processStoredSms(False)
    response = modem.deleteMultipleStoredSms(delFlag = 4, memory = "SM")
    smss = modem.listStoredSms(status=Sms.STATUS_ALL, memory = "SM", delete=False)
    response = modem.deleteMultipleStoredSms(delFlag = 4, memory = "ME")
    smss = modem.listStoredSms(status=Sms.STATUS_ALL, memory = "ME", delete=False)

    print((len(smss))+(len(smss)))

    modem.close()


if __name__ == '__main__':
    wakeGsm()
    main()