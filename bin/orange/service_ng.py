import psutil
import subprocess
import time
import os
from urllib.request import urlopen
import json
import requests

# Exemple de requête GET
url = "http://127.0.0.1/moneiko-ussd/apis/updateLastOzekiChecking"

# Effectuer la requête
response = requests.get(url)

# response = urlopen(url)
data = response.json()
result = json.dumps(data, indent=4)
# print(result)
# exit()


if data["is_autorestart_ozeki_activated"] == True:
    
    def checkIfProcessRunning(processName):

        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if processName.lower() in proc.name().lower():
                    return True
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return False;

    if checkIfProcessRunning('servmon') != True:
            os.startfile("C:\\Program Files (x86)\\Ozeki\\OzekiNG - SMS Gateway\\servmon.exe")
            time.sleep(2)

    if checkIfProcessRunning('OzekiNG'):
        print('OzekiNG was running')
    else:
        subprocess.Popen(['net', 'start', 'OzekiNG']) # start/stop/pause/continue



    # Getting usage of virtual_memory in GB ( 4th field)
    # print('RAM Used (GB):', psutil.virtual_memory()[3]/1000000000)
    usedRam = psutil.virtual_memory()[3]/1000000000

    if usedRam > 13.5:
        subprocess.Popen(['net', 'stop', 'OzekiNG']) # start/stop/pause/continue
        time.sleep(5)
        subprocess.Popen(['net', 'start', 'OzekiNG']) # start/stop/pause/continue
        pass
    
    