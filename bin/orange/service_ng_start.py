import psutil
import subprocess
import time
import os



def checkIfProcessRunning(processName):

    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


if checkIfProcessRunning('servmon') != True:
    os.startfile("C:\\Program Files (x86)\\Ozeki\\OzekiNG - SMS Gateway\\servmon.exe")
    time.sleep(2)

if checkIfProcessRunning('OzekiNG') != True:
    subprocess.Popen(['net', 'start', 'OzekiNG']) # start/stop/pause/continue
