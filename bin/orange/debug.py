# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-new
# pip install PDUUSSDConverter
# exemple gsmmodem-ussd.py com13 #123# 3 1 0325541393, rappel moi
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/modem.py ligne 978 timeout changer si ussd bug
# file:C:/Users/LOOM/AppData/Local/Programs/Python/Python38/Lib/site-packages/gsmmodem/modem.py ligne 432 timeout changer si erreur ATZ configuration

from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from PDUUSSDConverter import converter

import logging
import PDUUSSDConverter
import sys
import time
import serial
import re

# ligne 432, 950, 983,
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
ussdPrimary = cliArgs[2] if len(cliArgs) > 1 else '#144#'

PORT = cliArgs[1] if len(cliArgs) > 1 else 'com41' # exemple com13
BAUDRATE = 115200 # 115200
USSD_STRING = converter.text_to_pdu(ussdPrimary)
PIN = None  # SIM card PIN (if any)
NBPORT = re.search(r'\d+',PORT).group() # extract l'entier 

isOrange = False;
if ("#123" in ussdPrimary) or ("#144" in ussdPrimary):
    isOrange = True

def retry(attempt , attempts):

    if attempt < attempts:
        attempt = attempt + 1
        main(attempt)
    else: 
        print('Modem bug')
    quit()

def wakeGsm(essai = 1): 
    if ("#111" in ussdPrimary) == False:
        essais = 5
        ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)
        ser.write(("AT\r").encode())
        ser.write(("ATZ\r").encode())
        ser.close()
        # time.sleep(0.1)

        if (essai < essais):
            essai = essai + 1   
            wakeGsm(essai)

def replyUssd( response, ussd ):
   response = response.reply(converter.text_to_pdu(ussd))
   msg = response.message;
   msgText = converter.pdu_to_text(msg)
   return msgText

def main(attempt = 1):

    attempts = 3

    modem = GsmModem(PORT, BAUDRATE, 10)
    # logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
    # modem.smsTextMode = False
    modem.connect(PIN)
    # modem.waitForNetworkCoverage(10)
    ussdCommand = ' '.join(cliArgs)
    # print('Envoi USSD : {0}'.format(ussdCommand)+'\n')
    response = modem.sendUssd(USSD_STRING) # response type: gsmmodem.modem.Ussd

    # Force le modem à démarrer l'ussd vers le 1er menu pour Mvola seulement
    # bug du modem si commande avec envoi mvola + lancé 2 fois de suite, la 2nde affiche la réponse en mémoire du 1er, donc pas de menu 
    # Solution si "Rappelle" ne figure pas dans le message, on recommence le script

    firstMsg = converter.pdu_to_text(response.message)

    if isOrange == False: # SI TELMA
        if ("Echec de la transaction." in firstMsg) == True :
            print('En attente disponibilité réseau Mvola')
            quit()
        elif (("#111#" in ussdPrimary) and ("Rappelle" in firstMsg) == False): # "Rappelle" est un mot clé dans le 1er menu de #111#
            print('(bug)')
            modem.close()
            retry(attempt, attempts)
        elif (("#111*1*3*2" in ussdPrimary) and ("ransfer" in firstMsg) == False): # "ransfer" est un mot clé dans le 1er menu de #111#
            print('(bug)')
            modem.close()
            retry(attempt, attempts)
    else : # SI ORANGE
        if ("ransfer" in firstMsg) == False and ("#144" in ussdPrimary): # "Transfert" est un mot clé dans le 1er menu de #111#
            print('(bug)')
            
            if ("Choix non disponible" in firstMsg) == True:
                response.cancel()

            modem.close()
            retry(attempt, attempts)
    # ------------------------ fin vérification bug -------------------------

    nbCmd = len(cliArgs)
    
    if (nbCmd < 4): # si il n'y a pas de choix de réponse ussd pour la 1ère commande, on affiche le contenu du 1er ussd, equivalent isset($cliArgs[3]) en php
        msg = response.message;
        msgText = converter.pdu_to_text(msg)

        if "session" in msgText:
            reply = replyUssd(response, '2')
            print(converter.pdu_to_text(reply))
            response.cancel()
        else :
            print(msgText)

    # Gestion des réponses Ussd
    if response.sessionActive:

        nbCliArgs = len(cliArgs)-1
        for key, ussdReply in enumerate(cliArgs):
            if key > 2: # on affiche à partir du 3 ème paramètres pour les réponses ussd
                if nbCliArgs == key :
                    print(replyUssd(response, str(ussdReply)))
                    # response.reply(converter.text_to_pdu(ussdReply))
                    # print('reussi')
                else :
                    # modem.waitForNetworkCoverage(10)
                    replyMsg = replyUssd(response, str(ussdReply));
                    print(replyMsg);

                    if ('COUPON' in replyMsg) or ('ticket' in replyMsg) :
                        print("Arret - Coupon recquis")
                        quit()

                    if isOrange == False:
                        if 'Echec' in replyMsg: # si on a echec, c'est à dire qu'on a + de choix, dc on arrete le script sinon si le on force de choisir le modem bug
                            quit()
                        # time.sleep(0.5)
                    print('--------------------------------------------')

        response.cancel()
    else:
        print('Session fermée par le réseau GSM.')

    modem.close()

if __name__ == '__main__':
    wakeGsm()
    # time.sleep(0.5)
    main()