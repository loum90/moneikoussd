import serial
import sys
import time
import re
# import importlib
from subprocess import call

# importlib.import_module("gsmmodem-ussd")


cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'COM62' # exemple com13
BAUDRATE = 115200 # 115200

# call(["python", "get_network.py", PORT])
# call(["python", "gsmmodem-ussd.py", PORT, "*123#"])

def read(string, fetch = False):
    at = string
    at = at.encode()
    ser.write(at)
    msg = ser.readline().decode('utf-8').strip()
    if fetch == True :
        return print(msg)
    else :
        return msg

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)
# line = read('AT+CFUN=0\r', True)
# time.sleep(1)
# line = read('AT+CFUN=1,1\r', True)
# time.sleep(1)
line = read('AT+CNUM\r')
time.sleep(.5)
line = read('AT+CNUM\r', True)
time.sleep(.5)
# line = read('AT+CUSD=1,"231C0E3702",15', True)
ser.close()