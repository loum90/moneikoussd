import serial
import sys
import time

cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'com62' # exemple com13
BAUDRATE = 115200 # 115200

def read(string):
    at = string
    at = at.encode()
    ser.write(at)
    msg = ser.read(24)
    return print(msg)

ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)
read('AT+GSN\r')
ser.close()