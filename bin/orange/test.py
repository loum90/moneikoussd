from urllib.request import urlopen

url = "http://127.0.0.1/moneiko-ussd/apis/updateLastOzekiChecking"
response = urlopen(url)

# Lire le contenu de la réponse
content = response.read()

# Afficher le contenu de la réponse
print(content.decode('utf-8'))