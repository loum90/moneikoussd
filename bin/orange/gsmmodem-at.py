# doc https://guiott.com/GSMcontrol/Python_GSM/api.html
# pip install python-gsmmodem-new
# pip install PDUUSSDConverter
# https://m2msupport.net/m2msupport/atcpbw-write-phonebook-entry/
# 
from __future__ import print_function
from datetime import datetime
from gsmmodem.modem import GsmModem, Sms
from pprint import pprint
import io
import multiprocessing
import time
import logging
from subprocess import call
import sys
import time
import serial
import re
import os
from subprocess import Popen
from serial import SerialException


# ligne 432, 950, 983,
cliArgs = sys.argv; # prend les arguments passés en ligne de commandes
PORT = cliArgs[1] if len(cliArgs) > 1 else 'com6' # exemple com13
COMMAND = cliArgs[2] if len(cliArgs) > 1 else 'AT' # exemple com13
BAUDRATE = 115200 # 115200

class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        self.s = s

    def readline(self):
        i = self.buf.find(b"\n")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            i = data.find(b"\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                return r
            else:
                self.buf.extend(data)

def checkOwnNumber(n): 

    ser = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=10)

    # ser.write((" AT+CFUN?\r").encode()) # check SN
    # time.sleep(2)

    # ser.write(("AT+COPS=3,0\r").encode()) # 128-255 check list available options
    # time.sleep(2)

    # ser.write(("AT+COPS?").encode()) # 128-255 check service provider network operator
    # time.sleep(2)

    
    # r = ser.write(("AT+CPBW=?\r").encode()) # 128-255 check list available options
    # time.sleep(0.5)
    
    ser.write((COMMAND+'\r').encode()) # cherche et affiche le query
    time.sleep(2)

    # ser.write(("AT+CPBW=1\r").encode()) # supprime numero à la case 1
    # time.sleep(0.5)

    # ser.write(('AT+CPBW=1,"0331234568",129,"Moi"\r').encode()) # enregistre numero à la case 1
    # time.sleep(0.5)

    # ser.write(('AT+CPBR=1,5\r').encode())
    # time.sleep(0.5)


    try:
        while True:
            # Read data from the serial port
            data = ser.readline().decode('utf-8').strip()
            # data = ser.read()

            # If data received, print it
            if data:
                print("Received data from serial port: ", data)
                # Give the device time to send data again
                # if ('OK' in data) :
                #     ser.close()
                #     exit()
                #     break

    # To close the serial port gracefully, use Ctrl+C to break the loop
    except KeyboardInterrupt:
        print("Closing the serial port.")
        ser.close()


    # print(resp)

    # Read data from the serial port
    # data = ser.readline().decode('utf-8').strip()

    # # If data received, print it
    # if data:
    #     print("Received data from serial port: ", data)
    #     # Give the device time to send data again
    #     time.sleep(0.5)

    ser.close()

if __name__ == '__main__':
    # checkOwnNumber(1)

    # Start foo as a process
    p = multiprocessing.Process(target=checkOwnNumber, name="checkOwnNumber", args=(10,))
    p.start()

    # Wait 10 seconds for foo
    time.sleep(4)

    # If thread is active
    if p.is_alive():
        # Terminate foo
        p.terminate()

    # Cleanup
    p.join()