import ctypes

# Définition des constantes nécessaires
PWR_SUSPENDREQUEST = 1
PWR_FORCE = 0x2000

# Appel de la fonction SetSuspendState pour mettre l'ordinateur en veille
ctypes.windll.kernel32.SetSuspendState(PWR_SUSPENDREQUEST, PWR_FORCE, 0)
