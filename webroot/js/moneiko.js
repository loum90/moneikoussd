

var loader = ' <span class="fa fa-circle-o-notch text-success fa-spin loader"></span>';
var loadme = ' <span class="fa fa-circle-o-notch text-white fa-spin loadme"></span>';
var baseUrl = $('body').data('url');
var prodUrl = "https://moneiko.net/";
var isLocalhost = $('body').data('ip') == "127.0.0.1";
var type = $('.type').data('type');

var action = $('.params').data('action');
var controller = $('.params').data('controller');
check500Code = 0;

var script = document.createElement('script');
script.src = baseUrl+'js/onex.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);
champSmsOnFocus = false;


$.wait = function(ms) {
    var defer = $.Deferred();
    setTimeout(function() { defer.resolve(); }, ms);
    return defer;
};

function ucfirst(s)
{
    return s[0].toUpperCase() + s.slice(1);
}

function initTooltip() {
    $('[data-toggle=tooltip]').tooltip({
        html: true
    });
}

function closeTooltip() {
    $('[data-toggle=tooltip]').tooltip('hide');
    $('.tooltip').remove();
}

initTooltip();

function emptyAfter(classAttr) {
    $.wait(10000).then(function () {
        $(classAttr).empty();
    });
}

setTimeout(function () {
    $('.alert-dismissible').fadeOut(600,function() {
        $(this).remove();
    });
}, 5000);

$('button.exec').on('click', function(event) {
    event.preventDefault();
    var form = $(this).parents('form');
    var data = form.serialize();
    var self = $(this);
    var commandField = $('input#at-com');
    // console.log(commandField.val());
    // return false;

    commandField.attr('disabled', 'disabled');
    self.attr('disabled', 'disabled');
    $('.loading').html(' <span class="text-white">chargement ...</span>');
    $.post(form.attr('action'), data, function(data, textStatus, xhr) {
        $('.ussd-response').html(data.msg);
        $('.loading').empty();
        commandField.removeAttr('disabled');
        $(self).removeAttr('disabled');

        if (commandField.val().includes('2958')) {
            commandField.val("");
        }
    });
});

typeFormUssd = '';
$('#command-ussd').off().on('show.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    $('.btn', modal).removeClass('disabled');
    var linkUssd = $(e.relatedTarget);
    var linkReadSms = linkUssd.data('linkread-sms');
    typeFormUssd = linkUssd.data('type');
    $('.form-ussd').attr('data-type', typeFormUssd);
    $('.mobile', modal).html(linkUssd.data('mobile'));
    var actionForm = linkUssd.data('action');
    $('form.form-ussd', modal).attr('action', actionForm+'/'+typeFormUssd);

    $('.code-js-response').empty();
    if (typeFormUssd == "032") {
        $('.code-orange').removeClass('d-none');
        $('.code-mvola').addClass('d-none');
        $('.code-airtel').addClass('d-none');
        $('.lire-sms')
            .attr('data-href', linkReadSms)
            .attr('data-title', 'Orange')
        ;

    } else if(typeFormUssd == '034') {
        $('.code-mvola').removeClass('d-none');
        $('.code-airtel').addClass('d-none');
        $('.code-orange').addClass('d-none');
        $('.lire-sms')
            .attr('data-href', linkReadSms)
            .attr('data-title', 'Telma')
        ;

    } else if(typeFormUssd == '033') {
        $('.code-airtel').removeClass('d-none');
        $('.code-mvola').addClass('d-none');
        $('.code-orange').addClass('d-none');
        $('.lire-sms')
            .attr('data-href', linkReadSms)
            .attr('data-title', 'Airtel')
        ;

    }

    $('.container-solde-mvola').load(window.location+' .solde-mvola');
});

$('.transfert').on('submit', function(event) {
    event.preventDefault();
    var data = $(this).serialize();
    $('.exec-transfert').addClass('disabled');

    $.post($(this).attr('action'), data, function(data, textStatus, xhr) {
        $('.result-transfert').html(data);
        $('.exec-transfert').removeClass('disabled');
    });
});

$('#command-ussd').on('click', '.container-solde-mvola', function(event) {
    event.preventDefault();
    $('.container-solde-mvola .solde-mvola').html('<div class="float-right">'+loadme+'</div>');
    $('.container-solde-mvola').load(window.location+' .solde-mvola');
});

var formUssd = $('.form-ussd').off();
$('[name="montant"]').on('blur', function(event) {
    event.preventDefault();
    var numero = $.trim($('[name="numero"]', formUssd).val());
    var montant = $.trim($('[name="montant"]', formUssd).val());
    if (typeFormUssd == "032") {
        // var ussd = '#144# 1 1 ' + numero+' '+numero+' '+montant+' 2 2958';
        var ussd = '#144*1*1*' + numero+'*'+numero+'*'+montant+'*2# 2958';
    } else if(typeFormUssd == "033") {
        // var ussd = '#111# 1 3 2 '+numero+' '+montant+' retrait-moneiko 2958';
        var ussd = '*436*2# 1 1 ' + numero+' '+numero+' '+montant+' retrait-moneiko 2958';
    } else {
        $('[name="command"]').val(ussd);
        // var ussd = '#111# 1 3 2 '+numero+' '+montant+' retrait-moneiko 2958';
        var ussd = '#111# 1*2*'+numero+'*'+montant+'*2*moneiko 2958';
    }
    $('[name="command"]').val(ussd);

    if ($(this).val() != '') {
        $('[name="command"]').addClass('disabled');
    } else {
        $('[name="command"]').val('');
        $('[name="command"]').removeClass('disabled');
    }
});

$('[name="numero"]').on('blur', function(event) {
    event.preventDefault();
    var removedSpace = $(this).val().replace(/\s+/g, '');
    $(this).val(removedSpace);
});

$('.unlock-code').on('click', function(event) {
    event.preventDefault();
    $(this).blur();
    $('[name="command"]').removeClass('disabled');
    $('[name="command"]').removeAttr('disabled');
});


link = '';
modalDebugSms = '';
$('#debugall-sms').on('show.bs.modal', function (e) {
    $('#command-ussd').modal('hide');
    var modalDebugSms = $(e.currentTarget);
    var link = $(e.relatedTarget);
    var typeTitle = link.attr('data-titre');
    var linkHref = link.data('href');
    $('#label-title', modalDebugSms).html(typeTitle+' <span class="text-white">chargement...</span>');
    $('.modal-body', modalDebugSms).html("");

    closeTooltip();
    $.get(link.attr('data-href'), function(data) {
        $('.modal-body', modalDebugSms).html(data);
        $('#label-title', modalDebugSms).html(typeTitle+' <a href="'+linkHref+'"><span class="recharger text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
    });
    scrollDown(modalDebugSms);
});

// $('.form-ussd').on('click', '.lire-sms', function(event) {
//     event.preventDefault();
//     console.log($(this).attr('data-href'));
//     $('#debugall-sms').modal('show');
// });

$('#debugall-sms').on('click', '.recharger', function(event) {
    event.preventDefault();
    var self = $(this);
    $('.container-reload-sms').remove();
    $('.recharger').remove();
    $('.modal-body', modalDebugSms).html("Chargement ...");
    closeTooltip();
    $.get(self.data('href'), function(data) {
        // console.log(modalDebugSms);
        $('.modal-body', modalDebugSms).html(data);
        // $('#label-title', modalDebugSms).html(self.data('titre')+' <a href="javascript:;"><span class="recharger text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
    });
});


var initNbFailed = 0;
$('.backup-sms, .backup-sms-mvola').on('click', function(event) {
    var self = $(this);
    event.preventDefault();
    var href = $(this).attr('href');


    if (initNbFailed > 1) {
        $(this).removeClass('disabled');
        $('.loader', self).remove();
        self.removeClass('disabled');
        self.blur();
        initNbFailed = 0;
    }


    if (!$(this).hasClass('disabled')) {
        $(this).append(loader);
        self.addClass('disabled');
        closeTooltip();

        $.ajax({
            statusCode: {
                500: function() {
                    check500Code++;
                    if (check500Code >= 2) {
                        window.location = window.location;
                    }
                }
            },
            url: href,
            type: 'GET',
        })
        .done(function(data) {
            if (data.status == true) {
                var currentDate = new Date();
                var hour = currentDate.getHours();
                var minute = currentDate.getMinutes();

                $('.last-sms-maj').html('Dernier MAJ SMS : '+hour+'h'+("0" + minute).slice(-2));
                $('.fa-cloud-download', self).removeClass('text-danger').addClass('text-success');
            } else {
                $('.fa-cloud-download', self).removeClass('text-success').addClass('text-danger');
            }
            $('.loader', self).remove();
            self.removeClass('disabled');
            self.blur();
        });        
    } else {
        initNbFailed++;
    }
});


backupSmsInterval = 1; // 6
intervalMajTransBdd = 4; // 4

if ($('body').data('islocal') == true) {
    setInterval(function () {
        if (isLocalhost) {
            $('.backup-sms').click();
        }
    }, backupSmsInterval*60*1000);

    setInterval(function () {
        if (isLocalhost) {
            $('.backup-sms-mvola').click();
        }
    }, 5*60*1000); // 5 minutes
}

$('.vidageSmsModem').on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    var href = $(this).attr('href');
    $(this).append(loader);
    if (confirm("Êtes-vous de vouloir procéder ?")) {
        closeTooltip();
        $.get(href, function(data) {
            if (data.status == true) {
                $('.loader', self).remove();
            }
        });
    }
});

$('.redemarrage-ordi').on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    var href = $(this).attr('href');
    $(this).append(loader);
    if (confirm("Êtes-vous de vouloir procéder ?")) {
        closeTooltip();
        $.get(href, function(data) {
            $('.loader', self).remove();
        });
    }
});

$('#histo_sms_modal').on('shown.bs.modal', function (e) {
    modalHistoSms = $(e.currentTarget);
    linkHistoSms = $(e.relatedTarget);
    $('.modal-title', modalHistoSms).html(linkHistoSms.data('titre'));
    $('#label-title', modalHistoSms).html(linkHistoSms.data('titre')+' <span class="text-white chargement">chargement...</span>');
    $('.modal-body', modalHistoSms).html("");
    closeTooltip();
    $.get(linkHistoSms.data('href'), function(data) {
        $('.modal-body', modalHistoSms).html(data);
        $('.chargement', modalHistoSms).remove();
        $('#label-title', modalHistoSms).html(linkHistoSms.data('titre')+' <a href="javascript:;"><span class="recharger text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
        verificationSolde();
    });

    scrollDown($('#histo_sms_modal'));
});

function scrollDown(modal) {
    setTimeout(function () {
        var heightModal = $('.modal-dialog', modal).height();
        modal.animate({ scrollTop: 9999999 }, 100);
    }, 180);
}

function verificationSolde() {
    $('.container-montant').each(function(index, el) {
        let self = $(this);
        var soldeCourant = parseFloat($('.nouveauSolde', self).text());
        var transfertCourant = parseFloat($('.transfertAvecFrais', self).text());
        var soldePrecedent = parseFloat($($('.nouveauSolde').get(index-1)).text());
        if (soldeCourant == soldePrecedent-transfertCourant ) {
            self.parents('.row-sms').find('.verification-solde').html('<span class="fa fa-check text-success" data-toggle="tooltip" data-placement="bottom" title="Solde correct"></span>');
        } else {
            self.parents('.row-sms').find('.verification-solde').html('<span class="fa fa-exclamation-circle text-warning" data-toggle="tooltip" data-placement="bottom" title="Solde incorrect"></span>');
        }
        initTooltip();
    });
}


$('#histo_sms_modal').on('click', '.recharger', function(event) {
    $('.container-reload-sms').remove();
    $('.recharger').remove();
    closeTooltip();
    $.get(linkHistoSms.data('href'), function(data) {
        $('.modal-body', modalHistoSms).html(data);
        $('#label-title', modalHistoSms).html(linkHistoSms.data('titre')+' <a href="javascript:;"><span class="recharger text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
        setTimeout(function () {
            $('#histo_sms_modal').animate({ scrollTop: $('#histo_sms_modal .modal-dialog').height() }, 0);
        }, 120);
        verificationSolde();
    });
});

function reloadMajTransBdd() {
    $('.maj-trans-bdd-vers-site').each(function(index, el) {
        var url = $(this).attr('href');
        var self = $(this);
        self.addClass('disabled');
        $('.loading', self).html(loader);

        closeTooltip();
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(data) {
            $('.loader', self).remove();
            self.removeClass('disabled');
            $('.loading', self).html('<span class="fa fa-circle-o-notch"></span>');
        });
    });
}

setInterval(function () {
    if (isLocalhost) {
        reloadMajTransBdd();
    }
}, intervalMajTransBdd*1000*60);

function startNgrok(){
   WshShell = new ActiveXObject("Wscript.Shell"); //Create WScript Object
   WshShell.run("d://ngrok.bat"); // Please change the path and file name with your relevant available path in client system. This code can be used to execute .exe file as well
}

$('.reset-usb').on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    closeTooltip();
    $.get($(this).attr('href'), function(data) {
        self.blur();
    });
});

$('.code').on('click', function(event) {
    event.preventDefault();
    if (confirm("Êtes-vous sûr de vouloir procéder ?")) {
        var url = $(this).attr('href');
        var self = $(this);
        self.addClass('disabled');

        closeTooltip();
        $.get(url, function(data) {
            $('.code-js-response').html(data.msg).append('<br />');
            self.blur();
            self.removeClass('disabled');
        });
    }
});

$('.majparam').on('blur change', 'input, select, textarea', function(event) {
    event.preventDefault();
    $('.majparam .btn-status').remove();
    $('.majparam').submit();
});

$('.msg-relance').on('keyup', function(event) {
    event.preventDefault();
    var data = $(this).serialize();
    $.post(baseUrl+'tdb/editParameter', data, function(data, textStatus, xhr) {
        
    });
});


beginLoad = 0;
progressGeneralLoading = 0;
function loadMoreProcessProgress(addedLoaded) {
    beginLoad = eval(beginLoad+addedLoaded);
    $('.process-progressbar .progress-bar').attr('style', 'width: '+addedLoaded+'%');
    $('.process-progressbar .progress-bar').attr('aria-valuenow', addedLoaded);
    $('.process-progressbar .progress-label').html(addedLoaded+'%');
}

counterProgress = 0;
isRemapRoutageClicked = false;
isRemapLettreClicked = false;
isRemapDriverEnvoi  = false;

$('#config').off().on('click', '.majprocess-mappage', function(event) {
    var self = $(this);
    if (confirm('Veuillez confirmer !')) {
        loadMoreProcessProgress(0);
        counterProgress = 0;
        beginLoad = 0;
        progressGeneralLoading = 0;
        $('.process-progressbar').removeClass('d-none');
        self.append(loadme);
        self.addClass('disabled');
        event.preventDefault();
        $('#config .remapmsidn').trigger('click');
        var intervalProcess = setInterval(function () {
            loadMoreProcessProgress(progressGeneralLoading);
            counterProgress = eval(counterProgress + 1);

            if (progressGeneralLoading == 45) {
                $('.step-process').html('Remap routage');
                if (isRemapRoutageClicked == false) {
                    $('.remapRoutage').trigger('click');
                }
            }

            if (progressGeneralLoading == 55) {
                $('.step-process').html('Remap lettre');
                if (isRemapLettreClicked == false) {
                    $('.remapLettre').trigger('click');
                    // launchProgressLetter();
                }
            }

            if (progressGeneralLoading == 80) {
                $('.step-process').html('Remap driver envoi');
                if (isRemapDriverEnvoi == false) {
                    $('.remapDriverEnvoi').trigger('click');
                }
            }

            if (progressGeneralLoading >= 100) {
                clearInterval(intervalProcess);

                $('.majprocess-mappage .loadme').remove();
                $('.majprocess-mappage').removeClass('disabled');
                $('.process-progressbar .progress-bar').removeClass('bg-dark');
                
                $('.loadme', self).remove();

                $('.alert-mappage').addClass('alert-success').html('Procédure terminée avec succès');
                $.wait(5000).then(function () {
                    $('.alert-mappage').removeClass('alert-success').empty();
                });
            }
        }, 1000);
    }
});

var intervalProcessLetter
function launchProgressLetter() {
    intervalProcessLetter = setInterval(function () {
        $.when(checkProgressPortLetter()).then(function (data) {
            isRemapLettreClicked = true;
            console.log(data);
            if (data.progress >= 100) {
                clearInterval(intervalProcessLetter);
                return false;
            }
        });
    }, 5000);
}

$('#config').on('show.bs.modal', function (e) {
    closeTooltip();
    refreshPortList(true);

    // $.get(baseUrl+'tdb/getPortListDatas', function(data) {
    //     $.each(data.receiverDetails, function(index, value) {
    //         $("#recieverlist").append('<option value="'+value.receiver_id+'">'+value.name+'</option>');
    //     });
    // });

    $('.num-prod').html('Chargement... '+loadme);

    $.get(baseUrl+'SmsRecus/numProdsList', function(data) {
        $('.num-prod').html(data);
    });
});

function checkProgressPortLetter() {
    return $.get(baseUrl+'tdb/checkProgressPortLetter', function (data) {
        var letterProgress = parseInt((data.progress/10).toFixed(0));
        console.log('pr : '+letterProgress);
        progressGeneralLoading = eval(progressGeneralLoading+letterProgress);
    });
}

arrayPorts = [];
function refreshPortList(mode = false) {
    $('.port-list').html('Chargement ...');
    return $.get(baseUrl+'tdb/portList', function(data) {
        $('.port-list').html('<div class="">'+data+'</div>');
        initTooltip();
        $('.majparam .collapse').collapse({
            toggle: true
        });

        $('tr.port').each(function(index, el) {
            var self = $('tr.port').eq(index);
            var port = self.attr('data-port');
            $.get(baseUrl+'tdb/checkPort/'+port, function(data) {
                $('.statutport', self).html(data.result == 'busy' ? '<span class="text-danger">Occupé</span>' : 'ok');
            });
        });

        messageSiTypeNumeroManquantPortList();
        getNumComparaison();
        arrayPorts = JSON.parse($('#array-ports').val());
    });
}

function getNumComparaison() {
    var paramUrl = $('.param-url').attr('data-param');
    $('.state-num-local-prod').append(loadme);

    $.get(paramUrl, function(data) {
        $('tr.port').each(function(index, el) {
            var self = $('tr.port').eq(index);
            var tdOpType = $('td.op-type', self);
            var typeRempli = $('.typerempli', tdOpType);
            var keyNum = typeRempli.attr('data-op-type');
            // console.log(keyNum)
            // console.log(data[keyNum]);

            if (typeRempli.hasClass('mobile-key-num')) {
                if (data !== null) {
                    if (data[keyNum] != 'undefined') {
                        var numeroProd = data[keyNum];
                        // console.log(numeroProd+ ' : ' +$('.port-numero', tdOpType).text());
                        if ($('.port-numero', tdOpType).text() == numeroProd) {
                            $('.state-num-local-prod', tdOpType).html('<span class="fa fa-check-circle text-success"></span>');
                        } else {
                            $('.state-num-local-prod', tdOpType).html('<span class="fa fa-times-circle text-warning"></span>');
                        }
                    }
                }
            } else {
                $('.state-num-local-prod', tdOpType).html('<span class="fa fa-exclamation-circle text-secondary"></span>');
            }
        });
    });
}

$('#config').on('click', '.refreshport', function(event) {
    refreshPortList(true);
});


$('#histo_callback').on('show.bs.modal', function (e) {

    var modalHistoCallback = $(e.currentTarget);
    var linkHistoCallback = $(e.relatedTarget);
    $('.modal-title', modalHistoCallback).html(linkHistoCallback.data('titre'));
    $('#titreussd', modalHistoCallback).html(linkHistoCallback.data('titre')+' <span class="text-white chargement">chargement...</span>');

    $('.modal-body', modalHistoCallback).html("");
    closeTooltip();
    $.get(linkHistoCallback.attr('href'), function(data) {
        $('.modal-body', modalHistoCallback).html(data);
        $('#titreussd', modalHistoCallback).html(linkHistoCallback.data('titre')+' <a href="javascript:;"><span class="reload-histo text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
    });

    $('.modal-header', modalHistoCallback).off().on('click', '.reload-histo', function(event) {
        event.preventDefault();
        var self = $(this);
        self.append(loadme);

        closeTooltip();
        $.get(linkHistoCallback.attr('href'), function(data) {
            $('.modal-body', modalHistoCallback).html(data);
            $('#titreussd', modalHistoCallback).html(linkHistoCallback.data('titre')+' <a href="javascript:;"><span class="reload-histo text-white">recharger</span> <span class="container-reload-sms"><span class=" fa-2x fa fa-refresh text-success recharger reload-sms d-lg-none"></span></span></a>');
        $('.loadme', self).remove();
        });
    });

});


$('button[type="reset"]', formUssd).on('click', function(event) {
    $(this).blur();
    $('[name="command"]', formUssd).removeClass('disabled');    
});

$('.majparam').on('submit', function(event) {
    event.preventDefault();
    var data = $(this).serialize();
    var action = $(this).attr('action');
    var btnSubmit = $('.param-save', $(this));
    btnSubmit.addClass('disabled').attr('disabled', 'disabled');
    $('.errs-edit-param').removeClass('mt-4');
    $('.errs-edit-param').empty();

    if ($('.loader', btnSubmit).length < 1) {
        btnSubmit.append(loader);
    }

    $('.fa-check', $('.param-save')).remove();
    $.post(action, data, function(data, textStatus, xhr) {
        $('.param-save').blur();
        btnSubmit.removeClass('disabled').removeAttr('disabled');
        $('.loader', btnSubmit).remove();

        setTimeout(function () {
            $('.fa-check', $('.param-save')).remove();
        }, 2000);

        if (data.result != 'success') {
            var err = JSON.stringify(data.result);
            if (!$('.errs-edit-param').hasClass('mt-4')) {
                $('.errs-edit-param').addClass('mt-4');    
            }
            $('.errs-edit-param').html(err);
        } else {
            $('.errs-edit-param').html('<div class="text-success">Succès</div>');
        }
    });

});

var lastSolde = '';

function editSms() {

    $('.marksms').on('click', function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var self = $(this);
        self.blur();
        closeTooltip();
        $.get(href, function(data) {
            if (data.marked == 1 ) {
                self.removeClass('btn-dark').addClass('btn-info');
                $('.fa', self).removeClass('text-secondary').addClass('text-white');
            } else {
                self.removeClass('btn-info').addClass('btn-dark');
                $('.fa', self).removeClass('text-white').addClass('text-secondary');
            }
        });
    });

    $('.majestate').off().on('click', function(event) {
        closeTooltip();
        event.preventDefault();
        var href = $(this).attr('href');
        var self = $(this);
        self.blur();
        closeTooltip();
        $.get(href, function(data) {
            if (data.paiement_status == 'success' ) {
                $('.fa', self).removeClass('text-warning').addClass('text-success');
            } else {
                $('.fa', self).removeClass('text-success').addClass('text-warning');
            }

            reloadSms();
            reloadInfos();
        });
    });

    $('.container-sms').off().on('click', '.switchSolde0', function(event) {
        closeTooltip();
        event.preventDefault();
        var href = $(this).attr('href');
        var self = $(this);
        self.blur();
        closeTooltip();
        $.get(href, function(data) {
            if (data.paiement_status == 'success' ) {
                $('.fa', self).removeClass('text-white').addClass('text-warning');
            } else {
                $('.fa', self).removeClass('text-warning').addClass('text-white');
            }

            reloadSms();
            reloadInfos();
        });
    });

    $('.deletesms').on('click', function(event) {
        event.preventDefault();

        if (confirm('Êtes-vous sûr de vouloir supprimer ?')) {
            
            var rowSms = $(this).parents('.row-sms');
            var href = $(this).attr('href');
            var self = $(this);

            self.blur();
            closeTooltip();
            $.get(href, function(data) {
                if (data.result == 1 ) {
                    rowSms.fadeOut(function() {
                        rowSms.remove();
                        reloadSms();        
                    });
                }
            });

        }

    });

    $('div.msg').on('click', function(event) {
        event.preventDefault();
        var self = $(this);
        $('.done', self).remove();

        if (!$('.reloadsms .fa').hasClass('fa-spin')) {
            var smsSrc = $('#sms-src').clone();
            var self = $(this);
            var text = self.html();
            pauseTimerSmsReloadReloadSms();
            if ($('#sms-src', self).length == 0) {
                self.empty();
                self.append(smsSrc);
                $('#sms-src', self).val(text).focus();
                champSmsOnFocus = true;
            }
        } else {
            self.addClass('text-danger');
        }

    });

    $('.container-sms').on('blur', '#sms-src' , function(event) {
        event.preventDefault();
        var self = $(this);
        var val = (self.val()).trim();
        var currentContainer = $(this).parents('div.msg');
        var smsId = currentContainer.data('id');
        var urlEdit = currentContainer.data('url');

        self.remove();
        currentContainer.html(val);
        $.post(urlEdit, {msg: $(this).val()}, function(data, textStatus, xhr) {
            playTimerSmsReloadReloadSms();
            currentContainer.append(' <span class="fa fa-check text-success done"></span>');
            setTimeout(function () {
                $('.done', currentContainer).remove();
            }, 2000);
            champSmsOnFocus = false;
        });    
    });

    $('.container-sms div.msg').each(function(index, el) {
        if ($(this).text().includes('recu')) {
            var currentTr = $(this).parents('div.row-sms');
            // lastSolde = extractBetween($(this).text(), 'Votre solde est de ', ' Ar. Ref:');
            $('hr', currentTr).css({
                borderTop: "2px solid #168157",
            });
            return false;
        }
    });

    verificationSoldeMvola();

}

editSms();

var timerSmsReload = null;

function playTimerSmsReloadReloadSms() {
    if (!$('.block-sms').hasClass('container-sms')) {
        $('.block-sms').addClass('container-sms');
    }
    if (timerSmsReload !== null) {
        return;
    }
    timerSmsReload = setInterval(function() {
        reloadSms();        
    }, 10 * 1000);

    // console.log('playt');
}

function pauseTimerSmsReloadReloadSms() {
    $('.block-sms').removeClass('container-sms');
    clearInterval(timerSmsReload);
    timerSmsReload = null;
}

if (controller == 'SmsRecus') {
    playTimerSmsReloadReloadSms();
}


rSms = 0;
function reloadSms() {
    $('.fa-spinner').addClass('text-white fa-spin').removeClass('text-secondary');
    if ($('.container-sms').html() !== undefined) {
        $('.container-sms').load(window.location.href+' .sms-block', function () {
            editSms();
            closeTooltip();
            $('.fa-spinner').removeClass('text-white fa-spin').addClass('text-secondary');
            reglerCommandeFailed();
        });
        // reloadInfos();
        initTooltip();
        $('.bs-tooltip-bottom').remove();
    }

    rSms++;
    // console.log('rSms '+rSms);

}
reloadSms();


function reloadInfos() {
    $('.container-reload').load(window.location.href+' .block-reload', function () {
        editSms();
        $('.fa-spinner').removeClass('text-white fa-spin').addClass('text-secondary');
        closeTooltip();
    });
}

$('.reloadsms').on('click', function(event) {
    event.preventDefault();
    reloadSms();        
});

$('.checksolde').on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    var refreshSpan = $('.fa-refresh', self);
    self.html(loadme);
    refreshSpan.removeClass('text-danger');

    pauseTimerSmsReloadReloadSms();

    closeTooltip();
    $.get($(this).attr('href'), function(data) {
        if (data.status == 'success') {
            // $('.result-solde').html(data.solde);
            self.html('<span class="fa fa-refresh"></span>');
            reloadSms();
        } else {
            $('.loadme', self).remove();
            self.html('<span class="fa fa-refresh text-danger checksolde-failed"></span>');
            setTimeout(function () {
                $('.checksolde-failed').removeClass('text-danger');
            }, 3000)
        }
        playTimerSmsReloadReloadSms();
    });
});

$('.form-sendsms').on('submit', function(event) {
    event.preventDefault();
    var self = $(this);
    var actionUrl = self.attr('action');
    $('.button-send', self).attr('disabled', 'disabled').append(' '+loadme);
    $('.button-send', self).removeClass('text-danger');
    $('.res-sms-send', self).empty();
    $('.alert-sendsms').removeClass('alert-success').html('');


    $.post(actionUrl, self.serialize(), function(data, textStatus, xhr) {

        if (data != '') {
            $('#sendsms').modal('hide');
            $('input.reset', self).val('');
            $('.res-sms-send', self).html(data);
            reloadSms();
        } else {
            $('textarea', self).val('');
            $('.alert-sendsms').addClass('alert-success').html('Message envoyé');
        }
        $('.button-send .loadme', self).remove();
        $('.button-send', self).removeAttr('disabled');
    });
});

$('[name="montant_recu"]').on('blur', function(event) {
    if ($(this).val() != '') {
        var montantRecu = parseFloat($('[name="montant_recu"]').val());
        $('[name="solde_actuel"]').val(parseFloat(lastSolde)+montantRecu);
    }
});

function extractBetween(message, text1, text2 = '')
{
    part1 = message.split(text1)[1];
    if (text2) {
        text = $.trim(part1.split(text2)[0]);
    } else {
        text = part1;
    }
    return text.replace(' ', '');
}

$('.form-reclamation').on('submit', function(event) {
    event.preventDefault();
    var submitbtn = $('button[type="submit"]', $(this));
    submitbtn.attr('disabled', 'disabled').append(' '+loadme);

    $.post($(this).attr('action'), $(this).serialize(), function(data, textStatus, xhr) {
        $('.msg-reclam').html(data);
        $('.loadme', submitbtn).remove();
        submitbtn.removeAttr('disabled');
    });
});

$('.form-search').on('submit', function(event) {
    event.preventDefault();
    var submitbtn = $('button[type="submit"]', $(this));
    submitbtn.attr('disabled', 'disabled').append(' '+loadme);

    closeTooltip();
    $.get($(this).attr('action'), $(this).serialize(), function(data, textStatus, xhr) {
        $('.form-search .msg-reclam').html(data);
        $('.loadme', submitbtn).remove();
        submitbtn.removeAttr('disabled');
    });
});

$('.msg-reclam').on('click', '.majandreset', function(event) {
    event.preventDefault();
    var self = $(this);
    var searchUrl = self.data('search-url');
    var currentTr = $(this).parents('tr');
    $('.fa-search', currentTr).removeClass('text-danger').addClass('text-warning');
    closeTooltip();
    $.get(searchUrl, function(data) {
        if (data.status == true) {
            $('.fa-search', self).addClass('text-success').removeClass('text-warning');
            setTimeout(function () {
                window.open(self.attr('href'), '_blank');
                $('.fa-search', self).addClass('text-warning').removeClass('text-danger').removeClass('text-success');
            }, 2000);
        } else {
            $('.fa-search', self).addClass('text-danger').removeClass('text-warning');
        }
        $('.loadme', currentTr).remove();
        // $('.fa-search', self).append('&nbsp;'+loadme);

    });

});


function verificationSoldeMvola() {
    $('.container-sms .container-montant').each(function(index, el) {
        let self = $(this);
        var soldeCourant = parseFloat($('.solde_actuel', self).text());
        var montantRecu = parseFloat($('.montant_recu', self).text());
        var nextSolde = $('.solde_actuel').get(index+1);
        var soldePrecedent = parseFloat($(nextSolde).text());


        if (soldeCourant == soldePrecedent+montantRecu ) {
            self.parents('.row-sms').find('.verification-solde').html('<span class="fa fa-check text-success" data-toggle="tooltip" data-placement="bottom" title="Solde correct"></span>');
            self.parents('.row-sms').find('.majstatemontant').addClass('');
        } else {
            if (nextSolde !== undefined) {
                self.parents('.row-sms').find('.verification-solde').html('<span class="fa fa-exclamation-circle text-warning" data-toggle="tooltip" data-placement="bottom" title="Solde incorrect"></span>');
                self.parents('.row-sms').find('.majstatemontant').addClass('text-warning');
            }
        }

        initTooltip();
    });
}


var btnMajNgrok = $('.majgrockurl');
btnMajNgrok.on('click', function(event) {
    event.preventDefault();
    var self = $(this);

    $('.switch-tunnel .loader').remove();

    majNgrokUrl($(this));
});

var checkngrockurl = $('.checkngrockurl, .setsynchronisedtime');
checkngrockurl.on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    if ($('.loader', checkngrockurl).length == 0) {
        $(this).append(loader);
    }
    

    $.ajax({
        statusCode: {
            500: function() {
                check500Code++;
                if (check500Code >= 3) {
                    window.location = window.location;
                }
            }
        },
        url: self.attr('href'),
        type: 'GET',
    })
    .done(function(data) {
        if (data.status == true) {
            $('.fa', self).removeClass('text-danger').addClass('text-success');
        } else {
            check500Code++;
            $('.fa', self).addClass('text-danger').removeClass('text-success');
        }
        $('.loader', self).remove();
    });
    
});


// Check 3 fois toutes les 30 secondes
function reCheckNgrock(nbCheck = 2, eachSeconds = 30) {
    countRestartNgrock = 0;
    var ngrokMajServerTimer = setInterval(function () {
        countRestartNgrock++;

        if (!$('.fa', btnMajNgrok).hasClass('text-success')) {
            majNgrokUrl(btnMajNgrok);
        } 
        else {
            clearInterval(ngrokMajServerTimer);
        }

        if(countRestartNgrock > nbCheck) {
            clearInterval(ngrokMajServerTimer);

            setTimeout(function () {
                reCheckNgrock();
            }, eachSeconds*1000);
        }
    }, 3000);
}

reCheckNgrock();

function majNgrokUrl(btnMajNgrok) {

    if ($('.loader', btnMajNgrok).length == 0) {
        btnMajNgrok.append(loader);
    }
    if (btnMajNgrok.attr('href') !== undefined) {
        closeTooltip();

        $.ajax({
            statusCode: {
                500: function() {
                    check500Code++;
                    if (check500Code >= 3) {
                        window.location = window.location;
                    }
                }
            },
            url: btnMajNgrok.attr('href'),
            type: 'GET',
        })
        .done(function(data) {
            if (data.status !== undefined && data !== null) {
                if (data.status == true) {
                    $('.fa', btnMajNgrok).removeClass('text-danger').addClass('text-success');
                } else {
                    check500Code++;
                }
            } else {
                check500Code++;
            }
            $('.loader', btnMajNgrok).remove();
        });
    }

    setTimeout(function () {
        $('.fa', btnMajNgrok).removeClass('text-danger').addClass('text-success');
        $('.loader', btnMajNgrok).remove();
    }, 6000);
}

$('#ussd-historics').on('shown.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);
   
    closeTooltip();
   $.get(baseUrl+'tdb/listUssdHistorics', function(data) {
       $('.modal-body', modal).html(data);
   });
});

$('#stat-prod').on('shown.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);
    $('.nb-stat-doublon', modal).html(loadme);
    
    $.get(baseUrl+'apis/getStat', function(data) {
        $('.nb-stat-doublon', modal).html(data.result.nb_stat_doublon);
    });
});

var searchPanierModal = $('#searchpanier');
searchPanierModal.on('show.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);
    var href = link.attr('href');
    var type = link.data('type');
    $('.modal-body', modal).html('Chargement..'+loadme);
    $('.infos-type', modal).html(ucfirst(type));
    closeTooltip();
    $.get(link.attr('href'), function(data, xhr, sd) {
        if (xhr == "success") {
            $('.modal-body', modal).html(data);
            initTooltip();
        }
        $('.loadlink', modal).attr('href', href);
    });
});

searchPanierModal.on('click', '.reajustpaniersms', function(event) {
    event.preventDefault();
    var link = $(this).parents('a');
    var href = link.attr('href');
    link.addClass('text-danger');
    link.addClass('disabled');
    pauseTimerSmsReloadReloadSms();

    closeTooltip();
    $.get(href, function(data, xhr) {
        if (data.status == true) {
            $('.reload-idpanier').trigger('click');
        } else {
            link.addClass('text-danger');
        }
        link.removeClass('disabled');
        playTimerSmsReloadReloadSms();
    });
});

searchPanierModal.on('click', '.corriger-numero', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');
    link.append(' '+loadme);
    link.addClass('disabled');
    closeTooltip();
    $.get(href, function(data, xhr) {
        if (data.status == true) {
            $('.reload-idpanier').trigger('click');
        } else {
            link.addClass('text-danger');
        }
        link.removeClass('disabled');
        $('.reload-idpanier').trigger('click');
        $('.loadme', link).remove();
    });
});

searchPanierModal.on('click', '.corriger-montant', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');
    link.addClass('disabled');
    link.append(loadme);

    closeTooltip();
    $.get(href, function(data, xhr) {
        if (data.id) {
            $('.reload-idpanier').trigger('click');
        } else {
            link.addClass('text-danger');
        }
        link.removeClass('disabled');
        $('.reload-idpanier').trigger('click');
        $('.loadme', link).remove();
    });
});

searchPanierModal.on('click', '.reload-idpanier', function(event) {
    event.preventDefault();
    var href = $('.loadlink').attr('href');
    var link = $(this);
    link.find('.fa').addClass('fa-spin');
    link.addClass('disabled');

    closeTooltip();
    $.get(href, function(data, xhr) {
        if (xhr == "success") {
            $('.modal-body', searchPanierModal).html(data);
        }
        $('.fa-spin', link).removeClass('fa-spin');
        $('.loadlink', searchPanierModal).attr('href', href);
        link.removeClass('disabled');
    });
});

searchPanierModal.on('click', '.markaspaid', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');
    link.addClass('disabled');
    $.ajax({
        url: href,
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {
            if (data.status == 4) {
                link.find('.fa').addClass('text-success');
                $('.reload-idpanier', searchPanierModal).trigger('click');
            } else {
                link.find('.fa').addClass('text-warning');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status >= 500 && jqXHR.status < 600) {
                link.find('.fa').addClass('text-warning');
            }
        },
        complete: function(jqXHR, textStatus) {
            link.removeClass('disabled');

            var checkIfPaidTimer = setTimeout(function () {
                link.find('.fa').removeClass('text-success');
                link.find('.fa').removeClass('text-danger');
                link.find('.fa').removeClass('text-warning');
                link.removeClass('disabled');
                clearTimeout(checkIfPaidTimer);
            }, 10000);

        }
    });
});


searchPanierModal.on('click', '.reglercommande', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');
    link.addClass('disabled');
    link.find('.fa').removeClass('text-success').removeClass('text-danger');

    closeTooltip();

    $.ajax({
        url: href,
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {
            if (jqXHR.status === 200) {
                if (data.status == 'success') {
                    link.find('.fa').addClass('text-success');
                    $('.reload-idpanier').trigger('click');
                } 
                else if(data.msg == 'already_paid') {
                    link.find('.fa').addClass('text-danger');
                    $('.result-reglercommande', link).html('('+data.msg+')');
                    reloadSms();
                }
                else {
                    link.find('.fa').addClass('text-danger');
                    $('.result-reglercommande', link).html('('+data.msg+')');
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 500) {
                $('.checkhistoric-results').html("<span class='text-danger'>Erreur serveur : Code 500</span>");
            }
        },
        complete: function(jqXHR, textStatus) {
            link.removeClass('disabled');

            setTimeout(function () {
                link.find('.fa').removeClass('text-success').removeClass('text-danger');
                $('.result-reglercommande', link).fadeOut('500', function () {
                    $(this).empty();
                });
            }, 5000);
        }
    });
});


searchPanierModal.on('click', '.checkAndPay', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');

    $('.loadme', link).remove();
    link.append(loadme);

    link.addClass('disabled');
    link.find('.fa').removeClass('text-success').removeClass('text-danger');

    closeTooltip();

    $.ajax({
        url: href,
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {
            if (jqXHR.status === 200) {
                if (data.status == 'success') {
                    link.find('.fa').addClass('text-success');
                    $('.reload-idpanier').trigger('click');

                } 
                else {
                    link.find('.fa').addClass('text-danger');
                    $('.result-check-pay', link).html('('+data.status+')');
                    // console.log('ato');
                }
            }

            $('.checkhistoric-results').html('<pre>'+JSON.stringify(data, null, 4)+'</pre>');
            $('.loadme', link).remove();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status >= 500 && jqXHR.status < 600) {
                $('.checkhistoric-results').html("<span class='text-danger'>Erreur serveur : Code 500</span>");
            } else {
                $('.checkhistoric-results').html("<span class='text-danger'>Erreur serveur : Code "+jqXHR.status+"</span>");
            }
            $('.loadme', link).remove();
        },
        complete: function(jqXHR, textStatus) {
            link.removeClass('disabled');

            setTimeout(function () {
                link.find('.fa').removeClass('text-success').removeClass('text-danger');
                $('.result-check-pay', link).fadeOut('500', function () {
                    $(this).empty();
                });
            }, 5000);

            $('.loadme', link).remove();
        }
    });
});

searchPanierModal.on('click', '.reglerCommandeMvolaDebug', function(event) {
    event.preventDefault();
    var link = $(this);
    var href = link.attr('href');
    link.addClass('disabled');
    link.find('.fa').removeClass('text-success').removeClass('text-danger');

    closeTooltip();

    $.ajax({
        url: href,
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {
            if (jqXHR.status === 200) {
                $('.checkhistoric-results').html(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 500) {
                $('.checkhistoric-results').html("<span class='text-danger'>Erreur serveur : Code 500</span>");
            }
        },
        complete: function(jqXHR, textStatus) {
            link.removeClass('disabled');

            setTimeout(function () {
                link.find('.fa').removeClass('text-success').removeClass('text-danger');
                $('.result-reglercommande', link).fadeOut('500', function () {
                    $(this).empty();
                });
            }, 5000);
        }
    });
});

searchPanierModal.on('click', '.restaurersms', function(event) {
    event.preventDefault();

    if (confirm('Êtes-vous sûr de vouloir continuer ?')) {
        var href = $('.restaurersms').attr('href');
        var link = $(this);
        link.addClass('disabled');
        link.find('.fa').removeClass('text-success').removeClass('text-danger');

        closeTooltip();
        $.get(href, function(data, xhr) {

            if (data.status == 'success') {
                link.find('.fa').addClass('text-success');
            } 

            link.removeClass('disabled');
            reloadSms();

            setTimeout(function () {
                link.find('.fa').removeClass('text-success').removeClass('text-danger');
            }, 5000);
        });
    }
});

searchPanierModal.on('click', '.checkhistoric, .voirlog, .voirLogReclas', function(event) {
    event.preventDefault();

    var link = $(this);
    var href = link.attr('href');
    link.addClass('disabled');

    closeTooltip();

    $.ajax({
        url: href,
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {
            if (jqXHR.status === 200) {
                $('.checkhistoric-results').html(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('.checkhistoric-results').html("<span class='text-danger'>Erreur serveur : Code "+jqXHR.status+"</span>");
        },
        complete: function(jqXHR, textStatus) {
            link.removeClass('disabled');
            reloadSms();
        }
    });
});

$('#search').on('show.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);
    var type = link.data('type');
    $('.search-type', modal).html(type);
});

$('body').off().on('click', '.ajustermontant' , function(event) {
    event.preventDefault();
    var href = $(this).attr('href');
    var self = $(this);
    self.blur();
    pauseTimerSmsReloadReloadSms();

    $('.fa', self).addClass('text-secondary');
    $('.fa', self).removeClass('text-success');
    $('.fa', self).removeClass('text-warning');

    closeTooltip();
    $.get(href, function(data) {
        if (data.status == true ) {
            $('.fa', self).removeClass('text-warning').addClass('text-success');
        } else {
            $('.fa', self).removeClass('text-success').addClass('text-warning');
        }

        $('.fa', self).removeClass('text-secondary');
        playTimerSmsReloadReloadSms();
        reloadInfos();
        reloadSms();
    });
});

function reglerCommandeFailed() {
    // $('.row-sms').each(function(index, el) {
    //     var self = $(this);
    //     var diffMinutes = self.data('diff');
    //     var paiementStatus = self.data('paiement-status');

    //     if ((diffMinutes > 1 && diffMinutes < 10) && paiementStatus != 'success') {
    //         $('.regler_commande_mvola', self).trigger('click');
    //     }
    // });
}

$('.container-sms').off().on('click', '.regler_commande_mvola' , function(event) {
    event.preventDefault();
    var href = $(this).attr('href');
    var self = $(this);
    var parentRow = self.parents('.row');
    pauseTimerSmsReloadReloadSms();
    closeTooltip();

    self.blur();
    $('.fa', self).removeClass('text-success').removeClass('text-warning').addClass('text-secondary');

    $.get(href, function(data) {
        if (data.status == 'success' ) {
            $('.fa', self).removeClass('text-warning').addClass('text-success');
        }else if (data.msg == 'already_paid' ) {
            $('.fa', self).removeClass('text-warning').addClass('text-success');
            $('.result-reglement', parentRow).html('<span class="text-danger">'+data.msg+'</span>');
        } else {
            $('.fa', self).removeClass('text-success').addClass('text-warning');
            $('.result-reglement', parentRow).html('<span class="text-danger">'+data.msg+'</span>');
        }

        if (data.status == 'success' || data.msg == 'already_paid') {
            $('.fa', self).removeClass('text-secondary');
            reloadInfos();
            reloadSms();
        }
    }).promise().then(function (e) {
        playTimerSmsReloadReloadSms();
    });;
});


var cleanPaymentLink = $('.clean-payment');
var smsReloadTimer2 = setInterval(function () {
    $('.fa', cleanPaymentLink).addClass('text-white');
    closeTooltip();
    pauseTimerSmsReloadReloadSms();

    $.ajax({
        url: cleanPaymentLink.attr('href'),
        type: 'GET',
        data: {},
    })
    .done(function(data) {
        $('.fa', cleanPaymentLink).removeClass('text-white');
        if (champSmsOnFocus == false) {
            // reloadSms();
            playTimerSmsReloadReloadSms();
        }
    });

    function rechargeSms(mobile) {
        return $.ajax({
            url: baseUrl+'Ussd/rechargeSmsAuto/'+mobile,
            type: 'GET',
            data: {},
        })
        .done(function(data) {

            // console.log('Recharge auto '+mobile);
            // console.log(data);

            if (data.result == 'success') {
                $('.container-sms-'+mobile+' .fa-download').addClass('text-success');
            }
        });
    }

    $.when(rechargeSms('telma')).then(function () {
        rechargeSms('orange');
    });
    

}, 30*1000);

cleanPaymentLink.on('click', function(event) {
    event.preventDefault();
    $('.fa', cleanPaymentLink).addClass('text-white');
    closeTooltip();
    $.get(cleanPaymentLink.attr('href'), function(data, xhr) {
        // if (data.status == true) {
        if (xhr == 'success') {
            $('.fa', cleanPaymentLink).removeClass('text-white');
        } 

        cleanPaymentLink.blur();
    });
});


var compteurPanier = 0;

$('.check-paniers').click(function(event) {
    compteurPanier++;
    event.preventDefault();
    var self = $(this);
    $('.loadme').remove();
    self.append(loadme);
    self.addClass('text-white').removeClass('text-success');

    closeTooltip();
    $.get(self.attr('href'), function(data, xhr) {
        if (xhr == 'success') {
            if (data.nb) {
                $('.nbpendingtx').html("&nbsp;&nbsp;"+'<span class="text-danger">'+data.nb+'</span>');

                setTimeout(function () {
                    if (compteurPanier < 60) {
                        $('.check-paniers').trigger('click');
                    }
                }, 10*1000);

            } else {
                $('.nbpendingtx').html("");
                self.removeClass('text-white').addClass('text-success');
            }
        }
        $('.loadme').remove();
        self.tooltip('hide');

    });
});



$('.disable-mvola').click(function(event) {
    event.preventDefault();
    var self = $(this);
    $('.loadme').remove();
    $(this).append(loadme);

    closeTooltip();
    $.get($(this).attr('href'), function(data, xhr) {
        if (xhr == 'success') {
            if (data.is_depot_mvola2 == 0) {
                // $('.checkstatemvola').html("&nbsp;&nbsp;"+'<span class="fa fa-check text-success"></span>');
                $('.fa', self).removeClass('fa-pause text-success').addClass('fa-play text-warning');
            } else {
                // $('.checkstatemvola').html("&nbsp;&nbsp;"+'<span class="fa fa-remove text-danger"></span>');
                $('.fa', self).addClass('fa-pause text-success').removeClass('fa-play text-warning');
            }

            if (data.is_auto_check_localconnexion == 0) {
                $('.check-autolocalconnexion').addClass('text-danger').removeClass('text-white').removeClass('text-success');
            } else {
                $('.check-autolocalconnexion').addClass('text-success').removeClass('text-white').removeClass('text-danger');
            }
        }

        setTimeout(function ()
        {
            $('.checkstatemvola').empty();
        }, 2000);
        $('.loadme').remove();
    });
});

$('.disable-orange').click(function(event) {
    event.preventDefault();
    var self = $(this);
    $('.loadme').remove();
    $(this).append(loadme);

    closeTooltip();
    $.get($(this).attr('href'), function(data, xhr) {
        if (xhr == 'success') {
            if (data.is_orange_depot2 == 0) {
                // $('.checkstatemvola').html("&nbsp;&nbsp;"+'<span class="fa fa-check text-success"></span>');
                $('.fa', self).removeClass('fa-pause text-success').addClass('fa-play text-warning');
            } else {
                // $('.checkstatemvola').html("&nbsp;&nbsp;"+'<span class="fa fa-remove text-danger"></span>');
                $('.fa', self).addClass('fa-pause text-success').removeClass('fa-play text-warning');
            }

            if (data.is_auto_check_localconnexion == 0) {
                $('.check-autolocalconnexion').addClass('text-danger').removeClass('text-white').removeClass('text-success');
            } else {
                $('.check-autolocalconnexion').addClass('text-success').removeClass('text-white').removeClass('text-danger');
            }
        }

        self.tooltip('hide');


        setTimeout(function ()
        {
            $('.checkstatemvola').empty();
        }, 2000);
        $('.loadme').remove();
    });
});


$('.btn-checklocalconnexion').click(function(event) {
    event.preventDefault();
    var self = $(this);
    $('.loadme').remove();
    $(this).append(loadme);

    closeTooltip();
    $.get($(this).attr('href'), function(data, xhr) {
        if (xhr == 'success') {
            if (data.is_auto_check_localconnexion == 0) {
                $('.check-autolocalconnexion').addClass('text-danger').removeClass('text-white').removeClass('text-success');    
            } else {
                $('.check-autolocalconnexion').addClass('text-success').removeClass('text-white').removeClass('text-danger');
            }
        }

        self.tooltip('hide');

        setTimeout(function ()
        {
            $('.checkstatemvola').empty();
        }, 2000);
        $('.loadme').remove();
    });
});

var operateurs = $('.param-url').attr('data-operateurs');

function checkStateLocalConnexion() {
    var self = $('.param-url'); // param anle serveur prod ity fa tsy local


    // console.log(self.data('param'));
    // console.log(controller);
    if (controller != 'Tdb') {
        return false;
    }

    $('.solde-online-mvola').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.solde-online-orange').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.solde-online-airtel').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.solde-usdt').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.solde-payeer').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.solde-pm').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.nb_mail_jet_one').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.nb_mail_jet_second').html(($(loadme).removeClass('text-white').addClass('text-secondary')).clone());
    $('.btn-submit-maj-solde');

    $('.btn-submit-maj-solde').addClass("disabled");
    $('.btn-submit-maj-solde').append(loadme);

    $('.solde-online-mvola').attr('data-solde-mvola', parseInt(0));
    $('.solde-online-orange').attr('data-solde-orange', parseInt(0));
    $('.solde-online-airtel').attr('data-solde-airtel', parseInt(0));

    $('.solde-payeer').attr('data-solde-payeer', parseFloat(0));
    $('.solde-pm').attr('data-solde-pm', parseFloat(0));

    // $('.disable-mvola, .disable-orange, .disable-airtel').removeClass('text-warning').removeClass('text-white').removeClass('text-danger').removeClass('text-success');

    closeTooltip();

    return $.ajax({

        statusCode: {
            500: function() {
                check500Code++;
                if (check500Code >= 3) {
                    window.location = window.location;
                }
            }
        },
        url: self.data('param'),
        type: 'GET',
        data: {}
    })
    .done(function(data) {
        if (data !== null) {

            $('.load-total-solde').remove();

            var soldeOnline = data.solde_mvola+data.solde_mobile+data.solde_airtel;
            var soldeLocal = getTotalSoldeRecu();
            $('.total-local-reserve').html(soldeLocal+' <span class="fa fa-arrow-circle-down"></span>');
            $('.total-online-reserve').html(' + '+soldeOnline+' <span class="fa fa-arrow-circle-up"></span>');
            $('.total-reserve').html('= '+'<span class="text-white">'+eval(soldeOnline+soldeLocal)+' Ar</span> ');

            $('.solde-online-mvola').html('<span class="text-white">'+(data.solde_mvola).toLocaleString('fr-FR')+' <span class="libelle-min">Ar</span>'+'</span>').hide().fadeIn(200);
            $('.solde-online-orange').html('<span class="text-white">'+(data.solde_mobile).toLocaleString('fr-FR')+' <span class="libelle-min">Ar</span>'+'</span>').hide().fadeIn(200);
            $('.solde-online-airtel').html('<span class="text-white">'+(data.solde_airtel).toLocaleString('fr-FR')+' <span class="libelle-min">Ar</span>'+'</span>').hide().fadeIn(200);

            $('.solde-online-mvola').attr('data-solde-mvola', parseInt(data.solde_mvola));
            $('.solde-online-orange').attr('data-solde-orange', parseInt(data.solde_mobile));
            $('.solde-online-airtel').attr('data-solde-airtel', parseInt(data.solde_airtel));

            $('.solde-payeer').attr('data-solde-payeer', parseFloat(data.soldePayeer));
            $('.solde-pm').attr('data-solde-pm', parseFloat(data.soldePm));
            
            $('.solde-payeer').attr('data-reserve-payeer', parseFloat(data.reserve_py));
            $('.solde-pm').attr('data-reserve-pm', parseFloat(data.reserve_pm));

            $('.btn-submit-maj-solde').removeClass("disabled");
            $('.btn-submit-maj-solde .loadme').remove();

            
            $('.heure-online-mvola').html((data.last_mvola_retrait_at)).hide().fadeIn(200);
            $('.heure-online-orange').html((data.last_orange_retrait_at)).hide().fadeIn(200);
            $('.heure-online-airtel').html((data.last_airtel_retrait_at)).hide().fadeIn(200);

            $('.solde-usdt').html((data.soldeUsdt+' <span class="libelle-min">USDT</span> ')).hide().fadeIn(200);
            $('.solde-payeer').html((data.soldePayeer+' <span class="libelle-min">USD</span> ')).hide().fadeIn(200);
            $('.solde-pm').html((data.soldePm+' <span class="libelle-min">USD</span> ')).hide().fadeIn(200);
            $('.nb_mail_jet_one').html(data.nb_email_mailjet_first).hide().fadeIn(200);
            $('.nb_mail_jet_second').html(data.nb_email_mailjet_second).hide().fadeIn(200);

            if (data.nb_unverified_users > 0) {
                $('.user-notif').removeClass('text-secondary').addClass('text-white');
                $('.link-notif').append('<span class="badge">'+data.nb_unverified_users+'</span>');
            } else {
                $('.user-notif').addClass('text-secondary').removeClass('text-white');
                $('.link-notif .badge').html('');
            }
            // console.log(data);
            if (data.nbTotalVenteFailed > 0) {
                $('.failed-notif').removeClass('text-secondary').addClass('text-white');
                $('.link-failed').append('<span class="badge">'+data.nbTotalVenteFailed+'</span>');
            } else {
                $('.failed-notif').addClass('text-secondary').removeClass('text-white');
                $('.link-failed .badge').html('');
            }
            
            if (data.is_auto_check_localconnexion == 0) {
                $('.check-autolocalconnexion').addClass('text-danger').removeClass('text-white').removeClass('text-success');
            } else {
                $('.check-autolocalconnexion').addClass('text-success').removeClass('text-white').removeClass('text-danger');
            }


            if (data.is_depot_mvola2 == 0) {
                $('.disable-mvola').addClass('text-warning').removeClass('text-white').removeClass('text-white');
                $('.disable-mvola .alternate, .disable-mvola.alternate').removeClass('fa-pause').addClass('fa-play');
            } else {
                $('.disable-mvola').addClass('text-success').removeClass('text-white').removeClass('text-danger').removeClass('text-warning');
                $('.disable-mvola .alternate, .disable-mvola.alternate').addClass('fa-pause').removeClass('fa-play');
            }

            if (data.is_orange_depot2 == 0) {
                $('.disable-orange').addClass('text-warning').removeClass('text-white').removeClass('text-success');
                $('.disable-orange .alternate, .disable-orange.alternate').removeClass('fa-pause').addClass('fa-play');
            } else {
                $('.disable-orange').addClass('text-success').removeClass('text-white').removeClass('text-danger').removeClass('text-warning');
                $('.disable-orange .alternate, .disable-orange.alternate').addClass('fa-pause').removeClass('fa-play');
            }

            if (data.is_depot_airtel2 == 0) {
                $('.disable-airtel').addClass('text-warning').removeClass('text-white').removeClass('text-success');
                $('.disable-airtel .alternate').removeClass('fa-pause').addClass('fa-play');
            } else {
                $('.disable-airtel').addClass('text-success').removeClass('text-white').removeClass('text-danger').removeClass('text-warning');
                $('.disable-airtel .alternate, .disable-airtel.alternate').addClass('fa-pause').removeClass('fa-play');
            }

            if (type == "orange") {
                var num = data.numero_orange_retrait;
                $('.onlineretraitnum').text((num).substr(num.length - 2));
            }
            else if (type == "mvola") {
                var num = data.numero_mvola_retrait;
                $('.onlineretraitnum').text((num).substr(num.length - 2));
            }

            if (data.is_running_crypto_server == false) {
                $('.etat-server-crypto .fa-database').addClass('text-secondary').removeClass('text-white');
            } else {
                $('.etat-server-crypto .fa-database').addClass('text-white').removeClass('text-secondary');
            }
            $('.onlineretraitnumTelma').html((data.numero_mvola_retrait).substr(data.numero_mvola_retrait.length - 2));
            $('.onlineretraitnumOrange').html((data.numero_orange_retrait).substr(data.numero_mvola_retrait.length - 2));
            $('.onlineretraitnumAirtel').html((data.numero_airtel_retrait).substr((data.numero_airtel_retrait.length) - 2));

            $('.heureprod').html('Prod : <span class="text-white">'+data.heureactuelserveur+'</span>');
            $('.heurelocal').load(window.location+' .heurelocal');


            $.each($.parseJSON(operateurs), function(index, op) {
                var stats = data.statsmembres;
                var opName = op.name;
                var progress = eval('stats.'+opName+'.progress');
                $('.container-stats-membres .'+opName+' .users_total .detail').html(eval('stats.'+opName+'.users_total'));
                $('.container-stats-membres .'+opName+' .users_checked .detail').html(eval('stats.'+opName+'.users_checked'));
                $('.container-stats-membres .'+opName+' .users_unchecked .detail').html(eval('stats.'+opName+'.users_unchecked'));
                $('.container-stats-membres .'+opName+' .users_pending .detail').html(eval('stats.'+opName+'.users_pending'));
                $('.container-stats-membres .'+opName+' .users_success .detail').html(eval('stats.'+opName+'.users_success'));
                $('.container-stats-membres .'+opName+' .users_failed .detail').html(eval('stats.'+opName+'.users_failed'));
                $('.container-stats-membres .'+opName+' .progress .progress-bar').attr('style', 'width: '+progress+'%');
                $('.container-stats-membres .'+opName+' .progress .progress-bar').attr('aria-valuenow', progress);
                $('.container-stats-membres .'+opName+' .progress .progress-bar').html(progress+'%');

                if (progress < 97) {
                    $('.container-stats-membres .'+opName+' .progress .progress-bar').removeClass('bg-dark').removeClass('bg-warning').addClass('bg-danger');
                } else if (progress >= 97 && progress < 99) {
                    $('.container-stats-membres .'+opName+' .progress .progress-bar').removeClass('bg-dark').removeClass('bg-danger').addClass('bg-warning');
                } else {
                    $('.container-stats-membres .'+opName+' .progress .progress-bar').removeClass('bg-dark').removeClass('bg-danger').removeClass('bg-warning');
                }
            });

            autoUpdateSoldeMGAByClickProcess('mvola');
            autoUpdateSoldeMGAByClickProcess('orange');
            autoUpdateSoldeMGAByClickProcess('airtel');

            updateRatioChartGauge('mvola');
            updateRatioChartGauge('orange');
            updateRatioChartGauge('airtel');
            
            updateStatutAutoSwitch('mvola');
            updateStatutAutoSwitch('orange');
            updateStatutAutoSwitch('airtel');

            var currentDate = new Date();
            var hour = currentDate.getHours();
            var minute = currentDate.getMinutes();

            $('.check_state_local_connexion').removeAttr('title');
            $('.check_state_local_connexion').attr('data-original-title', 'Mise à jour des données du site (section du droite), dernier MAJ : '+hour+'h'+("0" + minute).slice(-2));
        }
    })
    ;
}
checkStateLocalConnexion();

$('.check_state_local_connexion').on('click', function (e) {
    var self = $(this);
    self.append(loadme);
    e.preventDefault();
    $.when(checkStateLocalConnexion()).then(function () {
        $('.loadme', self).remove();
    });
});

var switchNumero = $('.switchNumero');
switchNumero.click(function(event) {
    event.preventDefault();
    var self = $(this);
    self.addClass('text-white');
    var type = self.data('type');

    closeTooltip();
    $.get(self.attr('href'), function(data, xhr) {
        if (type == "orange") {
            $('.lastMvolaNumero', switchNumero).html(data.numero_orange_retrait.slice(-2));
        } else {
            $('.lastMvolaNumero', switchNumero).html(data.numero_mvola_retrait.slice(-2));
        }
        self.removeClass('text-white');
        self.tooltip('hide');
    });
});


var nbLimitMajSoldeNum = 0;
var timerMajSoldeNum = setInterval(function () {
    nbLimitMajSoldeNum++;
    automajsolde.click();

    if (nbLimitMajSoldeNum >= 5) {
        // clearInterval(timerMajSoldeNum);
        // console.log('Checking Màj Solde + Numéro Stoppé');
    }
}, 30*1000);

var automajsolde = $('.automajsolde');
automajsolde.click(function(event) {
    event.preventDefault();
    var self = $(this);
    self.removeClass('text-secondary').addClass('text-white');
    
    closeTooltip();
    $.get(self.attr('href'), function(data, xhr) {
        if (xhr == 'success') {
            if (data.modified == true) {
                self.addClass('text-success').removeClass('text-warning');
            } else {
                self.addClass('text-secondary').removeClass('text-success');
            }
        }
        self.removeClass('text-white').addClass('text-secondary');
        self.tooltip('hide');
        automajsolde.blur();
    });
});

var oldValNumOrange = $('#num_orange_msg').data('num');
$('#num_orange_msg').val(oldValNumOrange);
$('#num_orange_msg').each(function(index, el) {
    $(this).focusin(function(event) {
        if ($(this).val() == oldValNumOrange) {
            $(this).val('');
        }
    }).focusout(function(event) {
        if ($(this).val() == "") {
            $(this).val(oldValNumOrange);
        }
    });
});

var oldValNumAirtel = $('#num_airtel_msg').data('num');
$('#num_airtel_msg').val(oldValNumAirtel);
$('#num_airtel_msg').each(function(index, el) {
    $(this).focusin(function(event) {
        if ($(this).val() == oldValNumAirtel) {
            $(this).val('');
        }
    }).focusout(function(event) {
        if ($(this).val() == "") {
            $(this).val(oldValNumAirtel);
        }
    });
});

$('#command-ussd').on('click', '.check_state_diff_num', function(event) {
    event.preventDefault();
    var self = $('.check_state_diff_num');
    var url = $(this).attr('href');

    self.find('.fa').removeClass('text-success').removeClass('text-danger');
    self.addClass('disabled');

    closeTooltip();
    $.get(url, function(data) {
        if (data.status = 'success') {
            self.find('.fa').addClass('text-success');
        } else {
            self.find('.fa').addClass('text-danger');
        }
        setTimeout(function () {
            self.find('.fa').removeClass('text-success').removeClass('text-danger');
        }, 5000);
        self.removeClass('disabled').blur();
    });
});

var delaiTimerNumContainer = isLocalhost ? 30 : 60;
var delaiTimerCheckStateLocalConnexion = isLocalhost ? 5*60 : 120; // toutes les 5 minutes

function getTotalSoldeRecu() {
    totalSolde = 0;
    $('.container-bat').each(function(index, el) {
        var self = $(this);
        totalSolde = totalSolde + self.data('solde');
    });

    return parseFloat(totalSolde);
}


$('.nb_conn').html('<span class="text-secondary">'+loadme+'</span>');

let timerStat = delaiTimerNumContainer; // Début du compte à rebours


function startCounterStat() {
    
    $('.timer-bloc').text(timerStat);

    let countdownStat = setInterval(function () {

        timerStat--;
        // console.log(timerStat);
        $('.timer-bloc').text(timerStat);

        if (timerStat <= 0) {
            clearInterval(countdownStat); // Arrêter le compteur
            reactualiseBlocStat(); // Appeler ta fonction ici
            timerStat = 30; // Réinitialiser le compteur
            startCounterStat(); // Redémarrer le compte à rebours
        }

    }, 1000);

}

startCounterStat();


function reactualiseBlocStat() {
    $('.timer-bloc').html(loadme);
    $('.recap-num-container').load(window.location+' .recap-num-container', function (data) {
        loadBatt();
        closeTooltip();
        var totalSolde = getTotalSoldeRecu();
        $('.total-local-reserve').html(totalSolde+' <span class="fa fa-arrow-circle-down"></span>');
        applyGaugeRatio();
        updateRatioChartGauge('mvola');
        updateRatioChartGauge('orange');
        updateRatioChartGauge('airtel');
    });

    $('.container-mobilemoney-notifs').load(window.location+' .container-mobilemoney-notifs', function () {
        initTooltip();
        closeTooltip();
    });

    $('.container-2fa-stats').load(window.location+' .2fa-stats', function () {
        initTooltip();
        closeTooltip();
    });

    $.get(baseUrl+'tdb/checkServerNg', function(data) {
        if (data.status == false) {
            $('.etat-server-gateway .fa').addClass('text-danger').removeClass('text-secondary');
        } else {
            $('.etat-server-gateway .fa').addClass('text-secondary').removeClass('text-danger');
        }        
    });

    
    $('.etat-auto-start-ozeki .fa').addClass('text-danger').removeClass('text-secondary');
    $.get(baseUrl+'apis/getConfig', function(data) {
        if (data.is_autorestart_ozeki_activated == false) {
            $('.etat-auto-start-ozeki .fa').addClass('text-danger').removeClass('text-secondary');
        } else {
            $('.etat-auto-start-ozeki .fa').addClass('text-secondary').removeClass('text-danger');
        }   
    });


    $('.nb_conn').html('<span class="text-secondary">'+loadme+'</span>');
    // $('.active_ngrok_state .ng-percent').html('<span class="text-secondary">'+loadme+'</span>');
    $.when(loadStatNgrok()).then(function (data) {
        if (data.nb_conn < 18000) {
            $('.nb_conn').html('<span class="text-white">'+data.nb_conn+'/20K'+'</span>');
        } else {
            $('.nb_conn').html('<span class="text-danger">'+data.nb_conn+'/20K'+'</span>');
        }
        $('.active_ngrok_state .ng-percent').html(data.percent);
    });

    loadStatusNgrokFromProd();
}

reactualiseBlocStat();

var tunnelActiveId = $('.active_ngrok_state').attr('data-id');
function loadStatNgrok() {
    $('.nb_conn').html(loadme);
    $('.statut_ngrok').addClass('text-warning');
    return $.get(baseUrl+'apis/getStatNgrok', function (data) {
        if (data.url !== '') {
            $('.statut_ngrok').addClass('serversuccess').removeClass('text-secondary');
            $('.statut_ngrok').removeAttr('title');
            $('.statut_ngrok').attr('data-original-title', 'Statut Ngrok : '+data.url);
            $('.statut_ngrok').tooltip();

            var tunnelId = data.active_tunnel_id;
            var activeTunnel = $('.switch-tunnel[data-id='+tunnelId+']');

            // ato ny miactiver anle bordure anle bouton ngrok
            $('.switch-tunnel').removeClass('text-white').removeClass('active_ngrok_state').removeClass('border-grey');
            activeTunnel.addClass('text-white').addClass('active_ngrok_state').addClass('border-grey');

            if (tunnelActiveId != tunnelId) {
                // $('.majgrockurl').trigger('click');
                majNgrokUrl($('.majgrockurl'));
            }
        } else {
            $('.statut_ngrok').removeClass('serversuccess').addClass('text-secondary');
        }
    });
}

$.when(loadStatNgrok()).then(function (data) {
    // $('.nb_conn').html('<span class="text-white">'+data.nb_conn+'/20K'+'</span>');
});


// C'est ici qu'on change l'url NGROK toutes les XXX Minutes
function loadStatusNgrokFromProd() {

    if (controller == 'Tdb') {
        $('.nb_conn').html(loadme);

        var url = baseUrl+'apis/checkCodeNgrokProd';
        $.ajax({
            url : url,
            type : "GET",
            async: false,
            success: function (data) {
                if (data.status == 'success') {
                    $('.statut_ngrok_from_prod').addClass('serversuccess').removeClass('text-secondary');
                    console.log('ngrok_success');
                } else if(data.status == 'local_unreachable_from_prod') {
                    console.log('local_unreachable_from_prod');
                    $('.statut_ngrok_from_prod').removeClass('serversuccess').removeClass('text-secondary');
                    $('.statut_ngrok_from_prod').addClass('text-warning');

                    if ($('.active_ngrok_state').nextAll().not(".text-danger").html() !== undefined) { 
                        // switchTunnel($('.active_ngrok_state').nextAll().not(".text-danger").first()); // Ovaina fonction au lieu de click = efa mety
                    } else { // activer le 1er ngrok si le ngrok (bouton) courant est à la fin et qu'on ne peut plus selectionner le bouton suivant
                        // switchTunnel($('.switch-tunnel').not('.text-danger').first()); // Ovaina fonction au lieu de click = efa mety
                    }
                    

                } else if(data.status == 'no_local_connection') {
                    $('.statut_ngrok_from_prod').removeClass('serversuccess').removeClass('text-secondary');
                    $('.statut_ngrok_from_prod').addClass('text-danger');
                } else {
                    $('.statut_ngrok_from_prod').removeClass('serversuccess').addClass('text-secondary');
                }

                $('.statut_ngrok_from_prod').removeAttr('title');
                $('.statut_ngrok_from_prod').attr('data-original-title', 'Statut Ngrok from Prod : '+data.status);
                $('.statut_ngrok_from_prod').tooltip();
            }
        });
    }
}

$('.container-notifs').off().on('click', '.etat-server-gateway', function(event) {
    event.preventDefault();
    var self = $(this);
    self.html(loader);
    var href = $(this).attr('href');
    
    $.get(href, function(data) {
        self.html('<span class="fa fa-archive text-secondary "></span>');
        if (data.status == true) {
            $('.etat-server-gateway .fa').addClass('text-secondary').removeClass('text-danger');
        } else {
            $('.etat-server-gateway .fa').addClass('text-danger').removeClass('text-secondary');
        }        

        self.blur();
        // $('.loader', self).remove();
    });    
});


$('.container-check-rx').off().on('click', '.etat-rx-gateway', function(event) {
    event.preventDefault();
    var self = $(this);
    self.find('.fa-icon').html(loader);
    self.addClass('disabled').removeClass('border-green').removeClass('border-red');

    var href = $(this).attr('href');
    
    $.get(href, function(data) {
        if (data.status == 'restarted') {
            self.find('.fa-icon').html('<span class="fa fa-cloud-download text-success "></span>');
        } else {
            self.addClass('border-green');
            self.find('.fa-icon').html('<span class="fa fa-cloud-download text-secondary "></span>');
        }   

        $('.container-check-rx .ago-rx').html(loader+' Chargement !');
        setTimeout(function () {
            $.get(baseUrl+'tdb/getLastRappelMoi', function(data) {
                $('.container-check-rx .ago-rx').html(data.ago);
            });

            setTimeout(function () {
                self.removeClass('border-green');
            }, 30*1000);
            
        }, 3000);
        self.blur();
        $('.loader', self).remove();
        self.removeClass('disabled');
    });    

});


var hours = (new Date()).getHours(); 
var isDayTime = hours > 6 && hours < 23 // entre 6h du matin et 23h;

var timerCheckState = null;

function playTimerCheckStateLocalConexion() {
    if (timerCheckState !== null) {
        return;
    }
    timerCheckState = setInterval(function() {
        checkStateLocalConnexion();        
    }, delaiTimerCheckStateLocalConnexion * 1000);
}

function pauseTimerCheckStateLocalConexion() {
    console.log('Actualisation panneau droite en pause');
    clearInterval(timerCheckState);
    timerCheckState = null;
}

if (controller == 'Tdb') {
    if (isDayTime) {
        playTimerCheckStateLocalConexion();
    }
}

function planifierRepriseCheckStateLocalConnexion(horaire) {
    // Définir l'heure à laquelle vous souhaitez exécuter la fonction (24 heures format)
    const heureExecution = horaire; // Exemple : 12 représente midi

    // Définir l'intervalle pour vérifier l'heure d'exécution (en millisecondes)
    const intervalle = 24 * 60 * 60 * 1000; // 24 heures

    // Utiliser setInterval pour exécuter la fonction toutes les 24 heures
    setInterval(function () {
        const maintenant = new Date();
        
        // Vérifier si c'est l'heure d'exécution
        if (maintenant.getHours() === heureExecution && maintenant.getMinutes() === 0 && maintenant.getSeconds() === 0) {
            playTimerCheckStateLocalConexion();
        }
    }, intervalle);
}

planifierRepriseCheckStateLocalConnexion("06");

function loadBatt() {
    $('.container-bat').each(function(index, el) {
        self = $(this);
        var niveauSolde = self.data('progress')+'%';
        var bat = $('.bat', self);

        bat.delay((index+1)*100).animate({
            'height' : niveauSolde
        }, 350, 'swing');

        if (self.data('progress') <= 10) {
            bat.css({
                background: '#e81313'
            });
        }
        else if (self.data('progress') > 10 && self.data('progress') <= 25) {
            bat.css({
                background: 'orange'
            });
        } else {
            bat.css({
                background: '#cbcbcb'
            });
        }
    });
}
loadBatt();

// One click MVola
var switchTelma = $('.pause-telma');
$('.pause-telma').on('click', function(event) {
    event.preventDefault();
    var btn = $(this);

    // if (confirm('Êtes-vous sûr de vouloir procéder à la mise en pause des dépôts Telma ?')) {
        playPause(btn).promise().then(function (e) {
            checkPaniers(btn);
        });
    // }

});

// One click Orangemoney
var switchOrange = $('.pause-orange');
switchOrange.on('click', function(event) {
    event.preventDefault();
    var btn = $(this);

    // if (confirm('Êtes-vous sûr de vouloir procéder à la mise en pause des dépôts Orange ?')) {
        playPause(btn).promise().then(function (e) {
            checkPaniers(btn);
        });
    // }

});

// One click Airtel
var switchOrange = $('.pause-airtel');
switchOrange.on('click', function(event) {
    event.preventDefault();
    var btn = $(this);

    // if (confirm('Êtes-vous sûr de vouloir procéder à la mise en pause des dépôts Airtel ?')) {
        playPause(btn).promise().then(function (e) {
            checkPaniers(btn);
        });
    // }

});


function playPause(btn) {

    var switchUrl = btn.data('switchpause-url'); // /moneiko-ussd/sms-recus/play-pause-depot-airtel/1
    var type = btn.data('type');

    closeTooltip();
    return $.get(switchUrl, function(data, xhr) {

        if (xhr == 'success') {

            if (type == "mvola") {
                if (data.is_depot_mvola2 == 0) {
                    $('.fa-pause', btn).removeClass('fa-pause text-success').addClass('fa-play text-warning');
                } else {
                    $('.fa-pause', btn).addClass('fa-pause text-success').removeClass('fa-play text-warning');
                }


            } else if (type == "orange") {

                if (data.is_orange_depot2 == 0) {
                    $('.fa-pause', btn).removeClass('fa-pause text-success').addClass('fa-play text-warning');
                } else {
                    $('.fa-pause', btn).addClass('fa-pause text-success').removeClass('fa-play text-warning');
                }
            } else if (type == "airtel") {
                if (data.is_depot_airtel2 == 0) {
                    $('.fa-pause', btn).removeClass('fa-pause text-success').addClass('fa-play text-warning');
                } else {
                    $('.fa-pause', btn).addClass('fa-pause text-success').removeClass('fa-play text-warning');
                }
            }

            if (data.is_auto_check_localconnexion == 0) {
                $('.check-autolocalconnexion').addClass('text-danger').removeClass('text-white').removeClass('text-success');
            } else {
                $('.check-autolocalconnexion').addClass('text-success').removeClass('text-white').removeClass('text-danger');
            }
        }
        $('.loadme', btn).remove();

        setTimeout(function ()
        {
            $('.checkstatemvola').empty();
        }, 2000);

    });
}

var compteurPanier = 0;

function checkPaniers(self) {
    compteurPanier++;
    $('.loadme').remove();
    self.append(loadme);
    self.addClass('disabled').addClass('text-white').removeClass('text-success');
    var urlCheckPanier = self.data('panier-url'); // /moneiko-ussd/sms-recus/get-pending-tx/airtel2
    var urlAjustementSolde = self.data('ajustement-solde-url'); // /moneiko-ussd/sms-recus/ajuster-solde/033
    pauseTimerCheckStateLocalConexion();


    closeTooltip();
    return $.get(urlCheckPanier, function(data, xhr) {

        if (xhr == 'success') {
            if (data.nb) {
                $('.nbpendingtx', self).html("&nbsp;"+'<span class="text-danger">'+data.nb+'</span>');

                setTimeout(function () {
                    if (compteurPanier < 60) {
                        checkPaniers(self);
                    }
                }, 10*1000);

            } else {
                $('.nbpendingtx', self).html("");
                $('.fa-refresh', self).addClass('text-white fa-spin').removeClass('text-secondary').removeClass('text-danger');

                closeTooltip();
                $.get(urlAjustementSolde, function(data) {
                    if (data.status == 'success') {
                        $('.fa-refresh', self).removeClass('text-white fa-spin').addClass('text-success');
                        playPause(self);
                    } else {
                        $('.fa-refresh', self).removeClass('text-white fa-spin').addClass('text-danger');
                    }
                    playTimerCheckStateLocalConexion();
                    self.removeClass('disabled');
                });
            }
        }
        $('.loadme').remove();
        self.tooltip('hide');
        self.blur();

    });
}

$('.play-depot').on('click', function(event) {
    event.preventDefault();
    var self = $(this);
    var type = self.data('type');
    var switchNumUrl = self.data('switch-num-url');
    $('.loadme', self).remove();
    self.append(loadme);
    self.addClass('text-white');
    $.get(baseUrl+'apis/enableAutoRestartOzekiInServiceNgPy');

    // pauseTimerCheckStateLocalConexion();
    
    closeTooltip();
    $.get(switchNumUrl, function(data, xhr) {
        if (type == "mvola") {
            $('.last-num-mvola', self).html(data.numero_mvola_retrait.slice(-2));
        } else if(type == "orange") {
            $('.last-num-orange', self).html(data.numero_orange_retrait.slice(-2));
        } else {
            $('.last-num-airtel', self).html(data.numero_airtel_retrait.slice(-2));
        }
        $('.loadme', self).remove();
        self.append(loader);
        self.tooltip('hide');
    }).promise().then(function (e) {

        playPause($('.play-'+type)).promise().then(function (e) {
            playTimerCheckStateLocalConexion();
            self.removeClass('text-white');
            self.tooltip('hide');
            $('.disable-'+type).removeClass('text-warning').removeClass('text-white').removeClass('text-danger').removeClass('text-success');
            checkStateLocalConnexion();
            $('.loader', self).remove();
            $('.switch-'+type).removeClass('active');
            $('.progression-switch').empty();
        });
    });


});

$('.container-reserve').on('click', '.play-depot', function(event) {
    checkStateLocalConnexion();
});

$('.container-2fa-stats').on('click', '.recharge-sms', function(event) {
    event.preventDefault();

    if (confirm('Êtes-vous sûr de vouloir continuer à l\'achat de ce forfait ?')) {
        var self = $(this);
        var op = self.attr('data-op');
        var label = self.attr('data-label');
        self.addClass('text-secondary disabled').blur();

        $.get(self.attr('href'), function(data) {
            $('.msg-forfait-result').html(JSON.stringify(data, null, 8).replace(/\n( *)/g, function (match, p1) {
                 return '<br>' + '&nbsp;'.repeat(p1.length);
             }));
            self.removeClass('text-secondary disabled');
        });
    }
});

$('.container-2fa-stats').on('click', '.info-conso-forfait', function(event) {
    event.preventDefault();

    var self = $(this);
    var op = self.attr('data-op');
    var label = self.attr('data-label');
    self.addClass('text-secondary disabled').blur();

    $.get(self.attr('href'), function(data) {
        $('.msg-forfait-result').html('<span class="text-white">'+data.response+'</span>');
        setTimeout(function () {
            $('.msg-forfait-result').empty();
        }, 8000);
        // $('.container-2fa-stats').load(window.location+' .2fa-stats', function () {
        // });
        self.removeClass('text-secondary disabled');


    });
});

$('.msg-forfait-result').on('click', function(event) {
    event.preventDefault();
    $(this).empty();
});

function loadDriverStatus() {
    // $('.containerstatusserver .divstatusserver').html('Chargement ...');
    return $('.containerstatusserver').load(baseUrl+'tdb/portList .divstatusserver');
}

$('#sendsmsfromport').on('show.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);
   
    if (link.attr('data-href') != undefined) {
        var url = link.attr('data-href');
        $('.formsms-full', modal).attr('action', url);
        $('.com', modal).html(link.attr('data-label')+ ' : ' +link.attr('data-port'));
    }
});

$('.viewuser').on('click', function(event) {
    event.preventDefault();
    var self = $(this)
    var url = self.attr('data-href');
    var userId = self.attr('data-userid');
    var nom_complet = self.attr('data-nom_complet');
    var currentTr = $(this).parents('tr');
    if (currentTr.next('.container-pic').html() == undefined) {
        $('.trmodifuser').remove();
        $('.container-pic').remove();
        $.get(url, function(data) {
            currentTr.after('<tr class="align-middle trmodifuser"><td colspan="2"><input type="text" class="form-control modif-nomcomplet" value="'+nom_complet+'" name="nom_complet" data-userid="'+userId+'"/></td><td class="align-middle" colspan="2"><span class="result"></span></td></tr> <tr class="container-pic"><td colspan="2">'+data+'</td><td colspan="3"></td></tr>');
            $('html, body').animate({
                scrollTop: currentTr.offset().top
            }, 500);
        });
    }
});

$('.tableau-users').on('blur', '.modif-nomcomplet', function(event) {
    var self = $(this);
    var userId = self.attr('data-userid');
    var url = baseUrl+'pages/modifUser/'+userId;

    $.post(url, self.serialize(), function(data, textStatus, xhr) {
        var currentTr = self.parents('tr');
        $('.result', currentTr).html('<span class="fa fa-check-circle text-success"></span>');
    });
});

$('.majnoms').on('click', function(event) {
    event.preventDefault();
    var url = $(this).attr('data-href');
    var self = $(this);
    self.addClass('disabled');
    $.get(url, function(data, xhr) {
        self.blur().removeClass('disabled');

        if (xhr == 'success') {
            if (data.status == 'saved') {
                $('.alert-maj').removeClass('alert-warning').removeClass('mt-4').addClass('alert-success mt-4').html('Mise à jour en ligne effectuée');
            } else if(data.status == 'unsaved') {
                $('.alert-maj').removeClass('alert-success').removeClass('mt-4').addClass('alert-warning mt-4').html('Mise à jour en ligne non effectuée');
            }
        } else{
            $('.alert-maj').removeClass('alert-success').removeClass('mt-4').addClass('alert-warning mt-4').html('Connexion internet absente');
        }
    });
}); 

$('.tableau-users').on('click', '.img-viewuser', function(event) {
    var self = $(this);
    self.toggleClass('flipped');

    if (self.hasClass('flipped')) {
        self.css({
            '-webkit-transform' : 'scaleX(-1)',
            'transform' : 'scaleX(-1)'
        }); 
    } else {
        self.css({
            '-webkit-transform' : 'scaleX(1)',
            'transform' : 'scaleX(1)'
        });
    }
});

degrees = 45;
$('.tableau-users').on('click', '.rotate', function(event) {
    event.preventDefault();
    var self = $(this);
    $('.img-viewuser').toggleClass('flipped');

    rotate($('.img-viewuser'), degrees);
    degrees = eval(degrees + 45);
});

function rotate($el, degrees) {
    $el.css({
        '-webkit-transform' : 'rotate('+degrees+'deg)',
        '-moz-transform' : 'rotate('+degrees+'deg)',  
        '-ms-transform' : 'rotate('+degrees+'deg)',  
        '-o-transform' : 'rotate('+degrees+'deg)',  
        'transform' : 'rotate('+degrees+'deg)',  
        'zoom' : 1

    });
}

$('#at-cmd').on('show.bs.modal', function (e) {
    closeTooltip();

    var select = $('#selectports');
    emptyOption = select.find('option[value=""]').text();
    
    // $.get(baseUrl+'tdb/getPortNumListDatas', function(data) {
    //     // after ajax callback
    //     var option = new Option('Ports', "", true, true);
    //     select.html('');
    //     select.append(option);
            
    //     $.each(data, function (key, value) {
    //         var option = new Option(value, key, false, false);
    //         select.append(option);
    //     });
    // });
});

$('#config').on('click', '.remapmsidn', function(event) {
    event.preventDefault();
    var self = $(this);
    self.append(loader);
    self.addClass('disabled').attr('disabled', 'disabled');
    var modal = $('#config');

    $.when($.get(baseUrl+'tdb/cleanPortsTable'), $.get(baseUrl+'tdb/stopServiceNg')).then(function () {
        
        var calls = [];
        progressGeneralLoading = progressGeneralLoading + (5);

        i = 0;
        nbPort = $('tr.port', modal).length;
        var numsErrors = [];
        $('tr.port.actif', modal).each(function (item) {
            var tr = $('tr.port', modal).eq(item);
            var url = tr.attr('data-url');
            tr.find('.statuscheck').html('<span class="fa fa-circle text-secondary"></span>');

            calls.push(majPortState(url, modal, tr).then(function (data) {
                i++;
                if ((data.numero).slice(0, 2) != '03') {
                    numsErrors.push(data.numero)
                } else {
                    progressGeneralLoading = progressGeneralLoading + (5);
                    // console.log('remapmsidin'+progressGeneralLoading);
                    percent = eval(((i/nbPort)*100).toFixed(0));
                    $('.progression-port').html(percent+' %');
                    if (percent > 95) {
                        $('.progression-port').addClass('text-success');
                    }
                }
            }));
        }); 

        $.when.apply($, calls).then(function() {
            if (numsErrors.length > 0 ) {
                $('.remapmsidn').trigger('click');
                return false;
            }
            refreshPortList();
            $('.loader', self).remove();
            $('.progression-port').empty();
            self.removeClass('disabled').removeAttr('disabled');
            $.get(baseUrl+'tdb/startServiceNg');
        });

    });
});

$('#config').on('click', '.remapLettre', function(event) {
    event.preventDefault();
    isRemapLettreClicked = true; // utilisé dans progress bar
    // console.log('remapLettre_clicked');
    var self = $(this);
    self.append(loader);
    self.blur();
    self.addClass('disabled').attr('disabled', 'disabled');
    var modal = $('#config');
    // $('.table-routage .actions .fa', modal).remove();

    $.when($.get(baseUrl+'tdb/remapLettre'), $.get(baseUrl+'tdb/stopServiceNg')).then(function (data) {
        loadDriverStatus();
        var data = data[0];
        progressGeneralLoading = progressGeneralLoading + (15);


        $('.msgres').html('<div class="">'+data+'</div>');
        $('.loader', self).remove();
        $('.progression-port').empty();
        refreshPortList();
        self.removeClass('disabled').removeAttr('disabled');
        $.get(baseUrl+'tdb/startServiceNg', function () {
            $.wait(2500).then(loadDriverStatus());
            progressGeneralLoading = progressGeneralLoading + (10);
        });
    });
});

$('#config').on('click', '.stopOzekiAndStartForAction', function(event) {
    event.preventDefault();
    var self = $(this);
    self.append(loader);
    self.blur();
    self.addClass('disabled').attr('disabled', 'disabled');
    var modal = $('#config');
    // $('.table-routage .actions .fa', modal).remove();

    $.when($.get(baseUrl+'tdb/forceResetAllComPort'), $.get(baseUrl+'tdb/stopServiceNg')).then(function (data) {
        loadDriverStatus();
        var data = data[0];

        $('.msgres').html(data);
        $('.loader', self).remove();
        $('.progression-port').empty();
        self.removeClass('disabled').removeAttr('disabled');
        $.get(baseUrl+'tdb/startServiceNg', function () {
            $.wait(2500).then(loadDriverStatus());
            refreshPortList();
        });
    });
});

function majPortState(url, modal, tr) {
    return $.get(url, function(data) {
        if (data.result == true) {
            tr.find('.statuscheck').html('<span class="fa fa-check-circle text-success"></span>');
        } else {
            tr.find('.statuscheck').html('<span class="fa fa-times-circle text-danger"></span> '+data.msg);
        }
    });
}


$('#config').on('click', '.refreshsingle, .resetUsb', function(event) {
    event.preventDefault();
    var tr = $(this).parents('tr');
    var self = $(this);
    var url = self.attr('href');
    self.addClass('disabled').attr('disabled', 'disabled');
    self.append(loader);
    self.blur();
    tr.find('.statuscheck').show().html('<span class="fa fa-circle text-secondary"></span>');

    $.when($.get(url), $.get(baseUrl+'tdb/stopServiceNg')).done(function (data) {
        data = data[0];
        if (data.result == true) {
            tr.find('.statuscheck').html('<span class="fa fa-check-circle text-success"></span>');
        } else {
            tr.find('.statuscheck').html('<span class="fa fa-times-circle text-danger"></span> <span class="text-secondary statuscheck-result">'+data.msg+'</span>');
        }
        $('.loader', self).remove();
        self.blur().removeClass('disabled').removeAttr('disabled');
        tr.find('.statuscheck').after(loadme);
        $.get(baseUrl+'tdb/startServiceNg', function () {
            tr.find('.statuscheck').html('<span class="fa fa-check-circle text-success"></span>');
            tr.find('.statuscheck').delay(6000).fadeOut();
            tr.find('.loadme').remove();
            refreshPortList();
            $.wait(3000).then(loadDriverStatus());
        });
    });
    
    
});

$('#config').on('click', '.checkPort', function(event) {
    event.preventDefault();
    var tr = $(this).parents('tr');
    var self = $(this);
    var url = self.attr('href');
    self.addClass('disabled').attr('disabled', 'disabled');
    self.append(loader);
    self.blur();

    $.get(url, function(data) {
        tr.find('.resultcheck').show().html(data.result);
        $('.loader', self).remove();
        self.blur().removeClass('disabled').removeAttr('disabled');

        $('.resultcheck', tr).delay(6000).fadeOut();
    });
    // refreshPortList();
});

// function checkStatutPort(link) {
//     var url = link.attr('href');
//     return $.get(url, function(data));
// }

$('#config').on('click', '.remapRoutage', function(event) {
    event.preventDefault();
    isRemapRoutageClicked = true; // utilisé dans progress bar

    var self = $(this);
    var modal = $('#config');
    var form = $('form', modal);
    var dataForm = form.serialize();
    // console.log(dataForm);
    var url = self.attr('href');
    self.append(loader);
    self.blur();
    self.addClass('disabled').attr('disabled', 'disabled');

    $.when($.post(url)).then(function (data) {
        return $.post(url, dataForm, function(data) {
            if (data.status == 'success') {
            } else if(data.status = 'samevaluesoccur') {
                alert("Le tableau contient plusieurs doublons, le formulaire n'a pas été enregistré !");
            }

            $('.loader', self).remove();
            self.removeClass('disabled').removeAttr('disabled');
            refreshPortList(true);
            progressGeneralLoading = progressGeneralLoading + (10);
        });
    })/*.done($.get(baseUrl+'tdb/restartServiceNg', function (data) {
        
    }))*/;
    
});


$('#config').on('change', '.form-routagecom select', function(event) {
    event.preventDefault();
    var self = $(this);
    if (self.val() == '') {
        self.parents('.row').first().find('input[type="text"]').val('');
    }
});

$('#config').on('click', '.switchport', function(event) {
    event.preventDefault();
    var self = $(this);
    var opName = self.attr('data-op');
    self.blur();
    self.toggleClass('activated');

    // Swicth port
    var currentReceptionPort = $('.containerportreception.'+opName).find('input[name="port_reception_'+opName+'"]');
    var currentReceptionPortVal = currentReceptionPort.val();

    var currentEnvoiPort = $('.containerportenvoi.'+opName).find('input[name="port_'+opName+'"]');
    var currentEnvoiPortVal = currentEnvoiPort.val();

    currentReceptionPort.val(currentEnvoiPortVal);
    currentEnvoiPort.val(currentReceptionPortVal);

    // Swicth num
    var currentReceptionNum = $('.containernumreception.'+opName).find('select[name="num_port_reception_'+opName+'"]');
    var currentReceptionNumVal = currentReceptionNum.val();

    var currentEnvoiNum = $('.containernumenvoi.'+opName).find('select[name="num_port_envoi_'+opName+'"]');
    var currentEnvoiNumVal = currentEnvoiNum.val();

    currentReceptionNum.val(currentEnvoiNumVal);
    currentEnvoiNum.val(currentReceptionNumVal);

    $('.majparam').submit();

    $.get(self.attr('href'), function(data) {
    });

});

$('#config').on('click', '.startOrStopServerNg', function(event) {
    event.preventDefault();
    var self = $(this);
    var opName = self.attr('data-op');
    self.blur();
    self.html(loadme);
    self.addClass('disabled');
    $('.majparam').submit();

    $.get(self.attr('href'), function(data) {
        if (data.result == 1) {
            self.html('<span class="fa fa-stop"></span>');
        } else {
            self.html('<span class="fa fa-play"></span>');
        }

        $.wait('5000').then(loadDriverStatus());
        self.removeClass('disabled');
    });

});

$('#config').on('click', '.forceserver', function(event) {
    event.preventDefault();
    var self = $(this);
    var opName = self.attr('data-op');
    self.blur();
    self.html(loadme);
    self.addClass('disabled');
    $('.majparam').submit();

    $.get(self.attr('href'), function(data) {
        $('.msgres').html(data);
        $.wait('5000').then($('.containerstatusserver').load(baseUrl+'tdb/portList .divstatusserver'));
        self.removeClass('disabled');
    });

});
$('#config').on('click', '.remapDriverEnvoi, .definirNumeroTransactionVersProd', function(event) {
    event.preventDefault();
    isRemapDriverEnvoi = true;
    var self = $(this);
    var opName = self.attr('data-op');
    self.blur();
    self.addClass('disabled');
    self.append(loader);

    $.get(self.attr('href'), function(data, xhr) {
        self.removeClass('disabled');

        if (self.hasClass('remapDriverEnvoi')) {
            refreshPortList();
        }
        progressGeneralLoading = progressGeneralLoading + (eval(100-progressGeneralLoading));
        $.wait(10000).then(function () {
            $('.process-progressbar').addClass('d-none');
        });

        if (self.hasClass('definirNumeroTransactionVersProd')) {
            if (data.status == '') {
                $('.msgres').html("Un problème est survenu");
            } else {
                $('.msgres').html(JSON.stringify(data.datas_num_depots, null, 8));
            }

            emptyAfter('.msgres');
        }

        $('.majparam').submit();
        $('.loader', self).remove();
    });
});

function messageSiTypeNumeroManquantPortList() {
    if ($('.err-types').length > 0) {
        $('.alert-mappage').addClass('alert-danger').html('Veuillez remplir les cases des numéros non remplis ou faire un remap routage');
    }
}


$('#config').on('change', '.containernumselect select', function(event) {
    event.preventDefault();
    var self = $(this);
    var containerRow = self.parents('.row').first();
    var port = arrayPorts[self.val()];
    console.log(port);
    $('.containerport input', containerRow).val(port);
});

$('#searchpanier').on('keyup blur', 'input#senttime', function(event) {
    event.preventDefault();
    var self = $(this);
    var data = self.serialize();
    var smsId = self.attr('datasms-id');

    $.post(baseUrl+'tdb/updateSms/'+smsId, data, function(data, textStatus, xhr) {
        console.log(data);
    });
});
$('#searchpanier').on('keyup blur', 'input#receivedtime', function(event) {
    event.preventDefault();
    var self = $(this);
    var data = self.serialize();
    var smsId = self.attr('datasms-id');

    $.post(baseUrl+'tdb/updateSms/'+smsId, data, function(data, textStatus, xhr) {
        console.log(data);
    });
});

$('body').on('click', '.switchgeneral', function(event) {
    event.preventDefault();
    var self = $(this);
    if (!self.hasClass('active')) {
        self.addClass('active');
    }
    var type = self.attr('data-type');
    var colParent = self.parents('.col').first();

    $('.pause', colParent).click();

    console.log('Vérification Panier avant début process');
    $('.progression-switch').append('Vérification Panier avant début process <br>');

    var checkAbletoSwitch = setInterval(function () {
        
        if ($('span.fa.fa-refresh.text-success', colParent).length > 0) {

            console.log('Début du process switching');
            $('.progression-switch').append('Début du process switching <br>');

            $('.navbar-brand[data-target="#config"]').click(); // Click moneiko meny config modal

            var checkStopServerBtn = setInterval(function () {
                if ($('#config .forcestopserver').length >= 1) {

                    $('.forcestopserver').click();

                    $.get(baseUrl+'apis/disableAutoRestartOzekiInServiceNgPy');

                    console.log('Ozeki en cours d\'arrêt');
                    $('.progression-switch').append('Ozeki en cours d\'arrêt <br>');

                    var checkOzekiInterv = setInterval(function () {
                        
                        if ($('.ozeking').hasClass('text-danger')) {

                            console.log('Ozeking éteint');
                            $('.progression-switch').append('Ozeking éteint <br>');

                            $('#config .switchport[data-op="'+type+'"]').click();

                            setTimeout(function () {
                                
                                $('.remapRoutage ').click();

                                console.log('Remap routage effectué');
                                $('.progression-switch').append('Remap routage effectué <br>');

                                setTimeout(function () {

                                    $('.definirNumeroTransactionVersProd').click();

                                    console.log('Définition des numéros prod effectuée');
                                    $('.progression-switch').append('Définition des numéros prod effectuée <br>');

                                    $('.forcestartserver').click();
                                    $.get(baseUrl+'apis/enableAutoRestartOzekiInServiceNgPy');

                                    console.log('Redémarrage NG');
                                    $('.progression-switch').append('Redémarrage NG <br>');

                                    $('#config').modal('hide');

                                    setTimeout(function () {

                                        $('.play-'+type).click();

                                        console.log('Re mise à jour solde envoi '+type);
                                        $('.progression-switch').append('Re mise à jour solde envoi '+type+' <br>');

                                    }, 1000);


                                }, 1000);

                            }, 1000);

                            clearInterval(checkOzekiInterv);
                        }

                    });

                    clearInterval(checkStopServerBtn);
                }
            }, 200);


            clearInterval(checkAbletoSwitch);

        }
    }, 1000);
});

$('body').on('click', '.maj-solde', function(event) {
    event.preventDefault();
    var self = $(this);
    var urlMajSolde = self.attr('data-url-majsolde'); // /Tdb/majSolde/orange
    var op = self.attr('data-op');
    var containerReserve = self.parents('.container-reserve');
    var soldeOnline = $('.solde-online-'+op, containerReserve).attr('data-solde-'+op);
    $('.btn-submit-maj-solde').removeClass("disabled");

    if (soldeOnline > 0) {
        var inputMontantMaj = $('.form-majsolde input#montant');
        inputMontantMaj.val(soldeOnline);

        $('#maj_solde').modal('show');
        $('#maj_solde').on('shown.bs.modal', function () {
            var modal = $(this);
            $('.mobile', modal).html(op); 

            $('.form-majsolde').off().on('submit', function (e) {
                $('.btn-submit-maj-solde').addClass("disabled");
                e.preventDefault();
                var formmajsolde = $(this);
                var data = formmajsolde.serialize();
                $('.btn-submit-maj-solde .loadme').remove();
                $('.btn-submit-maj-solde').append(loadme);

                $.post(urlMajSolde, data, function(data, textStatus, xhr) {
                    $('.btn-submit-maj-solde .loadme').remove();
                    $('.btn-submit-maj-solde').removeClass("disabled");

                    var newValueMontant = parseFloat($('.form-majsolde input#montant').val());
                    $('.solde-online-'+op, containerReserve).attr('data-solde-'+op, newValueMontant);
                    $('.solde-online-'+op, containerReserve).find('span.text-white').html((newValueMontant).toLocaleString('fr-FR')+' <span class="libelle-min">Ar</span>');

                    $('.statemajed').removeClass('d-none');

                    setTimeout(function () {
                        $('.statemajed').addClass('d-none');
                    }, 4000);

                    setTimeout(function () {
                        $('#maj_solde').modal('hide');
                    }, 1000);


                    setTimeout(function () {
                        updateRatioChartGauge(op);
                        updateStatutAutoSwitch(op)
                    }, 1500);

                });
            });

            var soldeUp = parseInt($('.majinputsoldeUp').attr('data-up'));
            var soldeDown = parseInt($('.majinputsoldeDown').attr('data-down'));

            $('.resetSolde').off().click(function () {
                inputMontantMaj.val(soldeOnline);
            });

            $('.majinputsoldeUp').off().click(function () {
                var inputMontantMajVal = parseInt(inputMontantMaj.val());
                var montantMaj = eval(soldeUp+inputMontantMajVal);
                inputMontantMaj.val(montantMaj);
            });

            $('.majinputsoldeDown').off().click(function () {
                var inputMontantMajVal = parseInt(inputMontantMaj.val());
                var montantMaj = inputMontantMajVal > 0 ? eval(inputMontantMajVal-soldeDown) : 0;
                inputMontantMaj.val(montantMaj > 0 ? montantMaj : 0);
            });
        });
    } else {
        alert('Solde '+op+' non chargé');
    }
});

$('.reset-cache-achat-manu').off().click(function (e) {
    var self = $(this);
    self.html(loadme);
    e.preventDefault();

    $.get(self.attr('href'), function(data) {
        self.html('<span class="fa fa-trash text-danger"></span>');
    });
});

$('.testSendPushApi').off().click(function (e) {
    var self = $(this);
    self.html(loadme);
    e.preventDefault();

    $.get(self.attr('href'), function(data) {
        self.html('<span class="fa fa-check text-success"></span>');
        setTimeout(function () {
            $('.testSendPushApi').html('<span class="fa fa-bullhorn text-info"></span>');
        }, 5000);
    });
});

var simulatedSoldeCheckBox = $('.simulated-solde');

simulatedSoldeCheckBox.on('click', function () {
    if (simulatedSoldeCheckBox.is(':checked')) {
        // $('.majinputsoldeUp').attr('data-up', )
    }
});

$('body').on('click', '.majsoldeonline', function(event) {
    event.preventDefault();
    var self = $(this);
    var urlMajSolde = self.attr('data-url-majsolde'); // /Tdb/majSolde/orange
    var ptf = self.attr('data-ptf');
    var containerReserve = self.parents('.container-reserve');
    var soldePtf = $('.solde-'+ptf, containerReserve);
    var soldeOnline = soldePtf.attr('data-solde-'+ptf);
    var reserveOnline = soldePtf.attr('data-reserve-'+ptf);
    $('.btn-submit-maj-solde').removeClass("disabled");

    // console.log(soldeOnline);
    // console.log(soldePtf);
    // return false;

    if (parseFloat(soldeOnline) > 0) {
        var inputMontantMaj = $('.form-majsolde-usd input#montant');
        var inputReserveMaj = $('.form-majsolde-usd input#reserve');
        inputMontantMaj.val(soldeOnline);
        inputReserveMaj.val(reserveOnline);

        $('#maj_solde_usd').modal('show');
        $('#maj_solde_usd').on('shown.bs.modal', function () {

            var modal = $(this);
            $('.mobile', modal).html(ptf); 
            var formmajsoldeUsd = $('.form-majsolde-usd');
            $('.reserve-initial').html(reserveOnline+' $');

            var spanSolde = $('.majsoldeonline .solde-'+ptf);

            var soldeUp = parseFloat($('.majinputsoldeUp', formmajsoldeUsd).attr('data-up'));
            var soldeDown = parseFloat($('.majinputsoldeDown', formmajsoldeUsd).attr('data-down'));
            $('.statemajed').addClass('d-none');


            $('.resetSolde', formmajsoldeUsd).off().click(function () {
                inputMontantMaj.val(soldeOnline);
                inputReserveMaj.val(reserveOnline);
            });

            $('.majinputsoldeUp', formmajsoldeUsd).off().click(function () {

                var inputMontantMajVal = parseFloat(inputMontantMaj.val());
                var montantMaj = eval(soldeUp+inputMontantMajVal);
                inputMontantMaj.val(parseFloat(montantMaj).toFixed(2));

                var inputReserveMajVal = parseFloat(inputReserveMaj.val());
                var montantMaj = eval(inputReserveMajVal-soldeUp).toFixed(2);
                inputReserveMaj.val(parseFloat(montantMaj).toFixed(2));
            });

            $('.majinputsoldeDown', formmajsoldeUsd).off().click(function () {
                var inputMontantMajVal = parseFloat(inputMontantMaj.val());
                var montantMaj = inputMontantMajVal > 0 ? eval(inputMontantMajVal-soldeDown).toFixed(2) : 0;
                inputMontantMaj.val(montantMaj > 0 ? montantMaj : 0);

                var inputReserveMajVal = parseFloat(inputReserveMaj.val());
                var montantMaj = eval(inputReserveMajVal+soldeUp);
                inputReserveMaj.val(parseFloat(montantMaj).toFixed(2));
            });

            $('.reserve-statut').hide();

            inputMontantMaj.on('blur, change', function(event) {
                var inputMontantMajVal = parseFloat(inputMontantMaj.val());
                var diff = eval(inputMontantMajVal-soldeOnline);
                var reserveMajed = eval(reserveOnline-diff);
                $('.reserve-statut').html('<span class="fa fa-check text-success"></span>');
                $('.reserve-statut').fadeIn();
                inputReserveMaj.val(parseFloat(reserveMajed).toFixed(2));
            });

            formmajsoldeUsd.off().on('submit', function (e) {

                if (parseFloat(soldePtf.attr('data-solde-'+ptf)) == 0) {
                    alert('Solde '+ptf+' en cours de chargement');
                    return false;
                }

                $('.btn-submit-maj-solde', formmajsoldeUsd).addClass("disabled");
                e.preventDefault();
                var formmajsolde = $(this);
                var data = formmajsolde.serialize();
                $('.btn-submit-maj-solde .loadme', formmajsoldeUsd).remove();
                $('.btn-submit-maj-solde', formmajsoldeUsd).append(loadme);


                $.post(urlMajSolde, data, function(data, textStatus, xhr) {
                    
                    $('.btn-submit-maj-solde').blur();
                    $('.btn-submit-maj-solde .loadme', formmajsoldeUsd).remove();
                    $('.btn-submit-maj-solde', formmajsoldeUsd).removeClass("disabled");
                    $('.statemajed').removeClass('d-none');

                    soldePtf.attr('data-solde-'+ptf, inputMontantMaj.val());
                    soldePtf.attr('data-reserve-'+ptf, inputReserveMaj.val());

                    setTimeout(function () {
                        $('.statemajed').addClass('d-none');
                    }, 4000);

                    setTimeout(function () {
                        $('#maj_solde_usd').modal('hide');
                    }, 1000);

                    spanSolde.html(inputMontantMaj.val()+' <span class="libelle-min">USD</span>');
                });
            });
        });
    } else {
        alert('Solde '+ptf+' non chargé');
    }
});


function addDecimalToSolde() {
    var randDecimal = (Math.random() * 0.99).toFixed(2);

    if (simulatedSoldeCheckBox.is(':checked')) {
        var majUp = parseFloat($('.majinputsoldeUp').attr('data-up'));
        eval(majUp+randDecimal);
    }
}

function calculRatioOp(op = "mvola") {
    var soldeMgaRetrait = $('.solde-online-'+op).attr('data-solde-'+op);
    var soldeMgaRecu = $('.bat-'+op).attr('data-solde');
    var ratioMaj = $('#ratio-maj').val();
    var seuilMgaMaj = $('#seuil-mga-maj').val();
    var ratioCourant = parseFloat((soldeMgaRecu/soldeMgaRetrait)*100).toFixed(2);
}


function applyGaugeRatio() {
    $(".gauge").easyPieChart({
        easing: "easeOutElastic",
        delay: 3e3,
        barColor: "#ff7413",
        trackColor: "#636d7f",
        scaleColor: !1,
        size: 40,
        trackWidth: 0,
        lineCap: "butt",
        lineWidth: 3,
        onStep: function(a, b, c) {
            $(this.el).find(".percent").text(Math.round(c))
        }
    });
}


/**
 * Ity le manao mise à jour anle graph kely isakin interval de temps
 */

applyGaugeRatio();
function updateRatioChartGauge(op = "mvola") {

    var soldeMgaRetrait = parseFloat($('.solde-online-'+op).attr('data-solde-'+op));
    var soldeMgaRecu = parseFloat($('.bat-'+op).attr('data-solde'));
    var seuilRatioMaj = parseFloat($('#ratio-maj').val());
    var seuilMgaMaj = parseFloat($('#seuil-mga-maj').val());
    if (soldeMgaRecu <= 1) {
        var ratioCourant = 100;
    } else {
        var ratioCourant = parseFloat((soldeMgaRetrait/soldeMgaRecu)*100).toFixed(2);
    }

    if (ratioCourant >= 100) {
        var ratioCourant = 100;
    }

    // var ratioCourant = parseFloat(100 - ratioCourant);

    var linkSwitch = $('.switch-'+op); 
    var gaugeOp = $('.gauge-'+op);


    if (ratioCourant < seuilRatioMaj) {
        gaugeOp.data('easyPieChart').update(ratioCourant).options.barColor = '#ef4655';
    } else if(ratioCourant < 45) {
        gaugeOp.data('easyPieChart').update(ratioCourant).options.barColor = '#f7aa38';
    } else if(ratioCourant < 65) {
        gaugeOp.data('easyPieChart').update(ratioCourant).options.barColor = '#fffa50';
    } else {
        gaugeOp.data('easyPieChart').update(ratioCourant).options.barColor = '#5ee432';
    }
}


function updateStatutAutoSwitch(op = "mvola") {

    var soldeMgaRetrait = parseFloat($('.solde-online-'+op).attr('data-solde-'+op));
    var soldeMgaRecu = parseFloat($('.bat-'+op).attr('data-solde'));
    var ratioMaj = parseFloat($('#ratio-maj').val());
    var seuilMgaMaj = parseFloat($('#seuil-mga-maj').val());
    if (soldeMgaRecu <= 1) {
        var ratioCourant = 100;
    } else {
        var ratioCourant = parseFloat((soldeMgaRetrait/soldeMgaRecu)*100).toFixed(2);
    }

    // console.log(op + ' : ' + soldeMgaRetrait);
    // console.log(op + ' : ' + seuilMgaMaj);
    if (ratioCourant < ratioMaj && soldeMgaRetrait >= seuilMgaMaj) {
        $('.statut-si-pret-'+op).html('<span class="text-white">Prêt</span>');
    } else {
        $('.statut-si-pret-'+op).html('<span class="label-attente pointer" data-toggle="tooltip" data-placement="bottom" title="En attente, condition ratio incomplète : '+ratioCourant+'/'+ratioMaj+' %, Si la couleur vire au rouge, cela veut dire que le statut est prêt">En attente</span>');
        $('.label-attente').tooltip();
    }
}

/**
 * Ity le miclick auto refa tokony afamadika le solde
 */
function autoUpdateSoldeMGAByClickProcess(op = "mvola") {

    if ($('input#is-activated-auto-switch-solde-'+op).prop('checked')) {
        var soldeMgaRetrait = parseFloat($('.solde-online-'+op).attr('data-solde-'+op));
        var soldeMgaRecu = parseFloat($('.bat-'+op).attr('data-solde'));
        var ratioMaj = parseFloat($('#ratio-maj').val());
        var seuilMgaMaj = parseFloat($('#seuil-mga-maj').val());
        var ratioCourant = parseFloat((soldeMgaRetrait/soldeMgaRecu)*100).toFixed(2);
        var linkSwitch = $('.switch-'+op);
        if (!linkSwitch.hasClass('active')) {
            // linkSwitch.trigger('click');

            $.get(baseUrl+'apis/checkLastOzekiCheckedAt', function(data) {
                // console.log(data);
                // // ie que le dernier checking vient d'être executé il y a 0 seconde(s), il n y a 
                // donc pas de risque qu'on fait planter le switch à cause de la vérification ussd du solde 
                // qui pourrait faire planter le switching
                // if (data.result <= 15) { 
                    if (ratioCourant < ratioMaj && soldeMgaRetrait >= seuilMgaMaj) {
                        // linkSwitch.trigger('click');
                        // linkSwitch.addClass('active');
                        // alert('Mise à jour switch solde '+op);
                        console.log('Mise à jour switch solde '+op);
                    } else {
                        console.log('Mise à jour switch solde '+op+' non lancé, condition ratio incomplète : '+ratioCourant+' %');
                    }
                // } else {
                    // console.log('Mise à jour switch solde '+op+' inopportun, checking ozeki il y a '+data.result+' seconde(s)');
                // }
            });
        }

    }

}

// Contre activation de l'auto start du Ozeki 
// si jamais le Schedule bug ou un script/proces n'a pas réussi à mettre à jour 
// l'auto restart pouvant entraver la réception du mobile money des autres op
setInterval(function () {
    $.get(baseUrl+'apis/enableAutoRestartOzekiInServiceNgPy');
    $.get(baseUrl+'apis/wakePort');
}, 5*60*1000);

var textareaSendSms = $('.msg-tosend');
var formSmsContainer = textareaSendSms.parents('form').first();

textareaSendSms.on('blur change keyup', function() {
    var text = $(this).val();
    var charCount = text.length;
    $('#counterWord', formSmsContainer).text(charCount + ' / 160 mots');
});

$('.confirm-reclamation').on('click', function (e) {
    e.preventDefault();
    var self = $(this);
    var link = self.attr('href');
    var contentHtml = self.html();
    var containerRecla = self.parents('.container-reclamation-final');

    self.html(loadme);
    if (confirm('Êtes-vous sur de vouloir procéder') == true) {
        $.get(link, function(data) {
            $('.recla-results', containerRecla).html(JSON.stringify(data));
            // self.html(contentHtml);
            self.blur();
        });
    } else {
        // self.html(contentHtml);
    }
});

$('.recla-results').on('click', function () {
    $(this).fadeOut();
});

$('.switch-tunnel').on('click', function(event) {
    event.preventDefault();
    switchTunnel($(this));
});

function switchTunnel(element) {

    element.blur();
    // majNgrokUrl($('.majgrockurl'));
    $('.ng-percent', element).html('<span class="text-secondary">'+loadme+'</span>');
    $('.switch-tunnel').removeClass('border-grey').removeClass('text-white').removeClass('active_ngrok_state');
    var href = element.attr('href');
    $.get(href, function(data) {
        var tunId = data.tunnel_id;
        $('.switch-tunnel[data-id="'+tunId+'"]').addClass('border-grey').addClass('text-white').addClass('active_ngrok_state');
        element.find('.ng-percent').html(data.pourcentage);
        element.find('.loadme').remove();
        loadStatNgrok();
        // setTimeout(function () {
        //     majNgrokUrl($('.majgrockurl'));
        // }, 500);
    });
}


function confirmReclamation(className) {
    var when = $.when();
    var deferreds = [];
    
    $('.confirm-reclamation.'+className).each(function (index) {
        var self = $('.confirm-reclamation.'+className).eq(index);;
        var url = self.attr('href');
        var deferred = $.Deferred();


        when.then(function () {
            $.ajax({
                url : url,
                type : "GET",
                async: false,
                success: function (data) {
                    $('.confirm-reclamation.'+className).removeClass('disabled border-white');
                    self.addClass('disabled border-white');
                }
            });
            deferreds.push(deferred);
        });

    });

    $.when.apply($, deferreds).then(function() {

        setTimeout(function () {
            
            $('.confirm-reclamation.'+className).removeClass('disabled border-white');


            if ($('input[name="is_auto_recla_general"]').is(':checked') && className == 'confirm-reclamation-general') {
                $.get(baseUrl+'apis/updateLastAutoReclaAt', function(data) {
                    $('.info-confirm-reclamation').blur();
                    $('.info-confirm-reclamation').addClass('text-white').attr('data-original-title', 'Dernière requête auto recla générale : '+data.last_auto_recla_executed_at);
                    console.log('Heure Auto recla general mise à jour');
                });
            }

            if ($('input[name="is_auto_recla_erreur_reseau"]').is(':checked') && className == 'confirm-reclamation-erreur-reseau') {
                $.get(baseUrl+'apis/updateLastReseauAutoReclaAt', function(data) {
                    $('.info-confirm-reclamation-erreur-reseau').blur();
                    $('.info-confirm-reclamation-erreur-reseau').addClass('text-white').removeAttr('title').attr('data-original-title', 'Dernière requête erreur réseau : '+data.last_auto_recla_erreur_reseau_executed_at);
                    console.log('Heure Auto recla réseau mise à jour');
                });
            }
            
            console.log(className);
            console.log("Toutes les requêtes auto récla sont terminées.");

        }, 1000);
    });
}

setInterval(function () {
    if ($('input[name="is_auto_recla_general"]').is(':checked')) {
        confirmReclamation('confirm-reclamation-general');
    }
}, $('#time-interval-auto-recla-general').val()*60*1000);


setInterval(function () {
    if ($('input[name="is_auto_recla_erreur_reseau"]').is(':checked')) {
        confirmReclamation('confirm-reclamation-erreur-reseau');
    }
}, $('#time-interval-auto-recla-erreur-reseau').val()*60*1000);

$('.info-confirm-reclamation').on('click', function () {
    if (confirm('Êtes-vous sûr de vouloir procéder ?')) {
        confirmReclamation('confirm-reclamation-general');
    }
});
$('.info-confirm-reclamation-erreur-reseau').on('click', function () {
    if (confirm('Êtes-vous sûr de vouloir procéder ?')) {
        confirmReclamation('confirm-reclamation-erreur-reseau');
    }
});

$('.simpleload').on('click', function (event) {
    event.preventDefault();

    var url = $(this).attr('href');
    var self = $(this);
    self.append(loadme);  
    $('.result-success').remove();

    $.get(url, function(data) {
        $('.loadme', self).remove();
        self.append('<span class="fa fa-check text-success result-success"></span>');  
        self.blur();
        setTimeout(function () {
            $('.result-success').fadeOut();
        }, 4000);
    });
});

$('.reset-ngrok').on('click', function (event) {
    event.preventDefault();
    var href = $(this).attr('href');
    var self = $(this);
    self.html(loadme);
    $.get(href, function(data) {
        $('.switch-tunnel').not('.active_ngrok_state').each(function(index, el) {
            if ($(this).attr('data-progress') >= 80 ) {
                $(this).find('.ng-percent').html('0');
            }
        });
        self.html('<span class="fa fa-refresh"></span>');
    });
});

if ($('#paste-button').html() !== undefined) {

    const pasteButton = document.querySelector('#paste-button');

    pasteButton.addEventListener('click', async () => {
        try {
            const text = await navigator.clipboard.readText()
            document.querySelector('#at-com').value = text;
            console.log('input-command-ussd');
        } catch (error) {
            console.log('Failed to read clipboard');
        }
    });
}


function loadEstJ() {
    $.get(baseUrl+'tdb/getEstAchatJ', function(data) {
        $('.nb-est-j').html(data.current_nb+' / '+data.nb);
        $('.nb-variation').html('<span class="fa '+data.variation+'"></span>');
        $('.get-est-j').blur();
        closeTooltip();
    }); 
}
$('.get-est-j').on('click', function () {
    var self = $(this);
    $('.nb-est-j').html(loadme); 
    loadEstJ();
});

setInterval(function () {
    $('.nb-est-j').html(loadme); 
    loadEstJ();
}, 30*1000);

$('body').on('click', '.start-auto-rec', function(event) {
    event.preventDefault();
    startReclaByJS();
});

$('body').on('click', '.close-auto-rec', function(event) {
    event.preventDefault();
    $('.container-result-recla').fadeOut();
    $('.container-card-results-recla').fadeOut();
});

function startReclaByJS() {

    var btnStartAutoRec = $('.start-auto-rec');
    btnStartAutoRec.addClass('disabled');
    btnStartAutoRec.append(loadme);

    $('.container-result-recla').empty();
    $('.container-result-recla').append('<div class="">Recherche de paiements échoués <br /> <hr /></div>');
    $('.container-card-results-recla').show();
    $('.container-result-recla').show();
    $('.container-card-results-recla').removeClass('d-none');

    $.ajax({
        url: baseUrl+'SmsRecus/getNbFailedOrder',
        method: 'GET', // ou POST selon ton besoin
        success: function(data, textStatus, jqXHR) {

            var ajaxRequests = [];
            // Recherches nb paiements echoués

            if (data.nb_total_mvola > 0) {
                $('.container-result-recla').append('Nb total Mvola echoués : <span class="text-white">'+data.nb_total_mvola+'</span> <br>');
            }

            if (data.nb_total_orange > 0) {
                $('.container-result-recla').append('Nb total Orange echoués : <span class="text-white">'+data.nb_total_orange+'</span> <br>');
            }

            if (data.nb_total_airtel > 0) {
                $('.container-result-recla').append('Nb total Airtel echoués : <span class="text-white">'+data.nb_total_airtel+'</span> <br>');
            }

            if ((data.nb_total_mvola+data.nb_total_orange+data.nb_total_airtel) > 0) {
                $('.container-result-recla').append('<hr>');

                $('.loadme', btnStartAutoRec).remove();

                // Traitement par opérateur des réclamations
                $('.container-result-recla').append('Traitements des paiements <br /> <hr /');

                $.each(data.details.sms_ids, function(index, smsId) {


                    var request = $.get(baseUrl+'Sms/getSmsId/'+smsId, function(smsData) {

                        // console.log(smsData);
                        btnStartAutoRec.append(loadme);

                        $('.container-result-recla').append('<span class="sms-detail-'+smsId+'">Paiement SMS ID : <span class="text-white">'+smsId+'</span>, Montant : <span class="text-white">'+smsData.data.montant_recu+' Ariary</span> '+loadme+'</span>');
                        
                    }).then(function () {

                        return $.ajax({
                            url: baseUrl+'Sms/paySingleError/'+smsId+'?is_recla_from_js=1',
                            method: 'GET', // ou POST selon ton besoin
                            success: function(data, textStatus, jqXHR) {
                                var statusClassResult = data.status != 'success' ? 'text-danger' : 'text-success';
                                $('.sms-detail-'+smsId).find('.loadme').remove();
                                $('.sms-detail-'+smsId).append(', Résultat : <span class="'+statusClassResult+'">'+data.status+ '</span> <br>');
                                $('.loadme', btnStartAutoRec).remove();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                $('.container-result-recla').append("<span class='text-danger'> Paiement Sms ID : <span class='text-white'>"+smsId+"</span> Erreur serveur : Code "+jqXHR.status+"</span>");
                            },
                            complete: function(jqXHR, textStatus) {
                                
                            }
                        });
                    });
                    
                    ajaxRequests.push(request);

                    return false; // Juste traiter le premier cas, et faire ains de suite pour chaque traitement car cela peut ramer le site


                });


                $.when.apply($, ajaxRequests).always(function() {
                    $('.container-result-recla').append('<hr /> Traitement des réclamations terminé <br> ');
                    $('.loadme', btnStartAutoRec).remove();
                    btnStartAutoRec.blur();
                    btnStartAutoRec.removeClass('disabled');

                    setTimeout(function () {
                        $('.container-result-recla').empty();
                        $('.container-card-results-recla').addClass('d-none');
                    }, 20000);
                });
            } else {
                $('.container-result-recla').append('<div class="mt-2 text-white">Aucun paiement échoué <hr /></div>');
                $('.loadme', btnStartAutoRec).remove();
                btnStartAutoRec.blur();
                btnStartAutoRec.removeClass('disabled');
                setTimeout(function () {
                    $('.container-result-recla').empty();
                }, 10000);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
        },
        complete: function(jqXHR, textStatus) {
        }
    });
}

// setInterval(function () {
//     if (!$('.start-auto-rec').hasClass('disabled')) {
//         startReclaByJS();
//     }
// }, 1*60*1000); // toutes les 1 minutes