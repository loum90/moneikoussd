var containerForm = $('.container-onex-form');
var baseUrl = $('body').data('url');
var onexForm = $('.onex-form');

function reloadForm() {
    return $.get(baseUrl+'one-x/getOnexForm', function(data) {
        $('.container-onex-form').html(data);
        // console.log(data);
    });
}

reloadForm();

containerForm.off().on('click', '.reload', function(event) {
    $('.loadme', containerForm).remove(); 
    var self = $(this);
    self.append(loadme);
    event.preventDefault();
    reloadForm().promise().then(function (e) {
        $('.loadme', self).remove(); 
    });
});


containerForm.on('submit', '.onex-form', function(event) {
    event.preventDefault();
    var self = $(this);
    $('.validate-onex').append(loadme);

    $.post($(this).attr('action'), $(this).serialize(), function(data, textStatus, xhr) {
        if (data.status == 'success') {

            var smsId = data.sms_id;

            var rowSms =  $('.row-sms[data-sms-id="'+smsId+'"]');
            rowSms.fadeOut(function() {
                rowSms.remove();
                reloadSms();        
            });

            self.html('<div class="alert alert-success mt-2 alert-paiement-success" role="alert">\
                Paiement succès\
            </div>');

            setTimeout(function () {
                reloadForm();
            }, 1000);
        }
        $('.loadme', self).remove();
    });
});

new ClipboardJS('.copy');

$('#step-1').on('shown.bs.modal', function(event) {

    var self = $(this);
    $('.text-loading').remove();
    $('#id', self).val('');

    $('.step-one-ok', self).off().click(function () {

        if ($('.loadme', self).length < 1) {
            var textLoading = 'Chargement ' + loadme;
            $('.modal-body', self).append('<span class="text-loading">'+textLoading+'</span>');
        }

        setTimeout(function () {
            var valId = $('#id', self).val();
            self.modal('hide');
            var modalStep2 = $('#step-2');
            $('#id', modalStep2).val(valId);
            modalStep2.modal('show');
        }, 2000);
    });
});

$('#step-2').on('shown.bs.modal', function(event) {

    var self = $(this);
    $('.text-loading').remove();
    $('#montant', self).val("");

    $('.step-two-ok', self).off().click(function () {
        if ($('.loadme', self).length < 1) {
            var textLoading = 'Chargement ' + loadme;
            $('.modal-body', self).append('<span class="text-loading">'+textLoading+'</span>');
        }
        setTimeout(function () {
            $('#id', self).val();
            var valId = $('#id', self).val();
            var valMontant = $('#montant', self).val();
            self.modal('hide');
            var modalStep3 = $('#step-3');

            $('.amount-onex').html(valMontant);
            $('.account-onex').html(valId);

            modalStep3.modal('show');

        }, 500);
        
    });
});




$('#detail-msg').off().on('shown.bs.modal', function (e) {
    var modal = $(e.currentTarget);
    var link = $(e.relatedTarget);

    var smsId = link.attr('data-link-id');
    $.get(baseUrl+'onex/getImgSrcBySmsPaid/'+smsId, function(data) {

        if (data.src != false) {
            var imgSrc = data.src;
            var imgDetails = "<img src='"+imgSrc+"' width='100%'/>";
            $('.detail-modal-onex', modal).html(imgDetails);
        } else {
            $('.detail-modal-onex', modal).html('Aucune capture');
        }
    });

    // console.log(smsId);
    // console.log(modal);
    // console.log(link);
});